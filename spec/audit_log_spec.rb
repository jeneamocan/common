require "spec_helper"

describe SaltCommon::AuditLog do
  let(:public_key_path)  { "config/test_public.pem" }
  let(:private_key_path) { "config/test_private.pem" }
  let(:log_path)         { "config/test_audit.log" }
  let(:content)          { {a: 1, b: 2, c: 3}.to_json }

  before { File.write(log_path, content) }
  after  { File.unlink(log_path) if File.exists?(log_path) }

  describe ".encrypt/.decrypt" do
    def decrypt_check(data, meta)
      expect(
        described_class.decrypt(
          gzip_encrypted_data: data,
          private_key_path:    private_key_path,
          meta:                meta
        )
      ).to eq(content)
    end

    it "encrypts and gzips raw data from the given log file and returns a decryptable hash" do
      result = described_class.encrypt(log_path: log_path, public_key_path: public_key_path)
      expect(result.keys.sort).to        eq([:encrypted_data, :gzipped_data, :meta].sort)
      expect(result[:meta].keys.sort).to eq([:alg, :key, :iv, :padding].sort)

      decrypt_check(result[:gzipped_data], result[:meta])
    end

    it "writes output into tmp files that are available during the block and yields their pathes" do
      result = described_class.encrypt(log_path: log_path, public_key_path: public_key_path) do |key_path, log_path|
        data = File.read(log_path)
        meta = JSON.parse(File.read(key_path))

        expect(meta).to_not be_empty
        expect(data).to_not be_empty
        decrypt_check(data, meta)
      end
      expect(File.exists?(log_path)).to be_falsey
      expect(File.exists?(log_path.gsub(".log", ".key.json"))).to be_falsey
      expect(File.exists?(log_path.gsub(".log", ".enc.gz"))).to be_falsey

      decrypt_check(result[:gzipped_data], result[:meta])
    end
  end

  describe ".build_logger" do
    let(:data) { JSON.parse(content) }

    subject { described_class.build_logger(log_root: "./config", hostname: "test", identifiers: ["client", "13"]) }

    after { File.unlink(logfile) if File.exists?(logfile) }

    def logfile
      subject.instance_values["logdev"].filename
    end

    it "builds Logger instance that writes into a specific file" do
      subject.info(data)
      expect(logfile).to eq("./config/test-client-13-#{Date.today}.log")
    end

    context "serializing into json" do
      let(:meta) { JSON.parse({"logged_at_utc" => Time.now.utc}.to_json) }

      before { Timecop.freeze }
      after  { Timecop.return }

      it "tries to add logging time metadata to each hash message" do
        subject.error(data)
        expect(JSON.parse(File.readlines(logfile).last)).to eq(data.merge(meta))
      end

      it "tries to add logging time metadata if message is an array" do
        subject.fatal([data])
        expect(JSON.parse(File.readlines(logfile).last)).to eq([data, meta])
      end

      it "doesn't depend on logging severity" do
        subject.info(data)
        subject.warn(data)
        comment, first, second = File.readlines(logfile)
        expect(first).to eq(second)
      end
    end
  end
end

