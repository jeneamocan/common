require 'mock_redis'
require 'simplecov'
require 'simplecov-cobertura'
require 'active_model'
require 'minitest'
require 'shoulda/matchers'
require 'pry-byebug'
require 'timecop'

SimpleCov.start do
  add_filter "/spec/"
  add_filter "/vendor/"
end if ENV["COVERAGE"]

SimpleCov.formatter = SimpleCov::Formatter::CoberturaFormatter

ActiveSupport::JSON::Encoding.use_standard_json_time_format = true
ActiveSupport::JSON::Encoding.time_precision = 0

# TODO: load classes in their tests, not globally
load "lib/salt-common/version.rb"
load "lib/salt-common/ci/runner.rb"
load "lib/salt-common/ci/configuration.rb"
load "lib/salt-common/crypt.rb"
load "lib/salt-common/audit_log.rb"
load "lib/salt-common/sign.rb"
load "lib/salt-common/json_wrapper.rb"
load "lib/salt-common/redis_lock.rb"
load "lib/salt-common/rest_api.rb"
load "lib/salt-common/http_request.rb"
load "lib/salt-common/timezone.rb"
load "lib/salt-common/s3.rb"
load "lib/salt-common/worker.rb"
load "lib/salt-common/service_status.rb"
load "lib/salt-common/sql_sanitizer.rb"
load "lib/salt-common/validators/core.rb"
load "lib/salt-common/accounts_transactions/core.rb"
load "lib/salt-common/adapter_configurator/core.rb"
load "lib/salt-common/storage/base.rb"
load "lib/salt-common/logstash/sidekiq.rb"
load "lib/salt-common/logstash/exception_logger.rb"
load "lib/salt-common/logstash/log_mapper.rb"
load "lib/salt-common/sender.rb"
load "lib/salt-common/credentials.rb"
load "lib/salt-common/records_cleaner.rb"
load "lib/salt-common/helpers/exception_helpers.rb"
load "lib/salt-common/helpers/hash_filter.rb"
load "lib/salt-common/connectors/bucket.rb"
load "lib/salt-common/connectors/categorizer.rb"
load "lib/salt-common/connectors/desk.rb"
load "lib/salt-common/sso/base.rb"
load "spec/support/cbor_serializer.rb"
load "spec/support/vcr.rb"
load "spec/support/storage.rb"

I18n.enforce_available_locales = true

Mail.defaults do
  delivery_method "test"
end

RSpec.configure do |config|
  config.include(Shoulda::Matchers::ActiveModel, type: :model)
  config.include(Shoulda::Matchers::ActiveRecord, type: :model)

  config.raise_errors_for_deprecations!

  config.expect_with :rspec do |c|
    c.syntax = [:should, :expect]
  end

  config.mock_with :rspec do |c|
    c.syntax = [:should, :expect]
  end

  config.before(:each) do
    Redis.stub(:current).and_return(MockRedis.new)
  end
  config.include Storage
end
