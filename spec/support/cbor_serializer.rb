require "cbor"
# BLOG: note CBOR serialization

class CborSerializer
  def file_extension
    "cbor"
  end

  def serialize(hash)
    hash.to_cbor
  end

  def deserialize(string)
    CBOR.decode(string)
  end
end