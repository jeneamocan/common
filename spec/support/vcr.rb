require "vcr"

VCR.configure do |c|
  c.register_request_matcher :port do |request_1, request_2|
    # request_1.parsed_uri.port == request_2.parsed_uri.port
    # allows to ignore port matching
    true
  end
  c.cassette_serializers[:cbor] = CborSerializer.new
  c.default_cassette_options = { :serialize_with => :cbor }

  c.cassette_library_dir = "spec/vcr"
  c.hook_into :webmock
  c.configure_rspec_metadata!
end
