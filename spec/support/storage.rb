module Storage
  module Test
    class Item < SaltCommon::Storage::Item
      ATTRIBUTES = {
        id: true,
        name: true,
        credentials: false,
        interactive_credentials: false,
        internal_data: true,
        beta: true
      }

      define_attributes(ATTRIBUTES)

      def initialize(id, namespace=nil)
        @id = id
      end

      def primary_key
        id
      end
    end

    class Collection < SaltCommon::Storage::Collection
      NAMESPACE = "test"

      def add(params)
        item = Storage::Test::Item.new(params["id"])
        item.update(params)
        item.save
        item
      end
    end
  end
end
