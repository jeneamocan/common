require "spec_helper"

describe SaltCommon::RedisLock do
  let(:key)        { 123 }
  let(:redis_host) { ENV["REDIS_HOST"] || "127.0.0.1" }
  let(:redis)      { Redis.new(host: redis_host) }
  let(:timeout)    { 2 }
  let(:expire)     { 4 }

  subject { SaltCommon::RedisLock.new(key, timeout: timeout, expire: expire) }

  before(:each) do
    redis.flushdb
    subject.stub(:redis).and_return(redis)
  end

  after(:each) do
    redis.flushdb
  end

  context "public interface" do
    describe "#lock!" do
      it "raises an error if lock could not be acquired" do
        subject.stub(:lock) { false }
        expect { subject.lock! }.to raise_error(SaltCommon::RedisLock::NotAcquired)
      end

      it "yields the block if lock acquired" do
        test_flag = false

        subject.lock! do
          test_flag = true
          redis.get(subject.key).should be
        end

        test_flag.should be_truthy
      end
    end

    describe "#lock" do
      it "yields the block given and unlocks the lock if the result of obtaining lock was true" do
        test_flag = false

        subject.lock do
          test_flag = true
          redis.get(subject.key).should be
        end

        test_flag.should be_truthy
      end

      it "doesn't yield the block given and does not unlock the lock if the result of obtaining lock was false" do
        redis.set(subject.key, (Time.now.to_i + subject.expire).to_s)
        test_flag = false

        subject.lock do
          test_flag = true
        end

        test_flag.should be_falsey
      end
    end
  end

  context "private interface" do
    describe "#try_lock" do
      it "sets the lock with correct attributes in redis" do
        subject.stub(:now) { 123 }

        subject.send(:try_lock).should be_truthy
        subject.allow_unlock.should == 1

        redis.get(subject.key).should == (123 + subject.expire).to_s
      end

      it "returns false if a proces within a lock took too long to execute, and releases the lock itself" do
        subject.stub(:now) { 123 + expire + 1 }

        redis.set(subject.key, 123)

        subject.send(:try_lock).should == false
        subject.allow_unlock.should    == 1
        redis.get(subject.key).should  be_nil
      end

      it "returns false if a process already acquired the lock" do
        subject.stub(:now) { 122 }

        redis.set(subject.key, 123)

        subject.send(:try_lock).should == false
        subject.allow_unlock.should    == nil
        redis.get(subject.key).should  == 123.to_s
      end
    end

    describe "#unlock" do
      it "unlocks the lock if you are allowed to unlock it" do
        subject.stub(:allow_unlock) { true }

        redis.set(subject.key, 123)
        subject.send(:unlock).should be_truthy

        redis.get(subject.key).should be_nil
      end

      it "does not unlock the lock if you are not the current lock holder" do
        subject.stub(:allow_unlock) { false }

        redis.set(subject.key, 123)

        subject.send(:unlock).should  be_falsey
        redis.get(subject.key).should == 123.to_s
      end
    end
  end

  context "integration" do
    let(:threads) { [] }

    after do
      threads.each(&:exit)
    end

    # TODO: rewrite spec using Process with result check
    xit "handles the case when a thread sleeps more than the timeout" do
      thr1_executed = false
      thr2_executed = false

      threads << Thread.new do
        subject.lock do
          thr1_executed = true
          sleep subject.timeout + 1
        end
      end

      threads << Thread.new do
        subject.lock do
          thr2_executed = true
        end
      end

      threads.each(&:join)
      thr1_executed.should be_truthy
      thr2_executed.should be_falsey
    end

    it "handles the case when a thread works more than the supposed expiration time" do
      thr1_executed = false
      thr2_executed = false

      threads << Thread.new do
        subject.lock do
          thr1_executed = true
          # TODO: commenting following line(#136) does not change anything
          # sleep subject.expire + 1
        end
      end

      threads << Thread.new do
        sleep subject.timeout + 1
        subject.lock do
          thr2_executed = true
        end
      end

      threads.each(&:join)
      thr1_executed.should be_truthy
      thr2_executed.should be_truthy
    end

    it "handles the situation when 2 threads simultaneously try to enter critical section" do
      thr1_executed = false
      thr2_executed = false

      threads << Thread.new do
        subject.lock do
          thr1_executed = true
        end
      end

      threads << Thread.new do
        subject.lock do
          thr2_executed = true
        end
      end

      threads.each(&:join)
      thr1_executed.should be_truthy
      thr2_executed.should be_truthy
    end
  end
end
