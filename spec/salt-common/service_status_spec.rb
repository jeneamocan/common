require "spec_helper"

describe SaltCommon::ServiceStatus do
  let (:subject) { SaltCommon::ServiceStatus.new }
  let (:healthy_status) do
    Class.new(SaltCommon::ServiceStatus) do
      def all; {foo: true}; end
    end
  end

  let (:non_healthy_status) do
    Class.new(SaltCommon::ServiceStatus) do
      def all; {foo: false}; end
    end
  end

  it "caches the results" do
    status = healthy_status.new

    status.should_receive(:all).and_return(foo: false)
    status.healthy?
    status.should_not_receive(:all)
    status.healthy?
  end

  describe ".http_ok" do
    it "returns true if the host can be contacted" do
      RestClient.should_receive(:get).and_return(true)
      subject.http_ok(:test, "http://localhost:1111").should == [:test, true]
    end

    it "returns true if the host can be contacted" do
      RestClient.should_receive(:get).and_raise(SaltCommon::RestApi::HTTP_ERRORS.sample)
      subject.http_ok(:test, "http://localhost:1111").should == [:test, false]
    end
  end

  describe ".healthy?" do
    it "returns true if all services are running" do
      healthy_status.new.should be_healthy
    end

    it "returns false if one of the services is down" do
      non_healthy_status.new.should_not be_healthy
    end
  end

  describe ".status_string" do
    it "returns live if all services are running" do
      healthy_status.new.status_string.should == "live"
    end

    it "returns fail if one of the services is down" do
      non_healthy_status.new.status_string.should == "fail"
    end
  end

  describe ".redis_status" do
    let (:settings) { OpenStruct.new(redis: {host: "localhost", port: "6379"})}
    before { stub_const("Settings", settings) }

    it "returns true if everything is ok" do
      Redis.current.should_receive(:ping).and_return(true)
      subject.redis_status.should == ["redis_localhost_6379", true]
    end

    it "returns false if redis is down" do
      Redis.current.should_receive(:ping).and_raise(Redis::CannotConnectError)
      subject.redis_status.should == ["redis_localhost_6379", false]
    end
  end

  describe ".database_status" do
    let (:connection) { double }
    let (:base) do
      OpenStruct.new({
        connection_config: {
          host:     "localhost",
          database: "test_db"
        },
        connection: connection
      })
    end

    before { stub_const("ActiveRecord::Base", base) }

    it "returns true if everything is ok" do
      connection.should_receive(:execute).and_return(true)
      subject.database_status.should == ["postgres_localhost_test_db", true]
    end

    it "returns false if a dummy query cannot be executed" do
      connection.should_receive(:execute).and_raise(Errno::ECONNREFUSED)
      subject.database_status.should == ["postgres_localhost_test_db", false]
    end
  end
end