require 'spec_helper'

describe SaltCommon::HashFilter do
  let(:item_params) {
    {
      "login_id"                     => "111",
      "country_code"                 => "md",
      "provider_code"                => "victoriabank_md",
      "mode"                         => "automatic",
      "fetch_type"                   => "full",
      "automatic_fetch"              => true,
      "interactive"                  => false,
      "credentials"                  => "123zbs",
      "credentials_token"            => {"data" => {"key" => "value"}},
      :interactive_credentials_token => {"data" => {"key" => "value"}},
      "checksum"                     => "checksum",
      "session_id"                   => "123-123-123",
      "interactive_credentials"      => "zbszbs",
      "internal_data"                => {},
      "some_key"                     => {"test" => 1}
    }
  }
  describe ".set_filter_hash_keys(array)" do
    it "sets filter_hash_keys" do
      SaltCommon::HashFilter.set_filter_hash_keys([:key])
      SaltCommon::HashFilter.get_filter_hash_keys.should == ["key"]
    end
  end
  describe ".filter!(filter_hash_keys=[])" do
    it "filters sensitive keys from hash" do
      keys = [
        "credentials",
        :credentials_token,
        "interactive_credentials_token",
        :test
      ]
      SaltCommon::HashFilter.set_filter_hash_keys(keys)
      SaltCommon::HashFilter.filter!(item_params).should == {
        "login_id"                       => "111",
        "country_code"                   => "md",
        "provider_code"                  => "victoriabank_md",
        "mode"                           => "automatic",
        "fetch_type"                     => "full",
        "automatic_fetch"                => true,
        "interactive"                    => false,
        "credentials"                    => "[FILTERED]",
        "credentials_token"              => "[FILTERED]",
        :interactive_credentials_token   => "[FILTERED]",
        "checksum"                       => "checksum",
        "session_id"                     => "123-123-123",
        "interactive_credentials"        => "zbszbs",
        "internal_data"                  => {},
        "some_key"                       => {"test"=>"[FILTERED]"}
      }
    end
    it "allows custom keys" do
      keys = item_params.keys[1..-2] << "test"
      SaltCommon::HashFilter.filter!(item_params, keys).should == {
        "login_id"                       => "111",
        "country_code"                   => "[FILTERED]",
        "provider_code"                  => "[FILTERED]",
        "mode"                           => "[FILTERED]",
        "fetch_type"                     => "[FILTERED]",
        "automatic_fetch"                => "[FILTERED]",
        "interactive"                    => "[FILTERED]",
        "credentials"                    => "[FILTERED]",
        "credentials_token"              => "[FILTERED]",
        :interactive_credentials_token   => "[FILTERED]",
        "checksum"                       => "[FILTERED]",
        "session_id"                     => "[FILTERED]",
        "interactive_credentials"        => "[FILTERED]",
        "internal_data"                  => "[FILTERED]",
        "some_key"                       => {"test"=>"[FILTERED]"}
      }
    end
  end
end
