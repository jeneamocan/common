require 'spec_helper'

describe CryptoFetcher do
  let(:cryptocurrencies_path) { "spec/fixtures/cryptocurrencies" }
  let(:coinmarket_json)       { File.read("#{cryptocurrencies_path}/coinmarket.json") }
  let(:cached_list)           { File.read("#{cryptocurrencies_path}/cached_list.txt") }

  context "real request" do
    context "assets_list" do
      it "runs full cycle for assets_list", :vcr do
        list = described_class.assets_list
        expect(list.size).to eq(5154)
        redis_snapshot = Redis.current.get("v2:cryptocurrencies_list")
        list_of_assets = JSON.parse(redis_snapshot).map { |asset| asset["code"] }
        expect(list_of_assets.include?("BSV")).to be_truthy
        expect(list_of_assets.include?("OXT")).to be_truthy
      end
    end

    context "convert" do
      it "given crypto to currency", :vcr do
        expect(described_class.convert(1, "XRP", to: "USD")).to eq(1.67895230505006)
      end

      it "given crypto to currency with Float amount", :vcr do
        expect(described_class.convert(12.45, "XRP", to: "EUR")).to eq(14.930487211451984)
      end

      it "given crypto to currency with String amount", :vcr do
        expect(described_class.convert("9.99", "XRP", to: "EUR")).to eq(13.536697090876872)
      end

      it "fails in case any provided argument is nil" do
        expect {
          described_class.convert(nil, "XRP", to: nil)
        }.to raise_error(ArgumentError, "Blank arguments: amount, to")
      end

      it "does not validate inexistent currencies/crypto-currencies" do
        expect {
          described_class.convert(12.45, "XRP", to: "EURON")
        }.to raise_error(ArgumentError, "Following currencies not found in CryptoAssets/ISO4217: EURON")
      end
    end
  end

  it "gets updated cryptocurrencies list" do
    Timecop.freeze do
      expect(RestClient::Request).to receive(:execute).and_return(double(body: coinmarket_json))
      list = described_class.assets_list
      expect(list.as_json.size).to eq(2376)
      expect(list.as_json[0..3]).to eq(
        [
          {"code"=>"BTC", "name"=>"Bitcoin"},
          {"code"=>"LTC", "name"=>"Litecoin"},
          {"code"=>"NMC", "name"=>"Namecoin"},
          {"code"=>"TRC", "name"=>"Terracoin"}
        ]
      )

      expect(Redis.current.ttl("v2:cryptocurrencies_list")).to eq(3600)
    end
  end

  it "returns list from assets if error and no cached list" do
    expect(RestClient::Request).to receive(:execute).and_return("")

    list = described_class.assets_list
    expect(list.as_json.size).to eq(1953)
    expect(list.as_json[0..3]).to eq(
      [
        {"code" => "BTC", "name" => "Bitcoin"},
        {"code" => "ETH", "name" => "Ethereum"},
        {"code" => "XRP", "name" => "XRP"},
        {"code" => "BCH", "name" => "Cash"}
      ]
    )
  end

  it "returns cached list when present or if error" do
    Redis.current.set("v2:cryptocurrencies_list", cached_list)

    list = described_class.assets_list
    expect(list.as_json.size).to eq(2018)
    expect(list.as_json[0..3]).to eq(
      [
        {"code" => "BTC", "name" => "Bitcoin"},
        {"code" => "ETH", "name" => "Ethereum"},
        {"code" => "XRP", "name" => "XRP"},
        {"code" => "BCH", "name" => "Cash"}
      ]
    )
  end

  it "returns codes from assets if connection failed" do
    Timecop.freeze do
      list = described_class.assets_list
      expect(list.as_json.size).to eq(1953)
      expect(list.as_json[0..3]).to eq(
        [
          {"code" => "BTC", "name" => "Bitcoin"},
          {"code" => "ETH", "name" => "Ethereum"},
          {"code" => "XRP", "name" => "XRP"},
          {"code" => "BCH", "name" => "Cash"}
        ]
      )
      expect(Redis.current.ttl("v2:error_fetching_cryptos_list")).to eq(60)
    end
  end
end
