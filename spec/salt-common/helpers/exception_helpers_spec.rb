require 'spec_helper'

describe SaltCommon::ExceptionHelpers do
  before(:all) do
    class StandardError
      include SaltCommon::ExceptionHelpers
    end
    class NonCriticalError < StandardError
      def severity
        "WARNING"
      end
    end
  end

  let(:browser_handler) {
    double(
      text:             "Hello",
      html:             "<h1>Hello World!</h1>",
      screenshot:       "img",
      unhandled_errors: [],
      data_available?:  true
    )
  }
  let(:connector) { double(fail: "ok") }
  let(:item_hash) {
    {
      "login_id"      => 123,
      "attempt_id"    => 123,
      "country_code"  => "md",
      "provider_code" => "victoriabank",
      "mode"          => "automatic",
      "credentials"   => 123123,
      "interactive_credentials"            => { "sms" => "1234" },
      "rememberable_credentials"           => {},
      "encrypted_rememberable_credentials" => {
        algorithm: "AES-256-CBC",
        key:       "qwerty"
      }
    }
  }
  let(:message) { "SOME MESSAGE" }
  let(:error)   { StandardError.new(message).update(item_hash, browser_handler) }
  let(:file)    { double }

  before(:each) do
    stub_const("Settings", double(
        email:        double(notify: "notify-robber@example.com", error: "error-robber@example.com"),
        service_name: "robber",
        bucket:       double(base_url: "saltedge.com"),
        desk:         double(base_url: "desk.banksalt.com", service_to_desk_secret: "desk_secret", enabled: true)
      )
    )
    Mail::TestMailer.deliveries.clear
  end

  def filtered_item_hash
    error.item_hash.merge(
      "credentials"                        => "[FILTERED]",
      "interactive_credentials"            => "[FILTERED]",
      "rememberable_credentials"           => "[FILTERED]",
      "encrypted_rememberable_credentials" => "[FILTERED]"
    )
  end

  describe "#update" do
    it "returns object" do
      expect(error.item_hash).to include("admin_login_url" => "saltedge.com/admin/logins/123/show")
    end

    it "does not change original item_hash" do
      expect { error.update("a" => "b") }.not_to change { error.line }
      error.updated.should be_truthy

      expect(error.item_hash).to include(
        "a"               => "b",
        "admin_login_url" => "saltedge.com/admin/logins/123/show"
      )
    end

    it "casts all integer item_hash values to string" do
      expect(error.item_hash).to include(
        "login_id"    => "123",
        "attempt_id"  => "123",
        "credentials" => "123123"
      )
    end
  end

  describe "#send_data" do
    before do
      Settings.stub_chain(:log, :logstash, :enabled).and_return(false)
    end

    it "sends fail json" do
      browser_handler.stub(:data_available?).and_return(false)
      SaltCommon::Connectors::Bucket.should_receive(:fail).with(error.item_hash)
      error.send_data
    end

    it "logs non Adapter error" do
      browser_handler.stub(:data_available?).and_return(false)
      SaltCommon::Connectors::Bucket.should_receive(:fail)
      error.send_data
    end

    it "logs Adapter error and RestClient error" do
      browser_handler.stub(:data_available?).and_return(false)

      SaltCommon::Connectors::Bucket
        .should_receive(:fail)
        .with(hash_including({"a" => "b"})).and_raise(error)

      error.update({"a" => "b"}).send_data
    end
  end

  describe "#exception_hash" do
    it "can be rewritten" do
      new_exception_hash      = { a: :b }
      initial_exception_hash  = error.exception_hash
      error.exception_hash    = new_exception_hash

      initial_exception_hash.should_not == new_exception_hash
    end
  end

  describe "#send_email" do
    it "sends multipart email" do
      sender = error.send_email
      Mail::TestMailer.deliveries.length.should == 1

      sender.mail.to.should      == [Settings.email.error]
      sender.mail.subject.should == "[Robber][CRITICAL][#{item_hash['provider_code']}][#{item_hash['login_id']}] #{error.class.name}"
      sender.mail.body.should    == SaltCommon::JsonWrapper.encode(
        SaltCommon::Credentials.filter!(error.item_hash),
        {pretty: true}
      )
    end

    context "critical note" do
      it "sends critical email" do
        sender = StandardError.new.update.send_email
        sender.mail.subject.should == "[Robber][CRITICAL][UNKNOWN PROVIDER][UNKNOWN LOGIN] StandardError"
      end

      it "sends not critical email" do
        sender = NonCriticalError.new.update.send_email
        sender.mail.subject.should == "[Robber][WARNING][UNKNOWN PROVIDER][UNKNOWN LOGIN] NonCriticalError"
      end
    end
  end

  describe "#notify" do
    it "sends data to all the sources" do
      error.should_receive(:send_data)
      error.logger.should_receive(:send_log)
      error.should_receive(:send_ticket)
      error.notify
    end

    it "does not send error to bucket if flag is passed in" do
      error.should_not_receive(:send_data)
      error.logger.should_receive(:send_log)
      error.should_receive(:send_ticket)
      error.notify(fail_callback: false)
    end

    it "does not send error to desk if flag is passed in" do
      error.should_receive(:send_data)
      error.logger.should_receive(:send_log)
      error.should_not_receive(:send_ticket)
      error.notify(ticket: false)
    end
  end

  describe "#send_ticket" do
    it "unsuccessful sends ticket (desk.enabled = false)" do
      Settings.should_receive(:desk).and_return(double(base_url: "desk.banksalt.com", service_to_desk_secret: "desk_secret", enabled: false))
      SaltCommon::Connectors::Desk.should_not_receive(:create_ticket)
      error.send_ticket
    end

    it "successful sends ticket (desk.enabled = true)" do
      Settings.should_receive(:desk).and_return(double(base_url: "desk.banksalt.com", service_to_desk_secret: "desk_secret", enabled: true))
      SaltCommon::Connectors::Desk.should_receive(:create_ticket).with(
        data: hash_including(
          {
            "credentials"                        => "[FILTERED]",
            "interactive_credentials"            => "[FILTERED]",
            "rememberable_credentials"           => "[FILTERED]",
            "encrypted_rememberable_credentials" => "[FILTERED]"
          }
        )
      )

      error.send_ticket
    end
  end

  describe "#attachments_desk" do
    describe "get attachments hash (robber)" do
      it "get attachments hash (robber)" do
        item_hash_robber = item_hash.merge({"adapter_config"=> { use_selenium: true}})
        error_robber = StandardError.new(message).update(item_hash_robber, browser_handler)
        browser_handler.stub(:data_available?).and_return(true)
        attachments = error_robber.send(:attachments_desk)

        attachments.should == {
          file_html: {
            file: "PGgxPkhlbGxvIFdvcmxkITwvaDE+\n",
            extra: {
              original_filename: "browser_html",
              content_type:      "text/html",
              file_extension:    "html",
              file_size:         21
            }
          },
          file_png: {
            file: "img",
            extra: {
              original_filename: "screenshot",
              content_type:      "image/png",
              file_extension:    "png",
              file_size:         3
            }
          }
        }
      end

      it "get empty attachments hash (robber) (missing 'adapter_config' in item_hash)" do
        error_robber = StandardError.new(message).update(item_hash, browser_handler)
        browser_handler.stub(:data_available?).and_return(true)
        attachments = error_robber.send(:attachments_desk)
        attachments.should be_empty
      end

      it "get empty attachments hash (robber) (use_selenium false)" do
        item_hash_robber = item_hash.merge({"adapter_config"=> { use_selenium: false}})

        error_robber = StandardError.new(message).update(item_hash_robber, browser_handler)
        browser_handler.stub(:data_available?).and_return(true)
        attachments = error_robber.send(:attachments_desk)
        attachments.should be_empty
      end

      it "get empty attachments hash (robber) (missing browser_handler)" do
        item_hash_robber = item_hash.merge({"adapter_config"=> { use_selenium: true}})
        error_robber = StandardError.new(message).update(item_hash_robber)
        browser_handler.stub(:data_available?).and_return(true)
        attachments = error_robber.send(:attachments_desk)
        attachments.should be_empty
      end

      it "get empty attachments hash (robber) (data_available false for browser_handler)" do
        item_hash_robber = item_hash.merge({"adapter_config"=> { use_selenium: true}})

        error_robber = StandardError.new(message).update(item_hash_robber, browser_handler)
        browser_handler.stub(:data_available?).and_return(false)
        attachments = error_robber.send(:attachments_desk)
        attachments.should be_empty
      end
    end

    describe "get attachments hash (fencer)" do
      let(:file) {
        double({
          size: 9,
          read: "content 1"
          }
        )
      }

      it "get attachments hash (fencer)" do
        item_hash_fencer = item_hash.merge({"file_path" => "/data/csv/attachment.12.csv"})
        Settings.stub(:service_name).and_return("fencer")
        File.should_receive(:new).with(item_hash_fencer["file_path"], "rb").and_return(file)

        error_fencer = StandardError.new(message).update(item_hash_fencer)
        attachments = error_fencer.send(:attachments_desk)
        attachments.should == {
          file_fencer: {
            file: "Y29udGVudCAx\n",
            extra: {
              original_filename: "attachment.12",
              content_type:      "text/comma-separated-values",
              file_extension:    "csv",
              file_size:         9
            }
          }
        }
      end

      it "get empty attachments hash (fencer) (missing filepath in item_hash)" do
        Settings.stub(:service_name).and_return("fencer")

        error_fencer = StandardError.new(message).update(item_hash)
        attachments = error_fencer.send(:attachments_desk)
        attachments.should be_empty
      end
    end

    describe "get empty attachments hash (stealer)" do
      it "get empty attachments hash (stealer)" do
        Settings.stub(:service_name).and_return("stealer")

        attachments = error.send(:attachments_desk)
        attachments.should be_empty
      end
    end
  end

  describe "send_ticket_error_email" do
    let(:file) {
      double({
        size: 9,
        read: "content 1"
        }
      )
    }

    it "send_ticket_error_email" do
      item_hash_fencer = item_hash.merge({"file_path" => "/data/csv/attachment.12.csv"})
      Settings.stub(:service_name).and_return("fencer")

      Settings.should_receive(:desk).and_return(double(base_url: "desk.banksalt.com", service_to_desk_secret: "desk_secret", enabled: true))

      error_fencer = StandardError.new(message).update(item_hash_fencer)
      error_desk   = StandardError.new("desk error")

      SaltCommon::Connectors::Desk.should_receive(:create_ticket).and_raise(error_desk)
      File.should_receive(:new).with(item_hash_fencer["file_path"], "rb").and_return(file)

      sender = error_fencer.send(:send_ticket)

      sender.mail.to.should      == [Settings.email.error]
      sender.mail.subject.should == "[Fencer][CRITICAL][victoriabank][123] StandardError [Send Desk ERROR]"
    end
  end

  describe "#update_batch(parse_errors)" do
    it "updates batch error with parse errors' data" do
      parse_errors = [StandardError.new("msg").update]
      batch_error  = StandardError.new

      batch_error.update_batch(parse_errors, item_hash)
      batch_error.item_hash["parsing_errors"].first["message"].should == "msg"
    end
  end

  describe "add_error(error)" do
    it "adds parse error's data ti batch error" do
      parse_error = StandardError.new("msg").update("a" => "b")
      batch_error = StandardError.new.update("parsing_errors" => [])

      batch_error.add_error(parse_error)
      batch_error.item_hash["parsing_errors"].first["message"].should == "msg"
      batch_error.item_hash["parsing_errors"].first["item_hash"].should include({"a" => "b"})
    end
  end

  describe "#severity" do
    it "tells if error is critical" do
      StandardError.new.severity.should == "CRITICAL"
      NonCriticalError.new.severity.should == "WARNING"
    end

    it "can be overwritten" do
      err = StandardError.new
      err.severity.should == "CRITICAL"
      err.severity = "WARNING"
      err.severity.should == "WARNING"
      err.severity = "INFO"
      err.severity.should == "INFO"
    end
  end

  describe "#to_hash" do
    it "makes hash for JSON error" do
      hash = error.to_hash
      hash["error_class"].should      == StandardError.to_s
      hash["error_mode"].should       == "RobberError"
      hash["file"].should             =~ /#{__FILE__}$/
      hash["line"].should             > 0
      hash["time"].should             == Time.now.as_json
      hash["severity"].should         == "CRITICAL"
      hash["message"].should          == message
      hash["backtrace"].should           be_nil
    end

    it "filters backtrace correctly" do
      Settings.stub(service_name: "robber")
      error = ArgumentError.new
      error.stub(:backtrace => [
        "/data/www/robber/shared/bundle/ruby/2.3.0/gems/test_lib",
        "/data/www/robber/releases/20170619130055/lib/robber/class.rb",
        "/data/www/robber/shared/bundle/ruby/2.3.0/gems/other_lib",
        "/data/www/robber/shared/bundle/ruby/2.3.0/gems/another_lib",
      ])

      error.to_hash["backtrace"].should == [
        "/data/www/robber/shared/bundle/ruby/2.3.0/gems/test_lib",
        "/data/www/robber/releases/20170619130055/lib/robber/class.rb",
      ]
    end
  end

  context "Private" do
    describe "#update_item_hash" do
      it "filters credentials and sets finish_time on exceptions" do
        error = StandardError.new
        error.update(item_hash)
        hash = error.item_hash
        keys = ["login_id", "country_code", "provider_code", "mode"]
        hash.slice(keys).should == item_hash.slice(keys)

        hash["service_name"].should    == Settings.service_name
        hash["exception"].should       == error.to_hash
        hash["finish_time"].should_not be_nil
      end
      it "does nothing if called outside services" do
        Settings.should_receive(:service_name).and_return("not_service")
        error = StandardError.new
        error.update(item_hash)
        error.item_hash["admin_login_url"].should be_nil
      end
    end
  end
end
