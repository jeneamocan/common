require 'spec_helper'

describe SaltCommon::IbanParser do
  let(:invalid_iban)  { "DE0011111112222222222" }
  let(:invalid_iban2) { "XF00111111111222222222" }

  let(:ibans) do
    %w[
        AD1200012030200359100100 AL47212110090000000235698741 AT611904300234573201 AZ21NABZ00000000137010001944
        BA391290079401028494 BE68539007547034 BG80BNBG96611020345678 BH67BMAG00001299123456 BR1800360305000010009795493C1
        CH9300762011623852957 CR05015202001026284066 CY17002001280000001200527600 CZ6508000000192000145399 DE89370400440532013000
        DK5000400440116243 DO28BAGR00000001212453611324 EE382200221020145685 EG380019000500000000263180002 ES9121000418450200051332
        FI2112345600000785 FO6264600001631634 FR1420041010050500013M02606 GB29NWBK60161331926819 GE29NB0000000101904917
        GI75NWBK000000007099453 GL8964710001000206 GR1601101250000000012300695 GT82TRAJ01020000001210029690 HR1210010051863000160
        HU42117730161111101800000000 IE29AIBK93115212345678 IL620108000000099999999 IS140159260076545510730339
        IT60X0542811101000000123456 JO94CBJO0010000000000131000302 KW81CBKU0000000000001234560101 KZ86125KZT5004100100
        LB62099900000001001901229114 LI21088100002324013AA LT121000011101001000 LU280019400644750000 LV80BANK0000435195001
        MC5811222000010123456789030 MD24AG000225100013104168 ME25505000012345678951 MK07250120000058984 MR1300020001010000123456753
        MT84MALT011000012345MTLCAST001S NL91ABNA0417164300 NO9386011117947 PK36SCBL0000001123456702 PL61109010140000071219812874
        PS92PALS000000000400123456702 PT50000101231234567890192 QA58DOHB00001234567890ABCDEFG RO49AAAA1B31007593840000
        RS35260005601001611379 SA0380000000608010167519 SE4550000000058398257466 SI56191000000123438 SK3112000000198742637541
        SM86U0322509800000000270100 SV62CENR00000000000000700025 TL380080012345678910157 TR330006100519786457841326
        UA213996220000026007233566001 VA59001123000012345678 VG96VPVG0000012345678901 XK051212012345678906
      ]
  end

  describe "#parse" do
    it "skips an invalid iban" do
      expect(described_class.parse(invalid_iban)).to be_nil
    end

    it "skips an iban with unsuported country_code" do
      expect(described_class.parse(invalid_iban2)).to be_nil
    end

    it "parses ibans with all available country codes" do
      parsed_ibans = ibans.map { |iban| described_class.parse(iban) }

      expect(parsed_ibans.size).to eq(ibans.size)
      expect(parsed_ibans).to eq([
        {
          "country_code"        => "AD",
          "checksum"            => "12",
          "identification_code" => "0001",
          "branch_code"         => "2030",
          "account_number"      => "200359100100"
        },
        {
          "country_code"        => "AL",
          "checksum"            => "47",
          "identification_code" => "212",
          "branch_code"         => "1100",
          "account_number"      => "9000000023569874",
          "check_digit"         => "1"
        },
        {
          "country_code"        => "AT",
          "checksum"            => "61",
          "identification_code" => "19043",
          "account_number"      => "00234573201"
        },
        {
          "country_code"   => "AZ",
          "checksum"       => "21",
          "bic_code"       => "NABZ",
          "account_number" => "00000000137010001944"
        },
        {
          "country_code"        => "BA",
          "checksum"            => "39",
          "identification_code" => "129",
          "branch_code"         => "007",
          "account_number"      => "94010284",
          "check_digit"         => "94"
        },
        {
          "country_code"        => "BE",
          "checksum"            => "68",
          "identification_code" => "539",
          "account_number"      => "0075470",
          "check_digit"         => "34"
        },
        {
          "country_code"        => "BG",
          "checksum"            => "80",
          "bic_code"            => "BNBG",
          "identification_code" => "9661",
          "account_type"        => "10",
          "account_number"      => "20345678"
        },
        {
          "country_code"   => "BH",
          "checksum"       => "67",
          "bic_code"       => "BMAG",
          "account_number" => "00001299123456"
        },
        {
          "country_code"        => "BR",
          "checksum"            => "18",
          "identification_code" => "00360305",
          "branch_code"         => "00001",
          "account_number"      => "0009795493",
          "check_digit"         => "C1"
        },
        {
          "country_code"        => "CH",
          "checksum"            => "93",
          "identification_code" => "00762",
          "account_number"      => "011623852957"
        },
        {
          "country_code"        => "CR",
          "checksum"            => "05",
          "check_digit"         => "0",
          "identification_code" => "152",
          "account_number"      => "02001026284066"
        },
        {
          "country_code"        => "CY",
          "checksum"            => "17",
          "identification_code" => "002",
          "branch_code"         => "00128",
          "account_number"      => "0000001200527600"
        },
        {
          "country_code"        => "CZ",
          "checksum"            => "65",
          "identification_code" => "0800",
          "account_number"      => "0000192000145399"
        },
        {
          "country_code"        => "DE",
          "checksum"            => "89",
          "identification_code" => "37040044",
          "account_number"      => "0532013000"
        },
        {
          "country_code"        => "DK",
          "checksum"            => "50",
          "identification_code" => "0040",
          "account_number"      => "0440116243"
        },
        {
          "country_code"   => "DO",
          "checksum"       => "28",
          "bic_code"       => "BAGR",
          "account_number" => "00000001212453611324"
        },
        {
          "country_code"        => "EE",
          "checksum"            => "38",
          "identification_code" => "22",
          "branch_code"         => "00",
          "account_number"      => "22102014568",
          "check_digit"         => "5"
        },
        {
          "country_code"        => "EG",
          "checksum"            => "38",
          "identification_code" => "0019",
          "branch_code"         => "0005",
          "account_number"      => "00000000263180002"
        },
        {
          "country_code"        => "ES",
          "checksum"            => "91",
          "identification_code" => "2100",
          "branch_code"         => "0418",
          "check_digit"         => "45",
          "account_number"      => "0200051332"
        },
        {
          "country_code"        => "FI",
          "checksum"            => "21",
          "identification_code" => "123456",
          "account_number"      => "0000078",
          "check_digit"         => "5"
        },
        {
          "country_code"        => "FO",
          "checksum"            => "62",
          "identification_code" => "6460",
          "account_number"      => "000163163",
          "check_digit"         => "4"
        },
        {
          "country_code"        => "FR",
          "checksum"            => "14",
          "identification_code" => "20041",
          "branch_code"         => "01005",
          "account_number"      => "0500013M026",
          "check_digit"         => "06"
        },
        {
          "country_code"        => "GB",
          "checksum"            => "29",
          "bic_code"            => "NWBK",
          "identification_code" => "601613",
          "account_number"      => "31926819"
        },
        {
          "country_code"   => "GE",
          "checksum"       => "29",
          "bic_code"       => "NB",
          "account_number" => "0000000101904917"
        },
        {
          "country_code"   => "GI",
          "checksum"       => "75",
          "bic_code"       => "NWBK",
          "account_number" => "000000007099453"
        },
        {
          "country_code"        => "GL",
          "checksum"            => "89",
          "identification_code" => "6471",
          "account_number"      => "0001000206"
        },
        {
          "country_code"        => "GR",
          "checksum"            => "16",
          "identification_code" => "011",
          "branch_code"         => "0125",
          "account_number"      => "0000000012300695"
        },
        {
          "country_code"   => "GT",
          "checksum"       => "82",
          "bic_code"       => "TRAJ",
          "account_number" => "01020000001210029690"
        },
        {
          "country_code"        => "HR",
          "checksum"            => "12",
          "identification_code" => "1001005",
          "account_number"      => "1863000160"
        },
        {
          "country_code"        => "HU",
          "checksum"            => "42",
          "identification_code" => "117",
          "branch_code"         => "7301",
          "account_number"      => "6111110180000000",
          "check_digit"         => "0"
        },
        {
          "country_code"        => "IE",
          "checksum"            => "29",
          "bic_code"            => "AIBK",
          "identification_code" => "931152",
          "account_number"      => "12345678"
        },
        {
          "country_code"        => "IL",
          "checksum"            => "62",
          "identification_code" => "010",
          "branch_code"         => "800",
          "account_number"      => "0000099999999"
        },
        {
          "country_code"        => "IS",
          "checksum"            => "14",
          "identification_code" => "0159",
          "branch_code"         => "26",
          "account_number"      => "0076545510730339"
        },
        {
          "country_code"        => "IT",
          "checksum"            => "60",
          "check_digit"         => "X",
          "identification_code" => "05428",
          "branch_code"         => "11101",
          "account_number"      => "000000123456"
        },
        {
          "country_code"        => "JO",
          "checksum"            => "94",
          "bic_code"            => "CBJO",
          "identification_code" => "0010",
          "account_number"      => "000000000131000302"
        },
        {
          "country_code"   => "KW",
          "checksum"       => "81",
          "bic_code"       => "CBKU",
          "account_number" => "0000000000001234560101"
        },
        {
          "country_code"        => "KZ",
          "checksum"            => "86",
          "identification_code" => "125",
          "account_number"      => "KZT5004100100"
        },
        {
          "country_code"        => "LB",
          "checksum"            => "62",
          "identification_code" => "0999",
          "account_number"      => "00000001001901229114"
        },
        {
          "country_code"        => "LI",
          "checksum"            => "21",
          "identification_code" => "08810",
          "account_number"      => "0002324013AA"
        },
        {
          "country_code"       => "LT",
          "checksum"           => "12",
          "identification_code" => "10000",
          "account_number"      => "11101001000"
        },
        {
          "country_code"        => "LU",
          "checksum"            => "28",
          "identification_code" => "001",
          "account_number"      => "9400644750000"
        },
        {
          "country_code"   => "LV",
          "checksum"       => "80",
          "bic_code"       => "BANK",
          "account_number" => "0000435195001"
        },
        {
          "country_code"        => "MC",
          "checksum"            => "58",
          "identification_code" => "11222",
          "branch_code"         => "00001",
          "account_number"      => "01234567890",
          "check_digit"         => "30"
        },
        {
          "country_code"   => "MD",
          "checksum"       => "24",
          "bic_code"       => "AG",
          "account_number" => "000225100013104168"
        },
        {
          "country_code"        => "ME",
          "checksum"            => "25",
          "identification_code" => "505",
          "account_number"      => "0000123456789",
          "check_digit"         => "51"
        },
        {
          "country_code"        => "MK",
          "checksum"            => "07",
          "identification_code" => "250",
          "account_number"      => "1200000589",
          "check_digit"         => "84"
        },
        {
          "country_code"        => "MR",
          "checksum"            => "13",
          "identification_code" => "00020",
          "branch_code"         => "00101",
          "account_number"      => "00001234567",
          "check_digit"         => "53"
        },
        {
          "country_code"        => "MT",
          "checksum"            => "84",
          "bic_code"            => "MALT",
          "identification_code" => "01100",
          "account_number"      => "0012345MTLCAST001S"
        },
        {
          "country_code"   => "NL",
          "checksum"       => "91",
          "bic_code"       => "ABNA",
          "account_number" => "0417164300"
        },
        {
          "country_code"        => "NO",
          "checksum"            => "93",
          "identification_code" => "8601",
          "account_number"      => "111794",
          "check_digit"         => "7"
        },
        {
          "country_code"   => "PK",
          "checksum"       => "36",
          "bic_code"       => "SCBL",
          "account_number" => "0000001123456702"
        },
        {
          "country_code"        => "PL",
          "checksum"            => "61",
          "identification_code" => "109",
          "branch_code"         => "0101",
          "check_digit"         => "4",
          "account_number"      => "0000071219812874"
        },
        {
          "country_code"   => "PS",
          "checksum"       => "92",
          "bic_code"       => "PALS",
          "account_number" => "000000000400123456702"
        },
        {
          "country_code"        => "PT",
          "checksum"            => "50",
          "identification_code" => "0001",
          "branch_code"         => "0123",
          "account_number"      => "12345678901",
          "check_digit"         => "92"
        },
        {
          "country_code"   => "QA",
          "checksum"       => "58",
          "bic_code"       => "DOHB",
          "account_number" => "00001234567890ABCDEFG"
        },
        {
          "country_code"        => "RO",
          "checksum"            => "49",
          "identification_code" => "AAAA",
          "account_number"      => "1B31007593840000"
        },
        {
          "country_code"        => "RS",
          "checksum"            => "35",
          "identification_code" => "260",
          "account_number"      => "0056010016113",
          "check_digit"         => "79"
        },
        {
          "country_code"        => "SA",
          "checksum"            => "03",
          "identification_code" => "80",
          "account_number"      => "000000608010167519"
        },
        {
          "country_code"        => "SE",
          "checksum"            => "45",
          "identification_code" => "500000000",
          "account_number"      => "58398257466"
        },
        {
          "country_code"        => "SI",
          "checksum"            => "56",
          "identification_code" => "19",
          "branch_code"         => "100",
          "account_number"      => "00001234",
          "check_digit"         => "38"
        },
        {
          "country_code"        => "SK",
          "checksum"            => "31",
          "identification_code" => "1200",
          "account_number"      => "0000198742637541"
        },
        {
          "country_code"        => "SM",
          "checksum"            => "86",
          "check_digit"         => "U",
          "identification_code" => "03225",
          "branch_code"         => "09800",
          "account_number"      => "000000270100"
        },
        {
          "country_code"   => "SV",
          "checksum"       => "62",
          "bic_code"       => "CENR",
          "account_number" => "00000000000000700025"
        },
        {
          "country_code"        => "TL",
          "checksum"            => "38",
          "identification_code" => "008",
          "account_number"      => "00123456789101",
          "check_digit"         => "57"
        },
        {
          "country_code"        => "TR",
          "checksum"            => "33",
          "identification_code" => "00061",
          "check_digit"         => "0",
          "account_number"      => "0519786457841326"
        },
        {
          "country_code"        => "UA",
          "checksum"            => "21",
          "identification_code" => "399622",
          "account_number"      => "0000026007233566001"
        },
        {
          "country_code"        => "VA",
          "checksum"            => "59",
          "identification_code" => "001",
          "account_number"      => "123000012345678"
        },
        {
          "country_code"   => "VG",
          "checksum"       => "96",
          "bic_code"       => "VPVG",
          "account_number" => "0000012345678901"
        },
        {
          "country_code"        => "XK",
          "checksum"            => "05",
          "identification_code" => "12",
          "branch_code"         => "12",
          "account_number"      => "0123456789",
          "check_digit"         => "06"
        }
      ])
    end
  end
end
