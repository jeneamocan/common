require 'spec_helper'

describe SaltCommon::ParsingHelpers do
  let(:base) { Dummy.new }

  before(:all) do
    class Dummy
      include SaltCommon::ParsingHelpers
    end
  end

  describe "parse_float" do
    it "it parses a floating-point string" do
      base.parse_float("450,00 руб.").should    == 450.0
      base.parse_float("+31.00 WMR").should     == 31.0
      base.parse_float("28 410,00").should      == 28410.0
      base.parse_float("200,00 р.").should      == 200.0
      base.parse_float("- 300.00 (RUR)").should == -300.0
      base.parse_float("- 301.50 (RUR)").should == -301.5
      base.parse_float("- 300.00 (RUR)").should == -300.0
      base.parse_float("1 037,74 р.").should    == 1037.74
      base.parse_float("- 300.00 (RUR)").should == -300.0
      base.parse_float("

                        7 495,20 RUR

                    ").should == 7495.2
      base.parse_float("
        -4.600,90
      ").should == -4600.9
      base.parse_float("
        -4,600.90
      ").should == -4600.9
      base.parse_float("a
        -4,000,001.32 руб.").should              == -4_000_001.32
      base.parse_float("
        -4.000.001,32 руб.").should              == -4_000_001.32
      base.parse_float("835,01 CZK").should      == 835.01
      base.parse_float("123,123,123,123").should == 123_123_123_123
      base.parse_float("123.123.123.123").should == 123_123_123_123
      base.parse_float("123,123,123.123").should == 123_123_123.123
      base.parse_float("123.123.123,123").should == 123_123_123.123
      base.parse_float("123,123").should         == 123.123
      base.parse_float("123.123").should         == 123.123
      base.parse_float("-76  179                  BYR").should == -76179.0
    end

    it "handles integral numbers" do
      base.parse_float("123,123,123,123", integral: true).should == 123_123_123_123
      base.parse_float("123.123.123.123", integral: true).should == 123_123_123_123
      base.parse_float("123.123.123,123", integral: true).should == 123_123_123_123
      base.parse_float("123,123,123.123", integral: true).should == 123_123_123_123
      base.parse_float("123.123,123.123", integral: true).should == 123_123_123_123
    end
  end

  describe "#parse_date" do
    it "parse date" do
      base.parse_date("  30.04.2013  ", "%d.%m.%Y").should == "2013-04-30"
    end

    it "parse date with leap year" do
      Timecop.freeze(2021, 2, 29) do
        allow(Date).to receive(:strptime) do |string, format|
          Date.strptime_without_mock_date(string, format)
        end

        expect(base.parse_date("29/02/2021", "%d/%m/%Y")).to eq("2020-02-29")
        expect(base.parse_date("29 Feb", "%d %b")).to eq("2020-02-29")
      end
    end

    it "raises descriptive error" do
      expect { base.parse_date("34.05.2021", "%d.%m.%Y") }.to raise_error("Error while parsing date field. Tried to parse: 34.05.2021")
    end
  end

  describe "#parse_leap_date" do
    it "parse leap date" do
      base.parse_leap_date("29.02.2021", "%d.%m.%Y").should == "2020-02-29"
    end
  end

  describe "#parse_time" do
    it "parse time" do
      base.parse_time("  30.04.2013 20:34:56  ", "%d.%m.%Y %H:%M:%S").should == "20:34:56"
      base.parse_time("20:34:56", "%H:%M:%S").should == "20:34:56"
    end
  end

  describe "#parse_currency" do
    it "parse currency" do
      base.parse_currency("  mdl ").should        == "MDL"
      base.parse_currency("  euro ").should       == "EUR"
      base.parse_currency("  pomidor ").should    == nil
      base.parse_currency("-$").should            == "USD"
      base.parse_currency("$").should             == "USD"
      base.parse_currency("WMU").should           == "UAH"
      base.parse_currency("WMK").should           == "KZT"
      base.parse_currency("WMV").should           == "VND"
      base.parse_currency("HRD").should           == "HRK"
      base.parse_currency("WMB").should           == "BYN"
      base.parse_currency("008").should           == "ALL"
      base.parse_currency("498").should           == "MDL"
      base.parse_currency("MUS").should           == "MUR"
      base.parse_currency("₸").should             == "KZT"
      base.parse_currency("4a98").should          == nil
      base.parse_currency("#%$4*&^%9)(*8").should == nil
      base.parse_currency("4.98").should          == nil
      base.parse_currency("498.98").should        == nil
      base.parse_currency("/n 643 ").should       == nil
      base.parse_currency("928347").should        == nil
      base.parse_currency("MDL498").should        == nil
      expect { base.parse_currency(504) }.to raise_error(SaltCommon::ParsingHelpers::ParsingError)
    end
  end

  describe "#parse_asset_code" do
    before do
      cached_list = File.read("spec/fixtures/cryptocurrencies/cached_list.txt")
      Redis.current.set("cryptocurrencies_list", cached_list)
    end

    it "parse asset code" do
      base.parse_asset_code("  eth ").should        == "ETH"
      base.parse_asset_code("  1st ").should        == "1ST"
      base.parse_asset_code("ltc").should           == "LTC"
      base.parse_asset_code("bch").should           == "BCH"
      base.parse_asset_code("8bit").should          == "8BIT"
      base.parse_asset_code("  balalaika ").should  == nil
      base.parse_asset_code("TROLL").should         == "TROLL"
      base.parse_asset_code("4a98").should          == nil
      base.parse_asset_code("#%$4*&^%9)(*8").should == nil
      base.parse_asset_code("4.98").should          == nil
      base.parse_asset_code("498.98").should        == nil
      base.parse_asset_code("/n 643 ").should       == nil
      base.parse_asset_code("928347").should        == nil
      base.parse_asset_code("MDL498").should        == nil
      base.parse_asset_code("").should              == nil
      expect { base.parse_asset_code(504) }.to raise_error(SaltCommon::ParsingHelpers::ParsingError)
    end
  end

  describe "#parse_mcc" do
    it "parses mcc codes" do
      base.parse_mcc("").should      == nil
      base.parse_mcc(15200).should   == "1520"
      base.parse_mcc("15200").should == "1520"
      base.parse_mcc(8).should       == "0008"
      base.parse_mcc("8").should     == "0008"
      base.parse_mcc("2220").should  == nil
      base.parse_mcc(2220).should    == nil
    end
  end

  describe "#parse_description" do
    it "parse description" do
      base.parse_description("  \rDESCRIPTION\n \t\n \t\t\t").should == "DESCRIPTION"
      base.parse_description("Плата за обслуговування основної картки\n \t\n \t\t\tзгідно тарифів Банку").should == "Плата за обслуговування основної картки згідно тарифів Банку"
    end
  end

  describe "#parse_card_number" do
    it "parse card number" do
      base.parse_card_number("  1234-123-1234 -1234").should                                 == "1234********234"
      base.parse_card_number("  123412312341234").should                                     == "1234********234"
      base.parse_card_number(" 1234 1234 1234 1234").should                                  == "1234********1234"
      base.parse_card_number(" 1234-1234-1234-1234").should                                  == "1234********1234"
      base.parse_card_number(" 1234123412341234").should                                     == "1234********1234"
      base.parse_card_number(" 1234xxxxxxxx1234").should                                     == "1234xxxxxxxx1234"
      base.parse_card_number(" XXXXXXXXXXXX1234").should                                     == "XXXXXXXXXXXX1234"
      base.parse_card_number(" XXXX-XXXX-XXXX-1234").should                                  == "XXXX-XXXX-XXXX-1234"
      base.parse_card_number(" XXX-XXXX-XXXX-1234").should                                   == "XXX-XXXX-XXXX-1234"
      base.parse_card_number("BG76BPBI7911111111101").should                                 == "BG76BPBI7911111111101"
      base.parse_card_number("1234111111115678 [XxXxXXXxXXXX]").should                       == "1234********5678 [XxXxXXXxXXXX]"
      base.parse_card_number("1234111111115678 test string").should                          == "1234********5678 test string"
      base.parse_card_number("1234111111115678 123 123").should                              == "1234********5678 123 123"
      base.parse_card_number("11111111111111111 ALY BERSHAWY PHARMACY DOKKI EG EG").should   == "11111111111111111 ALY BERSHAWY PHARMACY DOKKI EG EG"
      base.parse_card_number("40111 111 1 11111111108 ККК_Кредитная карта platin_5%").should == "40111 111 1 11111111108 ККК_Кредитная карта platin_5%"
      base.parse_card_number("MasterCard 1111 1111 1111 1111 XXXX XXXX").should              == "MasterCard 1111********1111 XXXX XXXX"
      base.parse_card_number("  1234-123-1234 -1234 text").should                            == "1234********234 text"
      base.parse_card_number("  123412312341234 text").should                                == "1234********234 text"
      base.parse_card_number(" 1234 1234 1234 1234 text").should                             == "1234********1234 text"
      base.parse_card_number(" 1234-1234-1234-1234 text").should                             == "1234********1234 text"
      base.parse_card_number(" 1234123412341234 text").should                                == "1234********1234 text"
      base.parse_card_number(" 1234xxxxxxxx1234 text").should                                == "1234xxxxxxxx1234 text"
      base.parse_card_number(" XXXXXXXXXXXX1234 text").should                                == "XXXXXXXXXXXX1234 text"
      base.parse_card_number(" XXXX-XXXX-XXXX-1234 text").should                             == "XXXX-XXXX-XXXX-1234 text"
      base.parse_card_number(" XXX-XXXX-XXXX-1234 text").should                              == "XXX-XXXX-XXXX-1234 text"
      base.parse_card_number("BG76BPBI7911111111101 text").should                            == "BG76BPBI7911111111101 text"
      base.parse_card_number("ZP11 1111 1111 1111 1193").should                              == "ZP11 1111 1111 1111 1193"
    end
  end

  describe "#downcase(unicode_string)" do
    it "parse description" do
      base.downcase("ВОЛОДИМИРАС  СВАЛБОНАС").should == "володимирас свалбонас"
    end
  end

  describe "#titlecase(unicode_string)" do
    it "parse description" do
      base.titlecase("володимирас свалбонас").should == "Володимирас Свалбонас"
    end
  end

  describe "#as_regexp(string)" do
    it "makes a valid escaped Regexp from string" do
      base.as_regexp("^|*Транспортная* {карта} (г. Москва)$").should == /\^\|\*Транспортная\*\ \{карта\}\ \(г\.\ Москва\)\$/i
    end

    it "makes a valid escaped Regexp from several strings" do
      base.as_regexp("123", "^|*Транспортная* {карта} (г. Москва)$").should == /123|\^\|\*Транспортная\*\ \{карта\}\ \(г\.\ Москва\)\$/i
    end

    it "makes a valid escaped Regexp from valid & invalid arguments" do
      base.as_regexp("a", nil, "", nil, "b").should == /a|b/i
    end
  end

  describe "#normalize_string" do
    it "removes extra spaces from string" do
      string = "  #{SaltCommon::ParsingHelpers::NBSP * 4}123    abc "
      base.send(:normalize_string, string).should == "123 abc"
    end
  end

  describe "#iban_valid?" do
    it "checks a valid iban" do
      string = "AT611904300234573201"
      expect(base.iban_valid?(string)).to be_truthy
    end

    it "doesn't fail if invalid or blank ibans are sent" do
      invalid_list = %w[
        GR4501108950000011111111111 GDR 111111 XRP@e#
      ]

      expect(invalid_list.none? { |string| base.iban_valid?(string) }).to be_truthy

      expect(base.iban_valid?(nil)).to   be_falsey
      expect(base.iban_valid?(" ")).to   be_falsey
      expect(base.iban_valid?(12345)).to be_falsey
    end

    it "checks an invalid DE iban" do
      expect(base.iban_valid?("DE70500105172492783311")).to be_falsey
    end

    it "checks a valid DE iban" do
      expect(KontoCheck).to receive(:iban_check).and_return(1)
      expect(base.iban_valid?("DE70500105172492783311")).to be_truthy
    end
  end

  describe "#convert_to_alpha3" do
    it "converts country code into an ISO3166-alpha3 country code" do
      country_code = "CZ"
      expect(base.convert_to_alpha3(country_code)).to eq("CZE")
      expect { base.convert_to_alpha3('XF') }.to raise_error(IsoCountryCodes::UnknownCodeError, "No ISO 3166-1 code could be found for 'XF'")
      expect { base.convert_to_alpha3(nil) }.to raise_error(IsoCountryCodes::UnknownCodeError, "No ISO 3166-1 code could be found for ''")
    end
  end
end
