require 'spec_helper'

describe SaltCommon::Sender do
  describe "initialize(&block)" do
    it "calls the block with mail" do
      stub_const("Settings", double(email: double(notify: "notify-robber@example.com")))

      yielded = nil

      some_block = proc do |mail|
        mail.to      = "test@robber.local"
        mail.subject = "subject"
        mail.body    = "body"

        yielded      = mail
      end

      sender = SaltCommon::Sender.new(&some_block)

      sender.mail.should         == yielded
      sender.mail.to.should      == ["test@robber.local"]
      sender.mail.subject.should == "subject"
      sender.mail.body.should    == "body"
      sender.mail.charset.should == Encoding::UTF_8.to_s
      sender.mail.from.should    == [Settings.email.notify]

      Mail::TestMailer.deliveries.length.should == 1
    end
  end
end
