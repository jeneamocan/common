require "spec_helper"

describe SaltCommon::SSO::Authenticator do
  before do
    stub_const("Settings", double)
    Settings.stub_chain(:sso, :url).and_return("http://sso.banksalt.com")
    Settings.stub_chain(:sso, :application_id).and_return(22)
    Settings.stub_chain(:sso, :secret).and_return("secret")
  end

  describe ".get_token_params" do
    it "sends the correct request" do
      described_class.should_receive(:request).with(
        :method  => :post,
        :url     => "http://sso.banksalt.com/oauth/token",
        :payload => {
          :grant_type    => "authorization_code",
          :code          => "CODE",
          :redirect_uri  => "REDIRECT_URI",
          :client_id     => 22,
          :client_secret => "secret"
        }
      ) { {"one" => "two"} }

      result = described_class.get_token_params(code: "CODE", redirect_uri: "REDIRECT_URI")
      result.should == {"one" => "two"}
    end
  end

  describe ".get_client_data" do
    it "issues the correct HTTP request" do
      described_class.should_receive(:request).with(
        :method  => :get,
        :url     => "http://sso.banksalt.com/api/v1/me.json",
        :headers => {
          content_type:  "application/x-www-form-urlencoded",
          authorization: "Bearer TOKEN"
        }
      ) { {"one" => "two"} }

      result = described_class.get_client_data("TOKEN")
      result.should == {"one" => "two"}
    end
  end

  describe ".refresh_params!" do
    it "issues the correct HTTP request" do
      described_class.should_receive(:request).with(
        :method        => :post,
        :url           => "http://sso.banksalt.com/oauth/token",
        :payload       => {
          :grant_type    => "refresh_token",
          :refresh_token => "TOKEN",
          :client_id     => 22,
          :client_secret => "secret"
        }
      ) { {"one" => "two"} }

      result = described_class.refresh_params!("TOKEN")
      result.should == {"one" => "two"}
    end
  end

  describe ".request" do
    it "sends a valid request to SSO and decodes the response" do
      options = { method: "GET", url: "http://sso.banksalt.com", payload: { token: "TOKEN" } }
      RestClient::Request.should_receive(:execute).with(options) { "{\"one\":\"two\"}" }
      described_class.request(options).should == {"one" => "two"}
    end

    it "sends invalid request to SSO and raises error" do
      options = { method: "GET", url: "http://sso.banksalt.com", payload: { token: "INVALID_TOKEN" } }
      RestClient::Request.should_receive(:execute).with(options).and_raise(RestClient::Unauthorized)
      expect { described_class.request(options) }.to raise_error(SaltCommon::SSO::Session::Unauthorized)
    end
  end

  describe ".can_login?" do
    it "returns true when user has the sign in role" do
      user_info = { "email" => "user@email.com", "roles" => ["sign_in", "developer", "sales"] }
      expect(described_class.can_login?(user_info)).to eq(true)
    end

    it "return false when user has no sign in role" do
      user_info = { "email" => "user@email.com", "roles" => ["developer"] }
      expect(described_class.can_login?(user_info)).to eq(false)
    end
  end

  describe ".authorize_url" do
    it "constructs the correct URL from params given" do
      expect(described_class.authorize_url(redirect_uri: "here"))
        .to eq("http://sso.banksalt.com/oauth/authorize?response_type=code&client_id=22&redirect_uri=here")
    end
  end

  describe ".logout_url" do
    it "constructs the correct URL from params given" do
      expect(described_class.logout_url(redirect_uri: "here")).to eq("http://sso.banksalt.com/logout?redirect_uri=here")
    end
  end

  describe ".refresh_session_state_url" do
    it "returns sso refresh session state URL" do
      expect(described_class.refresh_session_state_url).to eq("http://sso.banksalt.com/refresh_session_state")
    end

    it "encodes given opts into url" do
      expect(described_class.refresh_session_state_url(redirect_uri: "google.com"))
        .to eq("http://sso.banksalt.com/refresh_session_state?redirect_uri=google.com")
    end
  end
end
