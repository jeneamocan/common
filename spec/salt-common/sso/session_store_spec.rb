require "spec_helper"

describe SaltCommon::SSO::SessionStore do
  let(:email1) { "email1@example.com" }
  let(:email2) { "email2@example.com" }

  before do
    stub_const("Settings", double)
    Settings.stub_chain(:application, :name).and_return("sso")
  end

  it "can set and get hashes in redis" do
    store = described_class.new(email1)
    store.set!(access_token: "access", refresh_token: "refresh")
    store.get.should == {"access_token" => "access", "refresh_token" => "refresh"}
  end

  it "sets different tokens for different emails" do
    store = described_class.new(email1)
    store.set!(access_token: "access", refresh_token: "refresh")

    store2 = described_class.new(email2)
    store2.set!(access_token: "access2", refresh_token: "refresh2")

    store.get.should  == {"access_token" => "access", "refresh_token" => "refresh"}
    store2.get.should == {"access_token" => "access2", "refresh_token" => "refresh2"}
  end

  it "can remove the token from redis" do
    store = described_class.new(email1)
    store.set!(access_token: "access", refresh_token: "refresh")
    store.remove!

    store.get.should == nil
  end
end
