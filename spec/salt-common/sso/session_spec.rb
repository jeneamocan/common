require "spec_helper"

describe SaltCommon::SSO::Session do
  let(:email)   { "email1@example.com" }
  let(:session) { SaltCommon::SSO::Session.new(email) }

  before do
    stub_const("Settings", double)
    Settings.stub_chain(:application, :name).and_return("sso")
  end

  describe "#store_session" do
    it "stores the session params into redis" do
      session.store_session(one: "two")
      SaltCommon::SSO::SessionStore.new(email).get.should == {"one" => "two"}
    end
  end

  describe "#valid?" do
    it "returns true if sso authentication returned the same admin" do
      client_data = { "email" => email, "roles" => ["developer"] }
      SaltCommon::SSO::Authenticator.should_receive(:get_client_data) { client_data }

      session.store_session(access_token: "ACCESS TOKEN")
      session.valid?.should == true
    end

    it "returns false if sso authentication returned a different admin" do
      client_data = { "email" => "othermail@example.com", "roles" => ["developer"] }
      SaltCommon::SSO::Authenticator.should_receive(:get_client_data) { client_data }

      session.store_session(access_token: "ACCESS TOKEN")
      session.valid?.should == false
    end

    it "raises Unauthorized error if SSO returned unauthorized response" do
      SaltCommon::SSO::Authenticator.should_receive(:get_client_data).and_raise(RestClient::Unauthorized)
      session.store_session(access_token: "ACCESS TOKEN")
      expect { session.valid? }.to raise_error(described_class::Unauthorized)
    end

    it "returns false if there is nothing stored in redis" do
      session.valid?.should == false
    end
  end

  describe "#user_can_login?" do
    it "returns true if client_data has sign_in role" do
      client_data = { "email" => email, "roles" => ["sign_in", "developer"] }
      SaltCommon::SSO::Authenticator.should_receive(:get_client_data) { client_data }

      session.store_session(access_token: "ACCESS TOKEN")
      session.user_can_login?.should == true
    end

    it "returns false when client has no sign_in role" do
      client_data = { "email" => email, "roles" => ["developer"] }
      SaltCommon::SSO::Authenticator.should_receive(:get_client_data) { client_data }

      session.store_session(access_token: "ACCESS TOKEN")
      session.user_can_login?.should == false
    end

    it "raises Unauthorized error if SSO returned unauthorized response" do
      SaltCommon::SSO::Authenticator.should_receive(:get_client_data).and_raise(RestClient::Unauthorized)
      session.store_session(access_token: "ACCESS TOKEN")
      expect { session.valid? }.to raise_error(described_class::Unauthorized)
    end

    it "returns false if there is nothing stored in redis" do
      session.valid?.should == false
    end
  end

  describe "#has_role?" do
    before do
      client_data = { "email" => email, "roles" => ["sign_in", "developer"] }
      SaltCommon::SSO::Authenticator.should_receive(:get_client_data) { client_data }
      session.store_session(access_token: "ACCESS TOKEN")
    end

    it "returns true because user has a role for a specific project" do
      session.has_role?("sign_in").should == true
    end

    it "returns false because user has no role for a specific project" do
      session.has_role?("admin").should == false
    end
  end

  describe "#try_extend!" do
    it "extends the token and writes it to session store" do
      SaltCommon::SSO::Authenticator.should_receive(:refresh_params!)
        .with("REFRESH TOKEN") { { "access_token" => "ACCESS TOKEN", "refresh_token" => "NEW REFRESH TOKEN" } }

      session.store_session(access_token: "ACCESS TOKEN", refresh_token: "REFRESH TOKEN")
      session.try_extend!
      SaltCommon::SSO::SessionStore.new(email).get.should == {
        "access_token"  => "ACCESS TOKEN",
        "refresh_token" => "NEW REFRESH TOKEN"
      }
    end

    it "raises unauthorized error if sso responded with unauthorized request" do
      SaltCommon::SSO::Authenticator.should_receive(:refresh_params!).and_raise(RestClient::Unauthorized)
      session.store_session(access_token: "ACCESS TOKEN")
      expect { session.try_extend! }.to raise_error(described_class::Unauthorized)
    end

    it "raises unauthorized if there is no session stored in redis" do
      expect { session.try_extend! }.to raise_error(described_class::Unauthorized)
    end
  end

  describe "#clear_session!" do
    it "clears the session from the session store" do
      session.store_session(access_token: "123")
      session.clear_session!
      SaltCommon::SSO::SessionStore.new(email).get.should == nil
    end
  end
end
