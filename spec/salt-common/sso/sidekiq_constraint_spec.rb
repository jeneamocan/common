require 'spec_helper'

describe SaltCommon::SSO::SidekiqConstraint do
  let(:user)    { OpenStruct.new(email: "user@banksalt.com") }
  let(:warden)  { double(user: user) }
  let(:request) { OpenStruct.new(env: { "warden" => warden }) }

  before do
    warden.should_receive(:authenticate!).with(scope: :admin)
  end

  context "on staging, production env" do
    let(:subject) { described_class.new(:admin, "sidekiq_actions", "staging") }

    it "contacts sso" do
      SaltCommon::SSO::Session.any_instance.should_receive(:valid?).and_return(true)
      SaltCommon::SSO::Session.any_instance
        .should_receive(:has_role?)
        .with("sidekiq_actions")
        .and_return(true)

      subject = described_class.new(:admin, "sidekiq_actions", "production")
      subject.matches?(request).should == true
    end

    it "returns false if sso session does not have role" do
      SaltCommon::SSO::Session.any_instance.should_receive(:valid?).and_return(true)
      SaltCommon::SSO::Session.any_instance
        .should_receive(:has_role?)
        .with("sidekiq_actions")
        .and_return(false)

      subject = described_class.new(:admin, "sidekiq_actions", "production")
      subject.matches?(request).should == false
    end

    it "returns false if sso is not authorized" do
      SaltCommon::SSO::Session.any_instance
        .should_receive(:valid?)
        .and_raise(SaltCommon::SSO::Session::Unauthorized.new)

      subject = described_class.new(:admin, "sidekiq_actions", "production")
      subject.matches?(request).should == false
    end
  end

  context "on any environment except staging or production" do
    let(:subject) { described_class.new(:admin, "sidekiq_actions", "development") }

    it "returns true if admin has role" do
      subject.matches?(request).should == true
    end
  end
end
