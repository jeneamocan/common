require 'spec_helper'

describe SaltCommon::S3 do
  let(:file_name)       { "sample.txt" }
  let(:file_local_path) { "spec/fixtures/#{file_name}" }
  let(:s3)              { SaltCommon::S3.new(options) }
  let(:options) {
    {
      "region"                => "eu-central-1",
      "bucket"                => "saltedge-private",
      "aws_access_key_id"     => "AKIAICHXVF5CV4XS3WSA",
      "aws_secret_access_key" => "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"
    }.with_indifferent_access # NOTE: eduardm test keys, please don't abuse
  }

  describe "#initialize(options)" do
    it "initializes with options" do
      SaltCommon::S3.any_instance.should_receive(:check_options!)
      SaltCommon::S3.any_instance.should_receive(:configure_aws)

      s3.options.should == options
    end
  end

  context "Public methods" do
    context "Real requests" do
      describe "#store_file(resource)" do
        it "uploads file to s3 and returns its url", :vcr do
          SecureRandom.should_receive(:uuid).and_return("test")
          response = s3.store_file(file_local_path)
          response.should == "https://saltedge-private.s3.eu-central-1.amazonaws.com/test.txt"
        end
      end
      describe "#get_file(file_name)" do
        it "downloads file from s3", :vcr do
          response = s3.get_file("test.txt")
          response.should == "s3 sample\n"
        end
      end
      describe "#remove_file(file_name)" do
        it "remove file from s3", :vcr do
          response = s3.remove_file("test.txt")
          # We receive a response which contains delete_marker with nil value,
          # and we can't handle if file was deleted or not deleted.
          response.should respond_to(:delete_marker)
        end
      end
    end
    context "Mocked requests" do
      describe "#store_file(resource)" do
        it "stores file and returns its url" do
          file = double

          s3.should_receive(:prepare_file).with(file_name).and_return([file, file_name])
          s3.should_receive(:store_object).with(file_name, file, "private")
          s3.should_receive(:public_path_for).with(file_name).and_return("https://example.com")

          s3.store_file(file_name).should == "https://example.com"
        end
      end

      describe "#get_file(file_name)" do
        it "checks file existence and returns its contend" do
          s3.should_receive(:check_exists!).with(file_name)
          s3.should_receive(:get_object).with(file_name)

          s3.get_file(file_name)
        end
      end

      describe "#remove_file(file_name)" do
        it "checks file existence and remove this file" do
          s3.should_receive(:delete_object).with(file_name)

          s3.remove_file(file_name)
        end
      end
    end
  end

  context "Private methods" do
    describe "#check_options!" do
      it "does not raise error if options have all the necessary keys" do
        expect { s3.send(:check_options!) }.not_to raise_error
      end
      it "raises error if some of the keys are not present n options" do
        options.delete("region")
        expect { s3.send(:check_options!) }.to raise_error(SaltCommon::S3::MissingKeys, 'Missing: ["region"]')
      end
    end

    describe "#configure_aws" do
      it "configures Aws module" do
        credentials = double
        Aws::Credentials.should_receive(:new)
          .with(options[:aws_access_key_id], options[:aws_secret_access_key])
          .twice # one more time in initialize
          .and_return(credentials)
        Aws.config.should_receive(:update).with(region: options[:region], credentials: credentials)
          .twice # one more time in initialize

        s3.send(:configure_aws)
      end
    end

    describe "#prepare_file(resource)" do
      context "with given file path" do
        it "prepares file and file_name" do
          SecureRandom.should_receive(:uuid).and_return("random_name")
          File.should_receive(:extname).with(file_local_path).and_return(".txt")
          File.should_receive(:open).with(file_local_path, "rb").and_return(file_local_path)

          s3.send(:prepare_file, file_local_path).should == [file_local_path, "random_name.txt"]
        end
      end
      context "with given file" do
        it "prepares file and file_name" do
          file = File.open(file_local_path, "rb")
          SecureRandom.should_receive(:uuid).and_return("random_name")

          s3.send(:prepare_file, file).should == [file, "random_name"]
        end
      end
      context "with given string" do
        it "prepares new file and file_name" do
          string_io = double
          test_string = "some_string"
          SecureRandom.should_receive(:uuid).and_return("random_name")
          StringIO.should_receive(:open).with(test_string).and_return(string_io)

          s3.send(:prepare_file, test_string).should == [string_io, "random_name"]
        end
      end
    end

    describe "#get_object(file_name)" do
      it "asks aws client to get file's content" do
        io = double(body: double(read: "test"))
        s3.send(:client).should_receive(:get_object).with(bucket: options[:bucket], key: file_name).and_return(io)
        s3.send(:get_object, file_name).should == "test"
      end
    end

    describe "#store_object(file_name, file)" do
      it "asks aws client to upload file's content" do
        file = double
        s3.send(:client).should_receive(:put_object).with(bucket: options[:bucket], key: file_name, body: file, acl: "private")
        s3.send(:store_object, file_name, file, "private")
      end
    end

    describe "#delete_object(file_name, file)" do
      it "asks aws client to delete file" do
        s3.send(:client).should_receive(:delete_object).with(bucket: options[:bucket], key: file_name)
        s3.send(:delete_object, file_name)
      end
    end

    describe "#public_path_for(file_name)" do
      it "returns url for the given file_name" do
        object = double(public_url: "http://example.com")
        Aws::S3::Object.should_receive(:new).with(options[:bucket], file_name).and_return(object)
        s3.send(:public_path_for, file_name).should == "http://example.com"
      end
    end

    describe "#check_exists!(file_name)" do
      it "does not raise error if file exists on s3" do
        object = double(exists?: true)
        Aws::S3::Object.should_receive(:new).with(options[:bucket], file_name).and_return(object)

        expect { s3.send(:check_exists!, file_name) }.not_to raise_error
      end
      it "raises error if file does not exist on s3" do
        object = double(exists?: false)
        Aws::S3::Object.should_receive(:new).with(options[:bucket], file_name).and_return(object)

        expect { s3.send(:check_exists!, file_name) }.to raise_error(SaltCommon::S3::FileNotFound)
      end
    end

    describe "#client" do
      it "initializes and instance of Aws::S3::Client" do
        s3.send(:client).class.should == Aws::S3::Client
      end
    end
  end
end
