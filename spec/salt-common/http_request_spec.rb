require "spec_helper"

# module RestClient
#   module AbstractResponse
#     def return!(&block)
#       case code
#       when 200..207
#         self
#       when 301, 302, 307
#         case request.method
#         when 'get', 'head'
#           check_max_redirects
#           follow_redirection(&block)
#         else
#           # NOTE: commented line with raise, added line which returns location
#           headers[:location]
#           # raise exception_with_response
#         end
#       when 303
#         check_max_redirects
#         follow_get_redirection(&block)
#       else
#         raise exception_with_response
#       end
#     end
#   end
# end

describe SaltCommon::HttpRequest do
  def mock_response(headers={})
    OpenStruct.new(follow_redirection: true, headers: headers)
  end

  def mock_error(method, error_class, headers={})
    RestClient.should_receive(method)
      .and_raise(error_class.new(mock_response(headers)))
  end

  describe "#execute" do
    it "executes a request" do
      described_class.should_receive(:execute).with(method: :get, url: "http://foo.com")
      described_class.new(method: :get, url: "http://foo.com").execute
    end
  end

  describe ".get" do
    it "delegates to RestClient" do
      RestClient.should_receive(:get).with("http://google.com")
      described_class.get("http://google.com")
    end
  end

  describe ".post" do
    it "delegates to RestClient" do
      RestClient.should_receive(:post).with("http://google.com", "body")
      described_class.post("http://google.com", "body")
    end

    it "returns location header on MovedPermanently" do
      mock_error(:post, RestClient::MovedPermanently, location: "http://location.com")
      described_class.post("http://google.com", "body").should == "http://location.com"
    end

    it "follows redirect for Found" do
      mock_error(:post, RestClient::Found, location: "http://location.com")
      described_class.post("http://google.com", "body").should == "http://location.com"
    end

    it "follows redirect for TemporaryRedirect" do
      mock_error(:post, RestClient::TemporaryRedirect, location: "http://location.com")
      described_class.post("http://google.com", "body").should == "http://location.com"
    end
  end

  describe ".patch" do
    it "delegates to RestClient" do
      RestClient.should_receive(:patch).with("http://google.com", "body")
      described_class.patch("http://google.com", "body")
    end

    it "follows redirect for MovedPermanently" do
      mock_error(:patch, RestClient::MovedPermanently, location: "http://location.com")
      described_class.patch("http://google.com", "body").should == "http://location.com"
    end

    it "follows redirect for Found" do
      mock_error(:patch, RestClient::Found, location: "http://location.com")
      described_class.patch("http://google.com", "body").should == "http://location.com"
    end

    it "follows redirect for TemporaryRedirect" do
      mock_error(:patch, RestClient::TemporaryRedirect, location: "http://location.com")
      described_class.patch("http://google.com", "body").should == "http://location.com"
    end
  end

  describe ".put" do
    it "delegates to RestClient" do
      RestClient.should_receive(:put).with("http://google.com", "body")
      described_class.put("http://google.com", "body")
    end

    it "follows redirect for MovedPermanently" do
      mock_error(:put, RestClient::MovedPermanently, location: "http://location.com")
      described_class.put("http://google.com", "body").should == "http://location.com"
    end

    it "follows redirect for Found" do
      mock_error(:put, RestClient::Found, location: "http://location.com")
      described_class.put("http://google.com", "body").should == "http://location.com"
    end

    it "follows redirect for TemporaryRedirect" do
      mock_error(:put, RestClient::TemporaryRedirect, location: "http://location.com")
      described_class.put("http://google.com", "body").should == "http://location.com"
    end
  end

  describe ".delete" do
    it "delegates to RestClient" do
      RestClient.should_receive(:delete).with("http://google.com", "body")
      described_class.delete("http://google.com", "body")
    end

    it "follows redirect for MovedPermanently" do
      mock_error(:delete, RestClient::MovedPermanently, location: "http://location.com")
      described_class.delete("http://google.com", "body").should == "http://location.com"
    end

    it "follows redirect for Found" do
      mock_error(:delete, RestClient::Found, location: "http://location.com")
      described_class.delete("http://google.com", "body").should == "http://location.com"
    end

    it "follows redirect for TemporaryRedirect" do
      mock_error(:delete, RestClient::TemporaryRedirect, location: "http://location.com")
      described_class.delete("http://google.com", "body").should == "http://location.com"
    end
  end

  describe ".head" do
    it "delegates to RestClient" do
      RestClient.should_receive(:head).with("http://google.com")
      described_class.head("http://google.com")
    end
  end

  describe ".options" do
    it "delegates to RestClient" do
      RestClient.should_receive(:options).with("http://google.com")
      described_class.options("http://google.com")
    end

    it "follows redirect for MovedPermanently" do
      mock_error(:options, RestClient::MovedPermanently, location: "http://location.com")
      described_class.options("http://google.com").should == "http://location.com"
    end

    it "follows redirect for Found" do
      mock_error(:options, RestClient::Found, location: "http://location.com")
      described_class.options("http://google.com").should == "http://location.com"
    end

    it "follows redirect for TemporaryRedirect" do
      mock_error(:options, RestClient::TemporaryRedirect, location: "http://location.com")
      described_class.options("http://google.com").should == "http://location.com"
    end
  end
end
