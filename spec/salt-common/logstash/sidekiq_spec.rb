require 'spec_helper'

describe Sidekiq::Logging::Json::Logger do
  let(:project) { "bucket" }
  let(:service) { "rails" }
  let(:logger)  { Sidekiq::Logging::Json::Logger.new(project, service) }

  context "Object methods" do
    describe "#call" do
      it "should provide a method that returns the formatted message" do
        Timecop.freeze do
          hash = SaltCommon::JsonWrapper.decode(logger.call("severity", Time.now, "name", ::Exception.new("message")))
          hash["@timestamp"].should   == Time.now.utc.iso8601
          hash["status"].should       == "exception"
          hash["severity"].should     == "severity"
          hash["duration"].should     == nil
          hash["message"].should      == "message"
          hash["project"].should      == "bucket"
          hash["service"].should      == "rails"
          hash["process_id"].should   == ::Process.pid
          hash["thread_id"].should    == Thread.current.object_id.to_s(36)
          hash["program_name"].should == "name"
          hash["worker"].should       == nil
        end
      end
    end
  end

  context "Private methods" do
    describe "#process_message" do
      it "should return exception if message is an Exception" do
        logger.send(:process_message, nil, nil, nil, Exception.new).should == {
          status: "exception"
        }
      end

      it "should return retry hash if message is a retry" do
        logger.send(:process_message, nil, nil, nil, { "retry" => true, "class" => "Kls", "args" => "args" }).should == {
          status: "retry", message: "Kls failed, retrying with args args."
        }
      end

      it "should return dead hash if message is a dead retry" do
        logger.send(:process_message, nil, nil, nil, { "class" => "Kls", "args" => "args" }).should == {
          status: "dead", message: "Kls failed with args args, not retrying."
        }
      end

      it "should return hash with status and duration" do
        logger.send(:process_message, nil, nil, nil, "done 128s").should == {
          status: "done", duration: 128.0
        }
      end
    end
  end
end
