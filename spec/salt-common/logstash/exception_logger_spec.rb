require 'spec_helper'

describe SaltCommon::ExceptionLogger do
  let(:login_id) { "123" }
  let(:item)     { Storage::Test::Item.new(login_id) }
  let(:storage)  { Storage::Test::Collection.new.add(params) }
  let(:params) {
    {
      "login_id"                => login_id,
      "country_code"            => "md",
      "provider_code"           => "victoriabank_md",
      "mode"                    => Storage::Item::AUTOMATIC,
      "fetch_type"              => Storage::Item::FULL,
      "automatic_fetch"         => true,
      "self_hosted"             => true,
      "start_time"              => Time.now.utc.as_json,
      "credentials"             => "123zbs",
      "pid"                     => 321,
      "host"                    => "robber.banksalt.com",
      "session_id"              => "123-123-123",
      "interactive_credentials" => "zbszbs",
      "interactive_data"        => "123123",
      "internal_data"           => { "relevant" => false },
      "adapter_config"          => { "relevant" => false }
    }
  }
  let(:logger) { described_class.new("rspec", "exception_rspec") }
  let(:item_hash) { item.insecure_to_h }
  let(:message) {
    {
      '@timestamp' => Time.now.utc.iso8601,
      :params      => item_hash,
      :project     => "rspec",
      :service     => "exception_rspec"
    }
  }
  let(:clean_item_hash) {
    item_hash
      .merge("credentials"=>"[FILTERED]", "interactive_credentials"=>"[FILTERED]")
      .except("internal_data", "adapter_config")
  }
  let(:clean_message) {
    {
      '@timestamp' => Time.now.utc.iso8601,
      :params      => clean_item_hash,
      :project     => "rspec",
      :service     => "exception_rspec"
    }
  }

  before(:each) do
    stub_const("Settings", double)
    Settings.stub_chain(:log, :logstash, :enabled).and_return(true)
    Settings.stub_chain(:log, :logstash, :host).and_return("rspec_localhost")
    Settings.stub_chain(:log, :logstash, :port).and_return(5258)
    Settings.stub_chain(:log, :logstash, :type).and_return("udp")
  end

  describe "#initialize" do
    it "initializes a logger" do
      logger.project.should   == "rspec"
      logger.service.should   == "exception_rspec"
    end
  end

  it "different instances do not create duplicate instances of the logger" do
    described_class.new("", "").send(:logger).object_id.should == described_class.new("", "").send(:logger).object_id
  end

  describe "#send_log" do
    it "sends exceptions to logstash" do
      Logger.any_instance
        .should_receive(:error)
        .with(SaltCommon::JsonWrapper.encode(clean_message))

      logger.send_log(item_hash)
    end

    it "won't sends exceptions to logstash" do
      Settings.stub_chain(:log, :logstash, :enabled) { false }
      Logger.any_instance.should_not_receive(:error)

      logger.send_log(item_hash)
    end
  end

  context "Private" do
    describe "#logger" do
      it "sends a UDP log" do
        exception_logger = logger.send(:logger).device

        exception_logger.class.should == LogStashLogger::Device::UDP
        exception_logger.host.should  == Settings.log.logstash.host
        exception_logger.port.should  == Settings.log.logstash.port
      end

      it "sends a UNIX log" do
        Thread.current[:saltedge_logger] = nil
        Settings.stub_chain(:log, :logstash, :path).and_return("some_path")
        Settings.stub_chain(:log, :logstash, :type).and_return("unix")
        exception_logger = logger.send(:logger).device

        exception_logger.class.should == LogStashLogger::Device::Unix
        exception_logger.instance_variable_get("@path").should == Settings.log.logstash.path
      end

      it "returns same instance of logger" do
        logger.send(:logger)
        another_logger = logger.send(:logger)
        logger.should_not == another_logger
      end
    end

    describe "#message" do
      it "returns a message hash" do
        exception_message        = logger.send(:message, item_hash)
        exception_message.should == clean_message
      end

      it "filters internal_data and adapter_config keys" do
        logger.send(:message, item_hash)[:params]
          .slice("adapter_config", "internal_data")
          .should == {}
      end
    end
  end
end
