require 'spec_helper'

describe SaltCommon::Logstash::LogMapper do
  let(:mapper) { described_class.new(rules) }
  let(:rules) do
    {
      /id/                         => described_class::ELASTIC_STRING,
      "from_date"                  => described_class::ELASTIC_DATE,
      "to_date"                    => described_class::ELASTIC_DATE,
      "daily"                      => described_class::ELASTIC_STRING,
      "consent.consent_expires_at" => described_class::ELASTIC_DATE_TIME,
      "consent.consent_given_at"   => described_class::ELASTIC_DATE_TIME,
      "client_provider_settings"   => described_class::ELASTIC_OBJECT,
      "custom"                     => proc { |string| string + " bar" },
      /nil/                        => described_class::ELASTIC_STRING,
      "custom.field"               => described_class::ELASTIC_DATE
    }
  end

  let(:params) do
    {
      "id"                       => 123,
      "nested"                   => { "other_id" => 456 },
      "from_date"                => "2020-02-16T09:20:14Z",
      "to_date"                  => "2020-04-16T09:20:38Z",
      "client_provider_settings" => "test",
      "custom"                   => "foo",
      "nil"                      => nil,
      75                         => "integer_key",
      "consent"                  => {
        "consent_expires_at" => "2020-04-12 08:03:19 UTC",
        "consent_given_at"   => "2020-01-12 08:03:19 UTC"
      }
    }
  end

  describe "#process!" do
    it "mutates given hash to match rules" do
      mapper.process!(params)
      expect(params).to eq(
        "id"                       => "123",
        "from_date"                => "2020-02-16",
        "to_date"                  => "2020-04-16",
        "client_provider_settings" => {},
        "custom"                   => "foo bar",
        "nil"                      => nil,
        75                         => "integer_key",
        "_converted_fields"        => {
          "id"                       => "original: 123",
          "from_date"                => "original: 2020-02-16T09:20:14Z",
          "to_date"                  => "original: 2020-04-16T09:20:38Z",
          "client_provider_settings" => "original: test",
          "custom"                   => "original: foo"
        },
        "consent"                  => {
          "consent_expires_at" => "2020-04-12T08:03:19Z",
          "consent_given_at"   => "2020-01-12T08:03:19Z",
          "_converted_fields"  => {
            "consent_expires_at" => "original: 2020-04-12 08:03:19 UTC",
            "consent_given_at"   => "original: 2020-01-12 08:03:19 UTC"
          }
        },
        "nested"                   => {
          "other_id"          => "456",
          "_converted_fields" => { "other_id" => "original: 456" }
        }
      )
    end
  end
end
