require 'spec_helper'

describe SaltCommon::Storage::Item do
  let(:params) {
    {
      id:   "123",
      name: "name"
    }
  }

  let(:storage_item) { Storage::Test::Item.new(params[:id]) }
  let(:storage)      { Storage::Test::Collection.new }

  describe "Object methods" do
    describe "#attributes" do
      it "compares items by their primary key" do
        storage_item.attributes.should           == [:id, :name, :credentials, :interactive_credentials, :internal_data, :beta]
        storage_item.whitelist_attributes.should == [:id, :name, :internal_data, :beta]
      end
    end

    describe "#==" do
      it "compares items by their primary key" do
        first_item  = Storage::Test::Item.new(1)
        second_item = Storage::Test::Item.new(1)
        third_item  = Storage::Test::Item.new(2)

        (first_item == second_item).should be_truthy
        (first_item == third_item).should  be_falsey
      end
      it "returns false when is compared with obejct of different class" do
        first_item  = Storage::Test::Item.new(1)
        second_item = "String"

        (first_item == second_item).should be_falsey
      end
    end

    describe "#read" do
      it "reads item from storage" do
        storage.add(params)
        item = storage_item.read
        item.id.should   == params[:id]
        item.name.should == params[:name]
      end

      it "reads item from given string" do
        item = storage_item.read(SaltCommon::JsonWrapper.encode(params))
        item.id.should   == params[:id]
        item.name.should == params[:name]
      end
    end

    describe "#update" do
      it "reassigns attributes from given hash" do
        storage.add(params)
        item = storage_item.read
        item.update(name: "RSPEC")
        item.name.should == "RSPEC"
      end
    end

    describe "#save" do
      it "saves to storage updated item" do
        storage.add(params)
        storage_item.update(name: "RSPEC")
        storage_item.save
        storage.get(params[:id]).name.should == "RSPEC"
      end
    end

    describe "#insecure_to_h" do
      it "return a hash from item values" do
        storage.add(params)
        item = storage_item.read
        item.insecure_to_h.should == {
          "id"=>"123",
          "name"=>"name",
          "credentials"=>nil,
          "interactive_credentials"=>nil,
          "internal_data"=>nil,
          "beta"=>nil
        }
      end
    end

    describe "#secure_to_h" do
      it "selects none if no whitelisted keys given" do
        storage.add(params)
        stub_const("Storage::Test::Item::ATTRIBUTES", {id: false, beta: false})
        item = storage_item.read
        expect { item.secure_to_h }.to raise_error("WHITELIST_ATTRIBUTES shouldn't be empty!")
      end

      it "selects default whitelisted keys" do
        storage.add(params)
        item = storage_item.read
        item.secure_to_h.should == {
          "id"=>"123",
          "name"=>"name",
          "internal_data"=>nil,
          "beta"=>nil
        }
      end
    end

    describe "#remove" do
      it "removes item from storage" do
        storage.add(params)
        item = storage_item.read
        item.remove
        expect { storage_item.read }.to raise_error(NotImplementedError)
      end
    end

    describe "#namespace" do
      it "returns the namespace of parent class" do
        storage_item.namespace.should == "test"
      end
    end
  end

  context "Private methods" do
    describe "#handle_error" do
      it "raises NotImplementedError" do
        expect { storage_item.send(:handle_error) }.to raise_error(NotImplementedError)
      end
    end

    describe "#attribute_value" do
      it "retrieves value by symbol or string name" do
        storage_item.send(:attribute_value, :name, {name: true}).should be_truthy
        storage_item.send(:attribute_value, :name, {"name" => true}).should be_truthy
        storage_item.send(:attribute_value, :name, {}).should be_nil
      end
    end
  end
end
