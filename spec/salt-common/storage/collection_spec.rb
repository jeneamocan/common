require 'spec_helper'

describe SaltCommon::Storage::Collection do
  let(:params) {
    {
      id:   "123",
      name: "name"
    }
  }

  let(:storage_item) { Storage::Test::Item.new(params[:id]) }
  let(:storage)      { Storage::Test::Collection.new }

  describe "Object methods" do
    describe "#redis" do
      it "returns current redis every time" do
        Redis.should_receive(:current).twice
        storage.redis
        storage.redis
      end
    end

    describe "#add/#get/#count" do
      it "adds item to storage" do
        expect { storage.add(params) }.to change { storage.count }.by(1)
        item = storage.get(params[:id])
        item.id.should   == params[:id]
        item.name.should == params[:name]
      end
    end

    describe "#exists?" do
      it "returns true if item exists in collection" do
        expect { storage.add(params) }.to change { storage.exists?(params[:id]) }.from(false).to(true)
      end
    end

    describe "#remove" do
      it "returns true if item exists in collection" do
        storage.add(params)
        expect { storage.remove(params[:id]) }.to change { storage.exists?(params[:id]) }.from(true).to(false)
      end
    end

    describe "#insecure_to_hash" do
      it "returns the collection as array of insecure hashes" do
        storage.add(params)
        storage.insecure_to_hash.should == [
          {
            "id"=>"123",
            "name"=>"name",
            "credentials"=>nil,
            "interactive_credentials"=>nil,
            "internal_data"=>nil,
            "beta"=>nil
          }
        ]
      end
    end

    describe "#secure_to_hash" do
      it "returns the empty collection no whitelisted attributes given" do
        storage.add(params)
        stub_const("Storage::Test::Item::ATTRIBUTES", {id: false, beta: false})
        expect { storage.secure_to_hash }.to raise_error("WHITELIST_ATTRIBUTES shouldn't be empty!")
      end
      it "uses whitelisted attributes" do
        storage.add(params)
        storage.secure_to_hash.should == [
          {
            "id"=>"123",
            "name"=>"name",
            "internal_data"=>nil,
            "beta"=>nil
          }
        ]
      end
    end

    describe "#all" do
      it "returns all items of this collection" do
        storage.add(params)
        items = storage.all
        first = items.first

        items.count.should      == 1
        first.attributes.should == [:id, :name, :credentials, :interactive_credentials, :internal_data, :beta]
        first.id.should         == params[:id]
        first.name.should       == params[:name]
      end
    end

    describe "#members" do
      it "caches the collection" do
        subject.redis.should_receive(:hgetall).once.and_return([])
        storage.members
        storage.members
      end
    end

    describe "#namespace" do
      it "returns parent's namespace" do
        storage.namespace.should == "test"
      end
    end
  end
end
