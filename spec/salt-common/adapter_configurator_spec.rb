require 'spec_helper'

describe SaltCommon::AdapterConfigurator do
  before(:each) do
    stub_const("Settings", double(
        proxies:
          {
            "random" => {
              "random1" => { "address" => "random1", "mode" => "socks" },
              "random2" => { "address" => "random2", "mode" => "socks" },
              "random4" => { "address" => "random4"},
              "random10" => { "address" => "random10", "mode" => "socks" }
            },
            "custom" => {
              "custom1" => { "address" => "custom1" },
              "custom2" => { "address" => "custom2" },
              "custom12" => { "address" => "custom12" }
            },
            "ru" => {}
          },
        beta_private_key: "beta",
        private_key:      "not beta"
      )
    )

    class BaseAdapterClass
      extend SaltCommon::AdapterConfigurator
      config do |c|
        c.inherited_option = "42"
      end
    end
  end

  let(:base) do
    BaseAdapterClass
  end

  let(:adapter) do
    Class.new(base)
  end

  let(:another_adapter) do
    Class.new(adapter)
  end

  it "returns attr_reader if it is set" do
    adapter.instance_variable_set("@configuration", "salt")
    adapter.config.should == "salt"
  end

  context "base" do
    it "raises error if configuration key is missing" do
      expect { adapter.config { |c| nil } }.to raise_error(SaltCommon::AdapterConfigurator::MissingRequiredKeyError)
    end

    it "raises does not raise error if configuration key is missing in Base" do
      expect { BaseAdapterClass.config { |c| nil } }.not_to raise_error
    end
  end

  context "when a valid configuration is specified" do
    before do
      adapter.config do |c|
        c.provider_code         = "fake_csv_parser_xf"
        c.provider_name         = "Fake CSV Parser"
        c.max_interactive_delay = 1
        c.full_timeout          = 480
        c.provider_mode         = "mode"
        c.automatic_fetch       = false
        c.interactive           = true
        c.recent_timeout        = 480
        c.country_code          = "XF"
        c.home_url              = "http://test.com"
        c.login_url             = "http://test.com"
        c.instruction           = <<-text
          Valid credentials for this provider are:
          login - "abc",
          password - "123"
        text
        c.client_provider_fields do |pf|
          pf.text     "app_id",     checksummable: false, english_name: "App ID",     localized_name: "App ID"
          pf.password "app_secret", checksummable: false, english_name: "App Secret", localized_name: "App Secret"
        end
        c.client_provider_settings do |ps|
          ps.select "environment", english_name: "Environment", localized_name: "Environment", checksummable: true do |select|
            select.option "sandbox", option_value: "sandbox", english_name: "Sandbox", localized_name: "Sandbox"
          end
        end
        c.required_fields do |rf|
          rf.select "image",      english_name: "Image",    localized_name: "Image", extra: { placeholder: "123" }, checksummable: false do |select|
            select.option "name", english_name: "One",      localized_name: "One",   option_value: 123
          end
          rf.select "secret_answer",   english_name: "Song", localized_name: "Song", extra: { placeholder: "antoshka" }, checksummable: false, optional: true do |select|
            select.option "antoshka",  english_name: "One",  localized_name: "One",  option_value: 1
            select.option "kartoshka", english_name: "Two",  localized_name: "Two",  option_value: 2, selected: true
          end
          rf.hidden "access_token", english_name: "Access Token", localized_name: "Access Token", checksummable: false
        end
        c.interactive_fields do |rf|
          rf.dynamic_select "account", english_name: "Account", localized_name: "Account", extra: { placeholder: "[qw-er-rt-yu]" }, checksummable: false
        end
        c.salt = "salt" # Custom method
      end
    end

    describe "#to_hash" do
      it "serializes configuration" do
        adapter.config.to_hash.except(:required_fields, :interactive_fields, :client_provider_fields, :client_provider_settings).should == {
          max_interactive_delay:   1,
          recent_timeout:          480,
          automatic_fetch:         false,
          interactive:             true,
          record_strategy:         "none",
          full_timeout:            480,
          holder_info:             [],
          deprecated_proxies:      [],
          provider_mode:           "mode",
          provider_code:           "fake_csv_parser_xf",
          provider_name:           "Fake CSV Parser",
          country_code:            "XF",
          login_url:               "http://test.com",
          home_url:                "http://test.com",
          proxy_code:              :random,
          inherited_option:        "42",
          instruction:             "          Valid credentials for this provider are:\n          login - \"abc\",\n          password - \"123\"\n",
          salt:                    "salt",
          supported_account_types: [],
        }
        adapter.config.to_hash[:required_fields].should be
        adapter.config.to_hash[:client_provider_fields].should eq([
          {
            nature:         :text,
            name:           "app_id",
            position:       1,
            optional:       false,
            checksummable:  false,
            english_name:   "App ID",
            localized_name: "App ID",
            extra:          {}
          },
          {
            nature:         :password,
            name:           "app_secret",
            position:       2,
            optional:       false,
            checksummable:  false,
            english_name:   "App Secret",
            localized_name: "App Secret",
            extra:          {}
          }
        ])
      end
    end

    describe "#validate_urls!" do
      it "validates login url" do
        expect {
          another_adapter.config { |c| c.login_url = "invalid" }
        }.to raise_error(SaltCommon::AdapterConfigurator::InvalidUrl, "Invalid login_url:'invalid'")
      end

      it "validates home url" do
        expect {
          another_adapter.config { |c| c.home_url = "http://google.com" }
        }.to_not raise_error
      end

      it "validates login url length" do
        expect {
          another_adapter.config do |c|
            url = "https://gitlab.com/service/global/issues"
            400.times { url += "1234567890" }
            c.login_url = url
          end
        }.to raise_error(SaltCommon::AdapterConfigurator::InvalidUrl, "Url length is more than 4000 symbols")
      end

      it "validates home and login urls" do
        expect {
          another_adapter.config do |c|
            c.home_url  = "invalid"
            c.login_url = "http://google.com"
          end
        }.to raise_error(SaltCommon::AdapterConfigurator::InvalidUrl, "Invalid home_url:'invalid'")
      end

      it "validates home and login urls" do
        expect {
          another_adapter.config do |c|
            c.home_url  = "http://google.com"
            c.login_url = "http://google.com"
          end
        }.to_not raise_error
      end

      it "validates empty urls" do
        expect {
          another_adapter.config do |c|
            c.login_url = ""
            c.home_url  = ""
          end
        }.to raise_error(RuntimeError, "fake_csv_parser_xf has empty login_url, home_url")
      end
    end

    describe "#validate_timeouts!" do
      it "validates timeouts" do
        expect {
          another_adapter.config do |c|
            c.automatic_fetch = false
            c.recent_timeout  = 479
            c.full_timeout    = 479
          end
        }.to raise_error(SaltCommon::AdapterConfigurator::InvalidTimeout, "Timeouts are to small for interactive login")
      end

      it "validates timeouts" do
        expect {
          another_adapter.config do |c|
            c.automatic_fetch = false
            c.recent_timeout  = 481
            c.full_timeout    = 481
          end
        }.to_not raise_error
      end
    end

    describe "#validate_account_types!" do
      it "validates supported account types" do
        expect {
          another_adapter.config do |c|
            c.supported_account_types = ["personal"]
          end
        }.not_to raise_error
      end

      it "fails to validate wrong account type" do
        expect {
          another_adapter.config do |c|
            c.supported_account_types = %w(personal shmersonal)
          end
        }.to raise_error(RuntimeError, "shmersonal is not valid value for supported_account_types")
      end
    end

    describe "#validate_interactivity!" do
      it "validates automatic fetch type" do
        expect {
          another_adapter.config do |c|
            c.automatic_fetch = true
            c.interactive_fields do |rf|
              rf.password "okey", english_name: "O-Key", localized_name: "O-Key", checksummable: false
            end
          end
        }.to raise_error(RuntimeError, "fake_csv_parser_xf is not automatic")
      end

      it "validates automatic fetch type in case of optional_interactivity" do
        expect {
          another_adapter.config do |c|
            c.interactive_fields {}
            c.interactive            = false
            c.optional_interactivity = true
            c.automatic_fetch        = true
          end
        }.to raise_error(RuntimeError, "fake_csv_parser_xf has optional_interactivity set to true and automatic_fetch is true")
      end

      it "validates automatic fetch type in case when interactive is truthy" do
        expect {
          another_adapter.config do |c|
            c.interactive_fields {}
            c.optional_interactivity = false
            c.automatic_fetch        = true
            c.interactive            = true
          end
        }.to raise_error(RuntimeError, "fake_csv_parser_xf has interactive set to true and automatic_fetch is true")
      end
    end

    describe "#validate_record_strategy!" do
      it "validates record_strategy type" do
        expect {
          another_adapter.config do |c|
            c.record_strategy = "custom"
          end
        }.to raise_error(RuntimeError, "'custom' is an invalid :record_strategy type")
      end
    end

    describe "#validate_provider_code!" do
      it "validates provider_code" do
        expect {
          another_adapter.config do |c|
            c.provider_code = "invalid_Provider"
          end
        }.to raise_error(RuntimeError, "provider_code consists of invalid characters")
      end

      it "validates provider_code" do
        expect {
          another_adapter.config do |c|
            c.provider_code = "invalid_provider_hh_xf"
          end
        }.to raise_error(RuntimeError, "provider code is invalid")
      end
    end

    describe "#validate_country_code!" do
      it "validates country_code" do
        expect {
          another_adapter.config do |c|
            c.country_code = "IH"
          end
        }.to raise_error(RuntimeError, "IH country does not exist")
      end

      it "validates XF country_code" do
        expect {
          another_adapter.config do |c|
            c.country_code = "XF"
          end
        }.not_to raise_error
      end

      it "validates XO country_code" do
        expect {
          another_adapter.config do |c|
            c.provider_code = "valid_provider_xo"
            c.country_code  = "XO"
          end
        }.not_to raise_error
      end

      it "validates country_code" do
        expect {
          another_adapter.config do |c|
            c.country_code = "us"
          end
        }.to raise_error(RuntimeError, "country_code consists of invalid characters")
      end

      it "validates empty country_code" do
        expect {
          another_adapter.config do |c|
            c.country_code = ""
            c.home_url  = "http://google.com"
            c.login_url = "http://google.com"
          end
        }.to raise_error(RuntimeError, "fake_csv_parser_xf has empty country_code")
      end
    end

    describe "#validate_max_allowed_automatic_daily_refreshes!" do
      it "validates validate_max_allowed_automatic_daily_refreshes" do
        expect {
          another_adapter.config do |c|
            c.max_allowed_automatic_daily_refreshes = 5
          end
        }.to raise_error(RuntimeError, "max_allowed_automatic_daily_refreshes should be between 1-4")
      end

      it "validates validate_max_allowed_automatic_daily_refreshes" do
        expect {
          another_adapter.config do |c|
            c.max_allowed_automatic_daily_refreshes = "4"
          end
        }.to raise_error(RuntimeError, "Max allowed automatic daily refreshes should be an integer")
      end

      it "validates validate_max_allowed_automatic_daily_refreshes" do
        expect {
          another_adapter.config do |c|
            c.max_allowed_automatic_daily_refreshes = 2
          end
        }.not_to raise_error
      end
    end

    describe "#validate_extra_lists!" do
      before(:each) do
        SaltCommon::Transaction.add_extra_field(:constant_code, String) do
          if extra[:constant_code].match?(/\D/)
            errors.add(:extra, "extra[constant_code] should contain only digits")
          end
          if extra[:constant_code].blank?
            errors.add(:extra, "extra[constant_code] should not be empty")
          end
        end

        SaltCommon::Account.add_extra_field(:minimum_payment, String) do
          if extra[:minimum_payment].match?(/\D/)
            errors.add(:extra, "extra[minimum_payment] should contain only digits")
          end
          if extra[:minimum_payment].blank?
            errors.add(:extra, "extra[minimum_payment] should not be empty")
          end
        end
      end

      after(:each) do
        SaltCommon::Transaction.instance_variable_set("@custom_extra_fields", {})
        SaltCommon::Account.instance_variable_set("@custom_extra_fields", {})
      end

      it "validates extra and passes" do
        expect {
          another_adapter.config do |c|
            c.supported_account_natures          = [SaltCommon::Account::SAVINGS, SaltCommon::Account::LOAN]
            c.supported_account_extra_fields     = ["account_name", "available_amount", "interest_rate", "open_date", "minimum_payment"]
            c.supported_transaction_extra_fields = ["posting_date", "constant_code"]
          end
        }.not_to raise_error
      end

      it "fails if any supported_account_natures include invalid nature" do
        expect {
          another_adapter.config do |c|
            c.supported_account_natures          = ["star_kebab"]
            c.supported_account_extra_fields     = ["available_amount"]
            c.supported_transaction_extra_fields = ["posting_date"]
          end
        }.to raise_error(RuntimeError, "star_kebab is not a valid account nature")
      end

      it "fails if any supported_account_extra_fields include invalid extra field" do
        expect {
          another_adapter.config do |c|
            c.supported_account_natures          = [SaltCommon::Account::SAVINGS]
            c.supported_account_extra_fields     = ["banana_bob"]
            c.supported_transaction_extra_fields = ["posting_date"]
          end
        }.to raise_error(RuntimeError, "banana_bob is not a valid account extra field")
      end

      it "fails if any supported_transaction_extra_fields include invalid extra field" do
        expect {
          another_adapter.config do |c|
            c.supported_account_natures          = [SaltCommon::Account::SAVINGS]
            c.supported_account_extra_fields     = ["account_name", "available_amount"]
            c.supported_transaction_extra_fields = ["chicken_stew"]
          end
        }.to raise_error(RuntimeError, "chicken_stew is not a valid transaction extra field")
      end

      it "fails to pass when card_type account extra is present and no card natures are included in supported_account_natures" do
        expect {
          another_adapter.config do |c|
            c.supported_account_natures      = [SaltCommon::Account::SAVINGS]
            c.supported_account_extra_fields = ["account_name", "available_amount", "cards", "card_type"]
          end
        }.to raise_error(RuntimeError, "account.extra card_type is present but supported_account_natures do not include any card natures")
      end
    end

    describe "#validate_holder_info!" do
      it "validates holder info config consistency" do
        expect {
          another_adapter.config do |c|
            c.holder_info            = %w(names emails phone_numbers)
            c.supported_fetch_scopes = ["accounts", "transactions", "holder_info"]
          end
        }.not_to raise_error
      end

      it "validates holder info config consistency with nil values" do
        expect {
          another_adapter.config
        }.not_to raise_error
      end

      it "raises error if holder_info keys are present but missing holder_info key in supported_fetch_scopes" do
        expect {
          another_adapter.config do |c|
            c.holder_info = %w(names emails phone_numbers)
          end
        }.to raise_error(RuntimeError, "c.holder_info is not blank in fake_csv_parser_xf. Add holder_info to c.supported_fetch_scopes")
      end

      it "raises error if holder_info key is present in supported_fetch_scopes but there are no holder_info keys in config" do
        expect {
          another_adapter.config do |c|
            c.supported_fetch_scopes = ["accounts", "transactions", "holder_info"]
          end
        }.to raise_error(RuntimeError, "c.holder_info is blank in fake_csv_parser_xf. Remove holder_info from c.supported_fetch_scopes")
      end
    end

    describe "#valdiate_supported_fetch_scopes!" do
      it "validates supported fetch scopes" do
        expect {
          another_adapter.config do |c|
            c.supported_fetch_scopes = ["accounts"]
          end
        }.not_to raise_error
      end

      it "validates accounts_without_balance fetch scope" do
        expect {
          another_adapter.config do |c|
            c.supported_fetch_scopes = ["accounts_without_balance"]
          end
        }.not_to raise_error
      end

      it "validates supported fetch scopes" do
        expect {
          another_adapter.config do |c|
            c.supported_fetch_scopes = %w[arsenal transactions]
          end
        }.to raise_error(SaltCommon::AdapterConfigurator::InvalidSupportedFetchScope, 'Invalid supported fetch scope: ["arsenal"]')
      end
    end

    describe "#validate_sandbox_config!" do
      it "validates sandbox config consistency" do
        expect {
          another_adapter.config do |c|
            c.identification_codes = []
            c.bic_codes            = []
          end
        }.not_to raise_error
      end

      it "doesn't raise error if provider is not Sandbox" do
        expect {
          another_adapter.config do |c|
            c.country_code         = "MD"
            c.provider_code        = "fake_csv_parser_md"
            c.identification_codes = [12345]
            c.bic_codes            = ["GENODEF1WOG"]
          end
        }.not_to raise_error
      end

      it "validates sandbox config consistency with nil values" do
        expect {
          another_adapter.config
        }.not_to raise_error
      end

      it "raises error if identification_codes values are present in sandbox" do
        expect {
          another_adapter.config do |c|
            c.identification_codes = ["1234"]
          end
        }.to raise_error(RuntimeError, "Remove c.identification_codes from fake_csv_parser_xf")
      end

      it "raises error if bic_codes values are present in sandbox" do
        expect {
          another_adapter.config do |c|
            c.bic_codes = ["GENODEF1WOG"]
          end
        }.to raise_error(RuntimeError, "Remove c.bic_codes from fake_csv_parser_xf")
      end
    end

    describe "#validate_credentials_fields!" do
      it "validates client_provider_fields, interactive_fields and required_fields consistency" do
        expect {
          another_adapter.config
        }.not_to raise_error
      end

      it "fails if client_provider_fields has nil fields" do
        expect {
          another_adapter.config do |c|
            c.client_provider_fields do |pf|
              pf.text "app_id", english_name: nil, localized_name: nil, checksummable: false
            end
          end
        }.to raise_error(RuntimeError, "fake_csv_parser_xf has an invalid 'app_id' field in c.client_provider_fields with blank english_name, localized_name")
      end

      it "fails if interactive_fields has nil fields" do
        expect {
          another_adapter.config do |c|
            c.interactive_fields do |rf|
              rf.dynamic_select "account", english_name: "Account", localized_name: "Account", checksummable: nil
            end
          end
        }.to raise_error(RuntimeError, "fake_csv_parser_xf has an invalid 'account' field in c.interactive_fields with blank checksummable")
      end

      it "fails if required_fields has nil fields" do
        expect {
          another_adapter.config do |c|
            c.required_fields do |rf|
              rf.hidden "access_token", english_name: nil, localized_name: "Access Token", checksummable: false
            end
          end
        }.to raise_error(RuntimeError, "fake_csv_parser_xf has an invalid 'access_token' field in c.required_fields with blank english_name")
      end

      it "fails if field options have nil fields" do
        expect {
          another_adapter.config do |c|
            c.required_fields do |rf|
              rf.select "secret_answer",   english_name: "Song", localized_name: "Song", extra: { placeholder: "antoshka" }, checksummable: false, optional: true do |select|
                select.option "antoshka",  english_name: nil,  localized_name: "One",  option_value: 1
                select.option "kartoshka", english_name: "Two",  localized_name: "Two",  option_value: 2, selected: true
              end
            end
          end
        }.to raise_error(RuntimeError, "fake_csv_parser_xf has an invalid 'antoshka' field_option in c.required_fields with blank english_name")
      end

      it "fails if field nature is wrongly set" do
        expect {
          another_adapter.config do |c|
            c.required_fields do |rf|
              rf.text "login",    english_name: "Login",    localized_name: "Логин",  checksummable: true
              rf.text "password", english_name: "Password", localized_name: "Пароль", checksummable: false
            end
          end
        }.to raise_error(RuntimeError, "In fake_csv_parser_xf update 'password' field type from rf.text to rf.password")

        expect {
          another_adapter.config do |c|
            c.required_fields do |rf|
              rf.text "login", english_name: "Login", localized_name: "Логин",  checksummable: true
              rf.text "phone", english_name: "Phone", localized_name: "Phone", checksummable: false
            end
          end
        }.to raise_error(RuntimeError, "In fake_csv_parser_xf update 'phone' field type from rf.text to rf.number")
      end
    end

    context "#generate_proxy_code" do
      it "picks 'random' sample key based on ':random' proxy group" do
        adapter.configuration.proxy_code = :random
        expect(adapter.generate_proxy_code).to be_in(Settings.proxies["random"].keys)
      end

      it "picks exact proxy key based on provided key" do
        adapter.configuration.proxy_code = :random10
        expect(adapter.generate_proxy_code).to eq("random10")
      end

      it "if key is invalid, picks recursively a key from same group, or falls back on ':random' if none valid found" do
        adapter.configuration.proxy_code = :random4
        expect(adapter.generate_proxy_code).to be_in(
          Settings.proxies["random"].keys.reject { |k| k == :random4 }
        )
      end

      it "picks ':random' key if provided one does not exists" do
        adapter.configuration.proxy_code = :ru2
        expect(adapter.generate_proxy_code).to be_in(Settings.proxies["random"].keys)
      end

      it "picks ':random' key if provided group is empty" do
        adapter.configuration.proxy_code = :ru
        expect(adapter.generate_proxy_code).to be_in(Settings.proxies["random"].keys)
      end

      it "picks ':random' key if provided group has no valid keys" do
        adapter.configuration.proxy_code = :custom
        expect(adapter.generate_proxy_code).to be_in(Settings.proxies["random"].keys)
      end

      it "picks ':random' key if provided one is not valid" do
        adapter.configuration.proxy_code = :custom2
        expect(adapter.generate_proxy_code).to be_in(Settings.proxies["random"].keys)
      end

      it "picks proxy from same group but considering deprecated proxies" do
        adapter.configuration.deprecated_proxies = ["random1", "random10", "random5"]
        adapter.configuration.proxy_code         = :random1
        proxy = adapter.generate_proxy_code

        proxy.should be_in(Settings.proxies["random"].keys)
        proxy.should_not be_in(adapter.configuration.deprecated_proxies)
      end
    end

    context "fields required flags" do
      it "if required fields are valid" do
        expect do
          another_adapter.config do |c|
            c.required_fields { |rf| rf.password "token", english_name: "Token", localized_name: "Token", checksummable: false }
          end
        end.not_to raise_error

        expect do
          another_adapter.config do |c|
            c.required_fields do |rf|
              rf.wrong_nature_field "access_token",
                english_name:   "Access Token",
                localized_name: "Access Token",
                checksummable:  false
            end
          end
        end.to raise_error(NoMethodError)

        expect do
          another_adapter.config do |c|
            c.required_fields { |rf| rf.text "txt", english_name: "A", localized_name: "A" }
          end
        end.to raise_error(KeyError, "key not found: :checksummable")

        expect do
          another_adapter.config do |c|
            c.required_fields { |rf| rf.text "txt", english_name: "A", checksummable: true }
          end
        end.to raise_error(KeyError, "key not found: :localized_name")

        expect do
          another_adapter.config do |c|
            c.required_fields { |rf| rf.text "txt", checksummable: true, localized_name: "A" }
          end
        end.to raise_error(KeyError, "key not found: :english_name")
      end
    end

    context "defaults" do
      it("config.provider_code")         { adapter.config.provider_code.should         == "fake_csv_parser_xf" }
      it("config.provider_name")         { adapter.config.provider_name.should         == "Fake CSV Parser" }
      it("config.country_code")          { adapter.config.country_code.should          == "XF" }
      it("config.provider_mode")         { adapter.config.provider_mode.should         == "mode" }
      it("config.max_interactive_delay") { adapter.config.max_interactive_delay.should == 1 }
      it("config.recent_timeout")        { adapter.config.recent_timeout.should        == 480 }
      it("config.full_timeout")          { adapter.config.full_timeout.should          == 480 }
      it("config.automatic_fetch")       { adapter.config.automatic_fetch.should be_falsey }
      it("config.interactive")           { adapter.config.interactive.should be_truthy }
      it("config.timezone")              { adapter.config.timezone.should              == "UTC" }
      it("config.proxy_code")            { adapter.config.proxy_code.should            == :random }
      it("config.instruction") do
        adapter.config.instruction.should == "Valid credentials for this provider are:\nlogin - \"abc\",\npassword - \"123\"\n"
      end

      it("config.required_fields") do
        adapter.config.required_fields.should == [
          {
            nature:         :select,
            name:           "image",
            checksummable:  false,
            english_name:   "Image",
            localized_name: "Image",
            optional:       false,
            extra:          { placeholder: "123" },
            position:       1,
            field_options:  [
              {
                name:           "name",
                english_name:   "One",
                localized_name: "One",
                option_value:   123,
                selected:       false,
                extra:          {},
                position:       1
              }
            ]
          },
          {
            nature:         :select,
            name:           "secret_answer",
            checksummable:  false,
            english_name:   "Song",
            localized_name: "Song",
            optional:       true,
            extra:          { placeholder: "antoshka" },
            position:       2,
            field_options:  [
              {
                name:           "antoshka",
                english_name:   "One",
                localized_name: "One",
                selected:       false,
                option_value:   1,
                extra:          {},
                position:       1
              },
              {
                name:           "kartoshka",
                english_name:   "Two",
                localized_name: "Two",
                selected:       true,
                option_value:   2,
                extra:          {},
                position:       2
              }
            ]
          },
          {
            nature:             :hidden,
            name:               "access_token",
            position:           3,
            optional:           false,
            extra:              {},
            english_name:       "Access Token",
            localized_name:     "Access Token",
            checksummable:      false
          }
        ]
      end

      it("config.interactive_fields") do
        adapter.config.interactive_fields.should == [
          {
            nature:         :dynamic_select,
            name:           "account",
            checksummable:  false,
            english_name:   "Account",
            localized_name: "Account",
            optional:       false,
            extra:          { placeholder: "[qw-er-rt-yu]" },
            position:       1
          }
        ]
      end

      it("config.client_provider_settings") do
        adapter.config.client_provider_settings.should == [
          {
            nature:         :select,
            name:           "environment",
            checksummable:  true,
            english_name:   "Environment",
            localized_name: "Environment",
            optional:       false,
            extra:          {},
            position:       1,
            field_options:  [
              {
                name:           "sandbox",
                english_name:   "Sandbox",
                localized_name: "Sandbox",
                selected:       false,
                option_value:   "sandbox",
                extra:          {},
                position:       1
              }
            ]
          }
        ]
      end
    end

    context "with custom method" do
      it "inherits some options from Base" do
        adapter.config.inherited_option.should == "42"
        adapter.config.inherited_option.should == base.config.inherited_option
      end

      it "allows custom fields to be defined and accessed" do
        adapter.config.salt.should == "salt"
      end

      it "not includes custom fields in to_h method" do
        adapter.config.to_h[:salt].should be_nil
      end

      it "includes custom fields in to_hash method" do
        adapter.config.to_hash[:salt].should == "salt"
      end
    end

    context "with specific timezone for a country_code" do
      it "returns timezone depending on country_code" do
        adapter.config.country_code     = "MD"
        adapter.config.timezone.should == "Europe/Chisinau"

        adapter.config.country_code     = "XO"
        adapter.config.timezone.should == "UTC"

        adapter.config.country_code     = nil
        adapter.config.timezone.should == "UTC"
      end

      it "returns correct timezone for sandboxes" do
        adapter.config.country_code     = "XF"
        adapter.config.provider_code    = "demobank_md_xf"
        adapter.config.timezone.should == "Europe/Chisinau"

        adapter.config.provider_code    = "demobank_xf"
        adapter.config.timezone.should == "UTC"
      end
    end

    describe "#validate_custom_pending_settings!" do
      it "does not raise when both values are of expected class" do
        expect {
          another_adapter.config do |c|
            c.custom_pendings        = true
            c.custom_pendings_period = 2
          end
        }.not_to raise_error
      end

      it "does not raise when custom_pendings is false and custom_pendings_period is 0" do
        expect {
          another_adapter.config do |c|
            c.custom_pendings        = false
            c.custom_pendings_period = 0
          end
        }.not_to raise_error
      end

      it "fails when period is missing while custom pendings setting is present" do
        expect {
          another_adapter.config do |c|
            c.custom_pendings = true
          end
        }.to raise_error(RuntimeError, "custom_pendings is set while period is not marked")
      end

      it "fails when custom_pendings_period is something other than Int" do
        expect {
          another_adapter.config do |c|
            c.custom_pendings        = true
            c.custom_pendings_period = "2"
          end
        }.to raise_error(RuntimeError, "Expected custom_pendings_period to be Integer got String")
      end

      it "fails when custom_pendings is something other than TrueClass of FalseClass" do
        expect {
          another_adapter.config do |c|
            c.custom_pendings        = 2
            c.custom_pendings_period = 2
          end
        }.to raise_error(RuntimeError, "Expected custom_pendings to be TrueClass or FalseClass got Integer")
      end

      it "fails when period is marked while custom pendings setting is missing" do
        expect {
          another_adapter.config do |c|
            c.custom_pendings_period = 2
          end
        }.to raise_error(RuntimeError, "custom_pendings_period is set to 2 while custom_pendings is missing")
      end

      it "fails when period is marked while custom pendings setting is set to false" do
        expect {
          another_adapter.config do |c|
            c.custom_pendings        = false
            c.custom_pendings_period = 2
          end
        }.to raise_error(RuntimeError, "custom_pendings set to false but custom_pendings_period is 2")
      end
    end
  end
end
