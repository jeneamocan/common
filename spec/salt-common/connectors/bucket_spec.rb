require 'spec_helper'

describe SaltCommon::Connectors::Bucket do
  subject { SaltCommon::Connectors::Bucket }

  let(:rest_api_instance) { double }
  let(:url)               { "http://example.com" }
  let(:params) {
    {
      "finished"    => false,
      "credentials" => {
        "login"    => "123123",
        "password" => "asdqwe"
      },
      "encrypted_rememberable_credentials" => {
        algorithm: "AES-256-CBC",
        key:       "qwerty"
      }
    }
  }

  before (:each) do
    stub_const("Settings", double(
        service_name: "robber",
        bucket:       double(base_url: "saltedge.com", robber_to_bucket_secret: "secret")
      )
    )
    Settings.stub(:get).with("bucket.robber_to_bucket_secret").and_return(Settings.bucket.robber_to_bucket_secret)
  end

  context "Public methods" do
    describe "#checksums" do
      it "send a request to checksums url in bucket" do
        rest_api_instance.should_receive(:execute_with_retry).with(:get)
        SaltCommon::RestApi.should_receive(:new)
          .with(
            Settings.bucket.base_url + "/api/service/v1/service/checksums",
            {
              params:  SaltCommon::Credentials.filter!(params),
              headers: subject.send(:headers),
              retries: 1,
              read_timeout: 15,
              open_timeout: 15
            }
          ).and_return(rest_api_instance)

        subject.checksums(params)
      end
    end

    describe "#bucket_accounts" do
      it "send a request to accounts url in bucket" do
        rest_api_instance.should_receive(:execute_with_retry).with(:get)
        SaltCommon::RestApi.should_receive(:new)
          .with(
            Settings.bucket.base_url + "/api/service/v1/service/accounts",
            {
              params:  SaltCommon::Credentials.filter!(params),
              headers: subject.send(:headers),
              retries: 1,
              read_timeout: 15,
              open_timeout: 15
            }
          ).and_return(rest_api_instance)

        subject.bucket_accounts(params)
      end
    end

    describe "#fail" do
      describe "request from robber" do
        it "send a request to fail_url in bucket" do
          rest_api_instance.should_receive(:execute_with_retry).with(:post)
          SaltCommon::RestApi.should_receive(:new)
            .with(
              Settings.bucket.base_url + "/api/service/v1/service/fail",
              {
                params:  SaltCommon::Credentials.filter!(params, [:encrypted_rememberable_credentials]),
                headers: subject.send(:headers),
                retries: 1,
                read_timeout: 15,
                open_timeout: 15
              }
            ).and_return(rest_api_instance)

          subject.fail(params)
        end
      end

      describe "request from stealer" do
        before do
          stub_const("Settings", double(
              service_name: "stealer",
              bucket:       double(base_url: "saltedge.com", stealer_to_bucket_secret: "secret")
            )
          )
          Settings.stub(:get).with("bucket.stealer_to_bucket_secret").and_return(Settings.bucket.stealer_to_bucket_secret)
        end

        it "send a request to fail_url in bucket" do
          rest_api_instance.should_receive(:execute_with_retry).with(:post)
          SaltCommon::RestApi.should_receive(:new)
            .with(
              Settings.bucket.base_url + "/api/service/v1/service/fail",
              {
                params:  params,
                headers: subject.send(:headers),
                retries: 1,
                read_timeout: 15,
                open_timeout: 15
              }
            ).and_return(rest_api_instance)

          subject.fail(params)
        end
      end
    end

    describe "#success" do
      describe "request from robber" do
        it "send a request to notify_url in bucket" do
          rest_api_instance.should_receive(:execute_with_retry).with(:post)
          SaltCommon::RestApi.should_receive(:new)
            .with(
              Settings.bucket.base_url + "/api/service/v1/service/success",
              {
                params:  SaltCommon::Credentials.filter!(params, [:encrypted_rememberable_credentials]),
                headers: subject.send(:headers),
                retries: 1,
                read_timeout: 15,
                open_timeout: 15
              }
            ).and_return(rest_api_instance)

          subject.success(params)
        end
      end

      describe "request from stealer" do
        before do
          stub_const("Settings", double(
              service_name: "stealer",
              bucket:       double(base_url: "saltedge.com", stealer_to_bucket_secret: "secret")
            )
          )
        end

        it "send a request to notify_url in bucket" do
          rest_api_instance.should_receive(:execute_with_retry).with(:post)
          Settings.stub(:get).with("bucket.stealer_to_bucket_secret").and_return(Settings.bucket.stealer_to_bucket_secret)
          SaltCommon::RestApi.should_receive(:new)
            .with(
              Settings.bucket.base_url + "/api/service/v1/service/success",
              {
                params:  params,
                headers: subject.send(:headers),
                retries: 1,
                read_timeout: 15,
                open_timeout: 15
              }
            ).and_return(rest_api_instance)

          subject.success(params)
        end
      end
    end

    describe "#stage" do
      it "send a request to notify_url in bucket" do
        rest_api_instance.should_receive(:execute_with_retry).with(:post)
        SaltCommon::RestApi.should_receive(:new)
          .with(
            Settings.bucket.base_url + "/api/service/v1/service/stage",
            {
              params:  SaltCommon::Credentials.filter!(params),
              headers: subject.send(:headers),
              retries: 1,
              read_timeout: 15,
              open_timeout: 15
            }
          ).and_return(rest_api_instance)

        subject.stage(params)
      end
    end

    describe "#import" do
      it "send a request to import_url in bucket" do
        rest_api_instance.should_receive(:execute_with_retry).with(:post)
        SaltCommon::RestApi.should_receive(:new)
          .with(
            Settings.bucket.base_url + "/api/service/v1/service/import",
            {
              params:  SaltCommon::Credentials.filter!(params),
              headers: subject.send(:headers),
              retries: 1,
              read_timeout: 15,
              open_timeout: 15
            }
          ).and_return(rest_api_instance)

        subject.import(params)
      end
    end

    describe "#import_pending" do
      it "send a request to import pending transactions url in bucket" do
        rest_api_instance.should_receive(:execute_with_retry).with(:post)
        SaltCommon::RestApi.should_receive(:new)
          .with(
            Settings.bucket.base_url + "/api/service/v1/service/import_pending",
            {
              params:  SaltCommon::Credentials.filter!(params),
              headers: subject.send(:headers),
              retries: 1,
              read_timeout: 15,
              open_timeout: 15
            }
          ).and_return(rest_api_instance)

        subject.import_pending(params)
      end
    end

    describe "#import_holder_info" do
      it "send a request to import holder info url in bucket" do
        rest_api_instance.should_receive(:execute_with_retry).with(:post)
        SaltCommon::RestApi.should_receive(:new).with(
          Settings.bucket.base_url + "/api/service/v1/service/import_holder_info",
          {
            params:  SaltCommon::Credentials.filter!(params),
            headers: subject.send(:headers),
            retries: 1,
            read_timeout: 15,
            open_timeout: 15
          }
        ).and_return(rest_api_instance)

        subject.import_holder_info(params)
      end
    end

    describe "#interactive" do
      it "send a request to interactive url in bucket" do
        rest_api_instance.should_receive(:execute_with_retry).with(:post)
        SaltCommon::RestApi.should_receive(:new).with(
          Settings.bucket.base_url + "/api/service/v1/service/interactive",
          {
            params:  SaltCommon::Credentials.filter!(params),
            headers: subject.send(:headers),
            retries: 1,
            read_timeout: 15,
            open_timeout: 15
          }
        ).and_return(rest_api_instance)

        subject.interactive(params)
      end
    end
  end

  context "Private methods" do
    describe ".request" do
      it "sends a get request with specified params to specified url" do
        rest_api_instance.should_receive(:execute_with_retry).with(:get)
        SaltCommon::RestApi.should_receive(:new).with(
          url,
          {
            params:  params,
            headers: subject.send(:headers),
            retries: 1,
            read_timeout: 15,
            open_timeout: 15
          }
        ).and_return(rest_api_instance)

        subject.send(:request, params, url, :get)
      end

      it "sends a post request with specified params to specified url" do
        rest_api_instance.should_receive(:execute_with_retry).with(:post)
        SaltCommon::RestApi.should_receive(:new).with(
          url,
          {
            params:  params,
            headers: subject.send(:headers),
            retries: 1,
            read_timeout: 15,
            open_timeout: 15
          }
        ).and_return(rest_api_instance)

        subject.send(:request, params, url, :post)
      end
    end

    describe ".headers" do
      describe "request from robber" do
        it "get headers" do
          headers = subject.send(:headers)
          headers[:APP_SECRET].should == Settings.bucket.robber_to_bucket_secret
        end
      end

      describe "request from stealer" do
        before do
          stub_const("Settings", double(
            service_name: "stealer",
            bucket:       double(base_url: "saltedge.com", stealer_to_bucket_secret: "Stealer_secret")
          )
        )
        end

        it "get headers" do
          Settings.stub(:get).with("bucket.stealer_to_bucket_secret").and_return(Settings.bucket.stealer_to_bucket_secret)
          headers = subject.send(:headers)
          headers[:APP_SECRET].should   == Settings.bucket.stealer_to_bucket_secret
        end
      end
    end
  end
end
