require 'spec_helper'

describe SaltCommon::Connectors::Categorizer do
  subject { SaltCommon::Connectors::Categorizer }

  let(:rest_api_instance) { double(post: true) }
  let(:rest_api_options)  { { timeout: 10 } }
  let(:params)            { { key: :value } }

  before (:each) do
    stub_const("Settings", double(
        service_name: "robber",
        categorizer:  double(base_url: "saltedge.com", service_to_categorizer_secret: "secret")
      )
    )
  end

  describe "#batch_guess" do
    it "send a request to guess/batch in categorizer" do
      rest_api_instance.should_receive(:post)
      SaltCommon::RestApi.should_receive(:new)
        .with(
          Settings.categorizer.base_url + "/api/v1/guess/batch",
          rest_api_options.merge(
            {
             params:  { "data" => params },
             headers: subject.send(:headers)
            }
          )
        ).and_return(rest_api_instance)

      subject.batch_guess(params, rest_api_options)
    end
  end

  context "Private methods" do
    describe ".post" do
      it "sends a post request with specified params to specified url" do
        url = Settings.categorizer.base_url + "/api/v1/guess/batch"
        rest_api_instance.should_receive(:post)
        SaltCommon::RestApi.should_receive(:new)
          .with(
            url,
            rest_api_options.merge(
              {
               params:  { "data" => params },
               headers: subject.send(:headers)
              }
            )
          ).and_return(rest_api_instance)

        subject.send(:post, params, url, rest_api_options)
      end

      it "fails a post request with categorizer timeout" do
        url = Settings.categorizer.base_url + "/api/v1/guess/batch"
        SaltCommon::RestApi.should_receive(:new)
          .with(
            url,
            rest_api_options.merge(
              {
               params:  { "data" => params },
               headers: subject.send(:headers)
              }
            )
          ).and_raise(RestClient::Exceptions::ReadTimeout)

        expect do
          subject.send(:post, params, url, rest_api_options)
        end.to raise_error(SaltCommon::Connectors::Categorizer::CategorizerTimeoutError, "Timeout Error in Categorizer")
      end
    end

    describe ".headers" do
      describe "request from robber" do
        it "get headers" do
          headers = subject.send(:headers)
          headers[:APP_SECRET].should == Settings.categorizer.service_to_categorizer_secret
        end
      end

      describe "request from stealer" do
        before do
          stub_const("Settings", double(
            service_name: "stealer",
            categorizer:  double(base_url: "saltedge.com", service_to_categorizer_secret: "secret")
          )
        )
        end

        it "get headers" do
          headers = subject.send(:headers)
          headers[:APP_SECRET].should == Settings.categorizer.service_to_categorizer_secret
        end
      end
    end

    describe ".build_post_params" do
      describe "request from robber" do
        it "get headers" do
          data = {"data" => params}
          params_post = subject.send(:build_post_params, params)
          params_post.should == data
        end
      end

      describe "request from stealer" do
        before do
          stub_const("Settings", double(
            service_name: "stealer",
            categorizer:  double(base_url: "saltedge.com", service_to_categorizer_secret: "secret")
          )
        )
        end

        it "get headers" do
          data = {"data" => params}
          params_post = subject.send(:build_post_params, params)
          params_post.should == data
        end
      end
    end
  end
end
