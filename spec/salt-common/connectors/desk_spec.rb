require 'spec_helper'

describe SaltCommon::Connectors::Desk do
  subject { SaltCommon::Connectors::Desk }

  let(:rest_client_instance) { double(post: true) }
  let(:data)                 { { data: :data } }

  before(:each) do
    stub_const("Settings", double(
        desk: double(base_url: "desk.banksalt.com", service_to_desk_secret: "desk_secret")
      )
    )
  end

  describe "#create_ticket" do
    it "send a request to tickets in desk" do
      RestClient::Request.should_receive(:execute)
        .with(
          method: :post,
          url:     Settings.desk.base_url + "/api/v2/tickets",
          payload: SaltCommon::JsonWrapper.encode(data),
          headers: {
            APP_SECRET: Settings.desk.service_to_desk_secret,
            content_type: :json
          }
        )
        .and_return(rest_client_instance)

      subject.create_ticket(data)
    end

    it "handles http errors (post)" do
      error = RestClient::Exception.new
      RestClient::Request.should_receive(:execute)
        .with(
          method: :post,
          url:     Settings.desk.base_url + "/api/v2/tickets",
          payload: SaltCommon::JsonWrapper.encode(data),
          headers: {
            APP_SECRET: Settings.desk.service_to_desk_secret,
            content_type: :json
          }
        )
        .and_raise(error)

      error.should_receive(:update).with(data.merge({url: Settings.desk.base_url + "/api/v2/tickets"})).and_return(error)
      error.should_receive(:send_email)

      subject.create_ticket(data)
    end
  end
end
