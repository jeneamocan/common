require 'spec_helper'

describe SaltCommon::Timezone do
  subject { SaltCommon::Timezone }
  describe ".timezone_for_country_code(country_code)" do
    it "returns timezone for specified country_code" do
      subject.timezone_for_country_code("US").should == "America/New_York"
    end

    it "returns UTC if country_code is not in the list" do
      subject.timezone_for_country_code("XF").should == "UTC"
    end

    it "returns UTC if country_code is of unsupported type" do
      subject.timezone_for_country_code(44.12).should == "UTC"
    end
  end
end
