require 'spec_helper'

describe SaltCommon::Validator::HolderInfo do
  let(:item)              { Storage::Test::Item.new("123") }
  let(:valid_holder_info) {
    {
      names: ["Name", "Another Name"],
      emails: ["test@example.com"],
      phone_numbers: ["+ 1 111 222 333"],
      addresses: [],
      extra: {"SSN" => "*1231"}
    }
  }
  let(:invalid_holder_info) { { names: [], phone_numbers: [], extra: {} } }
  let(:subject)             { SaltCommon::Validator::HolderInfo.new(item, valid_holder_info) }
  let(:invalid_subject)     { SaltCommon::Validator::HolderInfo.new(item, invalid_holder_info) }

  before(:each) do
    stub_const("Settings", double(
        email:        double(notify: "notify@example.com", error: "error@example.com"),
        service_name: "robber",
        bucket:       double(base_url: "saltedge.com"),
        log:          double(logstash: double(enabled: false))
      )
    )
  end

  describe "#validate!" do
    it "validates holder_info" do
      err = SaltCommon::Validator::BatchValidationError.new

      SaltCommon::Validator::BatchValidationError.should_receive(:new).and_return(err)
      subject.valid.should be_truthy

      err.should_receive(:send_ticket)
      SaltCommon::Validator::AccountValidationError.any_instance.should_not_receive(:send_email)
      subject.validate!

      err.item_hash["parsing_errors"].size.should == 1
      err.item_hash["parsing_errors"].first["item_hash"]["exception"]["message"].should == "Empty value for holder_info 'addresses' field"

      valid_holder_info.should == {
        names:         ["Name", "Another Name"],
        emails:        ["test@example.com"],
        addresses:     [],
        phone_numbers: ["+ 1 111 222 333"],
        extra:         {"SSN" => "*1231"}
      }

      subject.valid.should be_truthy
    end

    it "validates empty holder_info and filters sensitive data from error.item_hash" do
      err = SaltCommon::Validator::BatchValidationError.new

      SaltCommon::Validator::BatchValidationError.should_receive(:new).and_return(err)
      invalid_subject.valid.should be_truthy

      err.should_receive(:send_ticket)
      SaltCommon::Validator::AccountValidationError.any_instance.should_not_receive(:send_email)
      invalid_subject.validate!

      err.item_hash["parsing_errors"].size.should == 3

      err.item_hash["parsing_errors"].map do |error|
        error["item_hash"]["exception"]["message"]
      end.should == [
        "Empty value for holder_info 'names' field",
        "Empty value for holder_info 'phone_numbers' field",
        "Empty value for holder_info 'extra' field"
      ]

      invalid_holder_info.should == {
        names:         [],
        phone_numbers: [],
        extra:         {}
      }

      invalid_subject.valid.should be_falsey
    end

    it "validates holder_info keys names" do
      holder_info = {
        name:    ["Name", "Another Name"],
        email:   ["test@example.com"],
        address: ["some address"],
        phones:  ["+ 1 111 222 333"],
        extras:  {"SSN" => "*1231"}
      }
      subject = SaltCommon::Validator::HolderInfo.new(item, holder_info)

      err = SaltCommon::Validator::BatchValidationError.new

      SaltCommon::Validator::BatchValidationError.should_receive(:new).and_return(err)
      subject.valid.should be_truthy

      err.should_receive(:send_ticket)
      SaltCommon::Validator::AccountValidationError.any_instance.should_not_receive(:send_email)
      subject.validate!

      subject.valid.should be_truthy

      err.item_hash["parsing_errors"].map { |error| error["message"] }.should == [
        "'name' is not a valid holder_info field name",
        "'email' is not a valid holder_info field name",
        "'address' is not a valid holder_info field name",
        "'phones' is not a valid holder_info field name",
        "'extras' is not a valid holder_info field name"
      ]
    end
  end
end
