require 'spec_helper'

describe SaltCommon::Validator::Account do
  let(:item)             { Storage::Test::Item.new("123") }
  let(:valid_account)    { SaltCommon::Account.new(name: "Name", currency_code: "USD", balance: 1) }
  let(:invalid_account)  { SaltCommon::Account.new(name: "", currency_code: "nil", balance: 1) }
  let(:invalid_account2) { SaltCommon::Account.new(name: "Name", currency_code: "nil", balance: 1) }
  let(:accounts)         { SaltCommon::AccountList.new << valid_account << invalid_account << invalid_account2 }
  let(:subject)          { SaltCommon::Validator::Account.new(item, accounts) }

  before(:each) do
    stub_const("Settings", double(
        email:        double(notify: "notify-robber@example.com", error: "error-robber@example.com"),
        service_name: "robber",
        bucket:       double(base_url: "saltedge.com"),
        log:          double(logstash: double(enabled: false))
      )
    )
  end

  describe "#validate!" do
    it "validates accounts and filters sensitive data from error.item_hash" do
      err = SaltCommon::Validator::BatchValidationError.new

      SaltCommon::Validator::BatchValidationError.should_receive(:new).and_return(err)
      subject.valid.should be_truthy

      accounts.size.should == 3

      err.should_receive(:send_ticket)

      SaltCommon::Validator::AccountValidationError.any_instance.should_not_receive(:send_email)
      subject.validate!

      err.item_hash["parsing_errors"].size.should == 2

      err.item_hash["parsing_errors"].first["item_hash"]["exception"]["message"].should == [
        "Name can't be blank",
        "Name is too short (minimum is 2 characters)",
        "Currency code is not included in the list"
      ].to_s

      err.item_hash["parsing_errors"].first["item_hash"].should include(
        "account" => invalid_account.to_hash(false).slice(:checksum, :balance, :currency_code)
      )

      accounts.size.should == 1

      subject.valid.should be_falsey
    end
  end
end
