require 'spec_helper'

describe SaltCommon::Validator::Transaction do
  let(:item)                { Storage::Test::Item.new("123") }
  let(:valid_transaction)   {
    SaltCommon::Transaction.new(
      made_on: "2013-05-01",
      amount: 1,
      currency_code: "USD",
      description: "description",
      mode: SaltCommon::Transaction::FEE,
      internal_data: { account_name: "abc" }
    )
  }
  let(:invalid_transaction) {
    SaltCommon::Transaction.new(
      made_on: "nil",
      amount: 1,
      currency_code: "",
      description: "",
      mode: nil,
      internal_data: { account_name: "abc" }
    )
  }
  let(:another_invalid_transaction) {
    SaltCommon::Transaction.new(
      made_on: nil,
      amount: 2,
      currency_code: "",
      internal_data: { account_name: "abc" }
    )
  }
  let(:transactions)        { SaltCommon::TransactionList.new << valid_transaction << invalid_transaction << another_invalid_transaction }
  let(:subject)             { SaltCommon::Validator::Transaction.new(item, transactions) }

  before(:each) do
    stub_const("Settings", double(
        email:        double(notify: "notify-robber@example.com", error: "error-robber@example.com"),
        service_name: "robber",
        bucket:       double(base_url: "saltedge.com"),
        log:          double(logstash: double(enabled: false))
      )
    )
  end

  describe "#validate!" do
    it "validates transactions and filters sensitive data from error.item_hash" do
      err = SaltCommon::Validator::BatchValidationError.new

      SaltCommon::Validator::BatchValidationError.should_receive(:new).and_return(err)
      subject.valid.should be_truthy

      transactions.size.should == 3

      err.should_receive(:send_ticket)
      SaltCommon::Validator::TransactionValidationError.any_instance.should_not_receive(:send_email)
      subject.validate!

      err.item_hash["parsing_errors"].size.should == 2

      err.item_hash["parsing_errors"].first["item_hash"]["exception"]["message"].should == [
        "Currency code can't be blank",
        "Currency code is not included in the list",
        "Description can't be blank",
        "Made on should be in 'YYYY-MM-DD' format"
      ].to_s

      err.item_hash["parsing_errors"].first["item_hash"].should include(
        "transaction" => invalid_transaction.to_hash.slice(
          :checksum, :made_on, :amount, :currency_code
        )
      )

      transactions.size.should == 1

      subject.valid.should be_falsey
    end
  end
end
