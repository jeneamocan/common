require 'spec_helper'

describe SaltCommon::TransactionList do
  let(:acc_params)    { { name: "ACCOUNT", currency_code: "MDL", balance: 89.99 } }
  let(:account)       { SaltCommon::Account.new(acc_params) }
  let(:transactions_list) { described_class.new }

  let(:made_on)       { "2013-05-01" }
  let(:amount)        { 199.99 }
  let(:currency_code) { "MDL" }
  let(:description)   { "Retail USA 4157354488 HTTP//GITHUB.COM/C" }
  let(:mode)          { SaltCommon::Transaction::NORMAL }
  let(:internal_data) { { account_name: "abc" } }
  let(:status)        { SaltCommon::Transaction::POSTED }
  let(:extra)         { { original_amount: 10.99, original_currency_code: "USD", id: "t1" } }

  let(:params) {
    {
      made_on:       made_on,
      amount:        amount,
      currency_code: currency_code,
      description:   description,
      mode:          mode,
      extra:         extra,
      internal_data: internal_data,
      status:        status
    }
  }

  describe "#initialize" do
    it "initializes with array of checksums" do
      transactions_list.checksums.should == []
      transactions_list = described_class.new(["1", "2"])
      transactions_list.checksums.should == ["1", "2"]
    end
  end

  describe "#add" do
    it "adds transaction with zero amount only if it has convert flag" do
      params[:amount] = 0
      transactions_list.add(params)
      transactions_list.length.should == 0

      params[:extra][:convert] = true
      transactions_list.add(params)
      transactions_list.length.should == 1

      params[:extra][:convert] = false
      transactions_list.add(params)
      transactions_list.length.should == 1
    end

    it "adds 2 transactions to transaction list" do
      transaction = transactions_list.add(params)
      transaction.class.should == SaltCommon::Transaction

      transactions_list.add(params)
      transactions_list.length.should == 2

      first_transaction  = transactions_list.first
      second_transaction = transactions_list.last
      first_transaction.checksum.should_not == second_transaction.checksum
    end

    it "adds transaction to transaction list" do
      params[:internal_data] = { account_name: "bcd"}
      transaction = transactions_list.add(params)
      transaction.internal_data.should == {
        "account_name"    => "bcd",
        "short_checksums" => [
          "a9e97e8761744d969e6448eb781e6a4b3898f5891f0a5df82a7dcad2edecd52e",
          "dae3118cce11bdb9a39b92cc771e1b5ff1ac4321657dc7262fb1bb510a0ba771",
          "a93f2a1b43b308af5d011ffbb92432c4693d4b596d8c406832a28bc5604f8548",
          "fe0114bd924154cb6cea2fdc0d32485433f9b5a9857497827f5cb8711657527d",
          "fd70bbe17b0ce23cc318a1f4ad09fcc5c6f33d42b8f8b3c155bf6ecb1177863d",
          "2bab95cc098381abb6c4117684b114e05cd932c1653aae6a6b1bca9bfdf28159"
        ]
      }
    end

    it "adds transaction to transaction list" do
      params[:internal_data] = {"account_name" => "bcd"}
      transaction = transactions_list.add(params)
      transaction.internal_data.should == {
        "account_name"    => "bcd",
        "short_checksums" => [
          "a9e97e8761744d969e6448eb781e6a4b3898f5891f0a5df82a7dcad2edecd52e",
          "dae3118cce11bdb9a39b92cc771e1b5ff1ac4321657dc7262fb1bb510a0ba771",
          "a93f2a1b43b308af5d011ffbb92432c4693d4b596d8c406832a28bc5604f8548",
          "fe0114bd924154cb6cea2fdc0d32485433f9b5a9857497827f5cb8711657527d",
          "fd70bbe17b0ce23cc318a1f4ad09fcc5c6f33d42b8f8b3c155bf6ecb1177863d",
          "2bab95cc098381abb6c4117684b114e05cd932c1653aae6a6b1bca9bfdf28159"
        ]
      }
    end

    it "writes initials checksums properly" do
      transactions_list.add(params)
      transactions_list.add(params)
      transactions_list.add(params)
      transactions_list.length.should == 3

      first_transaction  = transactions_list[0]
      second_transaction = transactions_list[1]
      third_transaction  = transactions_list[2]

      first_transaction.initial_checksum.should  be_nil
      second_transaction.initial_checksum.should == first_transaction.checksum
      third_transaction.initial_checksum.should  == second_transaction.checksum
    end

    it "marks transaction as existing if its checksum is present in checksums array" do
      transactions_list = described_class.new(["95f34c9530a57ac0b84edafbfa689949a415be5bd4058dfb67747e154de6971f"])
      transactions_list.add(params)
      transactions_list.add(params.merge("description" => "asd"))

      transactions_list.size.should == 2
      transactions_list.not_existing.size.should == 1
    end

    it "does not add transaction if its amount is 0" do
      transactions_list.add(params)
      transactions_list.add(params.merge(amount: 0))
      transactions_list.size.should == 1
    end
  end

  describe "#find" do
    it "returns first by checksum" do
      transactions_list.add(params)
      transactions_list.add(params)
      transactions_list.find(transactions_list.first.checksum).class.should == SaltCommon::Transaction
    end
  end

  describe "#to_hash" do
    it "converts to array of hashes" do
      transactions_list.add(params)
      params[:extra] = params[:extra].merge({id: "t2"})
      transactions_list.add(params)
      transactions_list.to_hash.length.should == 2
    end

    it "converts to array of hashes" do
      transactions = transactions_list
      transactions_list.add(params)

      params[:internal_data] = { account_name: "bcd" }
      transactions_list.add(params)
      transactions_list.add(params)

      transactions.to_hash.length.should == 3
      transactions.map(&:internal_data).should == [
        {
          "account_name"    => "abc",
          "short_checksums" => [
            "a9e97e8761744d969e6448eb781e6a4b3898f5891f0a5df82a7dcad2edecd52e",
            "dae3118cce11bdb9a39b92cc771e1b5ff1ac4321657dc7262fb1bb510a0ba771",
            "a93f2a1b43b308af5d011ffbb92432c4693d4b596d8c406832a28bc5604f8548",
            "fe0114bd924154cb6cea2fdc0d32485433f9b5a9857497827f5cb8711657527d",
            "fd70bbe17b0ce23cc318a1f4ad09fcc5c6f33d42b8f8b3c155bf6ecb1177863d",
            "2bab95cc098381abb6c4117684b114e05cd932c1653aae6a6b1bca9bfdf28159"
          ]
        },
        {
          "account_name"    => "bcd",
          "short_checksums" => [
            "a9e97e8761744d969e6448eb781e6a4b3898f5891f0a5df82a7dcad2edecd52e",
            "dae3118cce11bdb9a39b92cc771e1b5ff1ac4321657dc7262fb1bb510a0ba771",
            "a93f2a1b43b308af5d011ffbb92432c4693d4b596d8c406832a28bc5604f8548",
            "fe0114bd924154cb6cea2fdc0d32485433f9b5a9857497827f5cb8711657527d",
            "fd70bbe17b0ce23cc318a1f4ad09fcc5c6f33d42b8f8b3c155bf6ecb1177863d",
            "2bab95cc098381abb6c4117684b114e05cd932c1653aae6a6b1bca9bfdf28159"
          ]
        },
        {
          "account_name"    => "bcd",
          "short_checksums" => [
            "a9e97e8761744d969e6448eb781e6a4b3898f5891f0a5df82a7dcad2edecd52e",
            "dae3118cce11bdb9a39b92cc771e1b5ff1ac4321657dc7262fb1bb510a0ba771",
            "a93f2a1b43b308af5d011ffbb92432c4693d4b596d8c406832a28bc5604f8548",
            "fe0114bd924154cb6cea2fdc0d32485433f9b5a9857497827f5cb8711657527d",
            "fd70bbe17b0ce23cc318a1f4ad09fcc5c6f33d42b8f8b3c155bf6ecb1177863d",
            "2bab95cc098381abb6c4117684b114e05cd932c1653aae6a6b1bca9bfdf28159"
          ]
        }
      ]
    end
  end

  describe "#unique_checksum" do
    it "calucaltes checksum for simple transaction" do
      transactions_list.add(params)
      transactions_list.add(params)
      transactions_list.add(params)

      transactions_list[0..2].map(&:checksum).should == [
        "95f34c9530a57ac0b84edafbfa689949a415be5bd4058dfb67747e154de6971f",
        "f0101f892da0506dcba9f316b36c51e5d039de7ddf7e5bda79ef7772df5150e2",
        "d1cd38224b5fd121b05e7b52b6954f7071cea24ef3ada853e9d0d81581fbaf4d"
      ]
    end

    it "generated random checksum for pending transaction" do
      transactions_list.add(params)
      transactions_list.add(params.merge(status: SaltCommon::Transaction::PENDING))
      transactions_list.add(params)

      transactions_list[0..2].map(&:checksum).should == [
        "95f34c9530a57ac0b84edafbfa689949a415be5bd4058dfb67747e154de6971f",
        "f0101f892da0506dcba9f316b36c51e5d039de7ddf7e5bda79ef7772df5150e2",
        "d1cd38224b5fd121b05e7b52b6954f7071cea24ef3ada853e9d0d81581fbaf4d"
      ]
    end
  end

  describe "#unique_short_checksum" do
    it "calculates checksum using chort_checksums" do
      transactions_list.short_checksums = ["a93f2a1b43b308af5d011ffbb92432c4693d4b596d8c406832a28bc5604f8548"]
      transactions = transactions_list
      3.times { transactions.add(params) }

      transactions[0].internal_data[:short_checksums].should == [
            "a9e97e8761744d969e6448eb781e6a4b3898f5891f0a5df82a7dcad2edecd52e",
            "dae3118cce11bdb9a39b92cc771e1b5ff1ac4321657dc7262fb1bb510a0ba771",
            "a93f2a1b43b308af5d011ffbb92432c4693d4b596d8c406832a28bc5604f8548",
            "fe0114bd924154cb6cea2fdc0d32485433f9b5a9857497827f5cb8711657527d",
            "fd70bbe17b0ce23cc318a1f4ad09fcc5c6f33d42b8f8b3c155bf6ecb1177863d",
            "2bab95cc098381abb6c4117684b114e05cd932c1653aae6a6b1bca9bfdf28159"
          ]

      transactions[0].internal_data[:short_checksums].none? do |short_checksum|
        short_checksum == transactions[0].checksum
      end.should be_truthy

      transactions[0].extra[:possible_duplicate].should == true

      transactions.map { |tr| tr.internal_data[:short_checksums] }.uniq.should be_truthy # NOTE: checks if all short_checksums are equal
      transactions.map { |tr| tr.extra[:possible_duplicate] }.all?.should be_truthy
    end
  end

  describe "#sort_by_made_on!" do
    it "sorts transaction list by date of making in reverse order of posting" do
      transactions_list.add(params)
      # NOTE: Adding order in transaction list matters
      transactions_list.add(params.merge(made_on: "2014-06-09", extra: { additional: "last" }))
      transactions_list.add(params.merge(made_on: "2014-06-09", extra: { additional: "middle" }))
      transactions_list.add(params.merge(made_on: "2014-06-09", extra: { additional: "first" }))
      transactions_list.add(params.merge(made_on: "2010-11-03"))

      transactions_list.sort_by_made_on!

      transactions_list.map { |t| t.extra[:additional] }.compact.should == ["last", "middle", "first"]
    end

    it "sorts transaction list by date of making in up-down order of posting" do
      transactions_list.add(params)
      transactions_list.add(params.merge(made_on: "2010-11-03"))
      # NOTE: Adding order in transaction list matters
      transactions_list.add(params.merge(made_on: "2014-06-09", extra: { additional: "first" }))
      transactions_list.add(params.merge(made_on: "2014-06-09", extra: { additional: "middle" }))
      transactions_list.add(params.merge(made_on: "2014-06-09", extra: { additional: "last" }))

      transactions_list.sort_by_made_on!

      transactions_list.map { |t| t.extra[:additional] }.compact.should == ["first", "middle", "last"]
    end

    it "considers extra[:time]" do
      transactions_list.add(params.merge(extra: { time: "12:00:00" }))
      transactions_list.add(params.merge(extra: { time: "11:00:00" }))
      transactions_list.add(params.merge(extra: { time: "13:00:00" }))

      transactions_list.sort_by_made_on!

      transactions_list.map { |t| t.extra[:time] }.should == ["11:00:00", "12:00:00", "13:00:00"]
    end
  end

  describe "#not_existing" do
    it "returns only transactions with false existing flag" do
      transaction  = SaltCommon::Transaction.new(params)
      transaction2 = SaltCommon::Transaction.new(params.merge(existing: true))
      transaction3 = SaltCommon::Transaction.new(params.merge(existing: false, status: SaltCommon::Transaction::PENDING))

      transactions_list << transaction << transaction2 << transaction3
      transactions_list.size.should == 3

      not_existing_list = transactions_list.not_existing
      not_existing_list.size.should  == 1
      not_existing_list.first.should == transaction
    end
  end

  describe "#duplicated" do
    it "returns only transactions with not nil initial_checksum" do
      transaction  = SaltCommon::Transaction.new(params)
      transaction2 = SaltCommon::Transaction.new(params.merge(initial_checksum: "123"))
      transaction3 = SaltCommon::Transaction.new(params.merge(initial_checksum: "456", status: SaltCommon::Transaction::PENDING))

      transactions_list << transaction << transaction2 << transaction3
      transactions_list.size.should == 3

      duplicated_list = transactions_list.duplicated
      duplicated_list.size.should == 1
      duplicated_list.last.should == transaction2
    end
  end

  describe "#pending" do
    it "returns only transactions with not nil initial_checksum" do
      transactions_list.add(params)
      transactions_list.add(params.merge(status: SaltCommon::Transaction::PENDING))
      transactions_list.add(params)

      transactions_list.size.should == 3

      pending_list = transactions_list.pending
      pending_list.size.should == 1
      pending_list.first.status.should == SaltCommon::Transaction::PENDING
    end
  end

  describe "#valid_amount?" do
    it "tells whether amount is valid" do
      params = { "amount" => 0 }
      transactions_list.valid_amount?(params).should be_falsey

      params = { "amount" => 1 }
      transactions_list.valid_amount?(params).should be_truthy

      params = { "amount" => 0, "extra" => { "original_amount" => 1, "original_currency_code" => "USD", "convert" => true } }
      transactions_list.valid_amount?(params).should be_truthy

      params = { "amount" => 0, "extra" => { "original_amount" => 0, "original_currency_code" => "USD", "convert" => true } }
      transactions_list.valid_amount?(params).should be_falsey

      params = { "amount" => 1, "extra" => { "original_amount" => 0, "original_currency_code" => "USD", "convert" => true } }
      transactions_list.valid_amount?(params).should be_falsey

      params = { "amount" => 0, "extra" => { "original_amount" => 10, "original_currency_code" => "USD" } }
      transactions_list.valid_amount?(params).should be_falsey

      params = { "amount" => 10, "extra" => { "original_amount" => 0, "original_currency_code" => "USD" } }
      transactions_list.valid_amount?(params).should be_truthy
    end
  end

  describe ".copy" do
    it "returns new TransactionList with transactions from array" do
      transaction = SaltCommon::Transaction.new(params)
      transactions_list << transaction

      array = transactions_list.to_hash
      new_transactions_list = described_class.copy(array)

      new_transactions_list.to_hash.first.should == transaction.to_hash
    end
  end
end
