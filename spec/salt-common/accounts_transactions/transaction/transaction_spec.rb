require 'spec_helper'

describe SaltCommon::Transaction, type: :model do
  let(:made_on)          { "2013-05-01" }
  let(:amount)           { 199.99 }
  let(:currency_code)    { "MDL" }
  let(:description)      { "Retail USA 4157354488 HTTP//GITHUB.COM/C" }
  let(:long_description) { "123456789 "*401 }
  let(:existing)         { true }
  let(:mode)             { SaltCommon::Transaction::NORMAL }
  let(:category_code)    { SaltCommon::Categories::UNCATEGORIZED }
  let(:mcc)              { SaltCommon::Constants::Mcc::CODES }
  let(:extra)            {{
    "original_amount"        => 10.99,
    "original_currency_code" => "USD",
    "id"                     => "t1",
    "mcc"                    => "0742"
  }}
  let(:internal_data)    {{
    "account_name"    => "abc",
    "short_checksums" => [
      "a9e97e8761744d969e6448eb781e6a4b3898f5891f0a5df82a7dcad2edecd52e",
      "dae3118cce11bdb9a39b92cc771e1b5ff1ac4321657dc7262fb1bb510a0ba771",
      "a93f2a1b43b308af5d011ffbb92432c4693d4b596d8c406832a28bc5604f8548",
      "fe0114bd924154cb6cea2fdc0d32485433f9b5a9857497827f5cb8711657527d",
      "fd70bbe17b0ce23cc318a1f4ad09fcc5c6f33d42b8f8b3c155bf6ecb1177863d",
      "2bab95cc098381abb6c4117684b114e05cd932c1653aae6a6b1bca9bfdf28159"
    ]
  }}
  let(:status)           { SaltCommon::Transaction::POSTED }
  let(:currencies)       { SaltCommon::Currencies::ISO4217.values }
  let(:transaction_params) {
    {
      made_on:       made_on,
      amount:        amount,
      description:   description,
      mode:          mode,
      currency_code: currency_code,
      extra:         extra,
      status:        status,
      category_code: category_code,
      existing:      existing,
      internal_data: internal_data
    }
  }
  let(:transaction) { SaltCommon::Transaction.new(transaction_params) }
  context 'Validation' do
    subject { transaction }

    it { should validate_presence_of(:made_on) }
    it { should validate_presence_of(:currency_code) }
    it { should validate_presence_of(:mode) }

    it { should allow_value({}).for(:extra) }

    it { should allow_value({exchange_rate:          0.828216    }).for(:extra) }
    it { should allow_value({account_number:         "123"       }).for(:extra) }
    it { should allow_value({posting_time:           "12:12:12"  }).for(:extra) }
    it { should allow_value({posting_date:           "2015-12-12"}).for(:extra) }
    it { should allow_value({original_amount: 10.99, original_currency_code: "USD"}).for(:extra) }

    it { should allow_value({"posting_date" => "2015-12-12"}).for(:extra) }
    it { should allow_value({"posting_time" => "12:12:12"  }).for(:extra) }
    it { should allow_value({"mcc"          => "0742"      }).for(:extra) }
    it { should allow_value({possible_duplicate: true}).for(:extra) }
    it { should allow_value({asset_code: "ETH", asset_amount: 12.34}).for(:extra) }

    it { should allow_value("property_taxes").for(:category_code) }
    it { should allow_value("amusement").for(:category_code) }
    it { should_not allow_value("pewdiepie").for(:category_code) }

    it { should_not allow_value({possible_duplicate: false}).for(:extra) }
    it { should_not allow_value("2016-12-23T11:13:12").for(:made_on) }
    it { should_not allow_value("1916-12-23").for(:made_on) }
    it { should_not allow_value(nil).for(:amount) }
    it { should_not allow_value("123").for(:amount) }
    it { should_not allow_value(10**SaltCommon::Constants::AMOUNT_PRECISION).for(:amount) }
    it { should_not allow_value(nil).for(:extra) }
    it { should_not allow_value("") .for(:extra) }

    it { should_not allow_value({exchange_rate:          "0.828216"           }).for(:extra) }
    it { should_not allow_value({available_balance:      123                  }).for(:extra) }
    it { should_not allow_value({account_number:         nil                  }).for(:extra) }
    it { should_not allow_value({account_number:         ""                   }).for(:extra) }
    it { should_not allow_value({additional:             123                  }).for(:extra) }
    it { should_not allow_value({convert:                false                }).for(:extra) }
    it { should_not allow_value({original_amount:        "16.283"             }).for(:extra) }
    it { should_not allow_value({posting_date:           "date"               }).for(:extra) }
    it { should_not allow_value({asset_code: nil,        asset_amount: 12.34  }).for(:extra) }
    it { should_not allow_value({asset_code: "BTC",      asset_amount: "12.1" }).for(:extra) }
    it { should_not allow_value({posting_date:           "2016-12-23T11:13:12"}).for(:extra) }
    it { should_not allow_value({posting_date:           "1916-12-2"          }).for(:extra) }
    it { should_not allow_value({posting_time:           "time"               }).for(:extra) }
    it { should_not allow_value({original_currency_code: "USDA"               }).for(:extra) }
    it { should_not allow_value({original_currency_code: "USD"                }).for(:extra) }
    it { should_not allow_value({mcc:                    "001"                }).for(:extra) }

    it { should validate_inclusion_of(:currency_code).in_array(currencies) }
    it { should validate_inclusion_of(:mode)         .in_array(SaltCommon::Transaction::MODES) }
    it { should validate_inclusion_of(:status)       .in_array(SaltCommon::Transaction::STATUSES) }
    it { should validate_inclusion_of(:category_code).in_array(SaltCommon::Categories::PERSONAL_CATEGORIES_ARRAY) }
    it { should validate_inclusion_of(:category_code).in_array(SaltCommon::Categories::BUSINESS_CATEGORIES_ARRAY) }

    describe "validate_time" do
      it "should validate extra[:time] format" do
        transaction.extra[:time] = "12:00"
        transaction.should_not be_valid
        transaction.errors.messages[:extra].should == ["extra[:time] should be in 'HH:MM:SS' format, got: '12:00'"]

        transaction.extra[:time] = "12:00:00"
        transaction.should be_valid

        transaction.extra = {}
        transaction.should be_valid
      end
    end

    describe "#extra_class" do
      context "extra fields specific validation" do
        it "should not allow Currencies not listed in ISO4217 for original_currency_code" do
          extra[:original_currency_code] = "GGG"
          transaction.should_not be_valid
          transaction.errors.messages.should == {
            extra: ["GGG is not included in Currencies"]
          }
        end

        it "should fail if asset_code is present and asset_amount is missing" do
          extra[:asset_code] = "ETH"
          transaction.should_not be_valid
          transaction.errors.messages.should == {
            extra: ["extra[:asset_amount] missing"]
          }
        end

        it "should fail if asset_amount is present and asset_code is missing" do
          extra[:asset_amount] = 12.22
          transaction.should_not be_valid
          transaction.errors.messages.should == {
            extra: ["extra[:asset_code] missing"]
          }
        end

        it "should not allow codes not listed in Assets::CODES for asset_code", :vcr do
          extra[:asset_code] = "BANAN"
          extra[:asset_amount] = 12.22
          transaction.should_not be_valid
          transaction.errors.messages.should == {
            extra: ["BANAN is included neither in Assets:CODES, nor in Currencies"]
          }
        end

        it "should allow codes not listed in Assets::CODES but present in Currencies" do
          extra[:asset_code] = "PKR"
          extra[:asset_amount] = 12.22
          transaction.should be_valid
        end

        it "should validate codes including those from dynamic currencies list", :vcr do
          extra[:asset_code] = "ETH"
          extra[:asset_amount] = 12.22
          transaction.should be_valid
        end

        it "should not allow original_amount, closing_balance and opening_balance with precision greater then 10" do
          transaction.extra.merge!({
            original_amount: 12345.123456789012,
            opening_balance: 12345.123456789012,
            closing_balance: 12345.123456789012
          })
          transaction.should_not be_valid
          transaction.errors.messages.should == {
            extra: [
              "key original_amount should not have precision greater then 10",
              "key opening_balance should not have precision greater then 10",
              "key closing_balance should not have precision greater then 10"
            ]
          }
        end

        it "should not allow original_amount greater then 10**AMOUNT_PRECISION" do
          transaction.extra.merge!({
            original_amount: 1000000000000000000000000000001.0,
            opening_balance: 1000000000000000000000000000001.0,
            closing_balance: 1000000000000000000000000000001.0
          })
          transaction.should_not be_valid
          transaction.errors.messages.should == {
            extra: [
              "key original_amount can not be greater than 10**30",
              "key opening_balance can not be greater than 10**30",
              "key closing_balance can not be greater than 10**30",
            ]
          }
        end
      end
    end

    describe "validate_amount" do
      it "should not allow amount with precision greater then 10" do
        transaction.amount = 12345.123456789012
        transaction.should_not be_valid
        transaction.errors.messages.should == {
          amount: ["should not have precision greater then 10"]
        }
      end
    end

    describe "validate_date" do
      xit "fails if extra[:posting_date] is > than made_on" do
        transaction.made_on = "2020-01-28"
        transaction.extra[:posting_date] = "2020-01-26"
        transaction.should_not be_valid
        transaction.errors.messages.should == {
          :extra => ["extra[:posting_date] should be > than made_on'"]
        }
      end

      xit "passes if extra[:posting_date] is == to made_on" do
        transaction.made_on = "2020-01-28"
        transaction.extra[:posting_date] = "2020-01-28"
        transaction.should be_valid
      end

      xit "passes if extra[:posting_date] is < to made_on" do
        transaction.made_on = "2020-01-26"
        transaction.extra[:posting_date] = "2020-01-28"
        transaction.should be_valid
      end
      it "should not allow made_on before 01.01.1970" do
        transaction.made_on = "1969-01-01"
        transaction.should_not be_valid
        transaction.errors.messages.should == {
          made_on: ["made_on should be after 01.01.1970"]
        }
      end

      it "should not allow made_on before 01.01.1970" do
        transaction.extra[:posting_date] = "1969-01-01"
        transaction.should_not be_valid
        transaction.errors.messages.should == {
          :extra => ["extra[:posting_date] should be after 01.01.1970"]
        }
      end

      it "should not allow made_on before 01.01.1970" do
        transaction.made_on              = "2021-01-01"
        transaction.extra[:posting_date] = "2021-01-01"
        transaction.should be_valid
      end
    end
  end

  it "allows to add a custom extra field with validation" do
    # NOTE: we create a new class instance to avoid mutating the Account class
    temp_class = Class.new(described_class)

    temp_class.add_extra_field(:new_key, String) do
      if extra[:new_key].size > 4
        errors.add(:extra, "extra[new_key] size should be <= 4")
      end
    end

    transaction2 = temp_class.new(transaction_params)
    transaction2.extra[:new_key] = "111"
    transaction2.should be_valid

    transaction2.extra[:new_key] = "121212121"
    transaction2.should_not be_valid
  end

  describe "CONSTANTS" do
    it "has modes constants" do
      SaltCommon::Transaction::FEE.should      == "fee"
      SaltCommon::Transaction::NORMAL.should   == "normal"
      SaltCommon::Transaction::TRANSFER.should == "transfer"
    end
    it "has statuses constants" do
      SaltCommon::Transaction::POSTED.should  == "posted"
      SaltCommon::Transaction::PENDING.should == "pending"
    end
  end

  describe "#validate_original_currency" do
    it "validates consistence of extra field" do
      expect {
        SaltCommon::Transaction.new(transaction_params)
      }.not_to raise_error

      transaction_params[:extra] = {original_amount: 12}
      transaction.should_not be_valid
    end
  end

  describe "#validate_asset_code_and_amount" do
    it "validates consistence of extra field" do
      expect {
        SaltCommon::Transaction.new(transaction_params)
      }.not_to raise_error

      transaction_params[:extra] = {asset_amount: 12.12}
      transaction.should_not be_valid
    end
  end

  describe "validate_codes" do
    it "validates variable_code, specific_code and constant_code" do
      transaction_params[:extra] = {
        variable_code: "abs21",
        specific_code: "abs21",
        constant_code: "abs21"
      }
      transaction.send(:extra_class)

      transaction.errors.messages[:extra].should == [
        "extra[variable_code] should contain only digits",
        "extra[specific_code] should contain only digits",
        "extra[constant_code] should contain only digits"
      ]
    end

    it "validates variable_code size" do
      transaction.extra[:variable_code] = "12345678910"
      transaction.send(:extra_class)

      transaction.errors.messages[:extra].should == ["extra[variable_code] size should be <= 10"]
    end
  end

  describe "#validate_mcc" do
    it "validates mcc code" do
      transaction_params[:extra] = {mcc: "00"}
      transaction.send(:extra_class)

      transaction.errors.messages[:extra].should == ["extra[:mcc] should be in valide range, got: '0000'"]
    end
  end

  describe "#transform_mcc_to_valid" do
    it "transform mcc code to valide value" do
      transaction_params[:extra] = {mcc: "021244"}

      transaction.send(:transform_mcc_to_valid)
      transaction.extra.should == {"mcc" => "1244"}
    end

    it "transform mcc code to valide value" do
      transaction_params[:extra] = {mcc: "13"}

      transaction.send(:transform_mcc_to_valid)
      transaction.extra.should == {"mcc" => "0013"}
    end

    it "nothing do with mcc" do
      transaction_params[:extra] = {mcc: "4250"}

      transaction.send(:transform_mcc_to_valid)
      transaction.extra.should == {"mcc" => "4250"}
    end
  end

  describe "#sanitize_extra_values" do
    it "sanitize extra values" do
      transaction_params[:extra] = {
        information: "test \u0000",
        additional:  "12345\u0000"
      }

      transaction.send(:sanitize_extra_values)
      transaction.extra.should == {
         "additional"  => "12345",
         "information" => "test "
      }
    end

    it "doesn't sanitize extra values" do
      transaction_params[:extra] = {
        original_amount: 10.99
      }

      transaction.send(:sanitize_extra_values)
      transaction.extra.should == {
        "original_amount" => 10.99
      }
    end
  end

  describe "#pending_transaction sign" do
    it "does not change amount sign if it is positive" do
      transaction_params[:amount] = 10
      transaction_params[:status] = SaltCommon::Transaction::PENDING
      transaction = SaltCommon::Transaction.new(transaction_params)
      transaction.amount.should   == 10
    end

    it "does not change amount sign if it is negative" do
      transaction_params[:amount] = -10
      transaction_params[:status] = SaltCommon::Transaction::PENDING
      transaction = SaltCommon::Transaction.new(transaction_params)
      transaction.amount.should   == -10
    end

    it "does nothing if transaction is not pending(>0)" do
      transaction_params[:amount] = 10
      transaction_params[:status] = SaltCommon::Transaction::POSTED
      transaction = SaltCommon::Transaction.new(transaction_params)
      transaction.amount.should   == 10
    end

    it "does nothing if transaction is not pending(<0)" do
      transaction_params[:amount] = -10
      transaction_params[:status] = SaltCommon::Transaction::POSTED
      transaction = SaltCommon::Transaction.new(transaction_params)
      transaction.amount.should == -10
    end
  end

  describe "#calculate_checksum" do
    it "calculates id for transaction" do
      transaction = SaltCommon::Transaction.new(transaction_params)
      transaction.calculate_checksum.should be
      transaction.calculate_checksum.length.should == 64
    end

    it "calculates same id for 2 exact transactions" do
      one = SaltCommon::Transaction.new(transaction_params)
      two = SaltCommon::Transaction.new(transaction_params)
      one.calculate_checksum.should == two.calculate_checksum
    end

    it "calculates same id for 2 exact transactions when block given to calculate_checksum" do
      one = SaltCommon::Transaction.new(transaction_params)
      two = SaltCommon::Transaction.new(transaction_params)

      first_checksum = one.calculate_checksum do |t|
        "#{t.made_on}-#{t.amount}"
      end
      first_checksum.should == (Digest::SHA2.new << "#{one.made_on}-#{one.amount}").to_s

      second_checksum = two.calculate_checksum do |t|
        "#{t.made_on}-#{t.amount}"
      end
      second_checksum.should == (Digest::SHA2.new << "#{two.made_on}-#{two.amount}").to_s

      first_checksum.should == second_checksum
    end

    it "calculates id for 2 exact transactions with the same block" do
      one = SaltCommon::Transaction.new(transaction_params)
      two = SaltCommon::Transaction.new(transaction_params)

      first_checksum = one.calculate_checksum do |t|
        "#{t.made_on}-#{t.amount}"
      end
      first_checksum.should == (Digest::SHA2.new << "#{one.made_on}-#{one.amount}").to_s

      second_checksum = two.calculate_checksum(first_checksum) do |t|
        "#{t.made_on}-#{t.amount}"
      end
      second_checksum.should == (Digest::SHA2.new << "#{two.made_on}-#{two.amount}-#{two.initial_checksum}").to_s

      first_checksum.should_not == second_checksum
    end

    context "without id" do
      before(:each) do
        transaction_params[:extra].delete("id")
      end

      it "calculates multiple short checksums depending on made_on, description and currency_code" do
        transaction = SaltCommon::Transaction.new(transaction_params)
        transaction.calculate_checksum

        transaction.internal_data["short_checksums"].size.should == 6
        transaction.internal_data["short_checksums"].should == [
          "a9fdaf79e6bbdb77d66bf08bc7122a64496320d10c7ab25ca994a930cbc4d328",
          "4542aaf04c079a1feb78ffb5f997909856dd51c8d5b61518a2a4a3802187bd67",
          "83f449f20f70ba255e74c47a0ff990394db24651d0da075c52a87d4c8f7aea91",
          "0662e5640b507aca0d74b3fd09fd2560c0754c4f5b256301888f708e05ece289",
          "7b61541a27f60a4e7156225e509226b4b6a8a5d059b98da127c81f9bbf842270",
          "ab29ecabe9808af1ca8ff3fac54694a80fdc5878af313002aaa64a1b6c791359"
        ]
      end

      it "does not determine as duplicate transaction with same made on, amount and discription (different currency code)" do
        transaction = SaltCommon::Transaction.new(transaction_params)
        transaction.calculate_checksum

        transaction.internal_data["short_checksums"].size.should == 6

        original_transaction_short_checksums = transaction.internal_data["short_checksums"]

        new_transaction_params                 = transaction_params
        new_transaction_params[:currency_code] = "USD"

        duplicate_transaction = SaltCommon::Transaction.new(new_transaction_params)
        duplicate_transaction.calculate_checksum

        duplicate_transaction.internal_data["short_checksums"].any? do |short_checksum|
          original_transaction_short_checksums.include?(short_checksum)
        end.should be_falsey
      end

      it "determines duplicate with same made_on, amount and currency code (different description)" do
        transaction = SaltCommon::Transaction.new(transaction_params)
        transaction.calculate_checksum

        transaction.internal_data["short_checksums"].size.should == 6

        original_transaction_short_checksums = transaction.internal_data["short_checksums"]

        new_transaction_params               = transaction_params
        new_transaction_params[:description] = "New description"

        duplicate_transaction = SaltCommon::Transaction.new(new_transaction_params)
        duplicate_transaction.calculate_checksum

        duplicate_transaction.internal_data["short_checksums"].any? do |short_checksum|
          original_transaction_short_checksums.include?(short_checksum)
        end.should be_truthy
      end

      it "determines duplicate with same amount, currency code and description (different date in range + 2 day)" do
        transaction = SaltCommon::Transaction.new(transaction_params)
        transaction.calculate_checksum

        transaction.internal_data["short_checksums"].size.should == 6

        original_transaction_short_checksums = transaction.internal_data["short_checksums"]

        new_transaction_params           = transaction_params
        new_made_on                      = new_transaction_params[:made_on].to_date + 1.day
        new_transaction_params[:made_on] = new_made_on.strftime("%Y-%m-%d")

        duplicate_transaction = SaltCommon::Transaction.new(new_transaction_params)
        duplicate_transaction.calculate_checksum

        duplicate_transaction.internal_data["short_checksums"].any? do |short_checksum|
          original_transaction_short_checksums.include?(short_checksum)
        end.should be_truthy
      end

      it "does not determine duplicate if all the same, but date bigger than +2 day range" do
        transaction = SaltCommon::Transaction.new(transaction_params)
        transaction.calculate_checksum

        transaction.internal_data["short_checksums"].size.should == 6

        original_transaction_short_checksums = transaction.internal_data["short_checksums"]

        new_transaction_params           = transaction_params
        new_made_on                      = new_transaction_params[:made_on].to_date + 3.days
        new_transaction_params[:made_on] = new_made_on.strftime("%Y-%m-%d")

        duplicate_transaction = SaltCommon::Transaction.new(new_transaction_params)
        duplicate_transaction.calculate_checksum

        duplicate_transaction.internal_data["short_checksums"].any? do |short_checksum|
          original_transaction_short_checksums.include?(short_checksum)
        end.should be_falsey
      end

      it "determines duplicate transaction with same amount and currency code (different description and date in admissible range)" do
        transaction = SaltCommon::Transaction.new(transaction_params)
        transaction.calculate_checksum

        transaction.internal_data["short_checksums"].size.should == 6

        original_transaction_short_checksums = transaction.internal_data["short_checksums"]

        new_transaction_params               = transaction_params
        new_made_on                          = new_transaction_params[:made_on].to_date + 1.day
        new_transaction_params[:made_on]     = new_made_on.strftime("%Y-%m-%d")
        new_transaction_params[:description] = "New description"

        duplicate_transaction = SaltCommon::Transaction.new(new_transaction_params)
        duplicate_transaction.calculate_checksum

        duplicate_transaction.internal_data["short_checksums"].any? do |short_checksum|
          original_transaction_short_checksums.include?(short_checksum)
        end.should be_truthy
      end
    end

    context "with id" do
      it "calculates multiple short checksums depending on extra[:id], made_on, description and currency_code" do
        transaction = SaltCommon::Transaction.new(transaction_params)
        transaction.calculate_checksum

        transaction.internal_data["short_checksums"].size.should == 6
        transaction.internal_data["short_checksums"].should      == [
          "a9e97e8761744d969e6448eb781e6a4b3898f5891f0a5df82a7dcad2edecd52e",
          "dae3118cce11bdb9a39b92cc771e1b5ff1ac4321657dc7262fb1bb510a0ba771",
          "a93f2a1b43b308af5d011ffbb92432c4693d4b596d8c406832a28bc5604f8548",
          "fe0114bd924154cb6cea2fdc0d32485433f9b5a9857497827f5cb8711657527d",
          "fd70bbe17b0ce23cc318a1f4ad09fcc5c6f33d42b8f8b3c155bf6ecb1177863d",
          "2bab95cc098381abb6c4117684b114e05cd932c1653aae6a6b1bca9bfdf28159"
        ]
      end

      it "calculates multiple short checksums depending on extra[:record_number], made_on, description and currency_code" do
        transaction_params[:extra].delete("id")
        transaction_params[:extra][:record_number] = "123456789"

        transaction = SaltCommon::Transaction.new(transaction_params)
        transaction.calculate_checksum

        transaction.internal_data["short_checksums"].size.should == 6
        transaction.internal_data["short_checksums"].should      == [
          "a8fe9d519e158189d7224281e8042eb0b536253a6e3305615dc686ac2571fa76",
          "303422c704662e63fc9f9a84f3e611dab5e0b48a06bd0a926efd3f6653569ab0",
          "b67366a0f93e6ddb977ac8a5cdcab42b38d25d06312785a9a12118e5da59f408",
          "da3f688a019d6859993331e4613e775b779256d26e743d761ba46a529045569a",
          "9a5b9bfb29d622d90278ea9170998b1a897ef5e027ec35553e3882894cc8ab57",
          "52b11c4fb0cf6c318ec8b7e53e379bcb3cefa6ef8a0d0c0e74030078057c9c32"
        ]
      end

      it "does not determine as duplicate transaction with same made on, amount, discription and currency code (different id)" do
        transaction = SaltCommon::Transaction.new(transaction_params)
        transaction.calculate_checksum

        transaction.internal_data["short_checksums"].size.should == 6

        original_transaction_short_checksums = transaction.internal_data["short_checksums"]

        new_transaction_params              = transaction_params
        new_transaction_params[:extra][:id] = "123"

        duplicate_transaction = SaltCommon::Transaction.new(new_transaction_params)
        duplicate_transaction.calculate_checksum

        duplicate_transaction.internal_data["short_checksums"].any? do |short_checksum|
          original_transaction_short_checksums.include?(short_checksum)
        end.should be_falsey
      end
    end
  end

  describe "#to_hash" do
    it "converts transaction to hash" do
      hash = transaction.to_hash
      hash[:made_on].should       == made_on
      hash[:amount].should        == amount
      hash[:currency_code].should == currency_code
      hash[:description].should   == description
      hash[:mode].should          == mode
      hash[:extra].should         == extra
      hash[:category_code].should == category_code
      hash[:existing].should      == existing
      hash[:internal_data].should == internal_data
      hash[:status].should        == status
      hash.should                 have_key(:initial_checksum)
    end

    it "trims transaction description to 4000 characters if longer" do
      transaction_params[:description] = long_description
      transaction                      = SaltCommon::Transaction.new(transaction_params)
      hash                             = transaction.to_hash
      hash[:description].should == long_description[0...4000]
    end
  end

  describe "Helpers" do
    it "has predefined mode helpers" do
      transaction.fee?.should be_falsey

      SaltCommon::Transaction::MODES.each do |name|
        transaction.mode = name
        transaction.send("#{name}?".to_sym).should be_truthy
      end
    end
    it "has predefined status helpers" do
      transaction.pending?.should be_falsey

      SaltCommon::Transaction::STATUSES.each do |name|
        transaction.status = name
        transaction.send("#{name}?".to_sym).should be_truthy
      end
    end
  end
end
