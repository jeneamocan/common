require 'spec_helper'

describe SaltCommon::AccountList do
  let(:al)            { SaltCommon::AccountList.new }
  let(:tl)            { SaltCommon::TransactionList.new }

  # AccountList attrs
  let(:name)          { "MD000111222333" }
  let(:currency_code) { SaltCommon::Currencies::MDL }
  let(:balance)       { 199.99 }
  let(:nature)        { SaltCommon::Account::ACCOUNT }
  let(:extra)         { { client_name: "DMITRI", cards: ["45678...1234"] } }
  let(:internal_data) { { selector:    "A.some-id" } }

  # Transaction attrs
  let(:made_on)           { "2013-01-01" }
  let(:amount)            { 199.99 }
  let(:description)       { "Retail USA 4157354488 HTTP//GITHUB.COM/C" }
  let(:mode)              { SaltCommon::Transaction::NORMAL }
  let(:transaction_extra) { { original_amount: 10.99, original_currency_code: "USD", id: "t1" } }
  let(:account_params)    {
    {
      name:          name,
      currency_code: currency_code,
      balance:       balance,
      extra:         extra,
      internal_data: internal_data,
      nature:        nature
    }
  }
  let(:transaction_params) {
    {
      made_on:       made_on,
      amount:        amount,
      description:   description,
      mode:          mode,
      extra:         transaction_extra,
      internal_data: { account_name: account.name },
      currency_code: account.currency_code
    }
  }
  let(:account)           { SaltCommon::Account.new(account_params) }
  let(:transaction)       { SaltCommon::Transaction.new(transaction_params) }
  let(:bucket_accounts)   {
    [
      {
        "id" => 684701028,
        "login_id" => 22,
        "currency_code" => "MDL",
        "balance" => 123.4512,
        "name" => "name1",
        "extra" => {
          "owner_name" => "John Smith"
        },
        "nature" => "account",
        "created_at" => "2015-09-29T07:06:23Z",
        "updated_at" => "2015-09-29T07:06:23Z",
        "internal_data" => { "whatever" => true }
      },
      {
        "id" => 684701029,
        "login_id" => 26,
        "currency_code" => "MDL",
        "balance" => 123.4512,
        "name" => "name2",
        "extra" => {
          "owner_name" => "John Smith"
        },
        "nature" => "account",
        "created_at" => "2015-09-30T09:44:56Z",
        "updated_at" => "2015-09-30T09:44:56Z",
        "internal_data" => { "something" => true }
      }
    ]
  }

  describe "#initialize" do
    it "initializes with array of exclude_accounts" do
      al.exclude_accounts.should == []
      al.bucket_accounts.should  == []
      al.include_natures.should  == []

      al = SaltCommon::AccountList.new(exclude_accounts: ["acc1", "acc2"], bucket_accounts: [{}])

      al.exclude_accounts.should == ["acc1", "acc2"]
      al.bucket_accounts.should  == [{}]
      al.include_natures.should  == []

      expect { SaltCommon::AccountList.new([]) }.to raise_error(TypeError)
    end

    it "initializes with array of include_natures" do
      al.exclude_accounts.should == []
      al.bucket_accounts.should  == []
      al.include_natures.should  == []

      al = SaltCommon::AccountList.new(
        exclude_accounts: [], bucket_accounts: [{}], include_natures: [SaltCommon::Account::ACCOUNT]
      )

      al.exclude_accounts.should == []
      al.bucket_accounts.should  == [{}]
      al.include_natures.should  == [SaltCommon::Account::ACCOUNT]

      expect { SaltCommon::AccountList.new([]) }.to raise_error(TypeError)
    end
  end

  describe "#add" do
    it "adds new Account into Account List and returns that account" do
      account = al.add(account_params)
      account.class.should  == SaltCommon::Account

      al.length.should      == 1
      al.first.class.should == SaltCommon::Account
    end

    it "adds new Account into Account List and returns that account" do
      account_params[:internal_data] = { selector: "B.some-id" }
      account = al.add(account_params)

      account.internal_data.should == {"selector"=>"B.some-id"}
    end

    it "raises exception if account with same name exists" do
      al.add(account_params)
      expect {
        al.add(account_params)
      }.to raise_error(SaltCommon::Validator::AccountValidationError, "'#{account_params[:name]}' already exists in account list.")
    end

    it "imports accounts depending on exclude_accounts values, when id is integer" do
      al = SaltCommon::AccountList.new(
        exclude_accounts: [684701028],
        bucket_accounts:  bucket_accounts,
        include_natures:  []
      )

      al.exclude_accounts.should == [684701028]
      al.bucket_accounts.should  == bucket_accounts
      al.include_natures.should  == []

      al.add(name: "name1", currency_code: "MDL", balance: 10)
      al.add(name: "name2", currency_code: "MDL", balance: 10)

      list_to_send = al.valid_account_list_without_transactions

      list_to_send.size.should       == 1
      list_to_send.first.name.should == "name2"
    end

    it "imports accounts depending on exclude_accounts values, when id is string" do
      al = SaltCommon::AccountList.new(
        exclude_accounts: ["684701028"],
        bucket_accounts:  bucket_accounts,
        include_natures:  []
      )

      al.exclude_accounts.should == ["684701028"]
      al.bucket_accounts.should  == bucket_accounts
      al.include_natures.should  == []

      al.add(name: "name1", currency_code: "MDL", balance: 10)
      al.add(name: "name2", currency_code: "MDL", balance: 10)

      al.size.should       == 1
      al.first.name.should == "name2"
    end

    it "imports accounts depending on include_natures values" do
      new_bucket_accounts              = bucket_accounts.dup
      new_bucket_accounts[1]["nature"] = SaltCommon::Account::CREDIT

      al = SaltCommon::AccountList.new(
        exclude_accounts: [],
        bucket_accounts:  new_bucket_accounts,
        include_natures:  [SaltCommon::Account::ACCOUNT]
      )

      al.exclude_accounts.should == []
      al.bucket_accounts.should  == new_bucket_accounts
      al.include_natures.should  == [SaltCommon::Account::ACCOUNT]

      # NOTE: account with nature 'account' (present in bucket)
      new_account_params        = account_params.dup
      new_account_params[:name] = new_bucket_accounts[0]["name"]

      # NOTE: account with nature 'credit' (present in bucket)
      new_credit_params          = account_params.dup
      new_credit_params[:name]   = new_bucket_accounts[1]["name"]
      new_credit_params[:nature] = new_bucket_accounts[1]["nature"]

      # NOTE: account with nature 'checking' (not present in bucket)
      new_checking_params          = account_params.dup
      new_checking_params[:nature] = SaltCommon::Account::CHECKING

      al.add(new_account_params)
      al.add(new_credit_params)
      al.add(new_checking_params)

      list_to_send = al.valid_account_list_without_transactions

      list_to_send.size.should          == 1
      list_to_send.map(&:nature).should == ["account"] # NOTE: credit and checking accounts ware ignored
    end
  end

  describe "#populate" do
    it "populates Account List with transactions" do
      al << account
      tl << transaction

      new_al = al.populate(tl)
      new_al[0].transactions.to_hash.should == tl.to_hash
    end
  end

  describe "#excluded?(account)" do
    it "returns true if bucket_account id is in exclude_accounts list" do
      al = SaltCommon::AccountList.new(
        exclude_accounts: [bucket_accounts[1]["id"]], bucket_accounts: bucket_accounts, include_natures: []
      )
      account.name = bucket_accounts[1]["name"]
      al << account

      expect(al.excluded?(account)).to eq(true)
    end

    it "returns false if bucket_account id is not in exclude_accounts list" do
      al = SaltCommon::AccountList.new(
        exclude_accounts: [bucket_accounts[1]["id"]], bucket_accounts: bucket_accounts, include_natures: []
      )
      al << account

      expect(al.excluded?(account)).to eq(false)
    end
  end

  describe "#excluded_by_nature?(account)" do
    it "returns true if account nature is not included into include_natures" do
      al = SaltCommon::AccountList.new(
        exclude_accounts: [], bucket_accounts: {}, include_natures: ["loan"]
      )
      account.nature = "card"
      al << account

      expect(al.excluded_by_nature?(account)).to eq(true)
    end

    it "returns false if account nature is included into include_natures" do
      al = SaltCommon::AccountList.new(
        exclude_accounts: [], bucket_accounts: {}, include_natures: ["card"]
      )
      account.nature = "card"
      al << account

      expect(al.excluded_by_nature?(account)).to eq(false)
    end

    it "returns false if include_natures is blank" do
      al = SaltCommon::AccountList.new(
        exclude_accounts: [], bucket_accounts: {}, include_natures: []
      )
      al << account

      expect(al.excluded_by_nature?(account)).to eq(false)
    end
  end

  describe "#split" do
    it "splits accounts list to batches" do
      stub_const("Settings", double)
      Settings.stub_chain(:bucket, :per_page).and_return(2)
      al.add(account_params)
      al.add(account_params.merge(name: "new"))

      first_transaction  = SaltCommon::Transaction.new(transaction_params.merge({internal_data: {account_name: al.first.name}}))
      second_transaction = SaltCommon::Transaction.new(transaction_params.merge({internal_data: {account_name: al.last.name}}))

      transaction_list   = SaltCommon::TransactionList.new

      4.times do
        transaction_list << first_transaction
      end
      transaction_list << second_transaction

      split = al.split(transaction_list)

      split.size.should                    == 3
      split[0].size.should                 == 2
      split[0][0].transactions.size.should == 2
      split[0][1].transactions.size.should == 0
      split[2][1].transactions.size.should == 1
      split[2].size.should                 == 2

      split.first.map(&:transactions).flatten.should == [first_transaction, first_transaction]
      split.last.map(&:transactions).flatten.should  == [second_transaction]
    end
  end

  describe "#find" do
    it "returns first by name" do
      al.add(account_params)
      al.add(account_params.merge(name: "another account"))
      al.find(name).class.should == SaltCommon::Account
    end
  end

  describe "#valid_account_list_without_transactions" do
    it "returns accounts list without transactions" do
      al.add(account_params)
      al.first.transactions << SaltCommon::Transaction.new(transaction_params)
      al.first.to_hash[:transactions].size.should == 1

      list = al.valid_account_list_without_transactions
      list.first.to_hash[:transactions].size.should == 0
    end

    it "filters out invalid accounts" do
      al.add(account_params.merge(currency_code: nil))

      list = al.valid_account_list_without_transactions

      expect(list).to be_empty
    end
  end

  describe "#clear_balances" do
    it "clears balances from account list" do
      account_params[:extra].merge!({
        "raw_balance"      => "{\"balances\":[{\"balanceType\":\"information\",\"balanceAmount\":{\"currency\":\"MDL\",\"amount\":\"54.76\"}}]}",
        "balance_type"     => "expected",
        "available_amount" => 9752.39,
        "closing_balance"  => -2247.61,
        "credit_limit"     => 12000.0,
        "current_date"     => "2020-07-27",
        "current_time"     => "16:25:12"
      })

      al.add(account_params)
      expect(al.clear_balances).to be_truthy
      expect(al.first.to_hash).to  eq(
        balance:             0.0,
        checksum:            "5c2125012dbef125d7ac0faf6a0c737fc7482339dcd06c5be1ddee7b673c449e",
        currency_code:       "MDL",
        extra:               {
          "current_date" => "2020-07-27",
          "current_time" => "16:25:12",
          "client_name"  => "DMITRI",
          "cards"        => ["45678...1234"]
        },
        internal_data:       { "selector" => "A.some-id" },
        name:                "MD000111222333",
        nature:              "account",
        recalculate_balance: false,
        transactions:        [],
      )
    end

    it "clears new balance fields from account list" do
      account_params[:extra].merge!({
        "penalty_amount"         => 123.00,
        "floating_interest_rate" => 12.12,
        "total_premium_amount"   => 32.23,
        "remaining_payments"     => 2,
        "interest_type"          => "משתנה לא צמודה"
      })

      al.add(account_params)
      expect(al.clear_balances).to be_truthy
      expect(al.first.extra).to    eq({
        "cards"       => ["45678...1234"],
        "client_name" => "DMITRI"
      })
    end
  end

  describe "#to_hash" do
    it "converts to array of hashes" do
      al.add(account_params)
      account_params[:internal_data] = {selector: "B.some-id"}
      al.add(account_params.merge(name: "another account"))
      al.add(account_params.merge(name: "one more account"))

      al.to_hash.size.should == 3
      al.to_hash.should == [
        {
          name:                "MD000111222333",
          currency_code:       "MDL",
          balance:             199.99,
          extra:               {"client_name"=>"DMITRI", "cards"=>["45678...1234"]},
          nature:              "account",
          checksum:            "5c2125012dbef125d7ac0faf6a0c737fc7482339dcd06c5be1ddee7b673c449e",
          internal_data:       {"selector"=>"A.some-id"},
          recalculate_balance: false,
          transactions:        []
        },
        {
          name:                "another account",
          currency_code:       "MDL",
          balance:             199.99,
          extra:               {"client_name"=>"DMITRI", "cards"=>["45678...1234"]},
          nature:              "account",
          checksum:            "3cc0d1eb4d93c40d309aa1691b9e5f1cc5a5aeb65b24b20460db240eb0c59dc8",
          internal_data:       {"selector"=>"B.some-id"},
          recalculate_balance: false,
          transactions:        []
        },
        {
          name:                "one more account",
          currency_code:       "MDL",
          balance:             199.99,
          extra:               {"client_name"=>"DMITRI", "cards"=>["45678...1234"]},
          nature:              "account",
          checksum:            "ca5bc137064ae8031a0c953ae75c2251820bd28da72aa7411f238201aa72c425",
          internal_data:       {"selector"=>"B.some-id"},
          recalculate_balance: false,
          transactions:        []
        }
        ]
    end

    describe "filtering" do
      let(:feb)    { "2013-02-01" }
      let(:mar)    { "2013-03-01" }
      let(:apr)    { "2013-04-01" }
      let(:may)    { "2013-05-01" }
      let(:months) { [feb, mar, apr, may] }

      before do
        al.add(account_params)
        months.each_with_index do |month, id|
          al.first.transactions << SaltCommon::Transaction.new(
            made_on:       month,
            amount:        amount,
            currency_code: currency_code,
            description:   description,
            mode:          mode,
            extra:         transaction_extra.merge({id: id}),
            internal_data: { account_name: "abc" }
          )
        end
      end

      it "accepts block to filter transactions" do
        al.to_hash.first[:transactions].map { |t| t[:made_on] }.sort.should == [feb, mar, apr, may]

        al.to_hash do |transaction|
          date = Date.parse(transaction.made_on)
          date > Date.parse(apr) || date < Date.parse(feb)
        end.first[:transactions].map { |t| t[:made_on] }.sort.should == [feb, mar, apr].sort

        al.to_hash do |transaction|
          date = Date.parse(transaction.made_on)
          date > Date.parse(mar) || date < Date.parse(feb)
        end.first[:transactions].map { |t| t[:made_on] }.sort.should == [feb, mar].sort

        al.to_hash do |transaction|
          date = Date.parse(transaction.made_on)
          date > Date.parse(apr) || date < Date.parse(mar)
        end.first[:transactions].map { |t| t[:made_on] }.sort.should == [mar, apr].sort

        al.to_hash do |transaction|
          date = Date.parse(transaction.made_on)
          date > Date.parse(feb) || date < Date.parse(feb)
        end.first[:transactions].map { |t| t[:made_on] }.should == [feb]

        al.to_hash do |transaction|
          date = Date.parse(transaction.made_on)
          date > Date.parse("2012-02-01") || date < Date.parse("2012-02-01")
        end.first[:transactions].should == []

        al.to_hash do |transaction|
          date = Date.parse(transaction.made_on)
          date > Date.parse("2015-02-01") || date < Date.parse("2015-02-01")
        end.first[:transactions].should == []

        # AND SOME MAGIC!!
        al.to_hash do |transaction|
          transaction.made_on == feb
        end.first[:transactions].map { |t| t[:made_on] }.should_not include(feb)
      end
    end
  end

  describe ".copy" do
    it "returns new AccountList with accounts from array" do
      account = SaltCommon::Account.new(account_params)
      al << account

      array = al.to_hash
      new_accounts_list = SaltCommon::AccountList.copy(array)

      new_acc_hash        = new_accounts_list.to_hash.first
      new_acc_hash.should == account.to_hash
      new_acc_hash[:checksum].should == "5c2125012dbef125d7ac0faf6a0c737fc7482339dcd06c5be1ddee7b673c449e"
    end
  end

  describe "#update_account_extra" do
    it "doesn't fail during accounts.add with no extra passed in payload" do
      expect {
        al.add({
          name:          "ES1111811197111201110565",
          currency_code: SaltCommon::Currencies::EUR,
          balance:       89.99
        })
      }.not_to raise_error

      expect(al.first.extra).to eq({"sort_code" => "11811197", "bban" => "11811201110565"})
    end

    it "doesn't update sort_code && bban if already present in extra" do
      account_params[:extra][:iban]      = "DE89370400440532013000"
      account_params[:extra][:sort_code] = "99990044"
      account_params[:extra][:bban]      = "12234455"

      al.add(account_params)

      expect(al.first.extra[:sort_code]).to eq("99990044")
      expect(al.first.extra[:bban]).to      eq("12234455")
    end

    it "returns default extra if nothing extracted" do
      expect(account_params[:extra]).not_to include("iban")

      al.add(account_params)

      expect(al.first.extra).to eq({
        "cards"       => ["45678...1234"],
        "client_name" => "DMITRI"
      })
    end

    it "extracts sort code from AT iban as name" do
      account_params[:name] = "AT61 1904 3002 3457 3201"
      al.add(account_params)
      expect(al.first.extra[:sort_code]).to eq("19043")
    end

    it "extracts sort code for different countries" do
      iban_list = %w[
        AD1200012030200359100100 AL47212110090000000235698741 AT611904300234573201 AZ21NABZ00000000137010001944
        BA391290079401028494 BE68539007547034 BG80BNBG96611020345678 BH67BMAG00001299123456 BR1800360305000010009795493C1
        CH9300762011623852957 CR05015202001026284066 CY17002001280000001200527600 CZ6508000000192000145399 DE89370400440532013000
        DK5000400440116243 DO28BAGR00000001212453611324 EE382200221020145685 EG380019000500000000263180002 ES9121000418450200051332
        FI2112345600000785 FO6264600001631634 FR1420041010050500013M02606 GB29NWBK60161331926819 GE29NB0000000101904917
        GI75NWBK000000007099453 GL8964710001000206 GR1601101250000000012300695 GT82TRAJ01020000001210029690 HR1210010051863000160
        HU42117730161111101800000000 IE29AIBK93115212345678 IL620108000000099999999 IS140159260076545510730339
        IT60X0542811101000000123456 JO94CBJO0010000000000131000302 KW81CBKU0000000000001234560101 KZ86125KZT5004100100
        LB62099900000001001901229114 LI21088100002324013AA LT121000011101001000 LU280019400644750000 LV80BANK0000435195001
        MC5811222000010123456789030 MD24AG000225100013104168 ME25505000012345678951 MK07250120000058984 MR1300020001010000123456753
        MT84MALT011000012345MTLCAST001S NL91ABNA0417164300 NO9386011117947 PK36SCBL0000001123456702 PL61109010140000071219812874
        PS92PALS000000000400123456702 PT50000101231234567890192 QA58DOHB00001234567890ABCDEFG RO49AAAA1B31007593840000
        RS35260005601001611379 SA0380000000608010167519 SE4550000000058398257466 SI56191000000123438 SK3112000000198742637541
        SM86U0322509800000000270100 SV62CENR00000000000000700025 TL380080012345678910157 TR330006100519786457841326
        UA213996220000026007233566001 VA59001123000012345678 VG96VPVG0000012345678901 XK051212012345678906
      ]

      sorte_code_list = iban_list.each_with_object({}) do |iban, hash|
        account_params[:extra][:iban] = iban
        al.add(account_params)

        hash[iban.first(2)] = al.first.extra[:sort_code]
        al.delete(al.first)
        account_params[:extra].delete(:sort_code)
      end

      expect(sorte_code_list).to eq({
        "AD" => "00012030",
        "AL" => "2121100",
        "AT" => "19043",
        "AZ" => "NABZ",
        "BA" => "129007",
        "BE" => "539",
        "BG" => "BNBG9661",
        "BH" => "BMAG",
        "BR" => "0036030500001",
        "CH" => "00762",
        "CR" => "152",
        "CY" => "00200128",
        "CZ" => "0800",
        "DE" => "37040044",
        "DK" => "0040",
        "DO" => "BAGR",
        "EE" => "2200",
        "EG" => "00190005",
        "ES" => "21000418",
        "FI" => "123456",
        "FO" => "6460",
        "FR" => "2004101005",
        "GB" => "601613",
        "GE" => "NB",
        "GI" => "NWBK",
        "GL" => "6471",
        "GR" => "0110125",
        "GT" => "TRAJ",
        "HR" => "1001005",
        "HU" => "1177301",
        "IE" => "931152",
        "IL" => "010800",
        "IS" => "015926",
        "IT" => "0542811101",
        "JO" => "CBJO0010",
        "KW" => "CBKU",
        "KZ" => "125",
        "LB" => "0999",
        "LI" => "08810",
        "LT" => "10000",
        "LU" => "001",
        "LV" => "BANK",
        "MC" => "1122200001",
        "MD" => "AG",
        "ME" => "505",
        "MK" => "250",
        "MR" => "0002000101",
        "MT" => "MALT01100",
        "NL" => "ABNA",
        "NO" => "8601",
        "PK" => "SCBL",
        "PL" => "1090101",
        "PS" => "PALS",
        "PT" => "00010123",
        "QA" => "DOHB",
        "RO" => "AAAA",
        "RS" => "260",
        "SA" => "80",
        "SE" => "500000000",
        "SI" => "19100",
        "SK" => "1200",
        "SM" => "0322509800",
        "SV" => "CENR",
        "TL" => "008",
        "TR" => "00061",
        "UA" => "399622",
        "VA" => "001",
        "VG" => "VPVG",
        "XK" => "1212"
      })
    end
  end
end
