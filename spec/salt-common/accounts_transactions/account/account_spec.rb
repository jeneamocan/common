require 'spec_helper'

describe SaltCommon::Account, type: :model do
  let(:name)              { "45678...1234" }
  let(:currency_code)     { "MDL" }
  let(:balance)           { 199.99 }
  let(:extra)             {
    {
      "client_name"   => "DMITRI",
      "cards"         => ["45678...1234"],
      "interest_rate" => 1.0,
      "sort_code"     => "12-34-56"
    }
  }
  let(:nature)            { SaltCommon::Account::CARD }
  let(:internal_data)     { { "selector" => "A.some-id" } }

  let(:made_on)           { "2013-05-01" }
  let(:amount)            { 199.99 }
  let(:description)       { "Retail USA 4157354488 HTTP//GITHUB.COM/C" }
  let(:mode)              { SaltCommon::Transaction::NORMAL }
  let(:transaction_extra) { { "original_amount" => 10.99, "original_currency_code" => "USD", "id" => "t1" } }
  let(:currencies)        { SaltCommon::Currencies::ISO4217.values }
  let(:account_params) {
    {
      name:                name,
      currency_code:       currency_code,
      balance:             balance,
      extra:               extra,
      internal_data:       internal_data,
      nature:              nature,
      recalculate_balance: true
    }
  }
  let(:account)            { SaltCommon::Account.new(account_params) }
  let(:transaction_params) {
    {
      made_on:       made_on,
      amount:        amount,
      description:   description,
      mode:          mode,
      extra:         transaction_extra,
      internal_data: { "account_name" => "abc" }
    }
  }

  context "Validation" do
    subject { account }

    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:nature) }
    it { should validate_presence_of(:currency_code) }

    it { should allow_value({}).for(:extra) }

    it { should allow_value({account_number:       "123"       }).for(:extra) }
    it { should allow_value({current_time:         "12:12:12"  }).for(:extra) }
    it { should allow_value({expiry_date:          "1234-12-12"}).for(:extra) }
    it { should allow_value({total_payment_amount: 123.12      }).for(:extra) }
    it { should allow_value({next_payment_date:    "1234-12-12"}).for(:extra) }
    it { should allow_value({statement_cut_date:   "1234-12-12"}).for(:extra) }
    it { should allow_value({card_type: SaltCommon::Account::CARD_TYPES.first}).for(:extra) }
    it { should allow_value({sort_code:            "12-1213" }).for(:extra) }
    it { should allow_value({partial:              true}).for(:extra) }
    it { should allow_value({bban:                 "NWBK6016133192681"}).for(:extra) }

    it { should allow_value({"balance_type"        => "InterimAvailable"}).for(:extra) }
    it { should allow_value({"account_number"      => "123"             }).for(:extra) }
    it { should allow_value({"next_payment_amount" => 123.12            }).for(:extra) }
    it { should allow_value({"expiry_date"         => "1234-12-12"      }).for(:extra) }
    it { should allow_value({"current_time"        => "12:12:12"        }).for(:extra) }
    it { should allow_value({"sort_code"           => "11-22-33"        }).for(:extra) }
    it { should allow_value({"sort_code"           => "GBXX3442"        }).for(:extra) }
    it { should allow_value({"card_type"           => SaltCommon::Account::CARD_TYPES.first}).for(:extra) }
    it { should allow_value({ # JSON
      "raw_balance" => "{\"balances\":[{\"balanceAmount\":{\"currency\":\"EUR\",\"amount\":\"3182.09\"},\"balanceType\":\"closingBooked\",\"lastChangeDateTime\":\"2021-03-16T11:13:50.000Z\",\"referenceDate\":\"2021-03-16\"}]}"
    }).for(:extra) }
    it { should allow_value({ # XML
        "raw_balance" => "<Bal> <Tp> <CdOrPrtry> <Cd>ITAV</Cd> </CdOrPrtry> </Tp> <CdtLine> <Incl>true</Incl> <Tp> <Prtry>Account credit line</Prtry> </Tp> <Amt Ccy=\"HUF\">0.000</Amt> </CdtLine> <Amt Ccy=\"HUF\">24647133.000</Amt> <CdtDbtInd>CRDT</CdtDbtInd> <Dt> <DtTm>2018-12-12T14:00:32.184000</DtTm> </Dt> </Bal>"\
                         "<Bal> <Tp> <CdOrPrtry> <Cd>INFO</Cd> </CdOrPrtry> <SubTp> <Cd>BLKD</Cd> </SubTp> </Tp> <Amt Ccy=\"HUF\">337835.000</Amt> <CdtDbtInd>CRDT</CdtDbtInd> <Dt> <DtTm>2018-12-12T14:00:32.184000</DtTm> </Dt> </Bal>"
    }).for(:extra) }

    it { should_not allow_value(nil).for(:balance) }
    it { should_not allow_value("123").for(:balance) }
    it { should_not allow_value(10**SaltCommon::Constants::AMOUNT_PRECISION).for(:balance) }

    it { should_not allow_value({available_balance:    123       }).for(:extra) }
    it { should_not allow_value({total_payment_amount: 123       }).for(:extra) }
    it { should_not allow_value({next_payment_date:    123       }).for(:extra) }
    it { should_not allow_value({next_payment_amount:  "string"  }).for(:extra) }
    it { should_not allow_value({statement_cut_date:   123       }).for(:extra) }
    it { should_not allow_value({account_number:       nil       }).for(:extra) }
    it { should_not allow_value({current_time:         "12-12-12"}).for(:extra) }
    it { should_not allow_value({expiry_date:          "1234-12" }).for(:extra) }
    it { should_not allow_value({iban:                 12345678  }).for(:extra) }
    it { should_not allow_value({card_type:            "card"    }).for(:extra) }
    it { should_not allow_value({account_number:       ""        }).for(:extra) }
    it { should_not allow_value({balance_type:         ""        }).for(:extra) }
    it { should_not allow_value({raw_balance:          ""        }).for(:extra) }
    it { should_not allow_value({raw_balance:          ["array"] }).for(:extra) }
    it { should_not allow_value({sort_code:            "12-1213#"}).for(:extra) }
    it { should_not allow_value({partial:              false}).for(:extra) }

    it { should_not allow_value({assets: [{"abc" => 123}]}).for(:extra) }
    it { should_not allow_value({interest_rate: 1}).for(:extra) }
    it { should_not allow_value(nil).for(:extra) }
    it { should_not allow_value("").for(:extra) }
    it { should_not allow_value({"expiry_date" => "1234-12"}).for(:extra) }
    it { should_not allow_value({"code" => "XWC"}).for(:extra) }

    it { should validate_length_of(:name).is_at_least(2).is_at_most(255) }
    it { should validate_inclusion_of(:nature).in_array(SaltCommon::Account::NATURES) }
    it { should validate_inclusion_of(:currency_code).in_array(currencies) }

    describe "#extra_class" do
      context "Investment account" do
        it "should not allow extra['assets'] for non Investment accounts" do
          account_params[:extra]["assets"] = [{"code" => "XMY", "amount" => 1.45678}]
          account.should_not be_valid
          account.errors.messages.should == {
            extra: ["extra contains investment account type keys, but account is not an investment"]
          }

          account.nature = SaltCommon::Account::INVESTMENT
          account.should be_valid
        end

        it "should allow extra['assets'] empty array for crypto-currency Investment accounts" do
          account.nature = SaltCommon::Account::INVESTMENT
          account.extra["assets"] = []
          account.should be_valid
        end

        it "should not allow extra['units'] for non Investment accounts" do
          account_params[:extra]["units"] = 66.6
          account.should_not be_valid
          account.errors.messages.should == {
            extra: ["extra contains investment account type keys, but account is not an investment"]
          }

          account.nature = SaltCommon::Account::INVESTMENT
          account.should be_valid
        end

        it "should not allow extra['unit_price'] for non Investment accounts" do
          account_params[:extra]["unit_price"] = 66.6
          account.should_not be_valid
          account.errors.messages.should == {
            extra: ["extra contains investment account type keys, but account is not an investment"]
          }

          account.nature = SaltCommon::Account::INVESTMENT
          account.should be_valid
        end

        it "should not allow any non Hash assets" do
          account_params[:nature] = SaltCommon::Account::INVESTMENT
          account_params[:extra]["assets"] = [
            {"code" => "XMY", "amount" => 1.45678},
            "invalid"
          ]
          account.should_not be_valid
          account.errors.messages.should == {
            extra: ["One of elements in extra[:assets] is not a Hash class."]
          }
        end

        it "should not allow to have 'code' without 'amount' in asset" do
          account_params[:nature] = SaltCommon::Account::INVESTMENT
          account_params[:extra]["assets"] = [{"code" => "XMY"}]
          account.should_not be_valid
          account.errors.messages.should == { extra: ["'amount' key is missing in asset"] }
        end

        it "should not allow keys with empty values in asset" do
          account_params[:nature] = SaltCommon::Account::INVESTMENT
          account_params[:extra]["assets"] = [{"code" => "XMY", "amount" => ""}]
          account.should_not be_valid
          account.errors.messages.should == {
            extra: ["One of assets has empty value for 'amount' key"]
          }
        end

        it "should not allow keys with wrong data types" do
          account_params[:nature] = SaltCommon::Account::INVESTMENT
          account_params[:extra]["assets"] = [{"code" => "XMY", "amount" => "123.1"}]
          account.should_not be_valid
          account.errors.messages.should == { extra: ["'amount' key should be Float class"] }
        end

        it "should not allow to have 'amount' without 'code' in asset" do
          account_params[:nature] = SaltCommon::Account::INVESTMENT
          account_params[:extra]["assets"] = [{"amount" => 1.45678}]
          account.should_not be_valid
          account.errors.messages.should == { extra: ["'code' key is missing in asset"] }
        end

        it "should not allow interest_rate with precision grater then 10" do
          account.extra[:interest_rate] = 12345.123456789012
          account.should_not be_valid
          account.errors.messages.should == {
            extra: ["key interest_rate should not have precision greater then 10"]
          }
        end

        it "should not allow interest_rate greater then 10**AMOUNT_PRECISION" do
          account.extra[:interest_rate] = 1000000000000000000000000000001.0
          account.should_not be_valid
          account.errors.messages.should == {
            extra: ["key interest_rate can not be greater than 10**30"]
          }
        end
      end
    end

    it "allows to add a custom extra field with validation" do
      # NOTE: we create a new class instance to avoid mutating the Account class
      temp_class = Class.new(described_class)

      expect {
        temp_class.add_extra_field(:new_key, String) do
          if extra[:new_key].size > 4
            errors.add(:extra, "extra[new_key] size should be <= 4")
          end
        end
      }.to raise_error(RuntimeError, "Add new_key in one of the extra fields lists BALANCE_FIELDS or ACCOUNT_WITHOUT_BALANCE_FIELDS")

      account2 = temp_class.new(name: "test", currency_code: currency_code, extra: {new_key: "1111"})
      account2.should be_valid

      account2.extra[:new_key] = "121212121"
      account2.should_not be_valid
    end

    it "allows to add a custom extra field with correct validation" do
      new_temp_class = Class.new(described_class)

      described_class.add_extra_field(:closing_balance, Float) do
        if extra[:closing_balance] != Float
          errors.add(:extra, "extra[new_key] size should be Float")
        end
      end

      new_temp_class.custom_extra_fields.include?("closing_balance").should be_truthy
    end

    it "allows to add extra[:partial] flag" do
      new_temp_class = Class.new(described_class)
      account2       = new_temp_class.new(name: "test", currency_code: currency_code, extra: {partial: true})
      account2.should be_valid
    end

    it "validates extra[:partial] flag" do
      new_temp_class = Class.new(described_class)
      account2       = new_temp_class.new(name: "test", currency_code: currency_code, extra: {partial: false})
      account2.should_not be_valid
    end

    it "should not allow balance with precision greater then 10" do
      account_params[:balance] = 12345.123456789012
      account.should_not be_valid
      account.errors.messages.first.should == [:balance, ["should not have precision greater then 10"]]
    end

    it "validates extra.status" do
      account.extra[:status] = "enabled"
      account.should_not be_valid
      expect(account.errors.messages).to eq({
        extra: ["enabled is not a valid value for extra[:status]"]
      })
    end

    it "validates extra.status" do
      account.extra[:status] = described_class::STATUSES.sample
      account.should be_valid
      expect(account.errors.messages).to be_empty
    end
  end

  describe "#assign_card_type" do
    it "does not assign card_type to account.extra if catd_type present" do
      extra.merge({"card_type" => "maestro"})
      account.should_not_receive(:get_card_type)
      account.send(:assign_card_type)
    end

    it "does not assign card_type to account.extra if not a card nature" do
      account.nature = SaltCommon::Account::ACCOUNT
      account.should_not_receive(:get_card_type)
      account.send(:assign_card_type)
    end

    it "does assign card_type to account.extra" do
      account.send(:assign_card_type)
      account.extra[:card_type].should == SaltCommon::Account::VISA
    end

    it "does assign card_type to account.extra" do
      new_params = account_params.merge(name: "Silver Card", extra: {account_number: "1234 1234 1234 1234"})

      account = SaltCommon::Account.new(new_params)
      account.extra[:card_type].should == SaltCommon::Account::UATP
    end

    it "does assign card_type to account.extra" do
      new_params = account_params.merge(name: "Gold Card", extra: {account_name: "5134 1234 1234 1234"})

      account = SaltCommon::Account.new(new_params)
      account.extra[:card_type].should == SaltCommon::Account::MASTER_CARD
    end

    it "adds ignore_card_type param" do
      new_params = account_params.merge(
        name:          "Not amex card",
        extra:         { account_number: "341111" },
        internal_data: { ignore_card_type: true }
      )

      account = SaltCommon::Account.new(new_params)
      account.extra[:card_type].should be_nil
    end
  end

  describe "#assign_name(name)" do
    it "masks credit card name" do
      account_params[:name] = "1234567890123456"
      account.name.should   == "1234********3456"
    end
    it "doesn't change name if it's not card" do
      account_params[:nature] = SaltCommon::Account::ACCOUNT
      account_params[:name]   = "1234567890123456"
      account.name.should     == "1234567890123456"
    end
    it "doesn't mask name if asked" do
      internal_data[:skip_rename] = true
      account_params[:name]       = "1234567890123456"
      account.name.should         == "1234567890123456"
    end
  end

  describe "#transactions" do
    it "initialize new Transaction List" do
      account.transactions.should be
      account.transactions.class.should == SaltCommon::TransactionList
    end
  end

  describe "#to_hash" do
    it "converts account to hash" do
      transaction = SaltCommon::Transaction.new(transaction_params)
      account.transactions << transaction

      account.to_hash.except(:transactions).should == {
        name: "45678...1234",
        currency_code: "MDL",
        balance: 199.99,
        extra: {
          "client_name"   => "DMITRI",
          "cards"         => ["45678...1234"],
          "interest_rate" => 1,
          "sort_code"     => "12-34-56",
          "card_type"     => "visa"
        },
        nature: "card",
        checksum: "7638684c9b0dcbffe1833027d984064490f46a5df36852f9276fa98f1603a8aa",
        internal_data: {"selector"=>"A.some-id"},
        recalculate_balance: true
      }
    end

    it "returns account's hash without transactions" do
      transaction = SaltCommon::Transaction.new(transaction_params)
      account.transactions << transaction

      hash = account.to_hash(false)
      hash.should have_key(:name)
      hash.should have_key(:currency_code)
      hash.should have_key(:balance)
      hash.should have_key(:nature)
      hash.should have_key(:extra)
      hash.should have_key(:recalculate_balance)
      hash.should_not have_key(:transactions)
    end
  end

  describe "Helpers" do
    it "has predefined nature helpers" do
      account.credit?.should be_falsey
      SaltCommon::Account::NATURES.each do |name|
        account.nature = name
        account.send("#{name}?".to_sym).should be_truthy
      end
    end

    it "has predefined card_type helpers" do
      account.master_card?.should be_falsey
      SaltCommon::Account::CARD_TYPES.each do |name|
        account.extra[:card_type] = name
        account.send("#{name}?".to_sym).should be_truthy
      end
    end
  end

  context "Private methods" do
    describe "#get_card_type" do
      it "returns CARD_TYPES constant from given string" do
        account.name  = "generic_name"
        account.extra = {}
        account.instance_variable_set(:@card_type, nil)

        account.send(:get_card_type, nil).should be_nil
        account.send(:get_card_type, false).should be_nil
        account.send(:get_card_type, "").should be_nil
        account.send(:get_card_type, "1321").should be_nil

        account.send(:get_card_type, "ViSA GOLD").should       == SaltCommon::Account::VISA
        account.send(:get_card_type, "4234 * * * 1234").should == SaltCommon::Account::VISA

        account.send(:get_card_type, "amex card").should       == SaltCommon::Account::AMERICAN_EXPRESS
        account.send(:get_card_type, "american  express card").should == SaltCommon::Account::AMERICAN_EXPRESS
        account.send(:get_card_type, "3404 * * * 1234").should == SaltCommon::Account::AMERICAN_EXPRESS

        account.send(:get_card_type, "5104 * * * 1234").should     == SaltCommon::Account::MASTER_CARD
        account.send(:get_card_type, "MasterCard Platinum").should == SaltCommon::Account::MASTER_CARD
        account.send(:get_card_type, "Master Card Platinum").should == SaltCommon::Account::MASTER_CARD
        account.send(:get_card_type, "MC Sberbank").should         == SaltCommon::Account::MASTER_CARD

        account.send(:get_card_type, "5604 * * * 1234").should == SaltCommon::Account::MAESTRO
        account.send(:get_card_type, "card maestro").should    == SaltCommon::Account::MAESTRO

        account.send(:get_card_type, "1234 * * * 1234").should == SaltCommon::Account::UATP
        account.send(:get_card_type, "6234 * * * 1234").should == SaltCommon::Account::CHINA_UNIONPAY
        account.send(:get_card_type, "3004 * * * 1234").should == SaltCommon::Account::DINERS_CLUB
        account.send(:get_card_type, "3529 * * * 1234").should == SaltCommon::Account::JCB

        account.send(:get_card_type, "МИР").should                 == SaltCommon::Account::MIR
        account.send(:get_card_type, "2200 ×××× ×××× 1111").should == SaltCommon::Account::MIR
      end
    end
  end
end
