require 'spec_helper'

describe SaltCommon::Assets::CODES do
  describe "CODES" do
    it "should initialize constants for each crypto currency" do
      SaltCommon::Assets::X_BTC.should == "BTC"
      SaltCommon::Assets::X_SALT.should == "SALT"
    end
  end
end