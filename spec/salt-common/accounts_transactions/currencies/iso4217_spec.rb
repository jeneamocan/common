require 'spec_helper'

describe SaltCommon::Currencies::ISO4217 do
  describe "ISO4217" do
    it "has ISO 4217 currency codes" do
      SaltCommon::Currencies::ISO4217[1001].should  == "BTC"
      SaltCommon::Currencies::ISO4217[498].should   == "MDL"
      SaltCommon::Currencies::ISO4217.length.should == 176
    end
  end
end
