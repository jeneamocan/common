require 'spec_helper'

describe SaltCommon::IbanParser do
  let(:invalid_iban)  { "DE0011111112222222222" }
  let(:invalid_iban2) { "XF00111111111222222222" }

  let(:ibans) do
    %w[
      AT611904300234573201 BE68539007547034 BG80BNBG96611020345678 CY17002001280000001200527600
      CZ6508000000192000145399 DE89370400440532013000 DK5000400440116243 EE382200221020145685
      ES9121000418450200051332 FI2112345600000785 FR1420041010050500013M02606 GB29NWBK60161331926819
      GR1601101250000000012300695 HR1210010051863000160 HU42117730161111101800000000 IE29AIBK93115212345678
      IS140159260076545510730339 IT60X0542811101000000123456 LI21088100002324013AA LT121000011101001000
      LU280019400644750000 MT84MALT011000012345MTLCAST001S NO9386011117947 PL61109010140000071219812874
      PT50000101231234567890192 SE4550000000058398257466 SI56191000000123438 SK3112000000198742637541
      UA213996220000026007233566001
    ]
  end

  describe "#parse" do
    it "skips an invalid iban" do
      expect(described_class.parse(invalid_iban)).to be_nil
    end

    it "skips an iban with unsuported country_code" do
      expect(described_class.parse(invalid_iban2)).to be_nil
    end

    it "parses ibans with all available country codes" do
      expect(ibans.map { |iban| described_class.parse(iban) }).to eq([
        {
          "account_number"      => "00234573201",
          "checksum"            => "61",
          "country_code"        => "AT",
          "identification_code" => "19043"
        },
        {
          "account_number"      => "0075470",
          "check_digit"         => "34",
          "checksum"            => "68",
          "country_code"        => "BE",
          "identification_code" => "539"
        },
        {
          "account_number"      => "20345678",
          "account_type"        => "10",
          "bic_code"            => "BNBG",
          "checksum"            => "80",
          "country_code"        => "BG",
          "identification_code" => "9661"
        },
        {
          "account_number"      => "0000001200527600",
          "checksum"            => "17",
          "country_code"        => "CY",
          "identification_code" => "002",
          "branch_code"         => "00128"
        },
        {
          "account_number"      => "0000192000145399",
          "checksum"            => "65",
          "country_code"        => "CZ",
          "identification_code" => "0800"
        },
        {
          "account_number"      => "0532013000",
          "checksum"            => "89",
          "country_code"        => "DE",
          "identification_code" => "37040044"
        },
        {
          "account_number"      => "0440116243",
          "checksum"            => "50",
          "country_code"        => "DK",
          "identification_code" => "0040"
        },
        {
          "account_number"      => "22102014568",
          "check_digit"         => "5",
          "checksum"            => "38",
          "country_code"        => "EE",
          "identification_code" => "22",
          "branch_code"         => "00"
        },
        {
          "account_number"      => "0200051332",
          "check_digit"         => "45",
          "checksum"            => "91",
          "country_code"        => "ES",
          "identification_code" => "2100",
          "branch_code"         => "0418"
        },
        {
          "account_number"      => "0000078",
          "check_digit"         => "5",
          "checksum"            => "21",
          "country_code"        => "FI",
          "identification_code" => "123456"
        },
        {
          "account_number"      => "0500013M026",
          "check_digit"         => "06",
          "checksum"            => "14",
          "country_code"        => "FR",
          "identification_code" => "20041",
          "branch_code"         => "01005"
        },
        {
          "account_number"      => "31926819",
          "bic_code"            => "NWBK",
          "checksum"            => "29",
          "country_code"        => "GB",
          "identification_code" => "601613"
        },
        {
          "account_number"      => "0000000012300695",
          "checksum"            => "16",
          "country_code"        => "GR",
          "identification_code" => "011",
          "branch_code"         => "0125"
        },
        {
          "account_number"      => "1863000160",
          "checksum"            => "12",
          "country_code"        => "HR",
          "identification_code" => "1001005"
        },
        {
          "account_number"      => "6111110180000000",
          "check_digit"         => "0",
          "checksum"            => "42",
          "country_code"        => "HU",
          "identification_code" => "117",
          "branch_code"         => "7301"
        },
        {
          "account_number"      => "12345678",
          "bic_code"            => "AIBK",
          "checksum"            => "29",
          "country_code"        => "IE",
          "identification_code" => "931152"
        },
        {
          "account_number"      => "0076545510730339",
          "checksum"            => "14",
          "country_code"        => "IS",
          "identification_code" => "0159",
          "branch_code"         => "26"
        },
        {
          "account_number"      => "000000123456",
          "check_digit"         => "X",
          "checksum"            => "60",
          "country_code"        => "IT",
          "identification_code" => "05428",
          "branch_code"         => "11101"
        },
        {
          "account_number"      => "0002324013AA",
          "checksum"            => "21",
          "country_code"        => "LI",
          "identification_code" => "08810"
        },
        {
          "account_number"      => "11101001000",
          "checksum"            => "12",
          "country_code"        => "LT",
          "identification_code" => "10000"
        },
        {
          "account_number"      => "9400644750000",
          "checksum"            => "28",
          "country_code"        => "LU",
          "identification_code" => "001"
        },
        {
          "account_number"      => "0012345MTLCAST001S",
          "bic_code"            => "MALT",
          "checksum"            => "84",
          "country_code"        => "MT",
          "identification_code" => "01100"
        },
        {
          "account_number"      => "111794",
          "check_digit"         => "7",
          "checksum"            => "93",
          "country_code"        => "NO",
          "identification_code" => "8601"
        },
        {
          "account_number"      => "0000071219812874",
          "check_digit"         => "4",
          "checksum"            => "61",
          "country_code"        => "PL",
          "identification_code" => "109",
          "branch_code"         => "0101"
        },
        {
          "account_number"      => "12345678901",
          "check_digit"         => "92",
          "checksum"            => "50",
          "country_code"        => "PT",
          "identification_code" => "0001",
          "branch_code"         => "0123"
        },
        {
          "account_number"      => "58398257466",
          "checksum"            => "45",
          "country_code"        => "SE",
          "identification_code" => "500000000"
        },
        {
          "account_number"      => "00001234",
          "check_digit"         => "38",
          "checksum"            => "56",
          "country_code"        => "SI",
          "identification_code" => "19",
          "branch_code"         => "100"
        },
        {
          "account_number"      => "0000198742637541",
          "checksum"            => "31",
          "country_code"        => "SK",
          "identification_code" => "1200"
        },
        {
          "account_number"      => "0000026007233566001",
          "checksum"            => "21",
          "country_code"        => "UA",
          "identification_code" => "399622"
        }
      ])
    end
  end
end
