require 'spec_helper'

describe SaltCommon::Sign do
  let(:payload)          { "https://github.com/saltedge/bucket/issues?id=1234" }
  let(:public_key_file)  { File.open(File.expand_path('../../../config/test_public.pem', __FILE__), "r").read }
  let(:private_key_file) { File.open(File.expand_path('../../../config/test_private.pem', __FILE__), "r").read }

  describe "#sign/verify" do
    it "signs / verify the payload with default digest" do
      signature = SaltCommon::Sign.new(
        data:             payload,
        private_key_file: private_key_file
      ).sign

      SaltCommon::Sign.new(
        data:            payload,
        public_key_file: public_key_file,
        signature:       signature
      ).verify.should be_truthy
    end


    it "signs / verify the payload with all digests" do
      SaltCommon::Sign::DIGESTS.keys.each do |digest|
        signature = SaltCommon::Sign.new(
          digest:           digest,
          data:             payload,
          private_key_file: private_key_file
        ).sign

        SaltCommon::Sign.new(
          digest:          digest,
          data:            payload,
          public_key_file: public_key_file,
          signature:       signature
        ).verify.should be_truthy

        SaltCommon::Sign.new(
          digest:          digest,
          data:            payload,
          public_key_file: public_key_file,
          signature:       "123#{signature}"
        ).verify.should be_falsey
      end
    end
  end

  describe "#valid_public?" do
    it "returns true if public key is valid" do
      SaltCommon::Sign.new(public_key_file: public_key_file).valid_public?.should be_truthy
    end

    it "returns true if a private key is sent as public key" do
      SaltCommon::Sign.new(public_key_file: private_key_file).valid_public?.should be_truthy
    end

    it "returns false if public_key_file is not a valid rsa key" do
      SaltCommon::Sign.new(public_key_file: "123").valid_public?.should be_falsey
    end
  end

  describe "#valid_private?" do
    it "returns true if private key is valid" do
      SaltCommon::Sign.new(private_key_file: private_key_file).valid_private?.should be_truthy
    end

    it "returns false if private_key_file is not a valid rsa key" do
      SaltCommon::Sign.new(private_key_file: "123").valid_private?.should be_falsey
    end

    it "returns false if private_key_file is not a private key" do
      SaltCommon::Sign.new(private_key_file: public_key_file).valid_private?.should be_falsey
    end
  end
end
