require "spec_helper"

RSpec.describe SaltCommon::SQLSanitizer do
  it "trims UPDATE queries" do
    described_class.sanitize(
      %Q{UPDATE "logins" SET "credentials_token" = 'SUPERSECRET'}
    ).should == %Q{UPDATE "logins"}
  end

  it "trims INSERT queries" do
    described_class.sanitize(
      %Q{INSERT INTO "accounts" ("login_id"), VALUES(secret)}
    ).should == %Q{INSERT INTO "accounts"}
  end

  it "filters strings from where clauses" do
    described_class.sanitize(
      %Q{SELECT "accounts".* FROM "accounts" WHERE "accounts"."name" = '5325 **** **** 1285'}
    ).should == %Q{SELECT "accounts".* FROM "accounts" WHERE "accounts"."name" = '[FILTERED]'}

    described_class.sanitize(
      %Q{SELECT \"payments\".* FROM \"payments\" WHERE \"payments\".\"status\" != 'deleted'}
    ).should == %Q{SELECT \"payments\".* FROM \"payments\" WHERE \"payments\".\"status\" != '[FILTERED]'}

    described_class.sanitize(
      %Q{SELECT "accounts".* FROM "accounts\" WHERE "accounts"."login_id" = 182726438183504074 AND "accounts"."name" IN ('secret1', 'secret2', 'secret3')}
    ).should == %Q{SELECT "accounts".* FROM "accounts" WHERE "accounts"."login_id" = 182726438183504074 AND "accounts"."name" IN ('[FILTERED]', '[FILTERED]', '[FILTERED]')}
  end

  it "does not fail if it sees an unrecognized string" do
    described_class.sanitize("HI POTATO").should == "HI POTATO"
  end
end
