require 'spec_helper'

describe SaltCommon::Worker do
  class DummyLogger
    def self.info(message)
    end
  end

  let(:worker) do
    Class.new(SaltCommon::Worker) do
      def send_mail
        true
      end

      def after
        send_log("bucket", "job")
      end

      def self.logger
        DummyLogger
      end
    end
  end

  describe "#perform" do
    subject { worker.new }

    it "calls all necessary methods" do
      subject.should_receive(:before)
      subject.should_receive(:run)
      subject.should_receive(:after)
      subject.should_receive(:send_mail)
      subject.stub(:run) { sleep 1.45 }
      subject.perform
      expect(subject.hash[:message]).to eq(worker.to_s)
      subject.hash[:duration].should be_within(0.5).of(1.5)
    end
  end

  describe "#send_mail" do
    subject { -> { SaltCommon::Worker.new.send(:send_mail) } }

    it { is_expected.to raise_error(NotImplementedError) }
  end

  describe "#logger" do
    subject { -> { SaltCommon::Worker.new.send(:logger) } }

    it { is_expected.to raise_error(NotImplementedError) }
  end

  describe ".logger" do
    subject { -> { SaltCommon::Worker.send(:logger) } }

    it { is_expected.to raise_error(NotImplementedError) }
  end

  describe "#logger_message" do
    subject { worker }

    it "logger should receive info method with params" do
      subject.logger.should_receive(:info).with({
        message: subject.to_s,
        duration: 0.0,
        project: "bucket",
        service: "job",
        params:  { one: 1, two: { three: 3, four: "hello" } }
      })

      subject.new.perform({ one: 1, two: { three: 3, four: "hello" } })
    end
  end

  describe "#send_log" do
    subject { worker.new }
    context 'when log? is true' do
      it 'sends log' do
        worker.logger.should_receive(:info).with({
          message: worker.to_s,
          project: "bucket",
          duration: 0.0,
          service: "job",
          params:  { a: 1 }
        })
        subject.perform({ a: 1 })
      end
    end

    context 'when log? is false' do
      it "doesn't send log" do
        worker.logger.should_not_receive(:info)
        subject.stub(:log?) { false }
        subject.perform({ a: 1 })
      end
    end
  end

  describe "#deliver?" do
    subject { worker.new.deliver? }

    it { is_expected.to be_truthy }
  end

  describe "#log?" do
    subject { worker.new.log? }

    it { is_expected.to be_truthy }
  end
end
