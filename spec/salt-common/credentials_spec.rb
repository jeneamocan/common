require 'spec_helper'

describe SaltCommon::Credentials do
  let(:data) {
    {
      "data" => {
        "login"    => "login",
        "password" => "password"
      }
    }
  }
  let(:data2) { { "data" => { "name" => "name" } } }
  let(:interactive_data) { { "data" => { "sms" => "123456" } } }
  let(:fields) {
    [
      {
        nature:         :text,
        name:           "login",
        position:       1,
        optional:       false,
        extra:          {},
        english_name:   "Login",
        localized_name: "Login",
        checksummable:  true
      },
      {
        nature:         :text,
        name:           "name",
        position:       1,
        optional:       false,
        extra:          {},
        english_name:   "Name",
        localized_name: "Name",
        checksummable:  true
      },
      {
        nature:         :password,
        name:           "password",
        position:       2,
        optional:       false,
        extra:          {},
        english_name:   "Password",
        localized_name: "Password",
        checksummable:  false
      }
    ]
  }
  let(:interactive_fields) {
    [
      {
        nature:         :text,
        name:           "sms",
        optional:       false,
        checksummable:  false,
        localized_name: "Sms"
      },
      { nature:         :text,
        name:           "token",
        optional:       true,
        checksummable:  false,
        localized_name: "Token"
      }
    ]
  }
  let(:config) {
    double(
      required_fields:    fields,
      interactive_fields: interactive_fields,
      beta:               false
    )
  }
  let(:credentials)         { SaltCommon::Credentials.new(config, item) }
  let(:id)                  { "111" }
  let(:params)              {
    {
      "provider_code"           => "victoriabank_md",
      "id"                      => id,
      "host"                    => "localhost",
      "session_id"              => "1",
      "pid"                     => 1111,
      "credentials"             => data,
      "internal_data"           => { "credentials" => "data"},
      "interactive_credentials" => interactive_data
    }
  }
  let(:item) {
    Storage::Test::Item.new(id).read(
      SaltCommon::JsonWrapper.encode(params)
    )
  }

  before(:each) do
    stub_const("Settings", double(
      beta_private_key: "beta",
      private_key:      "not beta"
    ))
  end

  describe ".filter!" do
    it "removes credentials keys from hash" do
      # NOTE: Method filter! works with any hash, ex: insecure_to_h should be cleaned as well
      hash = item.insecure_to_h.merge(
        "client_provider_token" => { "app_secret" => "very_secret" },
        "encrypted_rememberable_credentials" => { algorithm: "AES-256-CBC", key: "qwerty" }
      )
      hash["credentials"].should_not be_nil
      hash["interactive_credentials"].should_not be_nil

      new_hash = SaltCommon::Credentials.filter!(hash)
      new_hash["credentials"].should                        == "[FILTERED]"
      new_hash["interactive_credentials"].should            == "[FILTERED]"
      new_hash["internal_data"]["credentials"].should       == "[FILTERED]"
      new_hash["encrypted_rememberable_credentials"].should == "[FILTERED]"
      new_hash["client_provider_token"].should              == "[FILTERED]"
    end

    it "removes some credentials keys from hash and keeps several ones" do
      hash = item.insecure_to_h
      hash["credentials"].should_not be_nil
      hash["interactive_credentials"].should_not be_nil

      new_hash = SaltCommon::Credentials.filter!(hash, [:interactive_credentials])

      new_hash["credentials"].should                  == "[FILTERED]"
      new_hash["interactive_credentials"].should_not  == "[FILTERED]"
      new_hash["internal_data"]["credentials"].should == "[FILTERED]"
    end

    it "removes some credentials keys from an array of hashes" do
      hash = item.insecure_to_h
      hash["credentials"].should_not be_nil
      hash["interactive_credentials"].should_not be_nil

      batch_params = [] << hash << hash
      clean_batch_params = SaltCommon::Credentials.filter!(batch_params, [:interactive_credentials])

      clean_batch_params.first["credentials"].should                  == "[FILTERED]"
      clean_batch_params.first["interactive_credentials"].should_not  == "[FILTERED]"
      clean_batch_params.first["internal_data"]["credentials"].should == "[FILTERED]"
    end
  end

  describe "#set_required_credentials" do
    it "defines required credentials" do
      credentials.set_required_credentials(data)
      credentials.set_required_credentials(data2)
      credentials.login.should    == data["data"]["login"]
      credentials.password.should == data["data"]["password"]
      credentials.name.should     == data2["data"]["name"]
    end

    it "does not define credentials methods if params invalid" do
      credentials.stub(:required_fields).and_return([])
      expect { credentials.set_required_credentials({}) }.to raise_error(SaltCommon::Credentials::EmptyRequiredFields)
    end

    it "raises error if one of the fields was not set" do
      credentials.set_required_credentials({"data" => {"login" => "login"}})

      SaltCommon::Credentials::InvalidCredentialsKeys.any_instance
        .should_receive(:update)
        .with(item.secure_to_h)

      SaltCommon::Credentials::InvalidCredentialsKeys
        .any_instance
        .should_receive(:notify)
        .with(fail_callback: false)

      expect { credentials.password }.to raise_error(SaltCommon::Credentials::InvalidCredentials)
    end
  end

  describe "#set_interactive_credentials" do
    it "defines interactive credentials" do
      credentials.set_interactive_credentials(["sms"], item.interactive_credentials)
      credentials.sms.should == interactive_data["data"]["sms"]
    end

    it "does not raise error if all predefined fields_names were given" do
      expect { credentials.set_interactive_credentials(["sms"], { "data" => {"sms" => "123", "token" => "321"} }) }.to_not raise_error
      credentials.sms.should   == "123"
      credentials.token.should == "321"
    end

    it "raises error if one of the fields predefined fields_names was not set" do
      credentials.set_interactive_credentials(["sms"], { "data" => { "token" => "321" } })
      expect { credentials.token }.to_not raise_error
      SaltCommon::Credentials::InvalidCredentialsKeys.any_instance
        .should_receive(:update)
        .with(item.secure_to_h)
      SaltCommon::Credentials::InvalidCredentialsKeys.any_instance.should_receive(:notify)
      expect { credentials.sms }.to raise_error(SaltCommon::Credentials::InvalidCredentials)
    end
  end

  describe "#decrypt(data)" do
    let(:public_key) { File.read("config/test_public.pem") }
    let(:private_key) { File.read("config/test_private.pem") }
    let(:credentials) do
      {
        "data" => {
          "login"    => "LOGIN",
          "password" => "PASSWORD"
        }
      }
    end

    it "decrypts encrypted string" do
      crypt_params = credentials.merge("public_key_file" => public_key)
      encrypted_credentials = SaltCommon::Crypt.new(crypt_params).encrypt

      SaltCommon::Credentials.decrypt(encrypted_credentials, private_key).should == credentials["data"]
    end

    it "decrypts encrypted string with OAEP padding" do
      crypt_params = credentials.merge(
        "public_key_file" => public_key,
        "padding"         => OpenSSL::PKey::RSA::PKCS1_OAEP_PADDING
      )
      encrypted_credentials = SaltCommon::Crypt.new(crypt_params).encrypt

      SaltCommon::Credentials.decrypt(encrypted_credentials, private_key).should == credentials["data"]
    end
  end
end
