require 'spec_helper'

describe SaltCommon::Ci::Runner do
  describe "#log" do
    it "prints out log to STDERR" do
      STDERR.should_receive(:puts).with("rspec")
      subject.log("rspec")
    end
  end

  describe "#git_master?" do
    it "returns false if git branch isn't in MASTER_BRANCHES" do
      ENV.stub(:[]).with("GIT_BRANCH").and_return("rspec")
      subject.git_master?.should be_falsey
    end

    it "returns true if git branch is in MASTER_BRANCHES" do
      ENV.stub(:[]).with("GIT_BRANCH").and_return("origin/master")
      subject.git_master?.should be_truthy
      ENV.stub(:[]).with("GIT_BRANCH").and_return("origin/STAGING")
      subject.git_master?.should be_truthy
      ENV.stub(:[]).with("GIT_BRANCH").and_return("origin/PRODUCTION")
      subject.git_master?.should be_truthy
    end
  end

  describe "#specs_failed?" do
    it "returns true if no failing specs" do
      RSpec.configuration.reporter.should_receive(:failed_examples).and_return([1])
      subject.specs_failed?.should be_truthy
    end
  end

  describe "#spec_run" do
    it "runs the specs" do
      subject.should_receive(:log).with("Running specs with options: #{subject.configuration.options.join(' ')}")
      subject.should_receive :set_environment
      RSpec.should_receive :reset
      RSpec::Core::Runner.should_receive(:run).with(subject.configuration.options)
      subject.should_receive :unset_environment
      subject.stub(:specs_failed?) { true }
      subject.should_receive(:exit).with(1)
      subject.specs_run
    end
  end

  describe "#specs_exit" do
    it "sets status variable to 1 if there are failed specs" do
      RSpec.configuration.reporter.should_receive(:failed_examples).and_return([1])
      subject.should_receive(:exit).with(1)
      subject.specs_exit
      subject.status.should == 1
    end
  end

  describe "#set_environment / #unset_environment" do
    it "sets ENV variables" do
      subject.configuration.environments = {
        "HEADLESS" => true,
        "TEST_1"   => false
      }
      subject.set_environment
      ENV["HEADLESS"].should == "true"
      ENV["TEST_1"].should   == "false"
      subject.unset_environment
    end

    it "cleans ENV variables" do
      subject.configuration.environments = {
        "HEADLESS" => true,
        "TEST_1"   => false
      }
      subject.set_environment
      subject.unset_environment
      ENV["HEADLESS"].should be_nil
      ENV["TEST_1"].should be_nil
    end
  end

  describe "#run" do
    it "configures rspec and launches specs" do
      subject.should_receive(:at_exit)

      subject.should_receive(:build_config)
      subject.should_receive(:specs_run)

      subject.run { |c| c.rspec = "spec" }
    end
  end

  describe "#build_config / #configuration" do
    it "builds configurations" do
      subject.build_config do |c|
        c.options      = %w(--color)
        c.environments = { "HEADLESS" => false }
      end

      subject.configuration.options.should      == %w(--color)
      subject.configuration.environments.should == { "HEADLESS" => false }
    end
  end

  describe "#coverage_remove" do
    it "removes coverage folder" do
      FileUtils.should_receive(:rm_rf).with("coverage/")

      subject.coverage_remove
    end
  end

  describe "#file_copy" do
    it "copies files and removes the destination" do
      FileUtils.should_receive(:copy_entry).with("source", "destination", true)

      subject.file_copy("source", "destination")
    end
  end

  describe "#file_truncate" do
    it "truncates an existing log file" do
      File.stub(:exists?).and_return(true)
      File.should_receive(:truncate).with("filename", 0)

      subject.file_truncate("filename")
    end
  end

  describe "#rails_migration" do
    it "drops, creates and migrates the database" do
      subject.stub(:system).and_return(true)
      subject.should_receive(:system).with("rake db:drop")
      subject.should_receive(:system).with("rake db:create")
      subject.should_receive(:system).with("rake db:migrate")

      subject.rails_migration
    end
  end

  describe "#teaspoon_run" do
    it "runs JS tests with given options" do
      subject.should_receive(:log).with("Testing javascript with options: options")
      subject.stub(:system).and_return(true)
      subject.should_receive(:system).with("FORMATTERS=tap bundle exec teaspoon options")

      subject.teaspoon_run("options")
    end
  end
end
