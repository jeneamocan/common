require 'spec_helper'

describe SaltCommon::JsonWrapper do
  let(:hash)   { { test: "value" } }
  let(:string) { "{\"test\":\"value\"}" }

  describe ".encode" do
    it "returns a JSON representation of a hash" do
      json = SaltCommon::JsonWrapper.encode(hash)
      json.should eq("{\"test\":\"value\"}")
    end

    it "returns a hash from JSON string" do
      hash = SaltCommon::JsonWrapper.decode(string)
      hash.should eq({ "test" => "value" })
    end
  end
end
