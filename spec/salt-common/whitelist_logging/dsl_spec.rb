require 'spec_helper'
require 'salt-common/whitelist_logging/dsl'

describe SaltCommon::WhitelistLogging::DSL do
  describe '.log_params' do
    subject do
      Class.new do
        include SaltCommon::WhitelistLogging::DSL
        log_params :a, b: [:c]
      end
    end

    it 'populates log_whitelist' do
      expect(subject.log_whitelist).to eq({a: true, b: { c: true } })
    end

    it 'includes parent log params as well' do
      child = Class.new(subject) { log_params :four, b: %i[c d] }

      expect(child.log_whitelist).to eq({a: true, b: { c: true, d: true }, four: true })
    end
  end

  describe '#log_params' do
    subject do
      Class.new do
        include SaltCommon::WhitelistLogging::DSL
        log_params :a

        def first
          log_params :b
        end

        def second
          log_params :c
        end

        def current_action_log_whitelist
          @current_action_log_whitelist
        end
      end
    end

    it 'populates current action log whitelist' do
      first_instance = subject.new
      first_instance.first
      expect(first_instance.current_action_log_whitelist).to eq({b: true})

      second_instance = subject.new
      second_instance.second
      expect(second_instance.current_action_log_whitelist).to eq({c: true})
    end
  end

  describe '#santized_params' do
    subject do
      Class.new do
        include SaltCommon::WhitelistLogging::DSL
        log_params :a, b: [:c]

        def add_instance_params
          log_params b: [:d]
        end

        def perform(params)
          sanitized_params(params)
        end
      end
    end

    context 'when only log_whitelist set' do
      it 'sanitizes params' do
        params = { a: 1, b: { c: 'hello', d: 3 }, e: 5 }

        expect(subject.new.perform(params)).to eq({
          a: 1,
          b: {
            c: 'hello',
            '_filtered_keys' => [:d]
          },
          '_filtered_keys' => [:e]
        })
      end
    end

    context 'when current_action_log_whitelist also set' do
      it 'sanitizes params' do
        params = { a: 1, b: { c: 'hello', d: 3 }, e: 5 }

        instance = subject.new
        instance.add_instance_params
        expect(instance.perform(params)).to eq({
          a: 1,
          b: {
            c: 'hello',
            d: 3,
            '_filtered_keys' => []
          },
          '_filtered_keys' => [:e]
        })
      end
    end
  end
end
