require 'spec_helper'
require 'salt-common/whitelist_logging/sanitizer'

describe SaltCommon::WhitelistLogging::Sanitizer do
  describe '.build_mapping' do
    it 'should build mapping for nested hash' do
      result = subject.class.build_mapping([:id, test: [:id, nested: [:hello]]])

      expect(result).to eq({ id: true, test: { id: true, nested: { hello: true } } })
    end
  end

  describe '.sanitize_params' do
    context 'when whitelist is empty' do
      it 'it should put only top-level keys in _filtered_keys' do
        whitelist = {}
        params = {
          level1: {
            level2: {
              level3: {
                level4: [1, 2, 3]
              }
            }
          }
        }

        result = described_class.sanitize_params(params, whitelist)

        expect(result).to eq(
          '_filtered_keys' => [:level1]
        )
      end

      it 'should filter hashes in array values' do
        whitelist = {}
        params = { ids: [{ key: 1 }] }

        result = described_class.sanitize_params(params, whitelist)

        expect(result).to eq(
          ids: [ { "_filtered_keys" => [:key] }],
          "_filtered_keys" => []
        )
      end
    end

    context 'when some params are whilisted' do
      it 'should santize params that not in whitelist' do
        whitelist = { id: true, nested: { name: true } }
        params = {
          id: 1,
          some_bool: true,
          nested: { name: 'test', other: 'ok' },
          name: 'blacklisted'
        }

        result = subject.class.sanitize_params(params, whitelist)

        expect(result).to eq({
          id: 1,
          nested: {
            name: 'test',
            '_filtered_keys' => [:other]
          },
          '_filtered_keys' => [:some_bool, :name]
        })
      end
    end

    context 'when some params are whilisted and params is an array' do
      it 'should santize params that not in whitelist for each element' do
        whitelist = { id: true, nested: { name: true } }
        params = [
          { id: 1, nested: { name: 'test', other: 'ok' }, name: 'blacklisted' },
          { id: 2, nested: { name: 'test2', other: 'ok' }, name: 'blacklisted2' }
        ]

        result = subject.class.sanitize_params(params, whitelist)

        expect(result).to eq([
          {
            id: 1,
            nested: {
              name: 'test',
              '_filtered_keys' => [:other]
            },
            '_filtered_keys' => [:name]
          },
          {
            id: 2,
            nested: {
              name: 'test2',
              '_filtered_keys' => [:other]
            },
            '_filtered_keys' => [:name]
          }
        ])
      end
    end
  end
end
