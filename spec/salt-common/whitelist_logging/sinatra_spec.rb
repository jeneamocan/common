require "spec_helper"
require "rack/test"
require "sinatra"
require "salt-common/whitelist_logging/sinatra"

describe SaltCommon::WhitelistLogging::Sinatra do
  include Rack::Test::Methods

  class DummyLogger
    def self.info(*args)
    end
  end

  let(:app_class) do
    Class.new(Sinatra::Base) do
      register SaltCommon::WhitelistLogging::Sinatra
      log_with_whitelist DummyLogger do |env|
        {
          project: "robber",
          service: "sinatra"
        }
      end
    end
  end

  let(:logger) { App.logger }
  let(:app) { app_class.new }

  def assert_logged_params(payload)
    expected_params = if payload.is_a?(Hash)
      payload.deep_stringify_keys
    else
      payload.map(&:deep_stringify_keys)
    end

    DummyLogger
      .should_receive(:info)
      .with(hash_including(
        project: "robber",
        service: "sinatra",
        params:   expected_params))
  end

  context "with custom options" do
    let(:app_class) do
      Class.new(Sinatra::Base) do
        register SaltCommon::WhitelistLogging::Sinatra
        log_with_whitelist DummyLogger do |env|
          { datacenter: "de", db: 100 }
        end
      end
    end

    let(:app) { app_class.new }

    it "it logs correctly" do
      klass = Class.new(Sinatra::Base) do
        register SaltCommon::WhitelistLogging::Sinatra
        log_with_whitelist DummyLogger do |env|
          { datacenter: "de", db: 100 }
        end
      end

      app = klass.new

      DummyLogger
        .should_receive(:info)
        .with(hash_including(datacenter: "de", db: 100))

      get "/"
    end

    it "accepts began_at in float format" do
      app        = app_class.new
      middleware = SaltCommon::WhitelistLogging::Sinatra::Middleware.new(app, DummyLogger)
      env        = { "REQUEST_METHOD" => "", "PATH_INFO" => "", "REMOTE_ADDR" => "", "HTTP_USER_AGENT" => "" }
      expect { middleware.send(:log, env, 200, {}, 350895.555875494) }.to_not raise_error
    end
  end


  it "filters everything by default except booleans, preserving structure" do
    assert_logged_params(
      array:          ["value1", "value2"],
      _filtered_keys: ["foo", "nested", "safe_bool"]
    )

    get "/",
      foo:       "bar",
      nested:    { secret: "secret" },
      array:     ["value1", "value2"],
      safe_bool: true
  end

  it "allows to whitelist keys" do
    app_class.get "/" do
      log_params :one
      "OK"
    end

    assert_logged_params(one: "safe", _filtered_keys: ["two"])
    get "/", one: "safe", two: "secret"
  end

  it "allows to whitelist nested hashes" do
    app_class.get "/" do
      log_params internal_data: [:safe_key]
      "OK"
    end

    assert_logged_params(
      _filtered_keys: [],
      internal_data: {
        safe_key: "safe",
        _filtered_keys: ["secret"]
      }
    )
    get "/", internal_data: { safe_key: "safe", secret: "secret" }
  end

  it "does not fail on a post request with empty body" do
    app_class.post("/") { "OK" }
    expect { post "/" }.to_not raise_error
  end

  it "can whitelist array with hashes in the root of the request" do
    app_class.post "/" do
      log_params :safe_key
      "OK"
    end

    assert_logged_params(
      [
        { safe_key: "safe", _filtered_keys: ["secret"] }
      ]
    )

    post "/", [safe_key: "safe", secret: "secret"].to_json
  end

  it "allows to filter arrays" do
    app_class.get "/" do
      log_params array: [:safe_key]
      "OK"
    end

    assert_logged_params(
      _filtered_keys: [],
      array: [
        { safe_key: "safe", _filtered_keys: ["secret"] }
      ]
    )
    get "/", array: [{ safe_key: "safe", secret: "secret" }]
  end

  # NOTE: this functionality was added to transition to whitelist logging
  # without breaking logstash schema. In current implementations, *credentials_token
  # is sent to logstash as [FILTERED], but the whitelist logging sends a hash
  it "allows to filter hash as [FILTERED] string" do
    app_class.get "/" do
      log_params(
        credentials_token: :legacy_filtered,
        interactive_credentials_token: :legacy_filtered
      )

      "OK"
    end

    assert_logged_params(
      _filtered_keys: [],
      credentials_token: "[FILTERED]",
      interactive_credentials_token: "[FILTERED]"
    )

    get "/",
      credentials_token: { key: "key", data: "data" },
      interactive_credentials_token: nil
  end

  it "logs unicorn stats if X-Request-Start header is present" do
    app_class.get "/" do
      "OK"
    end

    DummyLogger.should_receive(:info) do |options|
      options[:queue_duration].should > 0
      options[:e2e_duration].should == options[:queue_duration] + options[:duration]
    end

    header "X-Request-Start", 1.second.ago.to_f.to_s
    get "/"
  end
end
