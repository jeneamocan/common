require 'spec_helper'

describe SaltCommon::Crypt do
  let(:params) {
    {
      "data" => {
        "login"    => "LOGIN",
        "password" => "PASSWORD"
      }
    }
  }

  let(:crypt_keys) {
    {
      "public_key_file"  => File.read("config/test_public.pem"),
      "private_key_file" => File.read("config/test_private.pem")
    }
  }

  describe ".encrypt/.decrypt" do
    it "encrypts/decrypts hash" do
      crypted = SaltCommon::Crypt.new(params.merge(crypt_keys)).encrypt
      crypted["data"].class.should == String

      crypted_params         = params.dup
      crypted_params["data"] = crypted["data"]
      crypted_params["key"]  = crypted["key"]
      crypted_params["iv"]   = crypted["iv"]

      decrypted = SaltCommon::Crypt.new(crypted_params.merge(crypt_keys)).decrypt
      decrypted.class.should == Hash

      decrypted.should == params["data"]
    end

    it "encrypts/decrypts hash with oaep rsa padding" do
      options = params.merge(crypt_keys).merge(
        "padding" => OpenSSL::PKey::RSA::PKCS1_OAEP_PADDING
      )

      crypted = SaltCommon::Crypt.new(options).encrypt
      crypted["data"].class.should == String

      crypted_params            = params.dup
      crypted_params["data"]    = crypted["data"]
      crypted_params["key"]     = crypted["key"]
      crypted_params["iv"]      = crypted["iv"]
      crypted_params["padding"] = options["padding"]

      decrypted = SaltCommon::Crypt.new(crypted_params.merge(crypt_keys)).decrypt
      decrypted.class.should == Hash

      decrypted.should == params["data"]
    end
  end
end
