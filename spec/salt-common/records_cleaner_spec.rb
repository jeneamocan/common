require 'spec_helper'

describe SaltCommon::RecordsCleaner do
  let(:dir)      { "md/victoriabank" }
  let(:records)  { File.expand_path("spec/fixtures/records") }
  let(:list) {
    [
      "md/victoriabank/spec.png",
      "md/victoriabank/spec.html",
      "md/victoriabank/spec.txt",
    ]
  }

  before(:each) do
    stub_const("Settings", double(
        service_name: "spec",
        recorder: double(path: "/tmp", lifetime: 30),
      )
    )
  end

  context "Object methods" do
    describe "#initialize" do
      it "sets path variable" do
        subject.path.should == "#{Settings.recorder.path}/**/*.*"
      end
    end

    describe "#clear" do
      it "deletes all files from folder older than 30 days" do
        subject.should_receive(:list).and_return([list[0]])

        File.should_receive(:dirname).once.ordered.with(list[0]).and_return(dir)
        FileUtils.should_receive(:rm_rf).once.ordered.with(list[0])

        Dir.should_receive(:glob).with("#{dir}/*").and_return([])
        FileUtils.should_receive(:rm_rf).with(dir)

        subject.should_receive(:clean_empty_direcories)

        subject.clear
      end

      it "deletes all files from folder older than 30 days" do
        subject.should_receive(:list).and_return([list[0]])

        File.should_receive(:dirname).once.ordered.with(list[0]).and_return(dir)
        FileUtils.should_receive(:rm_rf).once.ordered.with(list[0])

        Dir.should_receive(:glob).with("#{dir}/*").and_return(["#{dir}/not_empty"])
        FileUtils.should_not_receive(:rm_rf).with(dir)

        subject.should_receive(:clean_empty_direcories)

        subject.clear
      end
    end
  end

  context "Private methods" do
    describe "#clean_empty_direcories" do
      it "executes a shell command to clean all empty directories in records" do
        File.should_receive(:dirname).with(subject.path).and_return(records)
        subject.should_receive(:system).with("find #{records} -type d -empty -mtime +30 -delete 2>/dev/null")

        subject.send(:clean_empty_direcories)
      end

      it "does not execute a shell command to clean all empty directories in records" do
        File.should_receive(:dirname).with(subject.path).and_return("/tmp/**")

        expect { subject.send(:clean_empty_direcories) }.to raise_error("ACHTUNG! Tried to clean records in wrong directory /tmp/** for spec")
      end
    end

    describe "#list" do
      it "returns all files directories from given path" do
        Dir.should_receive(:glob).with(subject.path).and_return(list)
        subject.send(:list).should be_a(Array)
      end
    end

    describe "#include?(file)" do
      it "returns false if file is not a file" do
        File.stub(:file?).with(list[0]).and_return(false)
        subject.send(:include?, list[0]).should be_falsey
      end

      it "returns false if file is not a file" do
        File.stub(:file?).with(list[0]).and_return(true)
        File.stub(:mtime).with(list[0]).and_return(subject.send(:time) - 1.day)
        subject.send(:include?, list[0]).should be_truthy
      end

      it "returns false if dir is a directory and its created time is not older than 30 days" do
        File.stub(:file?).with(list[0]).and_return(true)
        File.stub(:mtime).with(list[0]).and_return(subject.send(:time) + 1.day)
        subject.send(:include?, list[0]).should be_falsey
      end

      it "returns true if dir is a directory and its created time is older than 30 days" do
        File.stub(:file?).with(list[0]).and_return(true)
        File.stub(:mtime).with(list[0]).and_return(subject.send(:time) - 1.day)
        subject.send(:include?, list[0]).should be_truthy
      end
    end

    describe "#time" do
      it "returns 30 days ago" do
        Timecop.freeze(Time.local(2015, 6, 4)) do
          subject.send(:time).should == Time.local(2015, 5, 5)
        end
      end
    end

    describe "#check_if_old?(dir)" do
      it "returns true if dir is older than 30 days" do
        File.stub(:mtime).with(list[0]).and_return(subject.send(:time) - 1.day)
        subject.send(:check_if_old?, list[0]).should be_truthy
      end

      it "returns false if dir is not older than 30 days" do
        File.stub(:mtime).with(list[0]).and_return(subject.send(:time) + 1.day)
        subject.send(:check_if_old?, list[0]).should be_falsey
      end
    end
  end
end
