require 'spec_helper'

describe SaltCommon::RestApi do
  let(:url)     { "http://www.example.com" }
  let(:retries) { 2 }
  let(:params)  { { test: 123 } }
  let(:headers) { { app_secret: "TEST_SECRET" } }
  let(:proxy)   { "http://example.com:3242" }

  describe "Class methods" do
    it "adds new parameters to the query string" do
      SaltCommon::RestApi.parse_url(url, params).should == "http://www.example.com?test=123"
    end

    context "with already existing query parameters" do
      let(:url) { "http://bucket.banksalt.com/api/v2/customers?login_id=11" }

      it "adds the additional parameters to the query" do
        SaltCommon::RestApi.parse_url(url, params).should == "http://bucket.banksalt.com/api/v2/customers?login_id=11&test=123"
      end
    end
  end

  context "Constants" do
    describe "HTTP_ERRORS" do
      it "includes an array of http errors" do
        SaltCommon::RestApi::HTTP_ERRORS.should == [
          EOFError,
          Errno::ECONNRESET,
          Errno::ECONNREFUSED,
          Errno::EHOSTUNREACH,
          Errno::EINVAL,
          Net::HTTPBadResponse,
          Net::HTTPHeaderSyntaxError,
          Net::ProtocolError,
          Timeout::Error,
          RestClient::Exception,
          SocketError
        ]
      end
    end
  end

  context "Public methods" do
    describe "#initialize" do
      it "assigns all params" do
        rest_api = SaltCommon::RestApi.new(url, error_mode: "error_mode", params: params, headers: headers, retries: retries, proxy: proxy)
        rest_api.url.should        == url
        rest_api.retries.should    == retries
        rest_api.error_mode.should == "error_mode"
        rest_api.params.should     == params
        rest_api.headers.should    == {
          app_secret:   "TEST_SECRET",
          content_type: :json,
          accept:       :json,
          user_agent:   "Salt Edge"
        }
        rest_api.proxy.should      == proxy
      end

    end
    describe "#hash(method)" do
      it "returns hashed attributes for built request" do
        rest_api = SaltCommon::RestApi.new(url, params: params, headers: headers, proxy: proxy)

        hash = rest_api.to_hash("POST")
        hash[:method].should  == "POST"
        hash[:url].should     == url
        hash[:params].should  == params
        hash[:headers].should == headers.merge(
          content_type: :json,
          accept:       :json,
          user_agent:   "Salt Edge"
        )
        hash[:proxy].should   == proxy
      end
    end
  end

  context "Dynamic methods" do
    describe "#get #post #patch #put #delete #head" do
      SaltCommon::RestApi::REQUESTS.each do |request|
        it "sends #{request} request" do
          SaltCommon::RestApi.any_instance.should_receive(:send_request).with(request.to_sym)
          SaltCommon::RestApi.new(url, error_mode: "error_mode", params: params, headers: headers).send(request)
        end
      end
    end
  end

  describe "#execute_with_retry" do
    it "delegates the call to RestClient with retries" do
      error    = SaltCommon::RestApi::HTTP_ERRORS.sample.new
      retries  = 3
      method   = :get
      rest_api = SaltCommon::RestApi.new(url, error_mode: "error_mode", params: params, headers: headers, retries: retries)

      error.should_receive(:update).with(
        params.merge(
          {
            url:               rest_api.url,
            last_request_time: rest_api.execution_time,
            open_timeout:      rest_api.open_timeout,
            read_timeout:      rest_api.read_timeout
          }
        )
      )

      error.should_receive(:notify).with(fail_callback: false)
      rest_api.should_receive(:send_request).with(method).exactly(retries).times.and_raise(error)

      expect { rest_api.execute_with_retry(method) }.to raise_error(error)
    end
  end

  context "Private methods" do
    describe "#send_request" do
      it "delegates the call to RestClient" do
        SaltCommon::RestApi.any_instance.should_receive(:notify)
        RestClient::Request.should_receive(:execute)
          .with(
            method:  :get,
            url:     url + "?test=123",
            headers: {
              app_secret:   "TEST_SECRET",
              content_type: :json,
              accept:       :json,
              user_agent:   "Salt Edge"
            },
            proxy:        proxy,
            open_timeout: 10,
            read_timeout: 10
          ).and_return(SaltCommon::JsonWrapper.encode({"response" => "ok"}))

        response = SaltCommon::RestApi.new(url, error_mode: "error_mode", params: params, headers: headers, proxy: proxy).send(:send_request, :get)
        SaltCommon::JsonWrapper.decode(response).should == { "response" => "ok"}
      end

      it "delegates the call to RestClient with no payload if params are nil" do
        SaltCommon::RestApi.any_instance.should_receive(:notify)
        RestClient::Request.should_receive(:execute)
          .with(
            method:  :get,
            url:     url,
            headers: {
              app_secret:   "TEST_SECRET",
              content_type: :json,
              accept:       :json,
              user_agent:   "Salt Edge"
            },
            open_timeout: 10,
            read_timeout: 10
          )

        SaltCommon::RestApi.new(url, error_mode: "error_mode", headers: headers).send(:send_request, :get)
      end
    end

    describe "#handle_exception" do
      it "handles the exception" do
        error = StandardError.new
        expect { SaltCommon::RestApi.new("/", error_mode: "test").send(:handle_exception, error) }.to raise_error(error)
      end
    end
  end
end
