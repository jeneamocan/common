lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'salt-common/version'


Gem::Specification.new do |spec|
  spec.name          = "salt-common"
  spec.version       = SaltCommon::VERSION
  spec.platform      = Gem::Platform::RUBY
  spec.authors       = ["wipxj3"]
  spec.email         = ["vladimirp@saltedge.com"]
  spec.description   = %q{Simple container gem for commonly used extensions.}
  spec.summary       = %q{Salt Edge common pieces of code extracted here.}
  spec.homepage      = "https://git.saltedge.com/shared/salt-common"
  spec.license       = "Proprietary"

  spec.files         = `git ls-files`.split($/)
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency 'iso_country_codes'
  spec.add_dependency 'countries'
  spec.add_dependency 'rest-client', '~> 2.0.2'
  spec.add_dependency 'aws-sdk-s3', '~> 1.94.1'
  spec.add_dependency 'ibandit', '~> 1.7.1'
  spec.add_dependency 'konto_check', '~> 6.13.0'
  spec.add_development_dependency 'bundler', '~> 1.3'
end
