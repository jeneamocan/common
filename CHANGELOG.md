## 0.7.0 (2015-07-22)

Breaking changes:

  - `SaltCommon::S3` now requires `aws-sdk` gem instead of `aws-s3`
  - Options for `SaltCommon::S3` object are `["bucket", "region", "aws_access_key_id", "aws_secret_access_key"]`, `"host"` is not required anymore

## 0.7.26 (2016-01-19)
  - added Bucket, Categorizer and Desk connectors
  - send service errors to desk

## 0.8.0 (2016-01-19)
  - changed transaction & account extra fields validation. Now checks for field type

## 0.8.1 (2016-01-21)
  - extra keys from now on are checked for valid formatting

## 0.8.2 (2016-01-21)
  - remove `--format documentation` from rspec run options

## 0.8.3 (2016-02-05)
  - enabled sends errors to desk for PRODUCTION env

## 0.8.5 (2016-02-08)
  - desk connector: fix problem with sending headers (APP_SECRET)

## 0.8.6 (2016-02-16)
  - exception_helpers: remove calling send_ticket method from send_data method
  - exception_helpers: add notify method, with only_email argument (default value is false). Call notify method with only_email true to call send_ticket and send_email methods. Call notify method with only_email false (or without) to call send_ticket and send_data methods.

## 0.8.7 (2016-02-22)
  - desk_connector: add send_duplicates method

## 0.8.8 (2016-03-02)
  - desk_connector: add rescue for HTTP_ERRORS
  - attachments_desk: add rescue for IOErrors
  - notify: call send_data or send_email and after call send_ticket method

## 0.8.9 (2016-03-04)
  - attachments_desk: instead of sending file object, send hash with file content and file proprieties

## 0.9.0 (2016-03-11)
  - Remove "rememberable_credentials" key from emails

## 1.0.0 (2016-05-17)
  - Make Slat-Common service independent
  - Cleanup default inclusion file, removed automatic require of `%w(crypt sign json_wrapper redis_lock rest_api timezone storage/base storage/collection storage/item)`

## 1.0.2 (2016-05-18)
  - Added require_relative to Storage::Base to exclude separate requires for collection and item, Ex: `require 'salt-common/storage/base'`

## 1.0.5 (2016-05-26)
  - Added SaltCommon::HashFilter

## 1.0.7 (2016-05-26)
  - Extend `Credentials.filter`

## 1.0.16 (2016-05-26)
  - Added account checksum

## 1.1.0 (2016-11-16)
  - Removed UUID for `Pending transactions`, now all transactions modes have `uniq_checksum`

## 1.1.1 (2016-11-16)
  - Added possibility to specify custom proxy in configuration

## 1.1.2 (2016-11-22)
  - config.proxy_code can now be set to "random", Ex:"us1", "us".

## 1.1.3 (2016-12-07)
  - SaltCommon::Credentials#filter! supports params as an array, Ex: "batch_params".

## 1.1.4 (2016-12-12)
  - LogStashLogger exception_logger work with :unix type.

## 1.1.5 (2016-12-23)
  - Improved :made_on validation.

## 1.1.6 (2016-12-27)
  - Added `RestApi#execute_with_retry`, now option `:retries` is available in initialize, default value `1`.

## 1.1.6 (2017-01-23)
  - Connector::Bucket requests reset retries to `1`, increased timeouts.

## 1.1.8 (2017-01-28)
  - RestApi execute with retry and empty params won't fail in rescue update erros.

## 1.1.10 (2017-01-29)
  - Handle errors in item.read

## 1.1.11 (2017-01-29)
  - Revert 1.1.10

## 1.1.15 (2017-05-11)
  - Account nature changed from `jbc` to `jcb`

## 1.1.19 (2017-05-29)
  - Required filed nature `number` added

## 1.2.0 (2017-05-30)
  - Bumped rest-client dependency to 2.0.2

## 1.4.0 (2017-08-22)
  - Crypto currencies added to CODES constant.
  - `:assets` key added in account.extra
  - `:asset_amount` and `:asset_code` added in `transaction[:extra]`

## 1.4.2 (2017-11-28)
  - `TransactionList` now calculates `short_checksum` for each transaction and store it in transaction's `internal_data`
  - `short_checksum` is calculated excluding `description` and `initial_checksum`
  - Depending on `short_checksums` list recieved from Bucket or previously calculated `short_checksum`s - flag `extra[:possible_duplicate]` is set

## 1.5.6 (2018-03-06)
  - `assets` extra field is not validated anymore for empty array. This was made for crypto-currency accounts identification.

## 1.6.0 (2018-04-06)
  - `BTG` asset currency added to assets

## 1.8.3 (11-05-2018)
  - Now `extra[:asset_code]` in transaction can pass validation if it's value is not present in asset codes but is present in currencies.

## 1.8.6 (12-06-2018)
  - Added more assets.

## 1.8.7 (18-06-2018)
  - Added payment templates validation.

## 2.0.11 (1800702018)
  - Implemented deprecated proxies mechanism.

## 2.0.12 (26-07-2018)
  - Filtered sensitive data (for accounts, transactions and holder info) from `error.item_hash` in case of `BatchValidation` error.

## 2.0.14 (08-08-2018)
  - Added `instruction`, `supports_faster_payments` attributes to templates. Added `extra` support for fields configurator.

## 2.0.16 (15-08-2018)
  - Extended acceptable data formats for `extra[:sort_code]`

## 2.2.1 (03-09-2018)
  - Changed `internal_data[:short_checksum]` field to `internal_data[:short_checksums]` in transaction object.
  - Changed method name from `calculate_short_checksum` to `calculate_short_checksums`.
  - Updated method `calculate_short_checksums` to consider transactions with variations in dates (+- 1 day) and same/different description.

## 2.2.2 (14-09-2018)
  - Updated timezone constants for all available country codes.

## 2.3.1 (11.10.2018)
  - Implemented 'include_natures' when adding an account.

## 2.3.2 (17.10.2018)
  - Convert 'exclude_accounts' array values and account['id'] to string, because of Postgres serialisation of array

## 2.4.1 (19.10.2018)
  - Added validation(partial) for config `supported_account_natures`, `supported_account_extra_fields` and `supported_transaction_extra_fields`

## 2.4.2 (21.11.2018)
  - Holder info Validator less strict - it is enough fore some fields being available for Holder Info to be valid.

## 2.4.3 (22.11.2018)
  - Added new `mcc` codes constants

## 2.4.4 (22.11.2018)
  - Updated `error.notify` arguments when `InvalidCredentials` error occurs

## 2.4.5 (22.11.2018)
  - Handle +2 days instead of +-1 day when calculating transaction's `internal_data[:short_checksums]`
  - Add `extra[:id]` or `extra[:record_number]` (if availalble) when calculating transaction's `internal_data[:short_checksums]`

## 2.4.6 (27.11.2018)
  - Remove PaymentTemplateConfigurator and PaymentFieldsConfigurator (moved to thief).

## 2.4.7 (30.11.2018)
  - Add MCC Code "6538" as "transfer", source => 'Money transfers MasterCard MoneySend Funding'.

## 2.4.8 (04.12.2018)
  - Added 'USDC' to assets and added timeout in `get_cryptocurrencies_list`.

## 2.4.9 (05.12.2018)
  - Added precision validation of float fields for account and transaction.

## 2.4.10 (31.12.2018)
  - Added validation of 'holder_info' keys names.

## 2.4.11 (11.01.2019)
  - Added several new mcc codes.

## 2.4.12 (05.02.2019)
  - Filter params from sensitive data when RestApi error occurs during sending data to bucket

## 2.4.13 (07.02.2019)
  - Remove rescue from Bucket `request` method in order to send ticket only once
  - Add method `filtered_params` RestApi to implement params filtering in projects which need it (returns original params by default)

## 2.5.0 (06.02.2019)
  - Added `V2` For `Connectors::Desk` and removed version module from all connectors

## 2.5.2 (25.03.2019)
  - Updated `send_ticket` for `Connectors::Desk`

## 2.5.3 (25.03.2019)
  - Updated `custom_extra_fields` for `SaltCommon::Account` to receive extra fields via inherited instance

## 2.5.4 (26.03.2019)
  - Updated MCC code `6513` to match new category `rent`

## 2.5.5 (02.04.2019)
  - Added business categories for transaction

## 2.5.6 (25.04.2019)
  - Updated Nokogiri

## 2.5.7 (26.04.2019)
  - Added MCC code `6540` to match `transfer` category

## 2.5.8 (30.04.2019)
  - Added MIR card type to constants and updated get_card_type

## 2.5.9 (23.05.2019)
  - Added new `supported_account_types` configuration field and a validation for it.

## 2.5.10 (21.06.2019)
  - Fixed validation for asset codes.

## 2.5.14 (25.06.2019)
  - Added live tests with VCR and fully tested flow.

## 2.5.15 (25.06.2019)
  - Added `extra[:partial]` flag and validation for it

## 2.5.18 (02.07.2019)
  - Fixed assets parse
  - Added BCHSV code

## 2.5.21 (16.07.2019)
  - Fixed `parse_card_number`

## 2.5.22 (27.09.2019)
  - New mcc codes were added

## 2.5.23 (20.12.2019)
  - Indian rupee symbol added

## 2.6.0 (06.01.2020)
  - Booleans no longer whitelisted

## 2.6.1 (09.01.2020)
  - CryptoFetcher updated

## 2.6.2 (28.01.2020)
  - Validating that posting_date is later than made_on

## 2.6.3 (17.02.2020)
  - Don't remove any text that follows the card number when parsing card number

## 2.6.4 (20.02.2020)
  - Update card parsing to handle more cases

## 2.6.5 (25.02.2020)
  - Update card sanitizing to always sub 8 digits irrelevant of card numbers amount

## 2.6.6 (25.02.2020)
  - Skip ibans when parsing card names

## 2.6.7 (26.02.2020)
  - Adding `opening_balance`, `closing_balance`, `payer`, `payer_information`, `payee_information`, `variable_code`, `specific_code`, `constant_code` transaction extra fields
  - Adding `opening_balance`, `closing_balance` account extra fields

## 2.8.1 (23.04.2020)
  - Only top-level keys are added to `_filtered_keys` when nested objects are not whitelisted.

## 2.8.2 (23.04.2020)
  - LogMapper: skip nil values when converting via regexp.

## 2.8.3 (13.05.2020)
  - Update Rack to 2.2.2
  - Sinatra: convert to float `now` and `began_at` due to new Rack version

## 2.8.5 (15.07.2020)
  - Add holder info config validation. Check presence and content of supported_fetch_scopes and holder_info config keys.

## 2.8.6 (17.07.2020)
  - Added validations for `provider_code` and `country_code`

## 2.8.7 (03.08.2020)
  - Added audit log

## 2.8.8 (05.08.2020)
  - Added handling for `custom.field` in `log_mapper`

## 2.8.9 (13.08.2020)
  - Added new account extra field `balance_type`

## 2.8.11 (27.10.2020)
  - Added CGLD(Celo) to assets

## 2.8.12 (28.10.2020)
  - Add `Sandbox` config validation. Ensure than no `identification_codes` or `bic_codes` are present in `Sandbox` config.

## 2.8.13 (30.10.2020)
  - Added validation for credentials config fields. Ensuring than `english_field_name`, `localized_field_name` and `checksummable` aren't blank.

## 2.8.14 (04.11.2020)
  - Rack moved in new version to a more precise `Process.clock_gettime`. This patch updates the way we calculate the diff in time.

## 2.9.0 (06.11.2020)
  - Brings major update to sinatra gem and rack gem to correspond the versions used in microservices.

## 2.9.1 (12.11.2020)
  - adds validation for supported_fetch_scopes.

## 2.9.2 (19.11.2020)
  - Added YFI(yearn.finance) to assets

## 2.9.3 (19.11.2020)
  - Added ALGO OXT REPV2 BAND UMA BAL to assets

## 2.9.4 (01.12.2020)
  - Added WBTC to assets

## 2.9.5 (07.12.2020)
  - Update gems: activesupport/activemodel ~> 5.2.4.3, json ~> 2.3.0, sidekiq ~> 4.2.10, nokogiri ~> 1.10.10

## 2.9.6 (28.12.2020)
  - Adds config fields nature validation

## 2.9.9 (27.01.2021)
  - Adds validation for extra[:status]

## 2.9.10 (29.01.2021)
  - Adds categorizer timeout error

## 2.9.11 (01.02.2021)
  - Fixes categorizer timeout error

## 2.10.0 (11.02.2021)
  - Adds validation for provider's config fields

## 2.11.0 (17.02.2021)
  - Adds parse_leap_date helper

## 2.11.1 (19.02.2021)
  - Fixes parse_leap_date

## 2.11.2 (09.03.2021)
  - Updates calculate_checksum

## 2.11.3 (15.03.2021)
  - Fixes calculate_checksum

## 2.12.0 (17.03.2021)
  - Added raw_balance account extra field

## 2.12.1 (18.03.2021)
  - raw_balance constrained to String class

## 2.13.0 (08.04.2021)
  - Add clear_balances method in AccountList

## 2.13.1 (12.04.2021)
  - Handle new BALANCE_FIELDS

## 2.13.2 (15.04.2021)
  - update clear_balances method to return true or false

## 2.13.3 (15.04.2021)
  - Add accounts_without_balance supported fetch scope

## 2.13.4 (12.05.2021)
  - Added exchange_rate transaction extra field

## 2.14.0 (18.05.2021)
  - Extends Crypto class with exchange mechanism

## 2.15.0 (25.05.2021)
  - Extract sort_code to account.extra from DE iban

## 2.15.2 (02.06.2021)
  - Update parse_date error message to include parsed string

## 2.15.3 (03.06.2021)
  - Improve sort_code extraction(use fast-updatable match patterns). Added AT, IE, ES patterns.

## 2.15.4 (08.06.2021)
  - Improve sort_code extraction( fix for cases with no extra passed ).

## 2.15.5 (16.06.2021)
  - Added validation to made on to not be before 01.01.1970

## 2.15.6 (17.06.2021)
  - Improve sort_code extraction. Add BE, GR patterns.

## 2.15.7 (05.07.2021)
  - Cast all Integer values from item_hash to String

## 2.15.8 (13.07.2021)
  - Move IbanParser to salt-common, add more iban extraction patterns

## 2.16.0 (15.07.2021)
  - Add Ibandit gem for Iban validations.

## 2.16.1 (16.07.2021)
  - Update sort-code validation pattern.

## 2.16.3 (26.07.2021)
  - Add Ibandit gem to gemspec dependencies

## 2.16.4 (27.07.2021)
  - More countries to iban parser and sort_code extraction

## 2.16.5 (17.08.2021)
  - Added konto_check gem for DE IBANs validation

## 2.16.6 (06.10.2021)
  - Added a new transaction extra field `end_to_end_id`

## 2.17.0 (25.10.2021)
  - Added sanitize_extra_values

## 2.17.1 (05.11.2021)
  - Added ignore_card_type parameter

## 2.17.2 (12.11.2021)
  - Added identification codes pattern for Romanian IBANs

## 2.17.3 (23.11.2021)
  - Included Clearing Number into Identification codes for Swedish IBANs

## 2.17.4 (10.01.2022)
  - Fixing failing date related tests

## 2.17.5 (10.01.2022)
  - Locking ibandit gem at 1.7.1

## 2.17.6 (10.01.2022)
  - Updating gemspec

## 2.17.7 (18.01.2022)
  - Moved `valid_account_list_without_transactions` method from services to `salt-common`

## 2.17.8 (25.01.2022)
  - Adds 'bban' account extra field.

## 2.17.9 (25.01.2022)
  - Filter account list by nature only before sending

## 2.18.0 (11.02.2022)
  - Adds iso_country_codes gem

## 2.19.0 (21.02.2022)
  - Extract bban to account.extra from DE/ES/PT IBANs

## 2.19.1 (11.03.2022)
  - Extract bban from IT IBANs
  
## 2.19.2 (23.03.2022)
  - Adds validation for max_allowed_automatic_daily_refreshes

## 2.19.3 (23.03.2022)
  - Fix Adds validation for max_allowed_automatic_daily_refreshes

## 2.19.4 (01.04.2022)
  - Extract bban from AD|AL|AT|AZ|BA|BE IBANs

## 2.19.5 (05.04.2022)
  - Extract bban from BG|BH|BR|CH|CR|CY|CZ IBANs

## 2.19.6 (14.04.2022)
  - Extract bban from rest of supported IBANs

## 2.20.0 (06.05.2022)
  - Adds parse_mcc helper

## 2.20.1 (10.06.2022)
  - Added return condition in parse_mcc

## 2.21.0 (10.06.2022)
  - Added config validation for custom pendings settings

## 2.21.3 (27.07.2022)
  - Handles cases where custom_pendings is set to false and custom_pending_period is 0
