module SaltCommon
  module Connectors
    class Desk
      class << self
        def create_ticket(data)
          post(data, Settings.desk.base_url + "/api/v2/tickets")
        end

      private

        def post(data, url)
          RestClient::Request.execute({
            method: :post,
            url:     url,
            payload: SaltCommon::JsonWrapper.encode(data),
            headers: {
              APP_SECRET: Settings.desk.service_to_desk_secret,
              content_type: :json
            }
          })

        rescue *SaltCommon::RestApi::HTTP_ERRORS => error
          error.update(data.merge({url: url})).send_email
        end
      end
    end
  end
end
