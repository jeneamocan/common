module SaltCommon
  module Connectors
    class Bucket
      class << self
        def checksums(params)
          request(SaltCommon::Credentials.filter!(params), Settings.bucket.base_url + "/api/service/v1/service/checksums", :get)
        end

        def bucket_accounts(params)
          request(SaltCommon::Credentials.filter!(params), Settings.bucket.base_url + "/api/service/v1/service/accounts", :get)
        end

        def fail(params)
          keep_keys = [:rememberable_credentials, :encrypted_rememberable_credentials]
          keep_keys << :credentials if Settings.service_name == "stealer"
          request(SaltCommon::Credentials.filter!(params, keep_keys), Settings.bucket.base_url + "/api/service/v1/service/fail", :post)
        end

        def success(params)
          keep_keys = [:rememberable_credentials, :encrypted_rememberable_credentials]
          keep_keys << :credentials if Settings.service_name == "stealer"
          request(SaltCommon::Credentials.filter!(params, keep_keys), Settings.bucket.base_url + "/api/service/v1/service/success", :post)
        end

        def stage(params)
          request(SaltCommon::Credentials.filter!(params), Settings.bucket.base_url + "/api/service/v1/service/stage", :post)
        end

        def import(params)
          request(SaltCommon::Credentials.filter!(params), Settings.bucket.base_url + "/api/service/v1/service/import", :post)
        end

        def import_pending(params)
          request(SaltCommon::Credentials.filter!(params), Settings.bucket.base_url + "/api/service/v1/service/import_pending", :post)
        end

        def import_holder_info(params)
          request(SaltCommon::Credentials.filter!(params), Settings.bucket.base_url + "/api/service/v1/service/import_holder_info", :post)
        end

        def interactive(params)
          request(SaltCommon::Credentials.filter!(params), Settings.bucket.base_url + "/api/service/v1/service/interactive", :post)
        end

      private

        def request(params, url, method)
          SaltCommon::RestApi.new(
            url,
            {
              params:  params,
              headers: headers,
              retries: 1, # reset retries to 1 for debug purpose
              open_timeout: 15, # default timeouts 10
              read_timeout: 15, # default timeout 10
            }
          ).execute_with_retry(method)
        end

        def headers
          { APP_SECRET: Settings.get("bucket.#{Settings.service_name}_to_bucket_secret") }
        end
      end
    end
  end
end
