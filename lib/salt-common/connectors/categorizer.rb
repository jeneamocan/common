module SaltCommon
  module Connectors
    class Categorizer
      class CategorizerTimeoutError < StandardError
        include SaltCommon::ExceptionHelpers
      end

      class << self
        def batch_guess(params, rest_api_options={})
          post(params, Settings.categorizer.base_url + "/api/v1/guess/batch", rest_api_options)
        end

      private

        def post(params, url, rest_api_options)
          SaltCommon::RestApi.new(
            url,
            rest_api_options.merge(
              {
                params:  build_post_params(params),
                headers: headers
              }
            )
          ).post
        rescue RestClient::Exceptions::ReadTimeout
          raise CategorizerTimeoutError.new("Timeout Error in Categorizer")  
        end

        def headers
          { APP_SECRET: Settings.categorizer.service_to_categorizer_secret }
        end

        def build_post_params(params)
          {"data" => params}
        end
      end
    end
  end
end
