require "redis"

module SaltCommon
  class RedisLock
    NotAcquired = Class.new(StandardError)

    attr_reader :key, :token_key, :redis, :timeout, :expire, :retry_time, :exec_time, :allow_unlock, :name

    NAMESPACE  = "saltedge:lock"
    TIMEOUT    = 30
    EXPIRE     = 60
    RETRY_TIME = 0.5
    EXEC_TIME  = 30
    NAME       = "general"

    def initialize(key, options={})
      @key        = (options[:namespace] || NAMESPACE) + ":" + key.to_s
      @timeout    = options[:timeout]    || TIMEOUT
      @expire     = options[:expire]     || EXPIRE
      @retry_time = options[:retry_time] || RETRY_TIME
      @exec_time  = options[:exec_time]  || EXEC_TIME
      @name       = options[:name]       || NAME
    end

    def lock!(&block)
      raise NotAcquired unless lock(&block)
    end

    def lock(&block)
      start_at      = Time.now
      lock_acquired = false
      while Time.now - start_at < timeout
        if try_lock
          lock_acquired = true
          break
        end
        sleep retry_time
      end

      if lock_acquired
        on_lock_acquired(&block)
        notify(Time.now - start_at)
      end

      lock_acquired
    ensure
      unlock
    end

  private

    # NOTE: extracted to a separate method so you can overload it, wrap inside transactions in other projects
    def on_lock_acquired(&block)
      yield
    end

    def try_lock
      result, @allow_unlock = redis.evalsha(sha_script(LOCK_SCRIPT), keys: [key], argv: [now, now + expire])

      case result
        when 'locked'    then return false
        when 'recovered' then return false
        when 'acquired'  then return true
      end

    rescue ::Redis::CommandError => error
      if error.message =~ /^NOSCRIPT/
        redis.script(:load, LOCK_SCRIPT)
        retry
      end
    end

    def unlock
      return false unless allow_unlock
      redis.evalsha(sha_script(UNLOCK_SCRIPT), keys: [key])

    rescue ::Redis::CommandError => error
      if error.message =~ /^NOSCRIPT/
        redis.script(:load, UNLOCK_SCRIPT)
        retry
      end
    end

    def redis
      @redis ||= Redis.current
    end

    def sha_script(script)
      Digest::SHA1.hexdigest script
    end

    def now
      Time.now.to_i
    end

    def notify(execution_time)
      # NOTE: Do something if execution_time > exec_time
    end

    LOCK_SCRIPT = <<-LUA
      local key = KEYS[1]
      local now = tonumber(ARGV[1])
      local expires_at = tonumber(ARGV[2])

      local prev_expires_at = tonumber(redis.call('get', key))

      if prev_expires_at == nil then
        redis.call('set', key, expires_at)
        return {'acquired', true}

      elseif prev_expires_at > now then
        return {'locked', false}

      else
        redis.call('del', key)
        return {'recovered', true}
      end
    LUA

    UNLOCK_SCRIPT = <<-LUA
      local key = KEYS[1]

      redis.call('del', key)
      return true
    LUA
  end
end
