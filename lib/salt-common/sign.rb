require 'openssl'
require 'base64'

module SaltCommon
  class Sign

    DIGESTS = {
      sha1:   OpenSSL::Digest::SHA1,
      sha256: OpenSSL::Digest::SHA256
    }

    def initialize(params)
      @data             = params[:data]
      @signature        = params[:signature]
      @public_key_file  = params[:public_key_file]
      @private_key_file = params[:private_key_file]
      @digest           = DIGESTS[params[:digest]] || DIGESTS[:sha1]
    end

    def sign
      Base64.encode64(rsa_key(@private_key_file).sign(@digest.new, @data))
    end

    def verify
      rsa_key(@public_key_file).verify(@digest.new, Base64.decode64(@signature), @data)
    end

    def valid_public?
      key = rsa_key(@public_key_file)
      key.public?
    rescue
      false
    end

    def valid_private?
      key = rsa_key(@private_key_file)
      key.private?
    rescue
      false
    end

    private

    def rsa_key(key)
      OpenSSL::PKey::RSA.new(key)
    end
  end
end
