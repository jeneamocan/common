require 'nokogiri'
require 'unicode_utils'
require 'date'
require 'ibandit'
require 'konto_check'
require 'iso_country_codes'

require_relative "../accounts_transactions/currencies/iso4217"
require_relative "../accounts_transactions/currencies/symbols"

module SaltCommon
  module ParsingHelpers
    LUT_FILE_PATH  = File.expand_path('../accounts_transactions/iban/blz/blz.lut', File.dirname(__FILE__))
    LUT_INIT_LEVEL = 5 # BLZ, PZ, NAME, ZIP, ORT, BIC

    unless defined?(ParsingError)
      class ParsingError < StandardError
        FLOAT      = "Error while parsing float field."
        DATE       = "Error while parsing date field."
        TIME       = "Error while parsing time field."
        TEXT       = "Error while parsing text field."
        CURRENCY   = "Error while parsing currency field."
        NATURE     = "Error while parsing account nature."
        ASSET_CODE = "Error while parsing asset code field."
        MCC_CODE   = "Error while parsing mcc code field"
      end
    end

    NBSP = Nokogiri::HTML("&nbsp;").text unless defined?(NBSP)

    def parse_float(incoming, options={})
      return incoming if incoming.is_a?(Float)
      string = incoming.dup
      sanitize_float_string!(string)

      if options[:integral]
        string.delete!(",")
        string.delete!(".")
        return string.to_f
      end

      indexes = {
                  "," => string.rindex(","),
                  "." => string.rindex(".")
                }

      return string.to_f if indexes["."].nil? && indexes[","].nil?

      if indexes["."] == nil
        if string.scan(/,/).size > 1
          string.delete!(",")  # 123,123,123
        else
          string.tr!(",", ".") # 123,123
        end
        return string.to_f
      end

      if indexes[","] == nil
        string.delete!(".") if string.scan(/\./).size > 1 # 123.123.123
        return string.to_f
      end

      if indexes[","] > indexes["."]
        # comma is decimal separator
        string.delete!(".")
        string.tr!(",", ".")
      else
        # dot is decimal separator
        string.delete!(",")
      end

      string.to_f
    rescue
      raise ParsingError.new(ParsingError::FLOAT)
    end

    def parse_date(string, format="%Y-%m-%d")
      Date.strptime(normalize_string(string).strip, format).as_json
    rescue => error
      if !error.message.include?("invalid strptime format") &&
         string.is_a?(String) && string[/\d+/] && (format[/%(Y|y)/] || string[/29/])
        return parse_leap_date(string, format)
      end

      raise ParsingError.new(ParsingError::DATE + " Tried to parse: #{string}")
    end

    def parse_leap_date(string, format)
      year_format = format[/%(Y|y)/]

      unless year_format
        string += " #{Time.now.prev_year.year}"

        return string.to_date.as_json
      end

      recent_year = if year_format.match?("%Y") 
        string[/\d{4}/]
      elsif year_format.match?("%y") && format.index("%y") > 1
        string[/\d{2}$/]
      else
        string[/\d{2}/]
      end

      unless Date.leap?(Date.strptime(recent_year, year_format).prev_year.year)
        raise ParsingError.new(ParsingError::DATE) 
      end

      new_date = string.gsub(recent_year, (recent_year.to_i - 1).to_s)
      Date.strptime(normalize_string(new_date).strip, format).as_json
    rescue
      date = new_date || string
      raise ParsingError.new(ParsingError::DATE + " Tried to parse: #{date}")
    end

    def parse_time(string, format="%H:%M")
      DateTime.strptime(normalize_string(string), format).strftime("%H:%M:%S")
    rescue
      raise ParsingError.new(ParsingError::TIME)
    end

    def parse_currency(string)
      currency_from_iso      = normalize_string(string).upcase
      currency_from_symbols  = SaltCommon::Currencies::SYMBOLS[parse_description(string.delete("-"))]
      currency_from_iso_code = SaltCommon::Currencies::ISO4217.detect { |key, value| key == (parse_description(string)).to_i }
      currency_from_iso_code = (parse_description(string)) =~ /\D/ ? nil : currency_from_iso_code
      return currency_from_iso_code.last if currency_from_iso_code
      return currency_from_symbols if currency_from_symbols
      return currency_from_iso if SaltCommon::Currencies::ISO4217.values.include?(currency_from_iso)
    rescue
      raise ParsingError.new(ParsingError::CURRENCY)
    end

    def parse_asset_code(string)
      return nil if string.blank?

      string     = normalize_string(string).upcase
      codes      = CryptoFetcher.assets_list
      code_match = codes.detect { |asset_code| asset_code["code"] == string }
      code_match ? code_match["code"] : nil
    rescue
      raise ParsingError.new(ParsingError::ASSET_CODE)
    end

    def parse_mcc(mcc)
      return if mcc.blank?

      mcc_string = mcc.to_s
      mcc_string.prepend('0') while mcc_string.size < 4
      mcc_string.slice!('0')  while mcc_string.size > 4
      SaltCommon::Constants::Mcc::CODES.keys.include?(mcc_string) ? mcc_string : nil
    rescue
      raise ParsingError.new(ParsingError::MCC_CODE)
    end

    def parse_description(string)
      normalize_string(string.gsub(/[\s]/, ' '))
    rescue
      raise ParsingError.new(ParsingError::TEXT)
    end

    def parse_card_number(string)
      string = parse_description(string)
      match  = find_card_number(string)
      return string if match.nil?

      digits    = match.gsub(/\D/, "")
      digits[4..11] = "*" * 8
      string.sub(match, digits)
    rescue
      raise ParsingError.new(ParsingError::TEXT)
    end

    def downcase(unicode_string)
      UnicodeUtils.downcase(normalize_string(unicode_string))
    rescue
      raise ParsingError.new(ParsingError::TEXT)
    end

    def titlecase(unicode_string)
      UnicodeUtils.titlecase(normalize_string(unicode_string))
    rescue
      raise ParsingError.new(ParsingError::TEXT)
    end

    def as_regexp(*args)
      Regexp.new(args.reject(&:blank?).map { |arg| Regexp.escape(arg) }.join("|"), true)
    end

    def iban_valid?(string)
      return false unless string.is_a?(String)

      return false unless Ibandit::IBAN.new(string).valid?

      if string.match?(/^DE/)
        init_konto_check
        return false unless KontoCheck.retval2txt_short(KontoCheck.iban_check(string)) == "OK"
      end

      true
    end

    def convert_to_alpha3(country_code)
      IsoCountryCodes.find(country_code).alpha3
    rescue IsoCountryCodes::UnknownCodeError
      raise IsoCountryCodes::UnknownCodeError.new("No ISO 3166-1 code could be found for '#{country_code}'")
    end

  private

    def init_konto_check
      KontoCheck.init(LUT_FILE_PATH, LUT_INIT_LEVEL) if KontoCheck.retval2txt_short(KontoCheck.lut_blocks) == "LUT2_NOT_INITIALIZED"
    end

    def find_card_number(string)
      return if string.gsub("\s", "").match?(/^[a-zA-Z]{2}\d{2}/)

      [
        /\b\d[\d]+\d\b/,
        /\b\d[\d\s]+\d\b/,
        /\b\d[\d-]+\d\b/,
        /\b\d[\d\s-]+\d\b/
      ].detect do |pattern|
        match = string[pattern]
        next unless match
        return match if [15, 16].include?(match.gsub(/\D/, "").size)
      end
    end

    def sanitize_float_string!(string)
      # replace weird minus sign with proper minus
      string.gsub!(8211.chr(Encoding::UTF_8), "-")
      # replace an even weirder minus sign with proper minus
      string.gsub!(8722.chr(Encoding::UTF_8), "-")
      # remove html everything except digits, dots, commas, '+', '-'
      string.gsub!(/[^0-9\-+.,]/, "")
      # remove trailing non digits
      string.gsub!(/[-+.,]+$/, "")
    end

    def normalize_string(string)
      return "" if string.nil?
      string.gsub(NBSP, " ").gsub(/[−–]/, "-").squeeze(' ').strip
    end

    def redis
      Redis.current
    end
  end
end
