module SaltCommon
  class HashFilter
    # Usage:
    # SaltCommon::HashFilter.set_filter_hash_keys([:key1, "key2"])
    # SaltCommon::HashFilter.filter!(hash)
    #
    #
    @@filter_hash_keys = []

    def self.set_filter_hash_keys(array)
      raise ArgumentError unless array.is_a?(Array)
      @@filter_hash_keys = array.map(&:to_s)
    end

    def self.get_filter_hash_keys
      @@filter_hash_keys
    end

    def self.filter!(hash, filter_hash_keys=SaltCommon::HashFilter.get_filter_hash_keys)
      hash.each do |key, value|
        if filter_hash_keys.map(&:to_s).include?(key.to_s)
          hash[key] = "[FILTERED]"
          next
        end
        filter!(value, filter_hash_keys) if value.is_a?(Hash)
      end
    end
  end
end
