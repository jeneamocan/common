require "mail"
require_relative "../sender"
require_relative "../logstash/exception_logger"

module SaltCommon
  module ExceptionHelpers
    PROVIDER_UNKNOWN    = "UNKNOWN PROVIDER"              unless defined?(PROVIDER_UNKNOWN)
    LOGIN_UNKNOWN       = "UNKNOWN LOGIN"                 unless defined?(LOGIN_UNKNOWN)
    MESSAGE_UNAVAILABLE = "MESSAGE UNAVAILABLE"           unless defined?(MESSAGE_UNAVAILABLE)
    SERVICES            = %w(robber stealer fencer thief) unless defined?(SERVICES)

    attr_reader   :file, :line, :item_hash, :extra, :updated, :logger, :browser_handler
    attr_accessor :exception_hash, :severity

    def update(item_hash={}, browser_handler=nil)
      if @updated
        # NOTE: we should not set again @file, @line and @item_hash
        # we just merge extra information to @item_hash
        @item_hash.merge!(item_hash.stringify_keys)
      else
        group = caller[0].match(/(?<file>[^\:]+):(?<line>[0-9]+)/)
        @file, @line, @item_hash, @browser_handler = group[:file], group[:line], item_hash.stringify_keys, browser_handler
        update_item_hash
        @updated = true
      end

      @item_hash.transform_values! { |v| v.is_a?(Integer) ? v.to_s : v }

      self
    end

    def notify(options={})
      send_data unless options[:fail_callback] == false
      logger.send_log(filtered_item_hash)
      send_ticket unless options[:ticket] == false
    end

    def send_data
      Connectors::Bucket.fail(item_hash)
    rescue => error
      error.update(filtered_item_hash).send_email
    end

    def severity
      @severity || "CRITICAL"
    end

    def send_email
      SaltCommon::Sender.new do |mail|
        mail.to      = Settings.email.error
        mail.subject = "[#{Settings.service_name.capitalize}][#{severity}][#{provider_code}][#{login_id}] #{self.class.name}"
        mail.body    = SaltCommon::JsonWrapper.encode(filtered_item_hash, {pretty: true})
      end
    end

    def send_ticket
      return unless Settings.desk.enabled
      params = item_hash.dup
      payload = defined?(SaltCommon::Credentials) ? SaltCommon::Credentials.filter!(params) : params

      payload[:subject]     = "[#{Settings.service_name.capitalize}][#{severity}][#{provider_code}][#{login_id}] #{self.class.name}"
      payload[:priority]    = severity
      payload[:attachments] = attachments_desk
      payload[:project]     = "services"
      payload[:ticket_type] = "error"

      Connectors::Desk.create_ticket(data: payload)
    rescue => error
      send_ticket_error_email(error)
    end

    def to_hash
      @exception_hash ||= {
        "error_mode"  => "#{Settings.service_name.capitalize}Error",
        "error_class" => self.class.name.demodulize,
        "file"        => file,
        "line"        => line.to_i,
        "time"        => Time.now.as_json,
        "severity"    => severity,
        "message"     => message.blank? ? MESSAGE_UNAVAILABLE : message,
        "backtrace"   => filter_backtrace
      }
    end

    def logger
      @logger ||= SaltCommon::ExceptionLogger.new(Settings.service_name, "exception")
    end

    def update_batch(errors, item_hash)
      update(item_hash.merge("parsing_errors" => []))

      errors.each do |error|
        add_error(error)
      end

      self
    end

    def add_error(error)
      error_data = {
        "message"   => error.message,
        "item_hash" => error.item_hash,
        "backtrace" => error.send(:filter_backtrace)
      }
      item_hash["parsing_errors"] << error_data
    end

  private

    def provider_code
      item_hash.try(:[], "provider_code") || PROVIDER_UNKNOWN
    end

    def login_id
      item_hash.try(:[], "login_id") || LOGIN_UNKNOWN
    end

    def update_item_hash
      service_name = Settings.service_name rescue nil
      return unless SERVICES.include?(service_name)

      item_hash["admin_login_url"] = Settings.bucket.base_url + "/admin/logins/#{item_hash['login_id']}/show"
      item_hash["service_name"]    = Settings.service_name
      item_hash["finish_time"]     = Time.now.utc.as_json
      item_hash["exception"]       = to_hash
    end

    def filter_backtrace
      return nil if backtrace.nil?

      found_app_line = false

      backtrace.reduce([]) do |result, line|
        if line.include?(Settings.service_name) && !line.include?("ruby")
          result << line
          found_app_line = true
        elsif !found_app_line
          result << line
        end

        result
      end
    end

    def attachments_desk
      case Settings.service_name
      when "robber"
        return {} unless item_hash["adapter_config"] && item_hash["adapter_config"][:use_selenium]
        return {} unless browser_handler && browser_handler.data_available?

        file_html = {
          file: Base64.encode64(browser_handler.html),
          extra: {
            original_filename: "browser_html",
            content_type:      "text/html",
            file_extension:    "html",
            file_size:         browser_handler.html.size
          }
        }

        file_png = {
          file: browser_handler.screenshot,
          extra: {
            original_filename: "screenshot",
            content_type:      "image/png",
            file_extension:    "png",
            file_size:         browser_handler.screenshot.size
          }
        }

        {file_html: file_html, file_png: file_png}
      when "fencer"
        path = item_hash["file_path"]
        return {} unless path

        file = File.new(path, "rb")
        file_fencer = {
          file: Base64.encode64(file.read),
          extra: {
            original_filename: path.split("/").last.rpartition(".").first,
            content_type:      MIME::Types.type_for(path).first.content_type,
            file_extension:    path.split(".").last,
            file_size:         file.size
          }
        }

        {file_fencer: file_fencer}
      else
        {}
      end
    end

    def send_ticket_error_email(error)
      SaltCommon::Sender.new do |mail|
        mail.to      = Settings.email.error
        mail.subject = "[#{Settings.service_name.capitalize}][#{severity}][#{provider_code}][#{login_id}] #{error.class.name} [Send Desk ERROR]"
        mail.body    = SaltCommon::JsonWrapper.encode({error: error.message, backtrace: error.backtrace}, {pretty: true})
      end
    end

    def filtered_item_hash
      defined?(SaltCommon::Credentials) ? SaltCommon::Credentials.filter!(item_hash.dup) : item_hash
    end
  end
end
