class SaltCommon::HttpRequest
  HTTP_METHODS = %w(get post patch put delete head options)

  HTTP_METHODS.each do |name|
    define_singleton_method(name) do |*args|
      follow_redirect do
        RestClient.public_send(name, *args)
      end
    end
  end

  attr_reader :options
  def initialize(options)
    @options = options
  end

  def execute
    self.class.execute(options)
  end

  def self.execute(options)
    follow_redirect do
      RestClient::Request.execute(options)
    end
  end

  def self.follow_redirect(&block)
    yield
  rescue RestClient::MovedPermanently, RestClient::Found, RestClient::TemporaryRedirect => err
    # NOTE: on redirect on a non-get route return the location
    err.response.headers[:location]
  end
end
