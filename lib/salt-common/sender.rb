require 'mail'

module SaltCommon
  class Sender
    attr_reader :mail

    def initialize(&block)
      @mail     = Mail.new
      mail.from = Settings.email.notify

      if block_given?
        Proc.new(&block).call(mail)
        mail.charset = Encoding::UTF_8.to_s
        mail.deliver
      end
    end
  end
end
