require 'fileutils'

module SaltCommon
  class RecordsCleaner
    attr_reader :path

    def initialize
      @path = "#{Settings.recorder.path}/**/*.*"
    end

    def clear
      list.each do |file|
        directory = File.dirname(file)

        FileUtils.rm_rf(file)
        FileUtils.rm_rf(directory) if Dir.glob("#{directory}/*").empty?
      end
      # TODO: replace ruby FileUtils by unix calls.
      # system("find #{Settings.recorder.path} -type f -name '*.png' -mtime +15 -print0 | xargs -0 rm -rf 2>/dev/null")
      # system("find #{Settings.recorder.path} -type f -name '*.html' -mtime +21 -print0 | xargs -0 rm -rf 2>/dev/null")

      clean_empty_direcories
    end

  private

    def clean_empty_direcories
      global_path = File.dirname(path)

      unless global_path.include?(Settings.service_name)
        raise StandardError.new("ACHTUNG! Tried to clean records in wrong directory #{global_path} for #{Settings.service_name}")
      end

      system("find #{global_path} -type d -empty -mtime +#{Settings.recorder.lifetime} -delete 2>/dev/null")
      # TODO: replace ruby FileUtils by unix calls.
      # find . -type d -empty -mtime +21 -print0 | xargs -0 rm -rf
    end

    def list
      Dir.glob(path).select { |file| include?(file) }
    end

    def time
      @time ||= Settings.recorder.lifetime.days.ago.to_time
    end

    def include?(file)
      File.file?(file) && check_if_old?(file)
    end

    def check_if_old?(file)
      File.mtime(file) < time
    end
  end
end
