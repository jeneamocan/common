require "rest-client"
require "redis"
require "cgi"
require_relative "../json_wrapper"

module SaltCommon
  module SSO
  end
end

require_relative "authenticator"
require_relative "session_store"
require_relative "session"
require_relative "sidekiq_constraint"
