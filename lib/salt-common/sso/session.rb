module SaltCommon
  module SSO
    class Session
      attr_reader :email

      Unauthorized = Class.new(StandardError)

      def initialize(email)
        @email = email
      end

      def store_session(params)
        session_store.set!(params)
      end

      def valid?
        return false if token_hash.nil?

        client_data["email"] == email
      rescue RestClient::Unauthorized
        raise Unauthorized
      end

      def user_can_login?
        return false if token_hash.nil?

        client_data["roles"].include?("sign_in")
      rescue RestClient::Unauthorized
        raise Unauthorized
      end

      def has_role?(role)
        return false if token_hash.nil?

        client_data["roles"].include?(role)
      rescue RestClient::Unauthorized
        raise Unauthorized
      end

      def try_extend!
        raise Unauthorized if token_hash.nil?

        token_params = SaltCommon::SSO::Authenticator.refresh_params!(token_hash["refresh_token"])
        session_store.set!(token_params)
      rescue RestClient::Unauthorized
        raise Unauthorized
      end

      def token_hash
        session_store.get
      end

      def client_data
        @client_data ||= SaltCommon::SSO::Authenticator.get_client_data(token_hash["access_token"])
      end

      def clear_session!
        session_store.remove!
      end

      private

      def session_store
        @session_store ||= SaltCommon::SSO::SessionStore.new(email)
      end
    end
  end
end
