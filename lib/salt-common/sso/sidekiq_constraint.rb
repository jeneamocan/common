class SaltCommon::SSO::SidekiqConstraint
  # usage, put this in routes.rb:
  #
  #   namespace :admin do
  #     constraints SaltCommon::SSO::SidekiqConstraint.new(:admin, Admin::SIDEKIQ_ACTIONS, Rails.env) do
  #       mount Sidekiq::Web, at: '/sidekiq'
  #     end
  #   end
  #
  def initialize(scope, role, env)
    @scope = scope
    @role  = role
    @env   = env
  end

  def matches?(request)
    request.env['warden'].authenticate!(scope: @scope)
    current_admin = request.env["warden"].user(@scope)

    if @env == "staging" || @env == "production"
      session = SaltCommon::SSO::Session.new(current_admin.email)
      session.valid? && session.has_role?(@role)
    else
      true
    end
  rescue SaltCommon::SSO::Session::Unauthorized
    false
  end
end
