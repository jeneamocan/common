module SaltCommon
  module SSO
    class Authenticator
      def self.get_token_params(opts={})
        request(
          method:  :post,
          url:     "#{Settings.sso.url}/oauth/token",
          payload: {
            grant_type:    "authorization_code",
            code:          opts[:code],
            redirect_uri:  opts[:redirect_uri],
            client_id:     Settings.sso.application_id,
            client_secret: Settings.sso.secret
          }
        )
      end

      def self.get_client_data(token)
        request(
          method:  :get,
          url:     "#{Settings.sso.url}/api/v1/me.json",
          headers: {
            content_type:  "application/x-www-form-urlencoded",
            authorization: "Bearer #{token}"
          }
        )
      end

      def self.refresh_params!(refresh_token)
        request(
          method:        :post,
          url:           "#{Settings.sso.url}/oauth/token",
          payload:       {
            grant_type:    "refresh_token",
            refresh_token: refresh_token,
            client_id:     Settings.sso.application_id,
            client_secret: Settings.sso.secret
          }
        )
      end

      def self.request(options)
        response = RestClient::Request.execute(options)
        SaltCommon::JsonWrapper.decode(response)
      rescue RestClient::Unauthorized
        raise SSO::Session::Unauthorized
      end

      def self.can_login?(user_info)
        user_info["roles"].include?("sign_in")
      end

      def self.authorize_url(opts={})
        url = "#{Settings.sso.url}/oauth/authorize?response_type=code&client_id=#{Settings.sso.application_id}"
        url += "&redirect_uri=#{CGI.escape(opts[:redirect_uri])}" if opts[:redirect_uri]
        url
      end

      def self.logout_url(opts={})
        url = "#{Settings.sso.url}/logout"
        url += "?#{URI.encode_www_form(opts)}" if opts.present?
        url
      end

      def self.refresh_session_state_url(opts={})
        url = "#{Settings.sso.url}/refresh_session_state"
        url += "?#{URI.encode_www_form(opts)}" if opts.present?
        url
      end
    end
  end
end
