module SaltCommon
  module SSO
    class SessionStore
      def initialize(email)
        @key = generate_key(email)
      end

      def set!(hash)
        redis.set(@key, SaltCommon::JsonWrapper.encode(hash))
      end

      def get
        result = redis.get(@key)
        return unless result
        SaltCommon::JsonWrapper.decode(result)
      end

      def remove!
        redis.del(@key)
      end

      private

      def generate_key(email)
        namespace + ":" + Digest::SHA1.hexdigest(email)
      end

      def redis
        @redis ||= Redis.current
      end

      def namespace
        @namespace ||= "#{::Settings.application.name}:session_store".freeze
      end
    end
  end
end
