require 'salt-common/whitelist_logging/sanitizer'

# usage:
#
# in controller:
#
# class BaseController < ApplicationController
#   include SaltCommon::WhitelistLogging::DSL
#   log_params :id, nested: [:name, :amount]
#
#   def index
#     log_params :key1, hash: [:key2, hash2: [:key3]]
#     ...
#   end
#
#   def append_info_payload(payload)
#     super
#     return payload unless payload[:params]
#
#     payload[:params] = sanitized_params(
#       payload[:params].except(:action, :controller, :format), # NOTE: already present in logstash
#     )
#   end
# end
#
# in worker:
#
# class BaseWorker < SaltCommon::Worker
#   include SaltCommon::WhitelistLogging::DSL
#   log_params :id, nested: [:name, :amount]
#
#   def run
#     ...
#   end
#
#   def after
#     send_log("Project Name", "Service")
#   end
#
#   def logger_message(project,service)
#     hash.merge(
#       project: project,
#       service: service,
#       params: sanitized_params(params)
#     )
#   end
# end
module SaltCommon
  module WhitelistLogging
    module DSL
      module ClassMethods
        def log_whitelist
          @log_whitelist ||= {}
        end

        def log_params(*args)
          log_whitelist.deep_merge!(
            SaltCommon::WhitelistLogging::Sanitizer.build_mapping(args)
          )
        end

        # NOTE: allow to subclasses to inherit whitelist mapping
        def inherited(subclass)
          # NOTE: rails helpers break in Fentury if this is not called,
          # I was unable to write a failing spec for this, don't remove it
          super

          subclass.log_whitelist.deep_merge!(log_whitelist)
        end
      end

      def self.included(base)
        base.extend(ClassMethods)
      end

      private

      def log_params(*args)
        @current_action_log_whitelist ||= {}
        @current_action_log_whitelist.deep_merge!(
          SaltCommon::WhitelistLogging::Sanitizer.build_mapping(args)
        )
      end

      def sanitized_params(params)
        mapping = self.class.log_whitelist.deep_merge(@current_action_log_whitelist || {})

        SaltCommon::WhitelistLogging::Sanitizer.sanitize_params(params, mapping)
      end
    end
  end
end

