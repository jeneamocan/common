module SaltCommon
  module WhitelistLogging
    class Sanitizer
      class << self
        def build_mapping(args)
          args.reduce({}) do |hash, arg|
            if arg.is_a?(Symbol)
              hash[arg] = true
            elsif arg.is_a?(Hash)
              arg.each do |key, value|
                hash[key] = value.is_a?(Symbol) ? value : build_mapping(value)
              end
            end

            hash
          end
        end

        def sanitize_params(params, whitelist)
          if params.is_a?(Array)
            params.map do |entry|
              sanitize_hash_with_mapping(
                entry || {},
                whitelist || {}
              )
            end
          else
            sanitize_hash_with_mapping(
              params || {},
              whitelist || {}
            )
          end
        end

        private

        def sanitize_hash_with_mapping(hash, mapping)
          # NOTE: handle key: :all, we just return the hash unmodified
          return hash.deep_dup.merge("_filtered_keys" => []) if mapping == :all
          return "[FILTERED]" if mapping == :legacy_filtered
          return { "_filtered_keys" => hash.keys } unless mapping

          clean_hash = hash.reduce({}) do |result, (key, value)|
            if value.is_a?(Hash) && nested_mapping(mapping, key)
              result[key] = sanitize_hash_with_mapping(value, nested_mapping(mapping, key))
            elsif value.is_a?(Array)
              result[key] = value.map do |el|
                sanitize_array_element(el, nested_mapping(mapping, key))
              end
            elsif mapping[key.to_sym] == :legacy_filtered
              result[key] = "[FILTERED]"
            elsif mapping[key.to_sym]
              result[key] = value
            end

            result
          end

          clean_hash["_filtered_keys"] = hash.keys - clean_hash.keys
          clean_hash
        end

        def nested_mapping(mapping, key)
          result = mapping[key.to_sym]
          result = {} if [true, false].include?(result)
          result
        end

        def sanitize_array_element(element, mapping)
          if element.is_a?(Hash)
            sanitize_hash_with_mapping(element, mapping)
          else
            element
          end
        end
      end
    end
  end
end
