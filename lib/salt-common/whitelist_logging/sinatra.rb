require 'salt-common/whitelist_logging/sanitizer'

module SaltCommon
  module WhitelistLogging
    # usage:
    #
    # class App < Sinatra::Base
    #   register SaltCommon::WhitelistLogging::Sinatra
    #   log_with_whitelist LoggerFactory.current do |env|
    #     {
    #       project: Settings.service_name,
    #       service: "sinatra"
    #     }
    #   end
    #
    #   get "/" do
    #     log_params :key1, hash: [:key2, hash2: [:key3]]
    #     ...
    #   end
    # end
    #
    # NOTE: if you are populating params hash manually, you need to put them in
    # request.env so that the logger can write and sanitize them, eg:
    #
    # class App < Sinatra::Base
    #   ...
    #
    #   before do
    #     params.shift
    #     begin
    #       @json_body  = request.body.read
    #       params_hash = SaltCommon::JsonWrapper.decode(@json_body)
    #       params.merge!(params_hash)
    #     rescue
    #       # Do nothing, validation will fail in robber
    #     end
    #
    #     NOTE: this is important
    #     request.env["sinatra.params"] = params
    #   end
    # end
    module Sinatra
      def log_with_whitelist(logger, &block)
        use SaltCommon::WhitelistLogging::Sinatra::Middleware, logger, &block
        helpers SaltCommon::WhitelistLogging::Sinatra::Helpers
      end

      module Helpers
        def log_params(*args)
          env["app.log_fields"] ||= {}
          env["app.log_fields"].deep_merge!(SaltCommon::WhitelistLogging::Sanitizer.build_mapping(args))
        end
      end

      class Middleware < Rack::CommonLogger
        attr_reader :project, :service

        def initialize(app, logger, &block)
          super(app, logger)
          @custom_options = block_given? ? Proc.new(&block) : ->(_) { {} }
        end

        private

        def log(env, status, header, began_at)
          now    = Process.clock_gettime(Process::CLOCK_MONOTONIC)
          length = extract_content_length(header)

          json = {
            '@timestamp' => Time.now.utc.iso8601,
            :method      => env['REQUEST_METHOD'],
            :message     => env['REQUEST_METHOD'] + " " + env['PATH_INFO'],
            :path        => env['PATH_INFO'],
            :status      => status.to_s[0..3],
            :size        => length,
            :format      => header['Content-Type'],
            :duration    => ((now - began_at) * 1000).to_i,
            :host        => Socket.gethostname,
            :remote_ip   => env['REMOTE_ADDR'],
            :user_agent  => env['HTTP_USER_AGENT'],
            :params      => sanitized_params(env),
          }.merge(@custom_options.call(env))

          request_started_at = env["HTTP_X_REQUEST_START"].try(:to_f)

          if request_started_at && request_started_at > 0
            # NOTE: began_at is Process::CLOCK_MONOTONIC, so we must convert it
            unicorn_pickup_time = Time.now.to_f - now + began_at

            json[:queue_duration] = ((unicorn_pickup_time - request_started_at) * 1000).to_i
            json[:e2e_duration]   = json[:duration] + json[:queue_duration]
          end

          @logger.info json
        end

        def sanitized_params(env)
          params = if env['REQUEST_METHOD'] == 'POST'
            env['rack.input'].rewind
            SaltCommon::JsonWrapper.decode(env['rack.input'].read)
          else
            env['rack.request.query_hash']
          end

          SaltCommon::WhitelistLogging::Sanitizer.sanitize_params(params, env["app.log_fields"])
        end
      end
    end
  end
end
