module SaltCommon
  module Currencies
    SYMBOLS_HASH = {
      Currencies::EUR => ["€", "ЕВРО", "Евро", "евро", "EUROS", "euro",
                          "Euro", "WME", "EURO"],
      Currencies::USD => ["$", "WMZ", "US Dollar", "ДОЛ", "долл", "долл.", "дол", "Dollar",
                          "Dollar USA", "Dólares"],
      Currencies::RUB => ["р.", "р", "руб", "руб.", "Руб", "RUR",
                          "Российский рубль", "РУБ", "Рубль", "WMR", "₽"],
      Currencies::UAH => ["ГРН", "ГРН.", "грн", "WMU", "Ukraine Hryvnia", "₴"],
      Currencies::TRY => ["TRL", "TL"],
      Currencies::ILS => ["NIS"],
      Currencies::HRK => ["HRD"],
      Currencies::GBP => ["£"],
      Currencies::BRL => ["R$"],
      Currencies::CHF => ["₣"],
      Currencies::CLP => ["Pesos"],
      Currencies::BTC => ["WMX"],
      Currencies::XAU => ["WMG"],
      Currencies::BYR => ["BYP"],
      Currencies::BYN => ["WMB"],
      Currencies::IDR => ["Rp."],
      Currencies::SAR => ["Saudi Arabia Riyals"],
      Currencies::PLN => ["PLZ"],
      Currencies::KZT => ["WMK", "₸", "tenge"],
      Currencies::VND => ["WMV"],
      Currencies::MUR => ["MUS"],
      Currencies::THB => ["฿"],
      Currencies::INR => ["₹"]
    }

    SYMBOLS = SYMBOLS_HASH.each_with_object({}) do |(currency_code, array), hash|
      array.each do |symbol|
        hash[symbol] = currency_code
      end
      hash
    end
  end
end
