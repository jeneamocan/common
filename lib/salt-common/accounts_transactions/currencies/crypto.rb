class CryptoFetcher
  NBSP = Nokogiri::HTML("&nbsp;").text

  def self.assets_list
    if redis.get("v2:cryptocurrencies_list")
      cached_list = JSON.parse(redis.get("v2:cryptocurrencies_list")) | SaltCommon::Assets::CODES
    end

    return cached_list               if cached_list
    return SaltCommon::Assets::CODES if redis.get("v2:error_fetching_cryptos_list")

    result = JSON.parse(get("https://pro-api.coinmarketcap.com/v1/cryptocurrency/map"))
    list   = result["data"].map { |row| { code: row["symbol"], name: row["name"] } }

    redis.setex("v2:cryptocurrencies_list", 3600, list.to_json)
    JSON.parse(list.to_json)
  rescue
    redis.setex("v2:error_fetching_cryptos_list", 60, "failed")
    SaltCommon::Assets::CODES
  end

  def self.parse_description(string)
    normalize_string(string.gsub(/[\s]/, ' '))
  end

  def self.convert(amount, from, to:)
    if [from, to, amount].any?(&:blank?)
      args = {
        "amount": amount,
        "from":   from,
        "to":     to
      }
      raise ArgumentError.new("Blank arguments: #{args.select { |k, v| v.blank? }.keys.join(", ")}")
    end

    invalid_currencies = [from, to].reject do |currency|
      (SaltCommon::Currencies::ISO4217.values + assets_list.map { |x| x["code"] }).include?(currency)
    end
    if invalid_currencies.present?
      raise ArgumentError.new("Following currencies not found in CryptoAssets/ISO4217: #{invalid_currencies.join(", ")}")
    end

    query = {
      amount:  amount.to_f,
      symbol:  from,
      convert: to
    }.to_query

    JSON.parse(
      get("https://pro-api.coinmarketcap.com/v1/tools/price-conversion?#{query}")
    ).dig("data", "quote", to, "price")
  end

  def self.get(url)
    RestClient::Request.execute(
      method:  :get,
      url:     url,
      headers: {
        "X-CMC_PRO_API_KEY" => "3b9547ce-c64d-4cca-96bc-49249533e3f2",
        "Accept"            => "application/json"
      }
    ).body
  end

  def self.normalize_string(string)
    return "" if string.nil?
    string.gsub(NBSP, " ").gsub(/[−–]/, "-").squeeze(' ').strip
  end

  def self.redis
    Redis.current
  end
end
