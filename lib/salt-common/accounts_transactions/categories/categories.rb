module SaltCommon
  module Categories
    UNCATEGORIZED = "uncategorized"
    PERSONAL_CATEGORIES_HASH = {
      auto_and_transport:  [:car_rental, :gas_and_fuel, :parking, :public_transportation, :service_and_parts, :taxi],
      bills_and_utilities: [:internet, :phone, :television, :utilities],
      business_services:   [:advertising, :office_supplies, :shipping],
      education:           [:books_and_supplies, :student_loan, :tuition],
      entertainment:       [:amusement, :arts, :games, :movies_and_music, :newspapers_and_magazines],
      fees_and_charges:    [:provider_fee, :loans, :service_fee, :taxes],
      food_and_dining:     [:alcohol_and_bars, :cafes_and_restaurants, :groceries],
      gifts_and_donations: [:charity, :gifts],
      health_and_fitness:  [:doctor, :personal_care, :pharmacy, :sports, :wellness],
      home:                [:home_improvement, :home_services, :home_supplies, :mortgage_and_rent],
      income:              [:bonus, :investment_income, :paycheck],
      insurance:           [:car_insurance, :health_insurance, :life_insurance, :property_insurance],
      kids:                [:allowance, :babysitter_and_daycare, :baby_supplies, :child_support, :kids_activities, :toys],
      pets:                [:pet_food_and_supplies, :pet_grooming, :veterinary],
      shopping:            [:clothing, :electronics_and_software, :sporting_goods],
      transfer:            [],
      travel:              [:hotel, :transportation, :vacation]
    }
    PERSONAL_CATEGORIES_ARRAY = PERSONAL_CATEGORIES_HASH.flatten.flatten.map(&:to_s)

    BUSINESS_CATEGORIES_HASH = {
      equipment_and_materials: [:electronics, :software, :supplies_and_furniture, :raw_materials, :consumer_goods],
      financials:              [:dividends, :donations, :interest, :fees, :fines, :loans],
      human_resources:         [:wages, :bonus, :employee_benefits, :education_and_trainings, :staff_outsourcing, :travel, :entertainment, :meals],
      income:                  [:investments, :sales, :returns, :prepayments],
      insurance:               [:business_insurance, :liability_insurance, :health_insurance, :equipment_insurance, :vehicle_insurance, :professional_insurance],
      real_estate:             [:office_rent, :mortgage, :construction_and_repair],
      services:                [:contractors, :accounting_and_auditing, :legal, :consulting, :storage, :marketing_and_media, :online_subscriptions, :it_services, :cleaning],
      taxes:                   [:vat, :federal_taxes, :property_taxes, :income_taxes, :duty_taxes, :tax_return, :payroll_taxes],
      transport:               [:shipping, :leasing, :gas_and_fuel, :taxi, :service_and_parts],
      utilities:               [:internet, :phone, :water, :gas, :electricity]
    }

    BUSINESS_CATEGORIES_ARRAY = BUSINESS_CATEGORIES_HASH.flatten.flatten.map(&:to_s)
  end
end
