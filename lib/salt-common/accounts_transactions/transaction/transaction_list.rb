module SaltCommon
  class TransactionList < Array
    attr_accessor :checksums, :short_checksums

    def initialize(checksums=[])
      @checksums       = checksums
      @short_checksums = []
    end

    def add(params, &block)
      params      = params.dup.with_indifferent_access
      return self unless valid_amount?(params)

      transaction = Transaction.new(params)
      self.unique_checksum(transaction, params["checksum"], &block)

      transaction.existing = true if checksums.include?(transaction.checksum)

      if transaction.internal_data[:short_checksums].any? { |checksum| short_checksums.include?(checksum) }
        transaction.extra[:possible_duplicate] = true
      end

      self << transaction
      transaction
    end

    def not_existing
      reject do |transaction|
        transaction.existing || transaction.status == SaltCommon::Transaction::PENDING
      end
    end

    def duplicated
      reject do |transaction|
        transaction.initial_checksum.nil? || transaction.status == SaltCommon::Transaction::PENDING
      end
    end

    def pending
      select do |transaction|
        transaction.status == SaltCommon::Transaction::PENDING
      end
    end

    def find(checksum)
      detect { |transaction| transaction.checksum == checksum }
    end

    def unique_checksum(transaction, checksum, &block)
      initial_checksum = transaction.calculate_checksum(checksum, &block)
      if find(initial_checksum).nil?
        transaction.checksum = initial_checksum
      else
        transaction.initial_checksum = initial_checksum
        unique_checksum(transaction, initial_checksum, &block)
      end
    end

    def to_hash(&block)
      if block_given?
        # NOTE: &block uses conditions to filter transactions
        # then remaining transactions are mapped into array
        reject(&block).map(&:to_hash)
      else
        map(&:to_hash)
      end
    end

    def sort_by_made_on!
      sort_by!.with_index do |transaction, index|
        "#{transaction.made_on}-#{transaction.extra[:time]}-#{index}"
      end
    end

    def valid_amount?(params)
      if params["extra"].try(:[], "convert")
        !params["extra"]["original_amount"].zero?
      else
        !params["amount"].zero?
      end
    end

    def self.copy(array)
      transaction_list = new
      array.each do |transaction_hash|
        transaction_list << Transaction.new(transaction_hash)
      end
      transaction_list
    end
  end
end
