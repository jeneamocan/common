require 'digest/sha2'
module SaltCommon
  class Transaction
    class InvalidTransaction < SaltCommon::Validator::TransactionValidationError; end

    MODES        = SaltCommon::Constants::Transaction::MODES
    STATUSES     = SaltCommon::Constants::Transaction::STATUSES
    EXTRA_FIELDS = {
      id:                     String,
      record_number:          String,
      information:            String,
      additional:             String,
      time:                   String,
      posting_date:           String,
      posting_time:           String,
      account_number:         String,
      asset_code:             String,
      asset_amount:           Float,
      original_category:      String,
      original_subcategory:   String,
      original_currency_code: String,
      type:                   String,
      check_number:           String,
      transfer_account_name:  String,
      payee:                  String,
      payer:                  String,
      payee_information:      String,
      payer_information:      String,
      mcc:                    String,
      original_amount:        Float,
      units:                  Float,
      unit_price:             Float,
      convert:                TrueClass,
      possible_duplicate:     TrueClass,
      tags:                   Array,
      variable_code:          String,
      specific_code:          String,
      constant_code:          String,
      opening_balance:        Float,
      closing_balance:        Float,
      exchange_rate:          Float,
      end_to_end_id:          String
    }.with_indifferent_access

    (MODES + STATUSES).each do |mode|
      const_set(mode.upcase, mode)
    end

    MODES.each do |name|
      define_method("#{name}?") do
        mode == name
      end
    end

    STATUSES.each do |name|
      define_method("#{name}?") do
        status == name
      end
    end

    include ActiveModel::Validations
    validates :currency_code, presence: true, inclusion: { in: Currencies::ISO4217.values }
    validates :mode,          presence: true, inclusion: { in: MODES }
    validates :status,        presence: true, inclusion: { in: STATUSES }
    validates :category_code, presence: true, inclusion: {
      in: SaltCommon::Categories::PERSONAL_CATEGORIES_ARRAY +
          SaltCommon::Categories::BUSINESS_CATEGORIES_ARRAY +
          [SaltCommon::Categories::UNCATEGORIZED]
    }
    validates :description,   presence: true
    validates :amount,        presence: true
    validates :made_on,       presence: true
    validate  :extra_class
    validate  :validate_amount
    validates :made_on, format: { with:    /\A\d{4}\-\d{2}\-\d{2}\z/,
                                  message: "should be in 'YYYY-MM-DD' format"}

    attr_accessor :made_on, :amount,   :currency_code, :description,
                  :mode,    :extra,    :internal_data, :checksum,
                  :status,  :existing, :category_code, :initial_checksum

    def self.custom_extra_fields
      @custom_extra_fields ||= {}.with_indifferent_access
    end

    def self.add_extra_field(key, type, &block)
      custom_extra_fields[key] = type

      validate do
        instance_eval(&block) if extra && extra[key]
      end
    end

    def initialize(params)
      params = params.deep_dup.with_indifferent_access

      unless params[:internal_data].try(:[], :account_name)
        raise SaltCommon::Validator::TransactionValidationError.new("Account name was not provided #{params.inspect}")
      end

      @made_on          = params.delete(:made_on)
      @amount           = params.delete(:amount)
      @currency_code    = params.delete(:currency_code)
      @description      = params.delete(:description)[0...4000] if params[:description].is_a?(String)
      @initial_checksum = params.delete(:initial_checksum)
      @checksum         = params.delete(:checksum)
      @mode             = params.delete(:mode)           || NORMAL
      @status           = params.delete(:status)         || POSTED
      @existing         = params.delete(:existing)       || false
      @category_code    = params.delete(:category_code)  || SaltCommon::Categories::UNCATEGORIZED
      @internal_data    = (params.delete(:internal_data) || {}).with_indifferent_access
      @extra            = (params.delete(:extra)         || {}).with_indifferent_access

      raise SaltCommon::Validator::TransactionValidationError.new("Redundant params #{params.inspect}") if params.any?

      calculate_short_checksums
      transform_mcc_to_valid
      sanitize_extra_values
    end

    def calculate_checksum(initial_checksum=nil, &block)
      checksum = if block_given?
        @initial_checksum = initial_checksum
        if initial_checksum.nil?
          yield self
        else
          "#{yield self}-#{initial_checksum}"
        end
      else
        "#{made_on}-#{amount.to_f}-#{currency_code}-#{description}-#{initial_checksum}"
      end
      (Digest::SHA2.new << checksum).to_s
    end

    def calculate_short_checksums
      internal_data[:short_checksums] = []

      return if made_on.blank?
      return unless made_on.match?(/\d{4}-\d{2}-\d{2}/)

      range_from = made_on.to_date
      range_to   = made_on.to_date + 2.days

      range_from.upto(range_to) do |date|
        date_text = date.strftime("%Y-%m-%d")
        transaction_id = extra[:id] || extra[:record_number]

        id_date_amount_currency = "#{transaction_id}-#{date_text}-#{amount.to_f}-#{currency_code}"
        internal_data[:short_checksums] << (Digest::SHA2.new << id_date_amount_currency).to_s

        id_date_amount_currency_description = "#{transaction_id}-#{date_text}-#{amount.to_f}-#{currency_code}-#{description}"
        internal_data[:short_checksums] << (Digest::SHA2.new << id_date_amount_currency_description).to_s
      end
    end

    def to_hash
      {
        made_on:          made_on,
        amount:           amount,
        currency_code:    currency_code,
        description:      description,
        mode:             mode,
        extra:            extra,
        category_code:    category_code,
        internal_data:    internal_data,
        existing:         existing,
        status:           status,
        initial_checksum: initial_checksum,
        checksum:         checksum
      }
    end

    def extra=(hash)
      @extra = hash.is_a?(Hash) ? hash.with_indifferent_access : hash
    end

    private

    def extra_class
      return errors.add(:extra, "should be a Hash") unless extra.is_a?(Hash)
      extra.each do |key, value|
        unless EXTRA_FIELDS.key?(key.to_s) || self.class.custom_extra_fields.key?(key.to_s)
          return errors.add(:extra, "key #{key} should not be present in extra")
        end

        if extra[key].blank?
          return errors.add(:extra, "key #{key} has empty value")
        end

        key_type = EXTRA_FIELDS[key] || self.class.custom_extra_fields[key]

        if value.is_a?(Float)
          if value.abs.round.to_s.length > SaltCommon::Constants::AMOUNT_PRECISION
            errors.add(:extra, "key #{key} can not be greater than 10**#{SaltCommon::Constants::AMOUNT_PRECISION}")
          elsif value.to_s.split(".").last.size > SaltCommon::Constants::DECIMAL_PRECISION
            errors.add(:extra, "key #{key} should not have precision greater then #{SaltCommon::Constants::DECIMAL_PRECISION}")
          end
        end

        unless value.class == key_type
          return errors.add(:extra, "key #{key} should be #{EXTRA_FIELDS[key]}")
        end
      end
      validate_time
      validate_date
      validate_currency
      validate_mcc
      validate_asset_code_and_amount
      validate_original_currency
      validate_codes
    end

    def validate_amount
      if !amount.is_a?(Numeric)
        return errors.add(:amount, "should be a number")
      elsif amount.abs.round.to_s.length > SaltCommon::Constants::AMOUNT_PRECISION
        errors.add(:amount, "can not be greater than 10**#{SaltCommon::Constants::AMOUNT_PRECISION}")
      elsif amount.to_s.split(".").last.size > SaltCommon::Constants::DECIMAL_PRECISION
        errors.add(:amount, "should not have precision greater then #{SaltCommon::Constants::DECIMAL_PRECISION}")
      end
    end

    def validate_time
      if extra[:time] && extra[:time] !~ /^\d{2}:\d{2}:\d{2}$/
        return errors.add(:extra, "extra[:time] should be in 'HH:MM:SS' format, got: '#{extra[:time]}'")
      end
      if extra[:posting_time] && extra[:posting_time] !~ /^\d{2}:\d{2}:\d{2}$/
        return errors.add(:extra, "extra[:posting_time] should be in 'HH:MM:SS' format, got: '#{extra[:posting_time]}'")
      end
    end

    def validate_currency
      return unless extra[:original_currency_code]
      if extra[:original_currency_code] !~ /^[A-Z]{3}$/
        return errors.add(:extra, "extra[:original_currency_code] should be in 'AAA' format, got: '#{extra[:original_currency_code]}'")
      end
    end

    def validate_date
      return if made_on == "nil" || !made_on

      if made_on.to_date < "01.01.1970".to_date
        return errors.add(:made_on, "made_on should be after 01.01.1970")
      end

      return unless extra[:posting_date]
      if extra[:posting_date] !~ /^\d{4}\-\d{2}\-\d{2}$/
        return errors.add(:extra, "extra[:posting_date] should be in 'YYYY-MM-DD' format, got: '#{extra[:posting_date]}'")
      end
      if extra[:posting_date].to_date < "01.01.1970".to_date
        return errors.add(:extra, "extra[:posting_date] should be after 01.01.1970")
      end
      if made_on.to_date > extra[:posting_date].to_date
        # return errors.add(:extra, "extra[:posting_date] should be > than made_on'")
      end
    end

    def validate_mcc
      return unless extra[:mcc]
      unless SaltCommon::Constants::Mcc::CODES.key?(extra[:mcc])
        return errors.add(:extra, "extra[:mcc] should be in valide range, got: '#{extra[:mcc]}'")
      end
    end

    def validate_original_currency
      if %i(original_currency_code original_amount).any? { |k| extra.key?(k) }
        missing_key = %i(original_currency_code original_amount).reject { |k| extra.key?(k) }
        return errors.add(:extra, "extra[:#{missing_key.first}] missing") unless missing_key.empty?
      end

      if extra[:original_currency_code]
        unless SaltCommon::Currencies::ISO4217.values.include?(extra[:original_currency_code])
          return errors.add(:extra, extra[:original_currency_code] + " is not included in Currencies")
        end
      end
    end

    def validate_asset_code_and_amount
      if %i(asset_code asset_amount).any? { |k| extra.key?(k) }
        missing_key = %i(asset_code asset_amount).reject { |k| extra.key?(k) }
        return errors.add(:extra, "extra[:#{missing_key.first}] missing") unless missing_key.empty?
      end
      if extra[:asset_code]
        if SaltCommon::Assets::CODES.none? { |code| code["code"] == extra[:asset_code] } &&
          !SaltCommon::Currencies::ISO4217.values.include?(extra[:asset_code]) &&
          !CryptoFetcher.assets_list.map { |asset| asset["code"] }.include?(extra[:asset_code])
          return errors.add(:extra, extra[:asset_code] + " is included neither in Assets:CODES, nor in Currencies")
        end
      end
    end

    def validate_codes
      [:variable_code, :specific_code, :constant_code].each do |key|
        next unless extra[key]
        next unless extra[key].match?(/\D/)
        errors.add(:extra, "extra[#{key}] should contain only digits")
      end

      if extra[:variable_code] && extra[:variable_code].size > 10
        errors.add(:extra, "extra[variable_code] size should be <= 10")
      end
    end

    def transform_mcc_to_valid
      return unless extra[:mcc]
      if extra[:mcc].length < 4
        extra[:mcc].prepend("0") until extra[:mcc].length == 4
      else
        extra[:mcc].slice!(0) while extra[:mcc].length > 4
      end
    end

    def sanitize_extra_values
      extra.transform_values! do |value|
        value.is_a?(String) ? value.delete("\u0000") : value
      end
    end
  end
end
