module SaltCommon
  class Account
    include SaltCommon::ParsingHelpers

    NATURES      = SaltCommon::Constants::Account::NATURES
    CARD_TYPES   = SaltCommon::Constants::Account::CARD_TYPES
    STATUSES     = %w(active inactive)
    ASSET_FIELDS = %w(code amount)
    VALID_PARAMS = %w(name currency_code balance extra internal_data nature recalculate_balance)
    EXTRA_FIELDS = {
      status:               String,
      client_name:          String,
      account_name:         String,
      iban:                 String,
      swift:                String,
      account_number:       String,
      expiry_date:          String,
      open_date:            String,
      current_time:         String,
      current_date:         String,
      card_type:            String,
      sort_code:            String,
      next_payment_date:    String,
      statement_cut_date:   String,
      balance_type:         String,
      raw_balance:          String,
      bban:                 String,
      next_payment_amount:  Float,
      total_payment_amount: Float,
      blocked_amount:       Float,
      available_amount:     Float,
      credit_limit:         Float,
      closing_balance:      Float,
      opening_balance:      Float,
      units:                Float,
      unit_price:           Float,
      interest_rate:        Float,
      assets:               Array,
      cards:                Array,
      partial:              TrueClass
    }.with_indifferent_access

    BALANCE_FIELDS = %w[
      assets available_amount balance_type blocked_amount
      cashback_amount closing_balance credit_limit installment_debt_amount
      interest_rate minimum_payment monthly_total_payment
      next_payment_amount next_payment_date opening_balance raw_balance
      total_payment_amount remaining_payments total_premium_amount

      fund_holdings indicative_unit_price interest_amount interest_type
      interest_income investment_amount floating_interest_rate profit_amount
      profit_rate total_unit_value unit_price units penalty_amount

      gross_surrender guaranteed_gross_surrender owned_policy_amount
      paid_up_conversion_reversionary_bonus policy_components
      policy_converted_to_paid_up policy_loan_limit premium_amount
      reversionary_bonus_cash_value single_premium_amount total_reversionary_bonus
    ]

    ACCOUNT_WITHOUT_BALANCE_FIELDS = %w[
      account_name account_number bban card_type cards client_name current_date
      current_time expiry_date iban open_date partial payment_type sort_code
      statement_cut_date status swift transactions_count asset_class product_type
      financial_consultant life_assured_name policy_status premium_frequency
    ]

    include ActiveModel::Validations
    validates :name,          presence: true, length: 2..255
    validates :nature,        presence: true, inclusion: { in: NATURES }
    validates :currency_code, presence: true, inclusion: { in: Currencies::ISO4217.values }
    validates :balance,       presence: true
    validate  :validate_balance
    validate  :extra_class

    attr_accessor :name,     :balance,      :internal_data,
                  :nature,   :extra,        :recalculate_balance,
                  :checksum, :currency_code
    attr_reader :card_type

    (STATUSES + NATURES + CARD_TYPES).each do |name|
      const_set(name.upcase, name)
    end

    NATURES.each do |name|
      define_method("#{name}?") do
        nature == name
      end
    end

    CARD_TYPES.each do |name|
      define_method("#{name}?") do
        extra["card_type"] == name
      end
    end

    def self.custom_extra_fields
      @@custom_extra_fields ||= {}.with_indifferent_access
    end

    def self.add_extra_field(key, type, &block)
      custom_extra_fields[key] = type

      validate do
        instance_eval(&block) if extra && extra[key]
      end

      unless [*BALANCE_FIELDS, *ACCOUNT_WITHOUT_BALANCE_FIELDS].include?(key.to_s)
        raise "Add #{key} in one of the extra fields lists BALANCE_FIELDS or ACCOUNT_WITHOUT_BALANCE_FIELDS"
      end
    end

    def initialize(params)
      params = params.deep_dup.with_indifferent_access

      @currency_code       = params.delete(:currency_code)
      @balance             = params.delete(:balance)             || 0
      @nature              = params.delete(:nature)              || ACCOUNT
      @recalculate_balance = params.delete(:recalculate_balance) || false
      @internal_data       = (params.delete(:internal_data)      || {}).with_indifferent_access

      self.extra = params.delete(:extra) || {}

      assign_name(params.delete(:name))
      assign_card_type
      assign_checksum(params.delete(:checksum))

      raise SaltCommon::Validator::AccountValidationError.new("Redundant params #{params.inspect}") if params.any?
    end

    def transactions
      @transactions ||= SaltCommon::TransactionList.new
    end

    def to_hash(include_transactions = true, &block)
      hash = {
        name:                name,
        currency_code:       currency_code,
        balance:             balance,
        extra:               extra,
        nature:              nature,
        checksum:            checksum,
        internal_data:       internal_data,
        recalculate_balance: recalculate_balance
      }
      hash[:transactions] = transactions.to_hash(&block) if include_transactions
      hash
    end

    def extra=(hash)
      @extra = hash.is_a?(Hash) ? hash.with_indifferent_access : hash
    end

    private

    def assign_name(name)
      sanitized_name = parse_description(name)
      return @name   = sanitized_name if @internal_data["skip_rename"]
      return @name   = parse_card_number(sanitized_name) if %w(card debit_card credit_card).include?(@nature)
      @name          = sanitized_name
    end

    def assign_card_type
      return if internal_data[:ignore_card_type]
      return unless extra[:card_type].blank?
      return unless card? || credit_card? || debit_card?

      [name, extra[:account_number], extra[:account_name]].any? do |string|
        @card_type = get_card_type(string)
      end

      extra[:card_type] = card_type unless card_type.blank?
    end

    def assign_checksum(checksum=nil)
      return @checksum = checksum unless checksum.blank?
      @checksum = (Digest::SHA2.new << "#{name}-#{currency_code}").to_s
    end

    def get_card_type(string)
      return if string.blank? || string.match(/^[\d]{4}\z/)
      case string
        when /^1/                                    then UATP
        when /^4|visa/i                              then VISA
        when /^62/                                   then CHINA_UNIONPAY
        when /^30[0-59]|^3[689]/                     then DINERS_CLUB
        when /^5[06-9]|^6[\d]|maestro/i              then MAESTRO
        when /^352[89]|^35[3-8][0-9]/                then JCB
        when /^34|^37|amex|american.*express/i       then AMERICAN_EXPRESS
        when /^5[1-5]|mastercard|mc\b|master.*card/i then MASTER_CARD
        when /^220[0-4]|мир/i                        then MIR
      end
    end

    def validate_balance
      if !balance.is_a?(Numeric)
        errors.add(:balance, "should be a number")
      elsif balance.abs.round.to_s.length > SaltCommon::Constants::AMOUNT_PRECISION
        errors.add(:balance, "can not be greater than 10**#{SaltCommon::Constants::AMOUNT_PRECISION}")
      elsif balance.to_s.split(".").last.size > SaltCommon::Constants::DECIMAL_PRECISION
        errors.add(:balance, "should not have precision greater then #{SaltCommon::Constants::DECIMAL_PRECISION}")
      end
    end

    def extra_class
      return errors.add(:extra, "should be a Hash") unless extra.is_a?(Hash)
      extra.each do |key, value|
        unless EXTRA_FIELDS.key?(key.to_s) || self.class.custom_extra_fields.key?(key.to_s)
          return errors.add(:extra, "key #{key} should not be present in extra")
        end

        if extra[key].blank? && key != "assets" # NOTE: second part added for crypto-currency accounts for their identifcation
          return errors.add(:extra, "key #{key} has empty value")
        end

        if value.is_a?(Float)
          if value.abs.round.to_s.length > SaltCommon::Constants::AMOUNT_PRECISION
            errors.add(:extra, "key #{key} can not be greater than 10**#{SaltCommon::Constants::AMOUNT_PRECISION}")
          elsif value.to_s.split(".").last.size > SaltCommon::Constants::DECIMAL_PRECISION
            errors.add(:extra, "key #{key} should not have precision greater then #{SaltCommon::Constants::DECIMAL_PRECISION}")
          end
        end

        key_type = EXTRA_FIELDS[key] || self.class.custom_extra_fields[key]

        unless value.class == key_type
          return errors.add(:extra, "key #{key} should be #{key_type}")
        end
      end
      if %w(assets units unit_price).any? { |key| extra.key?(key) } && nature != Account::INVESTMENT
        return errors.add(:extra, "extra contains investment account type keys, but account is not an investment")
      end
      validate_time
      validate_date
      validate_card_type
      validate_sort_code
      validate_assets
      validate_status
    end

    def validate_time
      if extra[:current_time] && extra[:current_time] !~ /\d{2}:\d{2}:\d{2}/
        errors.add(:extra, "extra[:current_time] should be in 'HH:MM:SS' format, got: '#{extra[:current_time]}'")
      end
    end

    def validate_date
      extra.each_key do |key|
        extra_key = %w(expiry_date open_date current_date next_payment_date statement_cut_date).detect { |field| field == key.to_s }
        if extra_key && extra[extra_key] !~ /^\d{4}\-\d{2}\-\d{2}$/
          return errors.add(:extra, "extra[#{extra_key}] should be in 'YYYY-MM-DD' format, got: #{extra[extra_key]}")
        end
      end
    end

    def validate_card_type
      if extra[:card_type] && !CARD_TYPES.include?(extra[:card_type])
        errors.add(:extra, "Card Type '#{extra[:card_type]}' is not included in CARD_TYPES")
      end
    end

    def validate_sort_code
      if extra[:sort_code] && !extra[:sort_code].match?(/^[a-z0-9\-]+$/i)
        errors.add(:extra, "extra[:sort_code] contains unacceptable characters, got: '#{extra[:sort_code]}'")
      end
    end

    def validate_assets
      assets = extra[:assets]
      unless assets.blank?
        unless assets.all? { |asset| asset.is_a?(Hash) }
          return errors.add(:extra, "One of elements in extra[:assets] is not a Hash class.")
        end

        assets.each do |asset|
          if asset.any? { |key, _| asset[key].blank? }
            wrong_value = asset.detect { |key, _| asset[key].blank? }
            return errors.add(:extra, "One of assets has empty value for '#{wrong_value.first}' key")
          end

          asset.each_key do |key|
            if key == "amount" && !asset[key].is_a?(Float)
              return errors.add(:extra, "'#{key}' key should be Float class")
            end
          end

          unless asset.keys.all? { |key| ASSET_FIELDS.include?(key) }
            wrong_key = assets.reject { |key| ASSET_FIELDS.include?(key) }
            return errors.add(:extra, "#{wrong_key} should not be in extra[:assets]")
          end

          if ASSET_FIELDS.any? { |key| asset.key?(key) }
            missing_key = ASSET_FIELDS.reject { |key| asset.key?(key) }
            return errors.add(:extra, "'#{missing_key.first}' key is missing in asset") unless missing_key.blank?
          end
        end
      end
    end

    def validate_status
      return unless extra.key?("status")
      errors.add(:extra, "#{extra['status']} is not a valid value for extra[:status]") unless STATUSES.include?(extra["status"])
    end
  end
end
