module SaltCommon
  class AccountList < Array
    attr_accessor :exclude_accounts, :bucket_accounts, :include_natures

    def initialize(options={exclude_accounts: [], bucket_accounts: [], include_natures: []})
      raise TypeError unless options.is_a?(Hash)
      @exclude_accounts = options[:exclude_accounts].is_a?(Array) ? options[:exclude_accounts] : []
      @bucket_accounts  = options[:bucket_accounts].is_a?(Array) ? options[:bucket_accounts] : []
      @include_natures  = options[:include_natures].is_a?(Array) ? options[:include_natures] : []
    end

    def add(params)
      account = Account.new(update_account_extra(params))
      raise SaltCommon::Validator::AccountValidationError.new("'#{account.name}' already exists in account list.") if find(account.name)
      return self if excluded?(account)
      self << account
      account
    end

    def populate(transactions)
      transactions.each do |transaction|
        account = find(transaction.internal_data[:account_name])
        account.transactions << transaction if account
      end
      self
    end

    def excluded?(account)
      excluded_account = bucket_accounts.detect do |bucket_account|
        bucket_account["name"] == account.name
      end

      return false unless excluded_account

      exclude_accounts.map(&:to_s).include?(excluded_account["id"].to_s) # NOTE: do not remove 'to_s' (it's because of Postgres serialisation)
    end

    def find(name)
      detect { |account| account.name == name }
    end

    def to_hash(&block)
      map { |account| account.to_hash(true, &block) }
    end

    def split(transactions)
      return [self] if transactions.size <= Settings.bucket.per_page
      result = []

      transactions.each_slice(Settings.bucket.per_page) do |transactions_batch|
        list_element = valid_account_list_without_transactions

        transactions_batch.each do |transaction|
          account = list_element.find(transaction.internal_data[:account_name])
          account.transactions << transaction
        end
        result << list_element
      end

      result
    end

    def clear_balances
      balance_deleted = false

      each do |account|
        balance_deleted = true if account.balance != 0.0

        account.balance = 0.0
        to_delete       = account.extra.keys - SaltCommon::Account::ACCOUNT_WITHOUT_BALANCE_FIELDS
        balance_deleted = true if to_delete.present?

        account.extra.delete_if { |key, _| to_delete.include?(key) }
      end

      balance_deleted
    end

    def update_account_extra(params)
      data = [params.dig(:extra, :iban), params[:name]].compact.map do |field|
        SaltCommon::IbanParser.parse(field.remove(/\s/))
      end.compact.first

      return params if data.blank?

      sort_code = collect_sort_code(data) unless params.dig(:extra, :sort_code)
      bban      = collect_bban(data)      unless params.dig(:extra, :bban)

      params[:extra]           ||= {}
      params[:extra][:sort_code] = sort_code if sort_code
      params[:extra][:bban]      = bban      if bban

      params
    end

    def collect_sort_code(data)
      code_keys = %w[identification_code branch_code]
      code_keys.unshift("bic_code") unless data["country_code"][/GB|IE/]

      data.slice(*code_keys).values.join
    end

    def collect_bban(data)
      components = data.slice("identification_code", "account_number").values

      return unless components.size == 2

      components.join
    end

    def excluded_by_nature?(account)
      include_natures.present? && include_natures.exclude?(account.nature)
    end

    def valid_account_list_without_transactions
      list_element = AccountList.new(
        exclude_accounts: exclude_accounts,
        bucket_accounts:  bucket_accounts,
        include_natures:  include_natures
      )

      select(&:valid?).each do |account|
        list_element.add(account.to_hash(false)) unless excluded_by_nature?(account)
      end

      list_element
    end

    def self.copy(array)
      # NOTE: not used yet
      accounts_list = new
      array.each do |account_hash|
        sanitized_hash = account_hash.with_indifferent_access.slice(*SaltCommon::Account::VALID_PARAMS)
        accounts_list << Account.new(sanitized_hash)
      end
      accounts_list
    end
  end
end
