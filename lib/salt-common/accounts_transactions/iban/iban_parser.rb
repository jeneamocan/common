module SaltCommon
  class IbanParser
    PATTERNS = {
      "AT" => { identification_code: "[0-9]{5}",    account_number: "[0-9]{11}" },
      "CZ" => { identification_code: "[0-9]{4}",    account_number: "[0-9]{16}" },
      "DE" => { identification_code: "[0-9]{8}",    account_number: "[0-9]{10}" },
      "DK" => { identification_code: "[0-9]{4}",    account_number: "[0-9]{10}" },
      "GL" => { identification_code: "[0-9]{4}",    account_number: "[0-9]{10}" },
      "HR" => { identification_code: "[0-9]{7}",    account_number: "[0-9]{10}" },
      "LB" => { identification_code: "[0-9]{4}",    account_number: "[0-9]{20}" },
      "LI" => { identification_code: "[0-9]{5}",    account_number: "[A-z0-9]{12}" },
      "LT" => { identification_code: "[0-9]{5}",    account_number: "[0-9]{11}" },
      "LU" => { identification_code: "[0-9]{3}",    account_number: "[A-z0-9]{13}" },
      "KZ" => { identification_code: "[0-9]{3}",    account_number: "[A-z0-9]{13}" },
      "SE" => { identification_code: "[0-9]{9}",    account_number: "[0-9]{11}" }, # Identification code includes Bank Code and Clearing Number
      "SK" => { identification_code: "[0-9]{4}",    account_number: "[0-9]{16}" },
      "CH" => { identification_code: "[0-9]{5}",    account_number: "[0-9]{12}" },
      "SA" => { identification_code: "[0-9]{2}",    account_number: "[0-9]{18}" },
      "VA" => { identification_code: "[0-9]{3}",    account_number: "[0-9]{15}" },
      "UA" => { identification_code: "[0-9]{6}",    account_number: "[A-z0-9]{19}" },
      "BE" => { identification_code: "[0-9]{3}",    account_number: "[0-9]{7}",  check_digit: "[0-9]{2}" },
      "ME" => { identification_code: "[0-9]{3}",    account_number: "[0-9]{13}", check_digit: "[0-9]{2}" },
      "FI" => { identification_code: "[0-9]{6}",    account_number: "[0-9]{7}",  check_digit: "[0-9]{1}" },
      "FO" => { identification_code: "[0-9]{4}",    account_number: "[0-9]{9}",  check_digit: "[0-9]{1}" },
      "NO" => { identification_code: "[0-9]{4}",    account_number: "[0-9]{6}",  check_digit: "[0-9]{1}" },
      "RS" => { identification_code: "[0-9]{3}",    account_number: "[0-9]{13}", check_digit: "[0-9]{2}" },
      "TL" => { identification_code: "[0-9]{3}",    account_number: "[0-9]{14}", check_digit: "[0-9]{2}" },
      "MK" => { identification_code: "[0-9]{3}",    account_number: "[0-9]{10}", check_digit: "[0-9]{2}" },
      "TR" => { identification_code: "[0-9]{5}",    check_digit: "[0-9]{1}", account_number: "[0-9]{16}" },
      "ES" => { identification_code: "[0-9]{4}",    branch_code: "[0-9]{4}",    check_digit: "[0-9]{2}", account_number: "[0-9]{10}" },
      "PL" => { identification_code: "[0-9]{3}",    branch_code: "[0-9]{4}",    check_digit: "[0-9]{1}", account_number: "[0-9]{16}" },
      "AD" => { identification_code: "[0-9]{4}",    branch_code: "[0-9]{4}", account_number: "[0-9]{12}" },
      "EG" => { identification_code: "[0-9]{4}",    branch_code: "[0-9]{4}", account_number: "[0-9]{17}" },
      "CY" => { identification_code: "[0-9]{3}",    branch_code: "[0-9]{5}", account_number: "[A-z0-9]{16}" },
      "GR" => { identification_code: "[0-9]{3}",    branch_code: "[0-9]{4}", account_number: "[0-9]{16}" },
      "IS" => { identification_code: "[0-9]{4}",    branch_code: "[0-9]{2}", account_number: "[0-9]{16}" },
      "IL" => { identification_code: "[0-9]{3}",    branch_code: "[0-9]{3}", account_number: "[0-9]{13}" },
      "EE" => { identification_code: "[0-9]{2}",    branch_code: "[0-9]{2}", account_number: "[0-9]{11}",    check_digit: "[0-9]{1}" },
      "MR" => { identification_code: "[0-9]{5}",    branch_code: "[0-9]{5}", account_number: "[0-9]{11}",    check_digit: "[0-9]{2}" },
      "MC" => { identification_code: "[0-9]{5}",    branch_code: "[0-9]{5}", account_number: "[0-9]{11}",    check_digit: "[0-9]{2}" },
      "BR" => { identification_code: "[0-9]{8}",    branch_code: "[0-9]{5}", account_number: "[0-9]{10}",    check_digit: "[A-z0-9]{2}" },
      "AL" => { identification_code: "[0-9]{3}",    branch_code: "[0-9]{4}", account_number: "[0-9]{16}",    check_digit: "[0-9]{1}" },
      "FR" => { identification_code: "[0-9]{5}",    branch_code: "[0-9]{5}", account_number: "[0-9A-Z]{11}", check_digit: "[0-9]{2}" },
      "HU" => { identification_code: "[0-9]{3}",    branch_code: "[0-9]{4}", account_number: "[0-9]{16}",    check_digit: "[0-9]{1}" },
      "PT" => { identification_code: "[0-9]{4}",    branch_code: "[0-9]{4}", account_number: "[0-9]{11}",    check_digit: "[0-9]{2}" },
      "SI" => { identification_code: "[0-9]{2}",    branch_code: "[0-9]{3}", account_number: "[0-9]{8}",     check_digit: "[0-9]{2}" },
      "BA" => { identification_code: "[0-9]{3}",    branch_code: "[0-9]{3}", account_number: "[0-9]{8}",     check_digit: "[0-9]{2}" },
      "XK" => { identification_code: "[0-9]{2}",    branch_code: "[0-9]{2}", account_number: "[0-9]{10}",    check_digit: "[0-9]{2}" },
      "IT" => { check_digit:         "[0-9A-Z]{1}", identification_code: "[0-9]{5}", branch_code: "[0-9]{5}", account_number: "[A-z0-9]{12}" },
      "SM" => { check_digit:         "[A-z]{1}",    identification_code: "[0-9]{5}", branch_code: "[0-9]{5}", account_number: "[0-9]{12}" },
      "CR" => { check_digit:         "[0-9]{1}",    identification_code: "[0-9]{3}", account_number: "[0-9]{14}" },
      "GB" => { bic_code:            "[A-Z]{4}",    identification_code: "[0-9]{6}", account_number: "[0-9]{8}" },
      "IE" => { bic_code:            "[A-Z]{4}",    identification_code: "[0-9]{6}", account_number: "[0-9]{8}" },
      "JO" => { bic_code:            "[A-Z]{4}",    identification_code: "[0-9]{4}", account_number: "[0-9]{18}" },
      "MT" => { bic_code:            "[A-Z]{4}",    identification_code: "[0-9]{5}", account_number: "[A-z0-9]{18}" },
      "BG" => { bic_code:            "[A-Z]{4}",    identification_code: "[0-9]{4}", account_type: "[0-9]{2}", account_number: "[A-z0-9]{8}" },
      "RO" => { identification_code: "[A-Z]{4}",    account_number: "[A-z0-9]{16}" },
      "QA" => { bic_code:            "[A-Z]{4}",    account_number: "[A-z0-9]{21}" },
      "AZ" => { bic_code:            "[A-Z]{4}",    account_number: "[0-9]{20}" },
      "NL" => { bic_code:            "[A-Z]{4}",    account_number: "[0-9]{10}" },
      "DO" => { bic_code:            "[A-Z]{4}",    account_number: "[0-9]{20}" },
      "GT" => { bic_code:            "[A-Z]{4}",    account_number: "[0-9]{20}" },
      "GI" => { bic_code:            "[A-Z]{4}",    account_number: "[0-9]{15}" },
      "LV" => { bic_code:            "[A-Z]{4}",    account_number: "[0-9]{13}" },
      "KW" => { bic_code:            "[A-Z]{4}",    account_number: "[0-9]{22}" },
      "SV" => { bic_code:            "[A-Z]{4}",    account_number: "[0-9]{20}" },
      "PS" => { bic_code:            "[A-Z]{4}",    account_number: "[0-9]{21}" },
      "GE" => { bic_code:            "[A-Z]{2}",    account_number: "[0-9]{16}" },
      "MD" => { bic_code:            "[A-Z]{2}",    account_number: "[0-9]{18}" },
      "PK" => { bic_code:            "[A-Z]{4}",    account_number: "[0-9]{16}" },
      "VG" => { bic_code:            "[A-Z]{4}",    account_number: "[0-9]{16}" },
      "BH" => { bic_code:            "[A-Z]{4}",    account_number: "[0-9]{14}" }
    }.freeze

    def self.parse(iban)
      return if iban.blank?

      iban_country_code = iban[/^[A-Z]{2}/]

      return if iban_country_code.nil? || PATTERNS.keys.exclude?(iban_country_code)

      generic_fields = "(?<country_code>#{iban_country_code})(?<checksum>[0-9]{2})"

      regex_string = PATTERNS[iban_country_code].map do |field_name, pattern|
        "(?<#{field_name}>#{pattern})"
      end.join

      iban.match(Regexp.new("^#{generic_fields}#{regex_string}$"))&.named_captures
    end
  end
end
