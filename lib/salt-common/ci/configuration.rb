module SaltCommon
  module Ci
    class Configuration
      attr_accessor :options, :environments

      def initialize
        @options      = %w(--color spec --profile)
        @environments = { COVERAGE: true }
      end
    end
  end
end
