require 'fileutils'
require 'rspec'
require 'pp'

require_relative 'configuration'

module SaltCommon
  module Ci
    module Runner
      class << self
        attr_reader :configuration, :status

        MASTER_BRANCHES = %w(origin/master origin/STAGING origin/PRODUCTION)

        def log(string)
          $stderr.puts(string)
        end

        def git_master?
          MASTER_BRANCHES.include?(ENV["GIT_BRANCH"])
        end

        def specs_failed?
          RSpec.configuration.reporter.failed_examples.size > 0
        end

        def specs_run
          log "Running specs with options: #{configuration.options.join(' ')}"
          set_environment
          RSpec.reset
          RSpec::Core::Runner.run(configuration.options)
          unset_environment
          specs_exit
        end

        def specs_exit
          if specs_failed?
            @status = 1
            exit(@status)
          end
        end

        def set_environment
          configuration.environments.each do |name, value|
            ENV[name.to_s] = value.to_s
          end
        end

        def unset_environment
          configuration.environments.each do |name, value|
            ENV.delete(name.to_s)
          end
        end

        def run(&block)
          @status = 0
          at_exit do
            exit(status)
          end
          build_config(&block)
          specs_run
        end

        def build_config(&block)
          @configuration = SaltCommon::Ci::Configuration.new
          yield configuration if block_given?
          configuration
        end

        def configuration
          @configuration ||= SaltCommon::Ci::Configuration.new
        end

        def coverage_remove
          FileUtils.rm_rf "coverage/"
        end

        def file_copy(source, destination, remove_destination = true)
          FileUtils.copy_entry(source, destination, remove_destination)
        end

        def file_truncate(filename)
          File.truncate(filename, 0) if File.exists?(filename)
        end

        def rails_migration
          system("rake db:drop")    or raise "Could not run rake db:drop"
          system("rake db:create")  or raise "Could not run rake db:create"
          system("rake db:migrate") or raise "Could not run rake db:migrate"
        end

        def teaspoon_run(options="")
          log "Testing javascript with options: #{options}"
          system("FORMATTERS=tap bundle exec teaspoon #{options}") or raise "Could not complete JS specs"
        end
      end
    end
  end
end
