# This class can be used to sanitize SQL queries in order to remove any
# personal and/or sensitive data from them
#
# Useful for logging.
#
# See specs for examples.

module SaltCommon
  class SQLSanitizer
    FILTERED            = "'[FILTERED]'".freeze
    STRING_VALUE_REGEX  = /'.*?'/

    def self.sanitize(query)
      if query.start_with?("UPDATE") || query.start_with?("INSERT")
        query.split('"')[0..1].join('"') + '"'
      elsif query.include?("WHERE")
        query.gsub(STRING_VALUE_REGEX) {|part| FILTERED }
      else
        query
      end
    end
  end
end
