module SaltCommon
  class Credentials
    class InvalidCredentialsKeys < StandardError; end

    class EmptyRequiredFields    < StandardError
      NOT_DEFINED = "Required fields names are not defined."
    end

    class InvalidCredentials < StandardError
      AUTHENTICATION_PROBLEM = "Authentication problem, please reconnect."
    end

    SENSITIVE_KEYS = %w(client_provider_token
                        credentials credentials_token
                        encrypted_rememberable_credentials
                        interactive_credentials interactive_credentials_token
                        rememberable_credentials rememberable_credentials_token)

    attr_reader :hash, :required_fields, :interactive_fields, :item

    def initialize(config, item)
      @required_fields    = config.required_fields
      @interactive_fields = config.interactive_fields
      @item               = item
      @hash               = {}
    end

    def set_required_credentials(item_credentials)
      raise EmptyRequiredFields.new(EmptyRequiredFields::NOT_DEFINED) if required_fields.empty?

      @hash.merge!(item_credentials["data"])

      required_fields.each do |field|
        field_name = field[:name]
        self.class.send(:define_method, field_name.to_sym) do
          if hash[field_name].nil? && field[:optional] == false
            error = InvalidCredentialsKeys.new("Not provided '#{field_name}' field.")
            error.update(item.secure_to_h)
            error.notify(fail_callback: false)
            raise InvalidCredentials.new(InvalidCredentials::AUTHENTICATION_PROBLEM)
          end
          hash[field_name]
        end
      end
    end

    def set_interactive_credentials(fields_names, item_interactive_credentials)
      @hash.merge!(item_interactive_credentials["data"])

      interactive_fields.each do |field|
        field_name = field[:name]
        self.class.send(:define_method, field_name.to_sym) do
          if hash[field_name].nil? && fields_names.include?(field_name)
            error = InvalidCredentialsKeys.new("Not provided '#{field_name}' field.")
            error.update(item.secure_to_h)
            error.notify(fail_callback: false)
            raise InvalidCredentials.new(InvalidCredentials::AUTHENTICATION_PROBLEM)
          end
          hash[field_name]
        end
      end
    end

    def self.filter!(params, keep_keys = [])
      return if params.nil?
      return filter_array(params, keep_keys) if params.is_a?(Array)

      filter_recursively(
        params.deep_dup,
        SENSITIVE_KEYS - keep_keys.map(&:to_s)
      )
    end

    def self.filter_array(params, keep_keys)
      params.deep_dup.each do |hash|
        filter_recursively(
          hash,
          SENSITIVE_KEYS - keep_keys.map(&:to_s)
        )
      end
    end

    def self.filter_recursively(hash, keep_keys)
      hash.each do |key, value|
        if keep_keys.include?(key.to_s)
          hash[key] = "[FILTERED]"
          next
        end
        filter_recursively(value, keep_keys) if value.is_a?(Hash)
      end
    end

    def self.decrypt(credentials, decrypt_key)
      return if credentials.nil?
      SaltCommon::Crypt.new(
        SaltCommon::Credentials.decrypt_params(credentials, decrypt_key)
      ).decrypt
    end

    def self.decrypt_params(credentials, decrypt_key)
      credentials.merge("private_key_file" => decrypt_key)
    end
  end
end
