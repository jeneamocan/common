require 'yajl'

module SaltCommon
  class JsonWrapper
    class << self
      def encode(hash, options={})
        Yajl::Encoder.encode(hash, options)
      end

      def decode(string, options={})
        Yajl::Parser.parse(string, options)
      end
    end
  end
end
