class SaltCommon::ServiceStatus
  LIVE = "live"
  FAIL = "fail"

  def status_string
    healthy? ? LIVE : FAIL
  end

  def to_hash
    @hash ||= all
  end

  alias_method :to_h, :to_hash

  # Each service should manually include all the different statuses that it
  # chooses to expose. Should return a hash, whose keys should be string and
  # values booleans
  def all
    raise NotImplementedError.new
  end

  def healthy?
    to_hash.values.all?(&:present?)
  end

  def http_ok(key, url)
    RestClient.get(url)
    [key, true]
  rescue *SaltCommon::RestApi::HTTP_ERRORS
    [key, false]
  end

  def redis_status
    config = Settings.redis
    key    = "redis_#{config[:host]}_#{config[:port]}"
    Redis.current.ping
    [key, true]
  rescue Redis::CannotConnectError
    [key, false]
  end

  def database_status
    config = ActiveRecord::Base.connection_config
    key    = "postgres_#{config[:host]}_#{config[:database]}"
    ActiveRecord::Base.connection.execute("SELECT 1;")
    [key, true]
  rescue StandardError
    [key, false]
  end
end