require 'time'

module SaltCommon
  module Logstash
    class LogMapper
      ### Mapping Rules:
      #
      # Rule hash keys help find the values that need to be converted and can be:
      # * String - a dot separated path to the key that needs to be converted;
      # * Regexp - the hash will be recursively searched for all keys matching the given pattern.
      #
      # Rule hash values define how or into what the value should be converted. Can be:
      # * Predefined constant (ELASTIC_STRING, ELASTIC_OBJECT, ELASTIC_DATE, ELASTIC_DATE_TIME);
      # * A callable (Proc, Lambda, Method etc.) - will be passed the value and must return the
      #   converted value.
      #
      # Examples:
      # rules = {
      #   /id/                         => SaltCommon::Logstash::LogMapper::ELASTIC_STRING,
      #   "from_date"                  => SaltCommon::Logstash::LogMapper::ELASTIC_DATE,
      #   "to_date"                    => SaltCommon::Logstash::LogMapper::ELASTIC_DATE,
      #   "daily"                      => SaltCommon::Logstash::LogMapper::ELASTIC_STRING,
      #   "consent.consent_expires_at" => SaltCommon::Logstash::LogMapper::ELASTIC_DATE_TIME,
      #   "consent.consent_given_at"   => SaltCommon::Logstash::LogMapper::ELASTIC_DATE_TIME,
      #   "client_provider_settings"   => SaltCommon::Logstash::LogMapper::ELASTIC_OBJECT,
      #   "custom"                     => proc { |string| string + " bar" }
      # }
      #
      ### Object initialization and usage:
      #
      # mapper = SaltCommon::Logstash::LogMapper.new(rules)
      # Note: the hash passed to #process! is mutated in place
      # mapper.process!(params)

      ELASTIC_STRING    = :elastic_string
      ELASTIC_OBJECT    = :elastic_object
      ELASTIC_DATE      = :elastic_date
      ELASTIC_DATE_TIME = :elastic_date_time

      def initialize(rules)
        @rules = rules
      end

      def process!(hash)
        @rules.each do |key, type|
          case key
          when String then convert_via_path!(hash, key, type)
          when Regexp then convert_via_regexp!(hash, key, type)
          end
        end
      end

      private

      def convert_via_path!(hash, path, type)
        path  = path.split(".")
        value = hash.dig(*path) rescue nil
        return unless value

        converted = convert_value(type, value)
        hash      = hash.dig(*path[0..-2]) if path.size > 1
        assign!(hash, path.last, converted)
      end

      def convert_via_regexp!(hash, regexp, type)
        hash.entries.each do |key, value|
          next unless key.is_a?(String)

          if value.is_a?(Hash)
            convert_via_regexp!(value, regexp, type)
          elsif value && !value.is_a?(Array) && key.match?(regexp)
            converted = convert_value(type, value)
            assign!(hash, key, converted)
          end
        end
      end

      def assign!(hash, key, value)
        return if hash[key] == value

        hash["_converted_fields"] ||= {}
        hash["_converted_fields"][key] = "original: " + hash[key].to_s

        hash.try(:[]=, key, value)
      end

      def convert_value(type, value)
        case type
        when ELASTIC_STRING    then value.to_s
        when ELASTIC_OBJECT    then value.to_h                rescue {}
        when ELASTIC_DATE      then Date.parse(value).iso8601 rescue nil
        when ELASTIC_DATE_TIME then Time.parse(value).iso8601 rescue nil
        else
          # Allow passing procs and other callables as type
          return nil unless type.respond_to?(:call)
          type.call(value) rescue nil
        end
      end
    end
  end
end
