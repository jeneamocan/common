require 'sidekiq'

FORMATTER = begin
  Sidekiq::Logger::Formatters::Pretty
  # NOTE: the logger for sidekiq version >= 6 version
rescue NameError
  Sidekiq::Logging::Pretty
  # NOTE: the logger for sidekiq < 6 version
end

module Sidekiq
  module Logging
    module Json
      class Logger < FORMATTER
        attr_reader :project, :service

        def initialize(project, service)
          super()
          @project, @service = project, service
        end

        def call(severity, time, program_name, message)
          worker = begin
            format_context.to_s.split(" ")[0]
            # NOTE: sidekiq verion >= 6 uses instead `context` == `format_context`
            # https://github.com/mperham/sidekiq/blob/main/lib/sidekiq/logger.rb#L127
          rescue NameError
            context.to_s.split(" ")[0]
          end

          SaltCommon::JsonWrapper.encode({
            "@timestamp"  => time.utc.iso8601,
            status:       nil,
            severity:     severity,
            duration:     nil,
            message:      message,
            project:      project,
            service:      service,
            process_id:   ::Process.pid,
            thread_id:    Thread.current.object_id.to_s(36),
            program_name: program_name,
            worker:       worker
          }.merge(process_message(severity, time, program_name, message))) + "\n"
        end

        private

        def process_message(severity, time, program_name, message)
          return { status: "exception" } if message.is_a? Exception
          return handle_hash(message)       if message.is_a? Hash

          result = message.split(" ")
          status = result[0].match(/^(start|done|fail):?$/) || []

          {
            status:   status[1],                               # start or done
            duration: status[1] && result[1] && result[1].to_f # run time in seconds
          }
        end

        def handle_hash(message)
          return { status: "retry", message: "#{message['class']} failed, retrying with args #{message['args']}." } if message["retry"]
          { status: "dead", message: "#{message['class']} failed with args #{message['args']}, not retrying." }
        end
      end
    end
  end
end
