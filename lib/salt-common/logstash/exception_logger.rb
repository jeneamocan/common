require "logstash-logger"

module SaltCommon
  class ExceptionLogger
    attr_reader :project, :service, :logger

    def initialize(project, service)
      @project, @service = project, service
    end

    def send_log(item_hash)
      return unless Settings.log.logstash.enabled
      logger.error SaltCommon::JsonWrapper.encode(message(item_hash))
    end

  private

    def logger
      logstash_type = Settings.log.logstash.type.to_sym

      Thread.current[:saltedge_logger] ||= if logstash_type == :unix
        LogStashLogger.new(
          type: logstash_type,
          path: Settings.log.logstash.path,
          # https://github.com/dwbutler/logstash-logger#sync-mode
          sync: true
        )
      elsif logstash_type == :udp || logstash_type == :tcp
        LogStashLogger.new(
          type: logstash_type,
          host: Settings.log.logstash.host,
          port: Settings.log.logstash.port
        )
      end
    end

    def message(item_hash)
      compact_item_hash = item_hash.except("adapter_config", "internal_data")

      {
        '@timestamp' => Time.now.utc.iso8601,
        :params      => Credentials.filter!(compact_item_hash),
        :project     => project,
        :service     => service
      }
    end
  end
end
