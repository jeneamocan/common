require "openssl"
require "base64"

module SaltCommon
  class AuditLog
    ALGORITHM = "AES-256-CBC"
    class << self
      def encrypt(log_path:, public_key_path:, alg: ALGORITHM, padding: OpenSSL::PKey::RSA::PKCS1_PADDING)
        data    = File.read(log_path)
        rsa_key = OpenSSL::PKey::RSA.new(File.read(public_key_path))
        cipher  = OpenSSL::Cipher.new(alg)

        cipher.encrypt
        cipher.key = cipher_key = cipher.random_key
        cipher.iv  = cipher_iv  = cipher.random_iv

        encrypted_data = cipher.update(data) << cipher.final
        gzipped_data   = Zlib.gzip(encrypted_data)
        meta           = {
          alg:     alg,
          key:     Base64.strict_encode64(rsa_key.public_encrypt(cipher_key, padding)),
          iv:      Base64.strict_encode64(rsa_key.public_encrypt(cipher_iv, padding)),
          padding: padding
        }

        if block_given?
          key_file_path      = File.expand_path(File.basename(log_path, ".*") + ".key.json", File.dirname(log_path))
          encrypted_log_path = File.expand_path(File.basename(log_path, ".*") + ".enc.gz", File.dirname(log_path))
          File.write(key_file_path, meta.to_json)
          File.write(encrypted_log_path, gzipped_data.force_encoding("UTF-8"))
          yield(key_file_path, encrypted_log_path)
          File.unlink(log_path, key_file_path, encrypted_log_path)
        end

        {encrypted_data: encrypted_data, gzipped_data: gzipped_data, meta: meta}
      end

      def decrypt(gzip_encrypted_data:, meta:, private_key_path:)
        encrypted_data = Zlib.gunzip(gzip_encrypted_data)
        rsa_key        = OpenSSL::PKey::RSA.new(File.read(private_key_path))
        meta           = meta.symbolize_keys
        cipher         = OpenSSL::Cipher.new(meta.fetch(:alg))

        cipher.decrypt
        cipher.key = rsa_key.private_decrypt(Base64.decode64(meta.fetch(:key)), meta.fetch(:padding))
        cipher.iv  = rsa_key.private_decrypt(Base64.decode64(meta.fetch(:iv)), meta.fetch(:padding))

        cipher.update(encrypted_data) << cipher.final
      end

      def build_logger(log_root:, hostname: nil, identifiers: [])
        hostname ||= `hostname`.strip
        logger = Logger.new(
          File.join(log_root, [hostname, *identifiers, Time.now.utc.to_date].join("-") +  ".log")
        )

        logger.formatter = Proc.new do |severity, time, progname, msg|
          meta_hash = {"logged_at_utc" => Time.now.utc}
          msg.merge!(meta_hash) if msg.is_a? Hash
          msg << meta_hash      if msg.is_a? Array
          "#{msg.to_json}\n"
        end

        logger
      end
    end
  end
end
