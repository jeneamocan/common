require 'sidekiq'

module SaltCommon
  class Worker
    include Sidekiq::Worker

    attr_reader :hash, :params

    def initialize
      @hash = { message: self.class.to_s }
    end

    def perform(params={})
      @params = params
      before
      record_duration do
        run
      end
      after
      send_mail
    end

    def before; end

    def run;    end

    def after;  end

    def send_mail
      raise NotImplementedError.new("You should define 'send_mail' method for worker.")
    end

    # NOTE: for email delivery
    def deliver?
      true
    end

    # NOTE: for log sending
    def log?
      true
    end

    def self.logger
      raise NotImplementedError.new("You should define 'self.logger' method for worker.")
    end

    private

    def record_duration(&block)
      start_time = Time.now
      yield
      hash[:duration] = (Time.now - start_time).round(1)
    end

    def logger_message(project, service)
      hash.merge(project: project, service: service, params: params)
    end

    def logger
      self.class.logger
    end

    def send_log(project, service)
      logger.info(logger_message(project, service)) if log?
    end
  end
end
