require 'aws-sdk-s3'

module SaltCommon
  class S3
    class FileNotFound < StandardError; end
    class MissingKeys  < StandardError; end

    REQUIRED_KEYS = %w(bucket region aws_access_key_id aws_secret_access_key)

    attr_reader :options, :client

    def initialize(options)
      @options = options.with_indifferent_access
      check_options!
      configure_aws
    end

    def store_file(resource, acl="private")
      file, file_name = prepare_file(resource)

      store_object(file_name, file, acl)
      public_path_for(file_name)
    end

    def get_file(file_name)
      check_exists!(file_name)
      get_object(file_name)
    end

    def remove_file(file_name)
      delete_object(file_name)
    end

    private

    def check_options!
      missing_keys = REQUIRED_KEYS - options.keys
      raise MissingKeys.new("Missing: #{missing_keys}") unless missing_keys.empty?
    end

    def configure_aws
      Aws.config.update(
        region:      options[:region],
        credentials: Aws::Credentials.new(options[:aws_access_key_id], options[:aws_secret_access_key])
      )
    end

    def prepare_file(resource)
      new_resource_name = SecureRandom.uuid

      if resource.respond_to?(:read)
        [resource, new_resource_name]
      else
        [File.open(resource, "rb"), "#{new_resource_name}#{File.extname(resource)}"]
      end

    rescue StandardError
      [StringIO.open(resource), new_resource_name]
    end

    def get_object(file_name)
      # http://docs.aws.amazon.com/sdkforruby/api/Aws/S3/Client.html#get_object-instance_method
      client.get_object(bucket: options[:bucket], key: file_name).body.read
    end

    def store_object(file_name, file, acl)
      # http://docs.aws.amazon.com/sdkforruby/api/Aws/S3/Client.html#put_object-instance_method
      client.put_object(bucket: options[:bucket], key: file_name, body: file, acl: acl)
    end

    def delete_object(file_name)
      # http://docs.aws.amazon.com/sdkforruby/api/Aws/S3/Client.html#delete_object-instance_method

      # From ducumentation, after calling a delete_object method, we need to receive a response
      # which contains delete_marker with boolean values true or false.
      # In case which object was not in a versioned bucket, we receive a response which contains
      # delete_marker with nil value, and we can't handle if the file was deleted or not deleted.
      # If the SDK does no raise an error from the response, it is successful.

      client.delete_object(bucket: options[:bucket], key: file_name)
    end

    def public_path_for(file_name)
      # http://docs.aws.amazon.com/sdkforruby/api/Aws/S3/Object.html#public_url-instance_method
      Aws::S3::Object.new(
        options[:bucket],
        file_name
      ).public_url
    end

    def check_exists!(file_name)
      unless Aws::S3::Object.new(options[:bucket], file_name).exists?
        raise FileNotFound
      end
    end

    def client
      # http://docs.aws.amazon.com/sdkforruby/api/Aws/S3/Client.html
      @client ||= Aws::S3::Client.new
    end
  end
end
