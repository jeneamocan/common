module SaltCommon
  module Storage
    class Item < SaltCommon::Storage::Base
      def self.define_attributes(attributes_hash)
        self.send(:attr_accessor, *attributes_hash.keys)
      end

      def ==(other_item)
        # Failure in == comparison should be avoided
        # https://github.com/rspec/rspec-expectations/issues/732
        return false unless other_item.respond_to?(:primary_key)
        self.primary_key == other_item.primary_key
      end

      def primary_key
        raise NotImplementedError.new("You should define a primary key for item.")
      end

      def read(string=nil)
        params = SaltCommon::JsonWrapper.decode(string || redis.hget(namespace, primary_key))
        update(params)
      rescue Yajl::ParseError
        handle_error
      end

      def update(params={})
        params.each do |name, value|
          send("#{name}=", attribute_value(name, params)) if attributes.include?(name.to_sym)
        end
        self
      end

      def save
        redis.hset(namespace, primary_key, SaltCommon::JsonWrapper.encode(to_h))
      end

      # NOTE: One should never use <insecure_to_h>, available only for debug purpose
      def insecure_to_h
        to_h
      end

      def secure_to_h
        raise "WHITELIST_ATTRIBUTES shouldn't be empty!" if whitelist_attributes.empty?
        to_h(whitelist_attributes)
      end

      def remove
        redis.hdel(namespace, primary_key)
      end

      def namespace
        @namespace ||= self.class.parent::Collection::NAMESPACE
      end

      def attributes
        self.class::ATTRIBUTES.keys
      end

      def whitelist_attributes
        self.class::ATTRIBUTES.map { |key, value| key if value }.compact
      end

      private

      def to_h(attributes_list=[])
        result_hash = attributes.inject({}) do |hash, method|
          hash[method] = send(method)
          hash.stringify_keys
        end

        return result_hash if attributes_list.empty?

        select_whitelisted_attributes(result_hash, attributes_list)
      end

      def select_whitelisted_attributes(hash, keys)
        hash.select do |key, value|
          keys.include?(key.to_sym)
        end
      end

      def handle_error
        raise NotImplementedError
      end

      def attribute_value(name, params)
        return params[name] unless params[name].nil?
        params[name.to_s]
      end
    end
  end
end
