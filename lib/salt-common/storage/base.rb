require 'redis'

module SaltCommon
  module Storage
    class Base
      attr_reader :namespace

      def redis
        Redis.current
      end
    end
  end
end

require_relative "collection"
require_relative "item"
