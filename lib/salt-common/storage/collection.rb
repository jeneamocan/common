module SaltCommon
  module Storage
    class Collection < SaltCommon::Storage::Base
      attr_reader :members

      def add(*args)
        raise NotImplementedError.new("You should define 'add' method for collection.")
      end

      def get(primary_key)
        self.class.parent::Item.new(primary_key, namespace).read
      end

      def exists?(primary_key)
        redis.hexists(namespace, primary_key)
      end

      def remove(primary_key)
        redis.hdel(namespace, primary_key)
      end

      # NOTE: One should never use <insecure_to_hash>, available only for debug purpose
      def insecure_to_hash
        members.map(&:insecure_to_h)
      end

      def secure_to_hash
        members.map(&:secure_to_h)
      end

      def count
        redis.hlen(namespace)
      end

      # NOTE: not cached collection
      def all
        redis.hgetall(namespace).map do |primary_key, string|
          self.class.parent::Item.new(primary_key, namespace).read(string)
        end
      end

      def members
        @members ||= all
      end

      def namespace
        @namespace ||= self.class.parent::Collection::NAMESPACE
      end
    end
  end
end
