require 'openssl'
require 'base64'

module SaltCommon
  class Crypt
    attr_reader :public_key_file, :private_key_file

    DEFAULT_ALGORITHM = "AES-256-CBC" unless defined? DEFAULT_ALGORITHM

    def initialize(params={})
      @key              = params["key"]
      @iv               = params["iv"]
      @data             = params["data"]
      @public_key_file  = params["public_key_file"]
      @private_key_file = params["private_key_file"]
      @algorithm        = params["algorithm"] || DEFAULT_ALGORITHM
      @padding          = params["padding"] || OpenSSL::PKey::RSA::PKCS1_PADDING
      @cipher           = OpenSSL::Cipher.new(@algorithm)
    end

    def encrypt
      public_key = OpenSSL::PKey::RSA.new(public_key_file)

      @cipher.encrypt
      @cipher.key = key = @cipher.random_key
      @cipher.iv  = iv  = @cipher.random_iv

      json           = SaltCommon::JsonWrapper.encode(@data)
      encrypted_data = @cipher.update(json) << @cipher.final

      {
        "algorithm" => @algorithm,
        "key"       => Base64.encode64(public_key.public_encrypt(key, @padding)),
        "iv"        => Base64.encode64(public_key.public_encrypt(iv, @padding)),
        "data"      => Base64.encode64(encrypted_data),
        "padding"   => @padding
      }
    end

    def decrypt
      private_key = OpenSSL::PKey::RSA.new(private_key_file)
      @cipher.decrypt
      @cipher.key = private_key.private_decrypt(Base64.decode64(@key), @padding)
      @cipher.iv  = private_key.private_decrypt(Base64.decode64(@iv), @padding)

      json = @cipher.update(Base64.decode64(@data)) << @cipher.final
      SaltCommon::JsonWrapper.decode(json)
    end
  end
end
