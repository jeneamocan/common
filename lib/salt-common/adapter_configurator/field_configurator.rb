module SaltCommon
  module AdapterConfigurator
    class FieldsConfigurator
      NATURES = %i(text number password hidden file)

      attr_reader :fields

      def initialize
        @fields = []
      end

      def select(name, attrs, &block)
        sc = SelectConfigurator.new
        yield sc

        self.fields.push({
          nature:         :select,
          name:           name,
          checksummable:  attrs.fetch(:checksummable),
          english_name:   attrs.fetch(:english_name),
          localized_name: attrs.fetch(:localized_name),
          optional:       attrs.fetch(:optional, false),
          extra:          attrs.fetch(:extra, {}),
          position:       fields.size + 1,
          field_options:  sc.options
        })
      end

      def dynamic_select(name, attrs)
        self.fields.push({
          nature:         :dynamic_select,
          name:           name,
          checksummable:  attrs.fetch(:checksummable),
          english_name:   attrs.fetch(:english_name),
          localized_name: attrs.fetch(:localized_name),
          optional:       attrs.fetch(:optional, false),
          extra:          attrs.fetch(:extra, {}),
          position:       fields.size + 1
        })
      end

      NATURES.each do |nature|
        define_method(nature) do |*attrs, &block|
          data    = attrs[1]

          options = {
            nature:         nature,
            name:           attrs.first,
            position:       fields.size + 1,
            optional:       data.fetch(:optional, false),
            checksummable:  data.fetch(:checksummable),
            english_name:   data.fetch(:english_name),
            localized_name: data.fetch(:localized_name),
            extra:          data.fetch(:extra, {})
          }

          data.each { |key, value| options.deep_merge!(key => data[key]) }

          self.fields.push(options)
        end
      end
    end
  end
end
