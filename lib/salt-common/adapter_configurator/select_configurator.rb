module SaltCommon
  module AdapterConfigurator
    class SelectConfigurator
      attr_reader :options

      def initialize
        @options = []
      end

      def option(name, attrs)
        options.push({
          name:           name,
          english_name:   attrs.fetch(:english_name),
          localized_name: attrs.fetch(:localized_name),
          option_value:   attrs.fetch(:option_value),
          selected:       attrs.fetch(:selected, false),
          extra:          attrs.fetch(:extra, {}),
          position:       options.size + 1
        })
      end
    end
  end
end
