require 'countries'

module SaltCommon
  module AdapterConfigurator
    class MissingRequiredKeyError    < StandardError; end
    class InvalidUrl                 < StandardError; end
    class InvalidTimeout             < StandardError; end
    class InvalidSupportedFetchScope < StandardError; end

    PROVIDER_MODES   = %w(file api web oauth)
    PROXY_MODES      = %w(http socks)
    STRATEGIES       = %w(all text none)
    HOLDER_INFO_KEYS = %w(names emails phone_numbers addresses extra)

    (PROVIDER_MODES + PROXY_MODES).each do |mode|
      const_set(mode.upcase, mode)
    end

    @@configuration_fields = {
      client_provider_fields:  [],
      required_fields:         [],
      interactive_fields:      [],
      holder_info:             [],
      deprecated_proxies:      [],
      supported_account_types: [],
      proxy_code:              :random,
      record_strategy:         "none",

      # NOTE: the following keys should be defined in adapter
      max_interactive_delay:  nil,
      recent_timeout:         nil,
      automatic_fetch:        nil,
      full_timeout:           nil,
      provider_mode:          nil,
      provider_code:          nil,
      provider_name:          nil,
      country_code:           nil
    }

    def self.set_configuration_fields(hash)
      @@configuration_fields = hash
    end

    def self.get_configuration_fields
      @@configuration_fields
    end

    attr_reader :configuration

    def config(&block)
      block_given? ? build_config(&block) : configuration
    end

    def build_config(&block)
      if self.ancestors[1].try(:configuration)
        @configuration = Configurator.new(self.ancestors[1].configuration.to_hash)
      else
        @configuration ||= Configurator.new(AdapterConfigurator.get_configuration_fields)
      end

      yield configuration
      validate_required_keys!(configuration)
      validate_urls!(configuration)
      validate_timeouts!(configuration)
      validate_extra_lists!(configuration)
      validate_account_types!(configuration)
      validate_interactivity!(configuration)
      validate_record_strategy!(configuration)
      validate_holder_info!(configuration)
      validate_country_code!(configuration)
      validate_provider_code!(configuration)
      validate_sandbox_config!(configuration)
      validate_credentials_fields!(configuration)
      valdiate_supported_fetch_scopes!(configuration)
      validate_max_allowed_automatic_daily_refreshes!(configuration)
      validate_custom_pending_settings!(configuration)
    end

    def generate_proxy_code
      key = configuration.proxy_code

      if key =~ /\d+$/
        group = Settings.proxies[key.to_s.gsub(/\d+/, "")]
        group = check_and_get_group(group)

        key = group.include?(key.to_s) ? key : group.keys.sample

        return check_and_get_key(group, key.to_s)
      end
      group = check_and_get_group(Settings.proxies[key.to_s])

      check_and_get_key(group, group.keys.sample)
    end

    def check_and_get_key(group, key)
      if key_not_valid?(group, key)
        if group.size > 1
          filtered_group = group.reject { |k| k == key }
          check_and_get_key(filtered_group, filtered_group.keys.sample)
        else
          Settings.proxies["random"].keys.sample
        end
      else
        key
      end
    end

    def key_not_valid?(group, key)
      group[key]["address"].nil? || group[key]["mode"].nil?
    end

    def check_and_get_group(group)
      group.blank? ? Settings.proxies["random"] : group.reject { |k, v| configuration.deprecated_proxies.include?(k) }
    end

    def validate_required_keys!(configuration)
      # NOTE: Base classes should not validate required keys
      return if self.to_s.start_with?("Base")

      missing_key = AdapterConfigurator.get_configuration_fields.keys.find { |key| configuration.send(key).nil? }
      raise MissingRequiredKeyError.new(missing_key) if missing_key
    end

    def validate_urls!(configuration)
      return unless configuration.provider_code

      blank_keys = %w[login_url home_url].select { |key| configuration.send(key).blank? }
      raise "#{configuration.provider_code} has empty #{blank_keys.join(", ")}" unless blank_keys.blank?

      { login_url: configuration.login_url, home_url: configuration.home_url }.each do |key, value|
        raise InvalidUrl.new("Invalid #{key}:'#{value}'") unless value =~ URI.regexp
        raise InvalidUrl.new("Url length is more than 4000 symbols") if value.size > 4000
      end
    end

    def validate_account_types!(configuration)
      return unless configuration.supported_account_types.present?
      return if configuration.supported_account_types.all? do |type|
        %w(personal business).include?(type)
      end
      wrong_value = configuration.supported_account_types.detect do |type|
        !%w(personal business).include?(type)
      end
      raise "#{wrong_value} is not valid value for supported_account_types"
    end

    def validate_interactivity!(configuration)
      if configuration.automatic_fetch && configuration.interactive_fields.size > 0
        raise "#{configuration.provider_code} is not automatic"
      end

      if configuration.automatic_fetch && (configuration.optional_interactivity || configuration.interactive)
        failing_condition = configuration.interactive ? "interactive" : "optional_interactivity"
        raise "#{configuration.provider_code} has #{failing_condition} set to true and automatic_fetch is true"
      end
    end

    def validate_timeouts!(configuration)
      if configuration.automatic_fetch == false && (configuration.recent_timeout < 480 || configuration.full_timeout < 480)
        raise InvalidTimeout.new("Timeouts are to small for interactive login")
      end
    end

    def validate_record_strategy!(configuration)
      unless STRATEGIES.any? { |strategy| configuration[:record_strategy] == strategy }
        raise "'#{configuration[:record_strategy]}' is an invalid :record_strategy type"
      end
    end

    def validate_provider_code!(configuration)
      return unless configuration.provider_code.present?

      if configuration.provider_code[/\s|[A-Z]/]
        raise "provider_code consists of invalid characters"
      end

      return if configuration.provider_code.match?(/fake|demobank/)

      if configuration.country_code.match?("XF")
        country_code = configuration.provider_code.split("_")[-2]
        return if country_code.size == 2 && ISO3166::Country.new(country_code)
      else
        country_code = configuration.provider_code.split("_").last
        return if country_code.size == 2 &&
          country_code.match?(configuration.country_code.downcase) &&
          (country_code.match?("xo") || ISO3166::Country.new(country_code))
      end

      raise "provider code is invalid"
    end

    def validate_country_code!(configuration)
      return unless configuration.provider_code.present?

      if configuration.country_code.blank?
        raise "#{configuration.provider_code} has empty country_code"
      end

      if configuration.country_code[/[a-z]/] || configuration.country_code.size != 2
        raise "country_code consists of invalid characters"
      end

      return if configuration.country_code.match?(/XF|XO/)
      unless ISO3166::Country.new("#{configuration.country_code}")
        raise "#{configuration.country_code} country does not exist"
      end
    end

    def validate_extra_lists!(configuration)
      accounts_natures    = configuration.supported_account_natures          || []
      accounts_extras     = configuration.supported_account_extra_fields     || []
      transactions_extras = configuration.supported_transaction_extra_fields || []

      accounts_natures.each do |nature|
        unless SaltCommon::Account::NATURES.include?(nature)
          raise "#{nature} is not a valid account nature"
        end
      end

      accounts_extras.each do |extra_field|
        unless (SaltCommon::Account::EXTRA_FIELDS.keys + SaltCommon::Account.custom_extra_fields.keys).include?(extra_field)
          raise "#{extra_field} is not a valid account extra field"
        end
      end

      transactions_extras.each do |extra_field|
        unless (SaltCommon::Transaction::EXTRA_FIELDS.keys + SaltCommon::Transaction.custom_extra_fields.keys).include?(extra_field)
          raise "#{extra_field} is not a valid transaction extra field"
        end
      end

      if accounts_extras.include?("card_type") && accounts_natures.none? { |nature| nature[/card/] }
        raise "account.extra card_type is present but supported_account_natures do not include any card natures"
      end
    end

    def validate_sandbox_config!(configuration)
      return unless configuration.provider_code && configuration.country_code == "XF"

      if configuration.identification_codes.present?
        raise "Remove c.identification_codes from #{configuration.provider_code}"
      end
      if configuration.bic_codes.present?
        raise "Remove c.bic_codes from #{configuration.provider_code}"
      end
    end

    def validate_holder_info!(configuration)
      return unless configuration.provider_code
      if configuration.supported_fetch_scopes&.include?("holder_info") && configuration.holder_info.blank?
        raise "c.holder_info is blank in #{configuration.provider_code}. Remove holder_info from c.supported_fetch_scopes"
      end

      if !configuration.supported_fetch_scopes&.include?("holder_info") && configuration.holder_info.present?
        raise "c.holder_info is not blank in #{configuration.provider_code}. Add holder_info to c.supported_fetch_scopes"
      end
    end

    def valdiate_supported_fetch_scopes!(configuration)
      return unless configuration.supported_fetch_scopes

      invalid_scopes = configuration.supported_fetch_scopes - %w[accounts transactions holder_info accounts_without_balance]
      raise InvalidSupportedFetchScope.new("Invalid supported fetch scope: #{invalid_scopes}") unless invalid_scopes.blank?
    end

    def validate_credentials_fields!(configuration)
      return unless configuration.provider_code

      %i[required_fields interactive_fields client_provider_fields].each do |field_type|
        configuration.send(field_type)&.each do |field|
          field[:field_options]&.each do |field_option|
            blank_keys = %i[english_name localized_name].select { |key| field_option[key].nil? }
            next if blank_keys.blank?

            raise "#{configuration.provider_code} has an invalid '#{field_option[:name]}' field_option in c.#{field_type} with blank #{blank_keys.join(", ")}"
          end

          blank_keys = %i[english_name localized_name checksummable].select { |key| field[key].nil? }
          if blank_keys.present?
            raise "#{configuration.provider_code} has an invalid '#{field[:name]}' field in c.#{field_type} with blank #{blank_keys.join(", ")}"
          end

          validate_field_nature(field)
        end
      end
    end

    def validate_max_allowed_automatic_daily_refreshes!(configuration)
      return unless configuration.max_allowed_automatic_daily_refreshes

      unless configuration.max_allowed_automatic_daily_refreshes.is_a?(Integer)
        raise "Max allowed automatic daily refreshes should be an integer"
      end

      unless configuration.max_allowed_automatic_daily_refreshes.between?(1,4)
        raise "max_allowed_automatic_daily_refreshes should be between 1-4"
      end
    end

    def validate_field_nature(field)
      return if %i[dynamic_select select hidden].include?(field[:nature])
      %w[access_token secret_answer secret_question].each { |f| return if field[:name].include?(f) }

      potential_numbers   = %w[phone number phone_number]
      potential_passwords = %w[password otp token secret sms code]

      if potential_numbers.any? { |target| field[:name] == target } && field[:nature] != :number
        raise "In #{configuration.provider_code} update '#{field[:name]}' field type from rf.#{field[:nature]} to rf.number"
      end

      if potential_passwords.any? { |target| field[:name] == target } && field[:nature] != :password
        raise "In #{configuration.provider_code} update '#{field[:name]}' field type from rf.#{field[:nature]} to rf.password"
      end
    end

    def validate_custom_pending_settings!(configuration)
      return if ["custom_pendings", "custom_pendings_period"].none? { |setting| configuration.send(setting.to_sym).present? }

      if configuration.custom_pendings.present? && !configuration.custom_pendings_period.present?
        raise "custom_pendings is set while period is not marked"
      end

      if configuration.custom_pendings_period.present? && configuration.custom_pendings == nil
        raise "custom_pendings_period is set to #{configuration.custom_pendings_period} while custom_pendings is missing"
      end

      unless configuration.custom_pendings_period.is_a?(Integer)
        raise "Expected custom_pendings_period to be Integer got #{configuration.custom_pendings_period.class}"
      end

      unless configuration.custom_pendings.is_a?(TrueClass) || configuration.custom_pendings.is_a?(FalseClass)
        raise "Expected custom_pendings to be TrueClass or FalseClass got #{configuration.custom_pendings.class}"
      end

      if configuration.custom_pendings.is_a?(FalseClass) && configuration.custom_pendings_period != 0
        raise "custom_pendings set to false but custom_pendings_period is #{configuration.custom_pendings_period}"
      end
    end
  end
end
