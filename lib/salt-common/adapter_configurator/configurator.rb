module SaltCommon
  module AdapterConfigurator
    class Configurator < Struct.new(*AdapterConfigurator.get_configuration_fields.keys)
      attr_reader :custom

      def initialize(options)
        @custom = {}

        options.each do |key, value|
          self.send "#{key}=", value
        end
      end

      def to_hash
        to_h.merge(custom)
      end

      def client_provider_fields(&block)
        if !block_given?
          super
        else
          fc = FieldsConfigurator.new
          yield fc
          self.client_provider_fields = fc.fields
        end
      end

      def client_provider_settings(&block)
        if !block_given?
          super
        else
          fc = FieldsConfigurator.new
          yield fc
          self.client_provider_settings = fc.fields
        end
      end

      def payment_templates(&block)
        if !block_given?
          super
        else
          fc = PaymentTemplatesConfigurator.new
          yield fc
          self.payment_templates = fc.payment_templates
        end
      end

      def required_fields(&block)
        if !block_given?
          super
        else
          fc = FieldsConfigurator.new
          yield fc
          self.required_fields = fc.fields
        end
      end

      def interactive_fields(&block)
        if !block_given?
          super
        else
          fc = FieldsConfigurator.new
          yield fc
          self.interactive_fields = fc.fields
        end
      end

      def method_missing(method, *args, &block)
        if method.to_s.end_with? "="
          @custom[method.to_s[0..-2].to_sym] = args.first
        else
          @custom[method.to_sym]
        end
      end

      def instruction
        @custom[:instruction].gsub(/^\ +/, "")
      end

      def timezone
        return unless provider_code

        Timezone.timezone_for_country_code(
          country_code == "XF" ? provider_code.split("_")[-2] : country_code
        )
      end
    end
  end
end
