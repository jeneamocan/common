module SaltCommon
  module Validator
    class HolderInfo
      attr_reader :item, :holder_info, :valid, :errors

      def initialize(item, holder_info)
        @item        = item
        @holder_info = holder_info
        @errors      = []
        @valid       = true
      end

      def validate!
        @valid = false if holder_info.values.all?(&:blank?)

        holder_info.each do |key, info|
          if info.blank?
            @errors << SaltCommon::Validator::HolderInfoValidationError.new(
              "Empty value for holder_info '#{key}' field"
            ).update
          end
          unless SaltCommon::AdapterConfigurator::HOLDER_INFO_KEYS.include?(key.to_s)
            @errors << SaltCommon::Validator::HolderInfoValidationError.new(
              "'#{key}' is not a valid holder_info field name"
            ).update
          end
        end

        send_errors unless @errors.empty?
        valid
      end

      def send_errors
        error = SaltCommon::Validator::BatchValidationError.new
        error.update_batch(@errors, item.secure_to_h)
        error.severity = 'WARNING'
        error.notify(fail_callback: false)
      end
    end
  end
end
