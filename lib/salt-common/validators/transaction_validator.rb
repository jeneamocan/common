module SaltCommon
  module Validator
    class Transaction
      attr_reader :item, :transactions, :valid, :errors

      def initialize(item, transactions)
        @item, @transactions, @errors, @valid = item, transactions, [], true
      end

      def validate!
        transactions.reject! do |transaction|
          if !transaction.valid?
            hash = {
              transaction: transaction.to_hash.slice(
                :checksum, :made_on, :amount, :currency_code
              )
            }

            @errors << SaltCommon::Validator::TransactionValidationError.new(
              transaction.errors.full_messages
            ).update(hash)

            @valid = false
            true
          else
            false
          end
        end

        send_errors unless @errors.empty?
        valid
      end

      def send_errors
        error = SaltCommon::Validator::BatchValidationError.new
        error.update_batch(@errors, item.secure_to_h)
        error.severity = "WARNING"
        error.notify(fail_callback: false)
      end
    end
  end
end
