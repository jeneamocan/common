module SaltCommon
  module Validator
    class Account
      attr_reader :item, :accounts, :valid, :errors

      def initialize(item, accounts)
        @item, @accounts, @errors, @valid = item, accounts, [], true
      end

      def validate!
        accounts.reject! do |account|
          if account.valid?
            false
          else
            hash = {
              account: account.to_hash(false).slice(:checksum, :balance, :currency_code)
            }

            @errors << SaltCommon::Validator::AccountValidationError.new(
              account.errors.full_messages
            ).update(hash)

            @valid = false
            true
          end
        end

        send_errors unless @errors.empty?
        valid
      end

      def send_errors
        error = SaltCommon::Validator::BatchValidationError.new
        error.update_batch(@errors, item.secure_to_h)
        error.severity = "WARNING"
        error.notify(fail_callback: false)
      end
    end
  end
end
