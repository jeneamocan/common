module SaltCommon
  module Validator
    class Error < StandardError
      include SaltCommon::ExceptionHelpers
    end
    class BatchValidationError       < Error; end
    class AccountValidationError     < Error; end
    class TransactionValidationError < Error; end
    class HolderInfoValidationError  < Error; end
  end
end
