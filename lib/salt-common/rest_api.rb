require 'rack'
require 'rest-client'
require 'active_support/core_ext'

module SaltCommon
  class RestApi
    USER_AGENT = "Salt Edge"
    attr_reader :url, :method, :params, :headers, :error_mode, :open_timeout,
      :retries, :execution_time, :read_timeout, :proxy

    unless defined? HTTP_ERRORS
      HTTP_ERRORS = [
        EOFError,
        Errno::ECONNRESET,
        Errno::ECONNREFUSED,
        Errno::EHOSTUNREACH,
        Errno::EINVAL,
        Net::HTTPBadResponse,
        Net::HTTPHeaderSyntaxError,
        Net::ProtocolError,
        Timeout::Error,
        RestClient::Exception,
        SocketError
      ]
    end

    REQUESTS = %w(get post patch put delete head) unless defined? REQUESTS

    REQUESTS.each do |request|
      define_method(request.to_s) do
        send_request request.to_sym
      end
    end

    def initialize(url, options={})
      options = {
        headers: {
          content_type: :json,
          accept:       :json,
          user_agent:   USER_AGENT
        },
        open_timeout: 10,
        read_timeout: 10,
        retries:      1
      }.deep_merge(options)

      @url          = url
      @params       = options[:params]
      @headers      = options[:headers]
      @error_mode   = options[:error_mode]
      @open_timeout = options[:open_timeout]
      @read_timeout = options[:read_timeout]
      @retries      = options[:retries]
      @proxy        = options[:proxy]
    end

    def execute_with_retry(method)
      send_request(method)
    rescue *HTTP_ERRORS => error
      sleep 1
      retry unless (@retries -= 1).zero?
      params_hash = params.blank? ? {} : filtered_params

      error.update(params_hash.merge(
        url:               url,
        last_request_time: execution_time,
        open_timeout:      open_timeout,
        read_timeout:      read_timeout
      ))
      error.notify(fail_callback: false)
      handle_exception(error)
    end

    def filtered_params
      # NOTE: this method should be overriden in each project for customized params filtering
      params
    end

    def to_hash(method)
      {
        method:  method,
        url:     @url,
        params:  @params,
        headers: @headers,
        retries: @retries,
        proxy:   @proxy
      }
    end

    def self.parse_url(url, params)
      uri        = URI.parse(url)
      url_params = Rack::Utils.parse_query(uri.query).merge(params)
      uri.query  = url_params.to_query
      uri.to_s
    end

    private

    def with_notify(&block)
      start_time      = Time.now
      response        = yield
      @execution_time = Time.now - start_time
      notify(execution_time)
      response
    end

    def send_request(method)
      with_notify do
        RestClient::Request.execute(request(method))
      end
    rescue *HTTP_ERRORS => error
      handle_exception(error)
    end

    def request(method)
      hash = {
        method:       method,
        url:          url,
        headers:      headers,
        open_timeout: open_timeout,
        read_timeout: read_timeout
      }
      hash[:proxy] = proxy unless proxy.blank?
      return hash if params.blank?

      hash[:url] = SaltCommon::RestApi.parse_url(url, params) if method == :get
      hash[:payload] = SaltCommon::JsonWrapper.encode(params) if method != :get
      hash
    end

    def handle_exception(error)
      # NOTE: this method should be overriden in each project for customized error handling
      raise error
    end

    def notify(execution_time)
      # NOTE: this method should be overriden in each project for customized warning handling
    end
  end
end
