# Salt Common Libs

Commonly used modules Salt Edge products.

## Installation

Add this line to your application's Gemfile:

    gem 'salt-common', git: 'git@git.saltedge.com:shared/salt-common.git'

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install specific_install
    $ gem specific_install git@git.saltedge.com:shared/salt-common.git

## Contributing

1. Fix.
2. Test.
3. Bump the version.
4. Run bundle.
5. Update changelog.
6. Commit your changes (`git commit -am 'Add some feature'`)
7. Push to the branch (`git push origin my-new-feature`)
8. Create a new Merge Request
9. Review and merge.
10. Ask someone who has the role to create a new tag.
